<?php
 $menuTitulo = array();
 $menuIcon = array();
 $menuUrl = array();


 $menuTitulo[0]= 'Dashboard';
 $menuIcon[0]= 'admin-home';
 $menuUrl[0]= 'home';

 $menuTitulo[1]= 'Construção DRE';
 $menuIcon[1]= 'admin-bar-chart';
 $menuUrl[1]= 'previsaoorcamentaria/menufilial';

 $menuTitulo[2]= 'Forecast';
 $menuIcon[2]= 'admin-pie-graph';
 $menuUrl[2]= 'forecast/menufilial';

 $menuTitulo[3]= 'Administração';
 $menuIcon[3]= 'admin-settings';
 $menuUrl[3]= '';

 $menuTitulo[4]= 'Importar';
 $menuIcon[4]= 'admin-download';
 $menuUrl[4]= 'importador';

 $menuTitulo[5]= 'Usuários';
 $menuIcon[5]= 'admin-people';
 $menuUrl[5]= 'users.index';

 $menuTitulo[6]= 'Centro de Gasto/Unidade';
 $menuIcon[6]= 'admin-list';
 $menuUrl[6]= 'centrodegastounidade.index';

 $menuTitulo[7]= 'Itens Receita';
 $menuIcon[7]= 'admin-list';
 $menuUrl[7]= 'receitarotaitens.index';

 $menuTitulo[9]= 'Itens Variaveis';
 $menuIcon[9]= 'admin-list';
 $menuUrl[9]= 'variaveis.index';

 $menuTitulo[8]= 'Exportar';
 $menuIcon[8]= 'admin-upload';
 $menuUrl[8]= 'exportador';

 $menuTitulo[10]= 'Menu Pacote';
 $menuIcon[10]= 'admin-list';
 $menuUrl[10]= 'centrodegastounidade/pacote';

 $menuTitulo[11]= 'Classificação X Custo';
 $menuIcon[11]= 'admin-list';
 $menuUrl[11]= 'custoclassificacao.index';

 $menuTitulo[12]= 'Itens EPI';
 $menuIcon[12]= 'admin-list';
 $menuUrl[12]= 'epiitens.index';

 $menuTitulo[13]= 'Impostos';
 $menuIcon[13]= 'admin-list';
 $menuUrl[13]= 'impostos.index';

 $menuTitulo[14]= 'Projeção DRE';
 $menuIcon[14]= 'admin-bar-chart';
 $menuUrl[14]= 'previsaoorcamentaria/dre';

 $menuTitulo[15]= 'Deduções';
 $menuIcon[15]= 'admin-list';
 $menuUrl[15]= 'deducoes.index';

 $menuTitulo[16]= 'Passivo op';
 $menuIcon[16]= 'admin-list';
 $menuUrl[16]= 'passivoop.index';

 $menuTitulo[17]= 'DRE';
 $menuIcon[17]= 'admin-bar-chart';
 $menuUrl[17]= 'dre';

return [
    'menuTitulo' => $menuTitulo,
    'menuIcon' => $menuIcon,
    'menuUrl' => $menuUrl,
];

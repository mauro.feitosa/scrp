<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
    //return view('welcome');

//});
//dd('SISTEMA EM MANUTENÇÃO : VOLTAMOS EM ALGUNS MINUTOS.');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'HomeController@index')->name('home');

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/acessonegado', 'AcessonegadoController@index')->name('acessonegado');
Route::resource('roles','RoleController');

Route::any('/importador','ImportadorController@index')->name('importador');
Route::any('/exportador','ExportadorController@index')->name('exportador');
Route::any('/exportador/export', 'ExportadorController@export')->name('exportador/export');
Route::any('/exportador/getprogress', 'ExportadorController@getprogress')->name('exportador/getprogress');


Route::any('/importador/importar','ImportadorController@importar')->name('importador/importar');
Route::any('/importador/getprogress','ImportadorController@getprogress')->name('importador/getprogress');


Route::any('/importador/rodcga','ImportadorController@rodcga')->name('importador/rodcga');
Route::any('/importador/rodgru','ImportadorController@rodgru')->name('importador/rodgru');
Route::any('/importador/rodunn','ImportadorController@rodunn')->name('importador/rodunn');
Route::any('/importador/rodcus','ImportadorController@rodcus')->name('importador/rodcus');
Route::any('/importador/pagcla','ImportadorController@pagcla')->name('importador/pagcla');
Route::any('/importador/cargo','ImportadorController@cargo')->name('importador/cargo');
Route::any('/importador/getprogressrodcus','ImportadorController@getprogressrodcus')->name('importador/getprogressrodcus');
Route::any('/importador/getprogressrodunn','ImportadorController@getprogressrodunn')->name('importador/getprogressrodunn');
Route::any('/importador/getprogressrodgru','ImportadorController@getprogressrodgru')->name('importador/getprogressrodgru');
Route::any('/importador/getprogressrodcga','ImportadorController@getprogressrodcga')->name('importador/getprogressrodcga');
Route::any('/importador/getprogresspagcla','ImportadorController@getprogresspagcla')->name('importador/getprogresspagcla');
Route::any('/importador/getprogresscargo','ImportadorController@getprogresscargo')->name('importador/getprogresscargo');

Route::any('/menudre','MenudreController@index')->name('menudre');
Route::any('/dre/pacote','DreController@pacote')->name('dre/pacote');
Route::any('/dre/download','DreController@download')->name('dre/download');
Route::any('/dre/detalhe','DreController@detalhe')->name('dre/detalhe');
Route::any('/dre/menu','DreController@menu')->name('dre/menu');
Route::any('/dre/processaebitda','DreController@processaebitda')->name('dre/processaebitda');
Route::any('/dre','DreController@index')->name('dre');

Route::any('/Previsaoorcamentaria/atualizadre','PrevisaoorcamentariaController@atualizadre')->name('previsaoorcamentaria/atualizadre');
Route::any('/Previsaoorcamentaria/simuladorepi','PrevisaoorcamentariaController@simuladorepi')->name('previsaoorcamentaria/simuladorepi');
Route::any('/Previsaoorcamentaria/dre','PrevisaoorcamentariaController@dre')->name('previsaoorcamentaria/dre');
Route::any('/Previsaoorcamentaria/pacoterateio','PrevisaoorcamentariaController@pacoterateio')->name('previsaoorcamentaria/pacoterateio');
Route::any('/Previsaoorcamentaria/simuladorqlp','PrevisaoorcamentariaController@simuladorqlp')->name('previsaoorcamentaria/simuladorqlp');
Route::any('/Previsaoorcamentaria/simuladorqlpadm','PrevisaoorcamentariaController@simuladorqlpadm')->name('previsaoorcamentaria/simuladorqlpadm');

Route::any('/Previsaoorcamentaria/receitarota','PrevisaoorcamentariaController@receitarota')->name('previsaoorcamentaria/receitarota');
Route::any('/Previsaoorcamentaria/variaveis','PrevisaoorcamentariaController@variaveis')->name('previsaoorcamentaria/variaveis');
Route::any('/Previsaoorcamentaria/trechos','PrevisaoorcamentariaController@trechos')->name('previsaoorcamentaria/trechos');
Route::any('/Previsaoorcamentaria/pacote','PrevisaoorcamentariaController@pacote')->name('previsaoorcamentaria/pacote');
Route::any('/Previsaoorcamentaria/operacional','PrevisaoorcamentariaController@operacional')->name('previsaoorcamentaria/operacional');
Route::any('/Previsaoorcamentaria/gente','PrevisaoorcamentariaController@gente')->name('previsaoorcamentaria/gente');
Route::any('/Previsaoorcamentaria/menu','PrevisaoorcamentariaController@menu')->name('previsaoorcamentaria/menu');
Route::any('/Previsaoorcamentaria/menupacote','PrevisaoorcamentariaController@menupacote')->name('previsaoorcamentaria/menupacote');
Route::any('/Previsaoorcamentaria/menufilial','PrevisaoorcamentariaController@menufilial')->name('previsaoorcamentaria/menufilial');
Route::any('/Previsaoorcamentaria/importar','PrevisaoorcamentariaController@importar')->name('previsaoorcamentaria/importar');
Route::any('/Previsaoorcamentaria/iniciaorclanfor','PrevisaoorcamentariaController@iniciaorclanfor')->name('previsaoorcamentaria/iniciaorclanfor');

Route::any('/forecast/receitarota','ForecastController@receitarota')->name('forecast/receitarota');
Route::any('/forecast/variaveis','ForecastController@variaveis')->name('forecast/variaveis');
Route::any('/forecast/pacote','ForecastController@pacote')->name('forecast/pacote');
Route::any('/forecast/operacional','ForecastController@operacional')->name('forecast/operacional');
Route::any('/forecast/gente','ForecastController@gente')->name('forecast/gente');
Route::any('/forecast/menu','ForecastController@menu')->name('forecast/menu');
Route::any('/forecast/menupacote','ForecastController@menupacote')->name('forecast/menupacote');
Route::any('/forecast/menufilial','ForecastController@menufilial')->name('forecast/menufilial');
Route::any('/forecast/importar','ForecastController@importar')->name('forecast/importar');
Route::any('/forecast/iniciaorclanfor','ForecastController@iniciaorclanfor')->name('forecast/iniciaorclanfor');

Route::any('/forecast','ForecastController@index')->name('forecast');

Route::any('/users-list', 'UsersController@usersList')->name('users-list');
Route::resource('users', 'UsersController');

//Route::any('/centrodegastounidade', 'CentrodegastounidadeController@index')->name('centrodegastounidade');
Route::any('/cdgu-list', 'CentrodegastounidadeController@cdguList')->name('cdgu-list');
Route::any('/centrodegastounidade/pacote', 'CentrodegastounidadeController@pacote')->name('centrodegastounidade/pacote');

Route::resource('centrodegastounidade', 'CentrodegastounidadeController');

Route::any('/receitarotaitens-list', 'ReceitarotaitensController@receitarotaitensList')->name('receitarotaitens-list');
Route::resource('receitarotaitens', 'ReceitarotaitensController');

Route::any('/passivoop-list', 'PassivoopController@passivoopList')->name('passivoop-list');
Route::resource('passivoop', 'PassivoopController');

Route::any('/deducoes-list', 'DeducoesController@deducoesList')->name('deducoes-list');
Route::resource('deducoes', 'DeducoesController');

Route::any('/impostos-list', 'ImpostosController@impostosList')->name('impostos-list');
Route::resource('impostos', 'ImpostosController');

Route::any('/variaveis-list', 'VariaveisController@variaveisList')->name('variaveis-list');
Route::resource('variaveis', 'VariaveisController');

Route::any('/epiitens-list', 'EpiitensController@epiitensList')->name('epiitens-list');
Route::resource('epiitens', 'EpiitensController');

Route::any('/custoclassificacao-list', 'CustoclassificacaoController@custoclassificacaoList')->name('custoclassificacao-list');
Route::resource('custoclassificacao', 'CustoclassificacaoController');

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rodunn extends Model
{
	protected $table = 'RODUNN';
	protected $fillable = ['CODUNN', 'CODFIL', 'DESCRI', 'CODPAD', 'SITUAC'];

}
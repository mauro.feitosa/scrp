<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;
use App\Rodcga;
use App\Rodcus;
use App\Rodgru;
use App\Rodunn;
use App\Pagcla;
use Session;
use Auth;

class AcessonegadoController extends Controller
{
    //
    public $sqlServerDb = 'sqlsrv';
    public $itemMenu = 1;

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(Request $request){
        Auth::logout();
        View::share ( 'itemMenu', $this->itemMenu );

        return view('errors.acessonegado');
    }

}

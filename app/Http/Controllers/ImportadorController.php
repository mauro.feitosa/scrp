<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;
use App\Rodcga;
use App\Rodcus;
use App\Rodgru;
use App\Rodunn;
use App\Pagcla;
use App\Cargo;
use Session;
use Carbon\Carbon;

class ImportadorController extends Controller
{
    public $itemMenu = 4;
    public $sqlServerDb = 'sqlsrv';
    public $totalRec = 0;

    public $tabelaRodopar  = 'VW_ORCAMENTO_COMPETENCIA_V6'; // RODOPAR
    public $tabelaScrp     = 'VW_ORCAMENTO_COMPETENCIA_V6'; // SCRP
    public $tabelaScrpFull = 'VW_ORCAMENTO_COMPETENCIA_V6FULL'; // SCRP
    public $vCodcga = '';

    public function __construct()
    {
        View::share ( 'itemMenu', $this->itemMenu );
        //$this->middleware('auth');
        //$this->middleware('ativo');
    }

    public function index(Request $request){

        return view('importador.index');
    }

    public function rodgru(Request $request){
        session(['rodgru'  => 0]);
        $sql = "SELECT CODIGO, CODPAD, CODFIL, DESCRI FROM RODGRU";
        $con = DB::connection('sqlsrv')->select($sql);
        $t = 0;
        $v = 0;
        $total = count($con);
        if($total > 0){
            DB::table('RODGRU')->truncate();
            foreach($con as $c){
                $t++;
                $v = ($total * 100 ) / $t;
                $array_val = (array) $c;
                $insert = Rodgru::create($array_val);
                $this->makeProgress('rodgru',round($v,0));
            }
        }
        echo "Total de registros importados: ".$t;

    }

    public function rodcus(Request $request){
        session(['rodcus'  => 0]);
        $sql = "SELECT CODCUS, CODFIL, CODPAD, CODGRU, DESCRI, SITUAC, TIPCUS, ACELAN FROM RODCUS";
        $con = DB::connection('sqlsrv')->select($sql);
        $t = 0;
        $v = 0;
        $total = count($con);
        if($total > 0){
            DB::table('RODCUS')->truncate();
            foreach($con as $c){
                $t++;
                $v = ($total * 100 ) / $t;
                $array_val = (array) $c;
                $insert = Rodcus::create($array_val);
                $this->makeProgress('rodcus',round($v,0));

            }
        }
        echo "Total de registros importados: ".$t;
    }

    public function rodunn(Request $request){
        session(['rodunn'  => 0]);
        $sql = "SELECT CODUNN, CODFIL, DESCRI, CODPAD, SITUAC FROM RODUNN";
        $con = DB::connection('sqlsrv')->select($sql);
        $t = 0;
        $v = 0;
        $total = count($con);
        if($total > 0){
            DB::table('RODUNN')->truncate();
            foreach($con as $c){
                $t++;
                $v = ($total * 100 ) / $t;
                $array_val = (array) $c;
                $insert = Rodunn::create($array_val);
                $this->makeProgress('rodunn',round($v,0));
            }
        }
        echo "Total de registros importados: ".$t;
    }

    public function rodcga(Request $request){
        session(['rodcga'  => 0]);
        $sql = "SELECT CODCGA, CODPAD, CODFIL, DESCRI, CODCON, CODGER, SITUAC FROM RODCGA";
        $con = DB::connection('sqlsrv')->select($sql);
        $t = 0;
        $v = 0;
        $total = count($con);
        if($total > 0){
            DB::table('RODCGA')->truncate();
            foreach($con as $c){
                $t++;
                $v = ($total * 100 ) / $t;
                $array_val = (array) $c;
                $insert = Rodcga::create($array_val);
                $this->makeProgress('rodcga',round($v,0));
            }
        }
        echo "Total de registros importados: ".$t;

    }



    public function pagcla(Request $request){
        session(['pagcla'  => 0]);
        $sql = "SELECT CODCLAP, CODFIL, CODCON, DESCRI, CODGER, CODGRU, TIPCLA, CODPAD, PAICLAP, SITUAC FROM PAGCLA";
        $con = DB::connection('sqlsrv')->select($sql);
        $t = 0;
        $v = 0;
        $total = count($con);
        if($total > 0){
            DB::table('PAGCLA')->truncate();
            foreach($con as $c){
                $t++;
                $v = ($total * 100 ) / $t;
                $array_val = (array) $c;
                $insert = Pagcla::create($array_val);
                $this->makeProgress('pagcla',round($v,0));
            }
        }
        echo "Total de registros importados: ".$t;

    }

    public function cargo(Request $request){
        session(['cargo'  => 0]);
        $sql = "SELECT R024CAR.codcar,R024CAR.titcar FROM R024CAR WHERE R024CAR.titcar NOT LIKE '%(Des%' AND R024CAR.titcar NOT LIKE '%RECUPERADO%' AND R024CAR.titcar NOT LIKE '%recuperado%'";
        $con = DB::connection('sqlsrv_vetor')->select($sql);
        //dd($con);
        $t = 0;
        $v = 0;
        $total = count($con);
        if($total > 0){
            DB::table('CARGO')->truncate();
            foreach($con as $c){
                $t++;
                $v = ($total * 100 ) / $t;
                $array_val = (array) $c;
                //dd($array_val);
                $insert = Cargo::create($array_val);
                //dd($insert);
                $this->makeProgress('cargo',round($v,0));
            }
        }
        echo "Total de registros importados: ".$t;

    }


    public function makeProgress($nomesessao,$valor) {

        session([$nomesessao => $valor]);

    }

    public function getprogressrodcus(Request $request) {
        echo session('rodcus');
    }
    public function getprogressrodunn(Request $request) {
        echo session('rodunn');
    }
    public function getprogressrodgru(Request $request) {
        echo session('rodgru');
    }
    public function getprogressrodcga(Request $request) {
        echo session('rodcga');
    }
    public function getprogresspagcla(Request $request) {
        echo session('pagcla');
    }
    public function getprogresscargo(Request $request) {
        echo session('cargo');
    }

    public function importar(Request $request){
        dd();

    set_time_limit(0);


    $anoImport = '2020';


    $sqlFil = 'SELECT DISTINCT CODCGA FROM MENUFILIAL';
    $rsFil = db::select($sqlFil);

    if(count($rsFil)>0){ // CRIA TABELAS POR FILIAL

        foreach($rsFil as $f){

        $sqlCreate = 'CREATE TABLE IF NOT EXISTS '.$this->tabelaScrp.'_'.$f->CODCGA.' (
            ID int(11) NOT NULL AUTO_INCREMENT,
            CODFIL int(11) DEFAULT NULL,
            CODUNN int(11) DEFAULT NULL,
            CODCGA int(11) DEFAULT NULL,
            CODCUS int(11) DEFAULT NULL,
            CODGER int(11) DEFAULT NULL,
            CODGRU int(11) DEFAULT NULL,
            SINTET int(11) DEFAULT NULL,
            SINTETICA varchar(60) DEFAULT NULL,
            ANALIT int(11) DEFAULT NULL,
            ANALITICA varchar(60) DEFAULT NULL,
            MES varchar(11) DEFAULT NULL,
            ANO int(11) DEFAULT NULL,
            VLRIMP decimal(38,11) DEFAULT NULL,
            VLRINI decimal(38,11) DEFAULT NULL,
            VLRREA decimal(38,11) DEFAULT NULL,
            VLRAJUS decimal(38,11) DEFAULT NULL,
            VLRAJUS2 decimal(38,11) DEFAULT NULL,
            VLRFOR decimal(38,11) DEFAULT NULL,
            created_at timestamp NULL DEFAULT NULL,
            updated_at timestamp NULL DEFAULT NULL,
            UNIQUE KEY ID (ID),
            KEY CODCGA (CODCGA),
            KEY CODUNN (CODUNN),
            KEY MES (MES),
            KEY ANO (ANO),
            KEY CODGER (CODGER),
            KEY CODGRU (CODGRU),
            KEY CODCUS (CODCUS),
            KEY SINTET (SINTET),
            KEY ANALIT (ANALIT)
          ) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;';

          $c = db::select($sqlCreate);

        }
    }

    if(count($rsFil)>0){ // POPULA TABELAS POR FILIAL

        foreach($rsFil as $f){

            //-->>>> INICIO
            $this->totalRec = 0;

            $INICIO     = Carbon::now()->format('Y-m-d H:i:s');

            DB::table($this->tabelaScrp.'_'.$f->CODCGA)->truncate();

            DB::connection($this->sqlServerDb)->table($this->tabelaRodopar)->select(DB::raw('CODFIL,CODCGA,MES,ANO,CODGER,CODGRU,CODCUS,CODUNN,SINTET,ANALIT,ANALITICA,SINTETICA,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRAJUS) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2, SUM(VLRREA) AS VLRREA, SUM(VLRFOR) AS VLRFOR'))
            ->where('ANO','=',$anoImport)
            ->where('CODCGA','=',$f->CODCGA)
            ->groupBy('CODFIL','CODCGA','MES','ANO','CODGER','CODGRU','CODCUS','CODUNN','SINTET','ANALIT','ANALITICA','SINTETICA')
            ->orderBy('SINTET')
            ->chunk(500, function ($dados) {
                $created_at = Carbon::now()->format('Y-m-d H:i:s');
                $updated_at = Carbon::now()->format('Y-m-d H:i:s');

                foreach ($dados as $d) {

                    $insertValues = [
                    'CODFIL'    =>$d->CODFIL,
                    'CODUNN'    =>$d->CODUNN,
                    'CODCGA'    =>$d->CODCGA,
                    'CODCUS'    =>$d->CODCUS,
                    'CODGER'    =>$d->CODGER,
                    'CODGRU'    =>$d->CODGRU,
                    'SINTET'    =>$d->SINTET,
                    'SINTETICA' =>$d->SINTETICA,
                    'ANALIT'    =>$d->ANALIT,
                    'ANALITICA' =>$d->ANALITICA,
                    'MES'       =>$d->MES,
                    'ANO'       =>$d->ANO,
                    'VLRIMP'    =>$d->VLRIMP,
                    'VLRINI'    =>$d->VLRINI,
                    'VLRREA'    =>$d->VLRREA,
                    'VLRAJUS'   =>$d->VLRAJUS,
                    'VLRAJUS2'  =>$d->VLRAJUS2,
                    'VLRFOR'    =>$d->VLRFOR,
                    'created_at'=>$created_at,
                    'updated_at'=>$updated_at];

                    DB::table($this->tabelaScrp.'_'.$d->CODCGA)->insert($insertValues);
                    $this->totalRec++;
                    $this->vCodcga = $d->CODCGA;

                }
            });

            //--->>> FIM
            $FIM = Carbon::now()->format('Y-m-d H:i:s');
            DB::table('LOGIMPORT')->insert(['STATUS' => $this->tabelaScrp.'_'.$this->vCodcga,'REGTOTAL'=>$this->totalRec, 'INICIO'=>$INICIO, 'FIM'=>$FIM]);
            echo '<b>IMPORTOU -> </b>'. $this->tabelaScrp.'_'.$this->vCodcga.' <b>INICIO -> </b>'.$INICIO.' <b>FIM -> </b>'.$FIM.'<br/>';
            echo '----------------------------------------------------------------------<br/>';

        }
    }


    $this->totalRec = 0;

    $INICIO     = Carbon::now()->format('Y-m-d H:i:s');

    DB::table($this->tabelaScrp)->truncate();

    DB::connection($this->sqlServerDb)->table($this->tabelaRodopar)->select(DB::raw('CODFIL,CODCGA,MES,ANO,CODGER,CODGRU,CODCUS,CODUNN,SINTET,ANALIT,ANALITICA,SINTETICA,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRAJUS) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2, SUM(VLRREA) AS VLRREA, SUM(VLRFOR) AS VLRFOR'))
    ->where('ANO','=',$anoImport)
    ->where('CODCGA','=',$f->CODCGA)
    ->groupBy('CODFIL','CODCGA','MES','ANO','CODGER','CODGRU','CODCUS','CODUNN','SINTET','ANALIT','ANALITICA','SINTETICA')
    ->orderBy('SINTET')
    ->chunk(500, function ($dados) {
        $created_at = Carbon::now()->format('Y-m-d H:i:s');
        $updated_at = Carbon::now()->format('Y-m-d H:i:s');

        foreach ($dados as $d) {

            $insertValues = [
            'CODFIL'    =>$d->CODFIL,
            'CODUNN'    =>$d->CODUNN,
            'CODCGA'    =>$d->CODCGA,
            'CODCUS'    =>$d->CODCUS,
            'CODGER'    =>$d->CODGER,
            'CODGRU'    =>$d->CODGRU,
            'SINTET'    =>$d->SINTET,
            'SINTETICA' =>$d->SINTETICA,
            'ANALIT'    =>$d->ANALIT,
            'ANALITICA' =>$d->ANALITICA,
            'MES'       =>$d->MES,
            'ANO'       =>$d->ANO,
            'VLRIMP'    =>$d->VLRIMP,
            'VLRINI'    =>$d->VLRINI,
            'VLRREA'    =>$d->VLRREA,
            'VLRAJUS'   =>$d->VLRAJUS,
            'VLRAJUS2'  =>$d->VLRAJUS2,
            'VLRFOR'    =>$d->VLRFOR,
            'created_at'=>$created_at,
            'updated_at'=>$updated_at];

            DB::table($this->tabelaScrp)->insert($insertValues);
            $this->totalRec++;

        }
    });

    //--->>> FIM
    $FIM = Carbon::now()->format('Y-m-d H:i:s');
    DB::table('LOGIMPORT')->insert(['STATUS' => $this->tabelaScrp,'REGTOTAL'=>$this->totalRec, 'INICIO'=>$INICIO, 'FIM'=>$FIM]);

    echo '<b>IMPORTOU -> </b>'. $this->tabelaScrp.' <b>INICIO -> </b>'.$INICIO.' <b>FIM -> </b>'.$FIM.'<br/>';
    echo '----------------------------------------------------------------------<br/><br/>';

    echo 'PROCESSO CONCLUIDO COM SUCESSO!';
    }

    public function getprogress(Request $request){

        $chave = Carbon::now()->format('Ymd');

        $p = DB::table('STATUSINPORTDRE')->select('PERCENTUAL')->where(['CHAVE' => $chave])->first();
        //dd($p);//if(){}
        if($p){
        echo $p->PERCENTUAL;
        }else{
        echo '0';
        }
    }
}

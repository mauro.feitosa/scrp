<?php
namespace App\Http\Controllers\Auth;
use App\Filial;
use App\User;
use App\Setor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Illuminate\Support\Facades\Auth;
use Adldap\Laravel\Facades\Adldap;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
    public function username()
    {
        return 'username';
    }
	
    protected function validateLogin(Request $request) {
       // dd($request);
       $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);
    }

	
    protected function attemptLogin(Request $request) {
        
        $credentials = $request->only($this->username(), 'password');

        $u = $request->username;
        $p = $request->password;
        $u2 = explode('@',$u);

        $username = $u2[0];
        $password = $p;
        
		$user_format = env('ADLDAP_USER_FORMAT', 'cn=%s,'.env('ADLDAP_BASEDN', ''));
        $userdn = sprintf($user_format, $username);
 


		// Create a configuration array.
		$config = [
		  'hosts'    => ['gh.local'],
		  'base_dn'  => 'dc=gh,dc=local',
		  'username' => $userdn,
		  'password' => $password,
		];

        try {
			$ad = new \Adldap\Adldap();
			$ad->addProvider($config);
			$provider = $ad->connect();
            
            if ($provider->auth()->attempt($userdn, $password, $bindAsUser = true)) {
				
				$v = $provider->search()->where(env('ADLDAP_USER_ATTRIBUTE'), '=', $username)->first();
				
				//dd($v["attributes"],$v["attributes"]["samaccountname"][0],$v["attributes"]["userprincipalname"][0],$v["attributes"]["displayname"][0]);

                  $user = \App\User::where('username','=',$username)->first();
				 // dd($user );
                  if (!$user) {

                    $user = new \App\User();
                    $user->username = $username;
                    $user->password = Hash::make($password);
                    $user->email = $v["attributes"]["userprincipalname"][0];
                    $user->name = $v["attributes"]["displayname"][0];
                    $user->save();
                      
                  }else{

                    $user->username = $username;
                    $user->password = Hash::make($password);
					$user->email = $v["attributes"]["userprincipalname"][0];
					$user->name = $v["attributes"]["displayname"][0];                  
                    $user->save();              

                  }
/*
                 if(Auth::attempt(['username' => $username, 'password' => $password])){
                    dd('oi');
                 }
*/
                  $this->guard()->login($user, true);
				 // dd($this->guard()->login($user, true));
				 // return redirect('/home');
                  return true;                  

            } 
            else {
                    dd('Erro No Acesso');
                  return false;
            }
      } 
      catch (\Adldap\Auth\BindException $e) {

            return false;
      }      

    }
	
    protected static function accessProtected ($obj, $prop) {
        $reflection = new \ReflectionClass($obj);
        $property = $reflection->getProperty($prop);
        $property->setAccessible(true);
        return $property->getValue($obj);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,DB,Config;
use Datatables;
use View;
use Session;

class VariaveisController extends Controller
{
    public $itemMenu = 9;
    public $arrayTipo = array('MOEDA' => 'M','NUMERO' => 'N','PERCENTUAL' => 'P','FUNÇÃO'=>'F','BOOLEANO'=>'B');

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu );
    }

    public function index()
    {
        //$pageIcon = 'admin-list';
        //$pageTitulo = 'Receita Rota Itens';
        //$itemMenu = 6;
        //View::share ( 'itemMenu', $itemMenu );

        return view('variaveis.index');
    }
    public function variaveisList()
    {
        $sql = "SELECT VARIAVEIS_ITENS.ID,VARIAVEIS_ITENS.IDPAI,VARIAVEIS_ITENS.TIPO,VARIAVEIS_ITENS.OBJTIPO,VARIAVEIS_ITENS.ORDEM,VARIAVEIS_ITENS.ORDEMFUNC,concat(UND_VARIAVEIS.CODUNN, '-' ,UND_VARIAVEIS.DESCRI) AS UNIDADE, VARIAVEIS_ITENS.DESCRI FROM VARIAVEIS_ITENS INNER JOIN UND_VARIAVEIS ON VARIAVEIS_ITENS.CODUNN = UND_VARIAVEIS.CODUNN ORDER BY VARIAVEIS_ITENS.ID,VARIAVEIS_ITENS.IDPAI";
        $lista = DB::select($sql);

       // dd($lista);
        return datatables()->of($lista)
        ->addIndexColumn()
        ->addColumn('action', function($lista){

               $btn =  '<a href="'.route('variaveis.edit',['ID'=>$lista->ID]).'" class="btn btn-sm btn-info pull-left" style="margin-right: 3px;">Editar</a>';

                return $btn;
        })
        ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        $listaUND_VARIAVEIS = DB::table('UND_VARIAVEIS')->select('CODUNN','DESCRI AS UNIDADE')->where('SITUAC','=','A')->orderBy('CODUNN','ASC')->get();
        $listaPai = DB::table('VARIAVEIS_ITENS')->select('ID','DESCRI')->where('IDPAI','=',0)->get();//->toArray();
        $ord = DB::table('VARIAVEIS_ITENS')->select('ORDEM')->orderBy('ID','DESC')->limit(1)->first();
        $ordem = $ord->ORDEM + 1;

        $v = (object) ['ID'=>0,'DESCRI'=>'Sem item pai'];
        $listaPai->push($v);
        return view('variaveis.create',['ordem'=>$ordem,'listaUND_VARIAVEIS'=>$listaUND_VARIAVEIS,'listaPai'=>$listaPai,'arrayTipo' =>$this->arrayTipo]);
    }

    public function store(Request $request)
    {

       // $val[]= array('CODUNN'=>$request->CODUNN,'DESCRI'=>$request->DESCRI,'IDPAI'=>$request->IDPAI);

        $val[]= array('CODUNN'=>$request->CODUNN,'DESCRI'=>$request->DESCRI,'IDPAI'=>$request->IDPAI,'ORDEM' => $request->ORDEM,'TIPO'=>$request->TIPO,'OBJTIPO'=>$request->OBJTIPO,'ORDEMFUNC'=>$request->ORDEMFUNC);

        $insertVal = DB::table('VARIAVEIS_ITENS')->insert($val);

        return redirect()->route('variaveis.index')
            ->with('flash_message',
             'Registro Adicionado.');
    }

    public function show($id)
    {

    }

    public function edit(Request $request,$id)
    {
        //dd($request,$id);

        //$arrayTipo = array('FUNÇÃO'=>'F','NUMERO' => 'N','MOEDA' => 'M','PERCENTUAL' => 'P');
        $listaPai = DB::table('VARIAVEIS_ITENS')->select('ID','DESCRI')->where('IDPAI','=',0)->orderBy('ID','ASC')->get();//->toArray();

        $listaUND_VARIAVEIS = DB::table('UND_VARIAVEIS')->select('CODUNN','DESCRI AS UNIDADE')->where('SITUAC','=','A')->orderBy('CODUNN','ASC')->get();

        if($id){
            $variaveis = DB::table('VARIAVEIS_ITENS')->select('*')->where('ID','=',$id)->first();
        }else{
            return redirect()->route('variaveis.index')
            ->with('flash_message',
             'Registro não encotrado.');
        }

       // dd($variaveis,$listaUND_VARIAVEIS);

        return view('variaveis.edit',['listaUND_VARIAVEIS'=>$listaUND_VARIAVEIS,'id'=>$id,'variaveis'=>$variaveis,'arrayTipo' =>$this->arrayTipo,'listaPai'=>$listaPai]);
    }

    public function update(Request $request, $id)
    {

        $val= array('CODUNN'=>$request->CODUNN,'DESCRI'=>$request->DESCRI,'IDPAI'=>$request->IDPAI,'ORDEM' => $request->ORDEM,'TIPO'=>$request->TIPO,'OBJTIPO'=>$request->OBJTIPO,'ORDEMFUNC'=>$request->ORDEMFUNC);

        $updateVal  = DB::table('VARIAVEIS_ITENS')
            ->where('ID','=', $id)
            ->update($val);

        return redirect()->route('variaveis.index')
            ->with('flash_message',
             'Registro atualizado.');

    }

    public function destroy($id)
    {

    }


}

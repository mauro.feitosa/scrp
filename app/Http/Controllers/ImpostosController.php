<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,DB,Config;
use Datatables;
use View;
use Session;

class ImpostosController extends Controller
{
    public $itemMenu = 13;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu );
    }

    public function index()
    {
        //$pageIcon = 'admin-list';
        //$pageTitulo = 'Receita Rota Itens';
        //$itemMenu = 6;
        //View::share ( 'itemMenu', $itemMenu );

        return view('impostos.index');
    }
    public function impostosList()
    {
        $sql = "SELECT IMPOSTOS.id AS ID, IMPOSTOS.ano AS ANO, IMPOSTOS.confis AS CONFIS, IMPOSTOS.filial AS FILIAL, IMPOSTOS.icms AS ICMS, IMPOSTOS.indiceiss AS INDICEISS, IMPOSTOS.pis AS PIS, IMPOSTOS.contribuicao_serv AS CONTRIBUICAO_SERV, IMPOSTOS.contribuicao_trans AS CONTRIBUICAO_TRANS, IMPOSTOS.irpj_serv AS IRPJ_SERV, IMPOSTOS.irpj_trans AS IRPJ_TRANS, IMPOSTOS.cprb AS CPRB, IMPOSTOS.despcorp AS DESPCORP,CONCAT( IMPOSTOS.filial, ' - ', RODCGA.DESCRI ) AS NOMEFILIAL FROM IMPOSTOS INNER JOIN RODCGA ON IMPOSTOS.filial = RODCGA.CODCGA WHERE RODCGA.SITUAC = 'A' ORDER BY IMPOSTOS.filial";
        $lista = DB::select($sql);

       // dd($lista);
        return datatables()->of($lista)
        ->addIndexColumn()
        ->addColumn('action', function($lista){

               $btn =  '<a href="'.route('impostos.edit',['ID'=>$lista->ID]).'" class="btn btn-sm btn-info pull-left" style="margin-right: 3px;">Editar</a>';

                return $btn;
        })
        ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        $listaRodcga = DB::table('RODCGA')->select('CODCGA','DESCRI AS NOMEFILIAL')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();
        return view('impostos.create',['listaRodcga'=>$listaRodcga]);
    }

    public function store(Request $request)
    {

       // $val[]= array('CODUNN'=>$request->CODUNN,'DESCRI'=>$request->DESCRI,'IDPAI'=>$request->idPAI);

       $val = array('ano'=>$request->ano, 'icms'=>$request->icms, 'confis'=>$request->confis, 'filial'=>$request->filial, 'indiceiss'=>$request->indiceiss, 'pis'=>$request->pis, 'contribuicao_serv'=>$request->contribuicao_serv, 'contribuicao_trans'=>$request->contribuicao_trans, 'irpj_serv'=>$request->irpj_serv, 'irpj_trans'=>$request->irpj_trans, 'cprb'=>$request->cprb, 'despcorp'=>$request->despcorp);

       // $val[]= array('CODUNN'=>$request->CODUNN,'DESCRI'=>$request->DESCRI,'IDPAI'=>$request->idPAI,'ORDEM' => $request->ORDEM,'TIPO'=>$request->TIPO,'OBJTIPO'=>$request->OBJTIPO,'ORDEMFUNC'=>$request->ORDEMFUNC);

        $insertVal = DB::table('IMPOSTOS')->insert($val);

        return redirect()->route('impostos.index')
            ->with('flash_message',
             'Registro Adicionado.');
    }

    public function show($id)
    {

    }

    public function edit(Request $request,$id)
    {


        $listaRodcga = DB::table('RODCGA')->select('CODCGA','DESCRI AS NOMEFILIAL')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();

        if($id){
            $impostos = DB::table('IMPOSTOS')->select('*')->where('id','=',$id)->first();
        }else{
            return redirect()->route('impostos.index')
            ->with('flash_message',
             'Registro não encotrado.');
        }

        return view('impostos.edit',['listaRodcga'=>$listaRodcga,'id'=>$id,'impostos'=>$impostos]);
    }

    public function update(Request $request, $id)
    {

        $val = array('ano'=>$request->ano,'icms'=>$request->icms, 'confis'=>$request->confis, 'filial'=>$request->filial, 'indiceiss'=>$request->indiceiss, 'pis'=>$request->pis, 'contribuicao_serv'=>$request->contribuicao_serv, 'contribuicao_trans'=>$request->contribuicao_trans, 'irpj_serv'=>$request->irpj_serv, 'irpj_trans'=>$request->irpj_trans, 'cprb'=>$request->cprb, 'despcorp'=>$request->despcorp);

        $updateVal  = DB::table('IMPOSTOS')
            ->where('id','=', $id)
            ->update($val);

        return redirect()->route('impostos.index')
            ->with('flash_message',
             'Registro atualizado.');

    }

    public function destroy($id)
    {

    }


}

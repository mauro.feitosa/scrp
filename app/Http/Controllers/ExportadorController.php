<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;
use App\Rodcga;
use App\Rodcus;
use App\Rodgru;
use App\Rodunn;
use App\Pagcla;
use Session;
use App\Exports\UsersExport;
use App\Exports\UsersExportview;
use App\Exports\UsersExportPacote;
use App\Exports\VariaveisExport;
use App\Exports\MultExport;
use App\Exports\MultExportvr;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;


class ExportadorController extends Controller
{
    public $itemMenu = 8;
    public $sqlServerDb = 'sqlsrv';

    public function __construct()
    {
        View::share ( 'itemMenu', $this->itemMenu );
        $this->middleware('auth');
        $this->middleware('ativo');
    }

    public function index(Request $request){

        View::share ( 'subMenu', '<li>Centro de Gasto</li>' );


        $filial = db::select('SELECT VW_ORCLAN_SP.CODCGA,COUNT(VW_ORCLAN_SP.CODCGA) AS TOTAL FROM `VW_ORCLAN_SP` GROUP BY VW_ORCLAN_SP.CODCGA');
        //$filialDesc = db::select('SELECT CODCGA,DESCRI FROM RODCGA WHERE SITUAC = "A"');
        $filialDesc = db::select('SELECT CODCGA,DESCRI FROM RODCGA');

        $arrayFilialDesc = array();

        foreach($filialDesc as $f){
            $arrayFilialDesc[$f->CODCGA] = $f->DESCRI;
        }

        return view('exportador.index',compact('filial','arrayFilialDesc'));
    }

    public function export(Request $request)
    {
        $sqlServerDb = 'sqlsrv';
        $tabelaOrclan = 'ORCLAN';
        $tabelaOrclanIni = 'HORI_ORCLAN_INI';

        $codcga = $request->codcga;
        $sqladd = '';
        /*if($codcga == 82){
            $sqladd = ' AND CODUNN = 21';
        }
        if($codcga == 9){
            $sqladd = ' AND CODUNN IN(15,21,18)';
        }*/
        //$uni
        //echo $codcga;


         $fixs = 'SELECT ANALIT,VLRREA,MES,ANO FROM `VW_ORCAMENTO_COMPETENCIA_V6_9` WHERE CODCGA = 9 AND CODUNN = 15 AND MES IN(1,2,3) AND ANO =2020' ;
         $fix = db::select($fixs);

         foreach($fix as $f){
             $mes = str_pad($f->MES , 2 , '0' , STR_PAD_LEFT);
             $ano = $f->ANO;
             $valor = abs($f->VLRREA);
             $analit = $f->ANALIT;
             $mesano = $mes.'/'.$ano;

             $itemsBusca = ['MESANO' => $mesano,'CODUNN'=>15 , 'CODCGA' => 9,'ANALIT'=>$analit];
             //dump($analit,$valor,$mesano);
             $itemstValue = ['CMVLRE' => $valor];
            // dd();
             //dump($mes,$ano,$valor,abs($valor),$mesano);
             $upIn = DB::connection($sqlServerDb)->table($tabelaOrclan)->where($itemsBusca)->update($itemstValue);
         }


        dd('FIM');

        if($codcga == 9){
            $sqladd = ' AND CODUNN IN(15)';
        }

        if($codcga == 43){
            $sqladd = ' AND CODUNN IN(21)';
        }

        if($codcga == 83){
            $sqladd = ' AND CODUNN IN(15)';
        }

        $sql = "SELECT * FROM VW_ORCLAN_SP WHERE CODCGA =  ".$codcga.$sqladd;
        //dd($sql);
        $dados = db::select($sql);
        $totalDados = 0;
        $totalDados = count($dados);
        $t = 0;
        $v = 0;
        $conta = 0;
        if($totalDados > 0){

            $INICIO = Carbon::now()->format('Y-m-d H:i:s');

            try{
                DB::connection($sqlServerDb)->beginTransaction();

                foreach($dados as $d){

                    $MESANO = $d->MESANO;
                    $CODUNN = $d->CODUNN;
                    $CODCGA = $d->CODCGA;
                    $CODCUS = $d->CODCUS;
                    $SINTET = $d->SINTET;
                    $ANALIT = $d->ANALIT;
                    $DEBCRE = 'C';
                    $CONTRO = 1;
                    $ALTERA = 0;
                    $CMVLPR = abs($d->VLRINI);
                    $CMVLRE = 0;
                    $CMPFPR = 0;
                    $CMPFRE = 0;
                    $CXVLPR = 0;
                    $CXVLRE = 0;
                    $CXPFPR = 0;
                    $CXPFRE = 0;
                    $USUINC = 'WORKFLOW';
                    $DATINC = Carbon::now()->format('Y-m-d H:i:s');
                    $USUATU = 'WORKFLOW';
                    $DATATU = Carbon::now()->format('Y-m-d H:i:s');

                    if($d->CODGER <> 0){
                        $DEBCRE = 'D';
                    }

                    if($d->SINTET <> 154){
                        $DEBCRE = 'D';
                    }
/*
                    $itemsBusca = ['MESANO' => $MESANO,'CODUNN'=>$CODUNN , 'CODCGA' => $CODCGA, 'CODCUS' => $CODCUS,'SINTET'=>$SINTET,'ANALIT' => $ANALIT,'DEBCRE' => $DEBCRE];
                    $itemstValue = [
                        'CONTRO' => $CONTRO,
                        'ALTERA' => $ALTERA,
                        'CMVLPR' => $CMVLPR,
                        'CMVLRE' => $CMVLRE,
                        'CMPFPR' => $CMPFPR,
                        'CMPFRE' => $CMPFRE,
                        'CXVLPR' => $CXVLPR,
                        'CXVLRE' => $CXVLRE,
                        'CXPFPR' => $CXPFPR,
                        'CXPFRE' => $CXPFRE,
                        'USUINC' => $USUINC,
                        'DATINC' => $DATINC,
                        'USUATU' => $USUATU,
                        'DATATU' => $DATATU];

*//*
                        $itemsBusca = ['MESANO' => $MESANO,'CODUNN'=>$CODUNN , 'CODCGA' => $CODCGA, 'CODCUS' => $CODCUS,'SINTET'=>$SINTET,'ANALIT' => $ANALIT,'DEBCRE' => $DEBCRE];
                        */
                       $itemstValue = ['CMVLPR' => $CMVLPR,'USUATU' => $USUATU,'DATATU' => $DATATU];

                       $itemInsert = [
                            'MESANO' => $MESANO,
                            'CODUNN' => $CODUNN,
                            'CODCGA' => $CODCGA,
                            'CODCUS' => $CODCUS,
                            'SINTET' => $SINTET,
                            'ANALIT' => $ANALIT,
                            'DEBCRE' => $DEBCRE,
                            'CONTRO' => $CONTRO,
                            'ALTERA' => $ALTERA,
                            'CMVLPR' => $CMVLPR,
                            'CMVLRE' => $CMVLRE,
                            'CMPFPR' => $CMPFPR,
                            'CMPFRE' => $CMPFRE,
                            'CXVLPR' => $CXVLPR,
                            'CXVLRE' => $CXVLRE,
                            'CXPFPR' => $CXPFPR,
                            'CXPFRE' => $CXPFRE,
                            'USUINC' => $USUINC,
                            'DATINC' => $DATINC,
                            'USUATU' => $USUATU,
                            'DATATU' => $DATATU];

                            //$upIn = DB::connection($sqlServerDb)->table($tabelaOrclan)->insert($itemInsert);

                            //$upIn2 = DB::connection($sqlServerDb)->table($tabelaOrclanIni)->insert($itemInsert);

                            $itemsBusca = ['MESANO' => $MESANO,'CODUNN'=>$CODUNN , 'CODCGA' => $CODCGA, 'CODCUS' => $CODCUS,'SINTET'=>$SINTET,'ANALIT' => $ANALIT,'DEBCRE' => $DEBCRE];
                            $sel =  DB::connection($sqlServerDb)->table($tabelaOrclan)->select('*')->where($itemsBusca)->get();

                            if(count($sel) == 0){
                                $upIn = DB::connection($sqlServerDb)->table($tabelaOrclan)->insert($itemInsert);
                            } else {
                                $upIn = DB::connection($sqlServerDb)->table($tabelaOrclan)->where($itemsBusca)->update($itemstValue);

                            }

                            $upIn2 = DB::connection($sqlServerDb)->table($tabelaOrclanIni)->insert($itemInsert);
                            //dump(count($sel));


                        /*
                       $upIn = DB::connection($sqlServerDb)->table($tabelaOrclan)->updateOrInsert(
                        $itemsBusca,
                        $itemstValue
                        );

                        $upIn2 = DB::connection($sqlServerDb)->table($tabelaOrclanIni)->updateOrInsert(
                            $itemsBusca,
                            $itemstValue
                        );*/

                        $t++;
                        $conta++;
                        $p = 0;
                        if($conta > 300){
                            $p = round((($totalDados * 100 ) / $conta) ,0);
                            DB::table('STATUSPROGRESSO')
                            ->updateOrInsert(
                                ['CODCGA' => $CODCGA],
                                ['TOTAL'=>$totalDados, 'ATUAL'=>$conta, 'PERCENTUAL'=>$p, 'INICIO'=>$INICIO]
                            );
                            $conta = 0;
                        }

                }
            } catch (Exception $e) {
                DB::connection($sqlServerDb)->rollBack();
            }finally{
                DB::connection($sqlServerDb)->commit();
                $FIM = Carbon::now()->format('Y-m-d H:i:s');

                DB::table('STATUSPROGRESSO')
                ->updateOrInsert(
                    ['CODCGA' => $CODCGA],
                    ['TOTAL'=>$totalDados, 'ATUAL'=>$totalDados, 'PERCENTUAL'=>100, 'FIM'=>$FIM]
                );

            }
        dd('PUBLICADO COM SUCESSO!',$t);
    }
}

    public function getprogress(Request $request){

        $codcga = $request->codcga;

        $p = DB::table('STATUSPROGRESSO')->select('PERCENTUAL')->where(['CODCGA' => $codcga])->first();
        //dd($p);//if(){}
        if($p){
        echo $p->PERCENTUAL;
        }else{
        echo '0';
        }
    }

}

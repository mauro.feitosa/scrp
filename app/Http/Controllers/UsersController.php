<?php

namespace App\Http\Controllers;

use App\Exports\UsersExport;
use App\Exports\UsersExportview;
use App\Exports\UsersExportPacote;
use App\Exports\MultExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Redirect,Response,DB,Config;
use Datatables;
use View;
use Auth;
use Session;
use GuzzleHttp\Client;

class UsersController extends Controller
{
    public $itemMenu = 5;
    public $id = 0;
    public $codcga = null;
    public $rodunn = null;
    public $codigo = null;

    public $itensCodcga  = array();
    public $itensRodunn  = array();
    public $itensCodigo  = array();

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu);
    }

    public function index()
    {

           // dd(Auth::user());



        $pageIcon = 'admin-people';
        $pageTitulo = 'Usuários';
        $itemMenu = 2;

        View::share ( 'itemMenu', $itemMenu );
        View::share ( 'pageIcon', $pageIcon );
        View::share ( 'pageTitulo', $pageTitulo );

        return view('users.index');
    }

    public function usersList()
    {
        $users = DB::table('users')->select('*');
       // dd($users);
        return datatables()->of($users)
        ->addIndexColumn()
        ->addColumn('action', function($users){

                // $btn = '<a href="javascript:alert('.$users->id.')" class="edit btn btn-primary btn-sm">Editar</a>';
               // return $btn;
                $btn =  '<a href="'.route('users.edit',['ID'=>$users->id]).'" class="btn btn-sm btn-info pull-left" style="margin-right: 3px;">Editar</a>';

                return $btn;
        })
        ->rawColumns(['action'])
            ->make(true);
    }

    public function export()
    {
        $ano = 2019;
        $codunn = 15;
        $codcga = 9;
       // return Excel::download(new UsersExportview, 'users.xlsx');
        //return Excel::download(new UsersExportPacote($ano,$codgru,$codunn,$codcga), 'unidade.xlsx');
        //return (new UsersExportPacote)->ano(2019)->codgru(5)->codcga(9)->codunn(15)->download('unidade.xlsx');
    return (new MultExport($ano,$codunn,$codcga))->download('unidade.xlsx');


    }

    public function edit(Request $request,$id)
    {


        $usuario = DB::table('users')->select('*')->where('users.id','=',$id)->first();
        $usuario_codcga  = DB::table('USU_CGA')->select('CODCGA')->where('ID_USUARIO','=',$id)->pluck('CODCGA')->toArray();
        $usuario_unidade = DB::table('USU_UNIDADE')->select('CODUNN')->where('ID_USUARIO','=',$id)->pluck('CODUNN')->toArray();
        $usuario_pacote  = DB::table('USU_PACOTE')->select('CODIGO')->where('ID_USUARIO','=',$id)->pluck('CODIGO')->toArray();
        $permissaoeditarR = DB::table('USU_ACAO')->select('EDITAR')->where('ID_USUARIO','=',$id)->first();

        if($permissaoeditarR == null){
            $permissaoeditar = 'N';
        }else{
            $permissaoeditar = $permissaoeditarR->EDITAR;
        }
        //dd($permissaoeditar,$id);

        $RODCGA = DB::table('RODCGA')->select('CODCGA','DESCRI')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();
        $RODUNN = DB::table('RODUNN')->select('CODUNN','DESCRI')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();
        $RODGRU = DB::table('RODGRU')->select('CODIGO','DESCRI')->orderBy('DESCRI','ASC')->get();

//dd($usuario_codcga,$RODCGA);

        return view('users.edit',['permissaoeditar'=>$permissaoeditar,'RODCGA'=>$RODCGA,'RODUNN'=>$RODUNN,'RODGRU'=>$RODGRU,'usuario'=>$usuario,'usuario_codcga'=>$usuario_codcga,'usuario_unidade'=>$usuario_unidade,'usuario_pacote'=>$usuario_pacote]);
    }

    public function update(Request $request, $id)
    {
       // dd($id);
        $this->id = $id;
        $this->codcga = $request->codcga;
        $this->rodunn = $request->rodunn;
        $this->codigo = $request->codigo;

        $ativo = $request->ativo;
        $perfil = $request->perfil;
        $permissaoeditar = $request->permissaoeditar;

        DB::table('users')->where('id','=',$id)->update(['ativo'=>$ativo,'perfil'=>$perfil]);

        if($this->codigo){

            $tranUSU_PACOTE = DB::transaction(function () {

                $deleteItemUSU_PACOTE  = DB::table('USU_PACOTE')->where('ID_USUARIO','=',$this->id)->delete();

                foreach($this->codigo as $cod){

                    $this->itensCodigo[]  = ['CODIGO' => $cod,'ID_USUARIO'=>$this->id];

                }

                $insertItemUSU_PACOTE     = DB::table('USU_PACOTE')->insert($this->itensCodigo);

            });
        }else{
            DB::transaction(function () {
                DB::table('USU_PACOTE')->where('ID_USUARIO','=',$this->id)->delete();
            });
        }

        if($this->rodunn){

            $tranUSU_UNIDADE = DB::transaction(function () {

                $deleteItemUSU_UNIDADE = DB::table('USU_UNIDADE')->where('ID_USUARIO','=',$this->id)->delete();

                foreach($this->rodunn as $unn){

                    $this->itensRodunn[]  = ['CODUNN' => $unn,'ID_USUARIO'=>$this->id];

                }

                $insertItemUSU_UNIDADE = DB::table('USU_UNIDADE')->insert($this->itensRodunn);
            });
        }else{
            DB::transaction(function () {
                DB::table('USU_UNIDADE')->where('ID_USUARIO','=',$this->id)->delete();
            });
        }

        if($this->codcga){

           $tranUSU_CGA = DB::transaction(function () {

                $deleteItemUSU_CGA = DB::table('USU_CGA')->where('ID_USUARIO','=',$this->id)->delete();

                foreach($this->codcga as $cga){

                    $this->itensCodcga[]  = ['CODCGA' => $cga,'ID_USUARIO'=>$this->id];

                }

                $insertItemUSU_CGA = DB::table('USU_CGA')->insert($this->itensCodcga);
            });

        }else{
           DB::transaction(function () {
                DB::table('USU_CGA')->where('ID_USUARIO','=',$this->id)->delete();
            });
        }


        $itemBusca = ['ID_USUARIO' => $id];
        $itemValue = ['EDITAR' =>  $permissaoeditar];

        DB::table('USU_ACAO')->updateOrInsert(
            $itemBusca,
            $itemValue
            );

        return redirect()->route('users.edit', ['id' => $this->id]);

      //  dd($tranUSU_PACOTE,$tranUSU_UNIDADE,$tranUSU_CGA,$itensCodcga,$itensRodunn,$itensCodigo);
      // dd($request, $id);
    }

    public function create()
    {

    }

    public function destroy($id)
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }
}

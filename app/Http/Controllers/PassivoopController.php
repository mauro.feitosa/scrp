<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,DB,Config;
use Datatables;
use View;
use Session;

class PassivoopController extends Controller
{
    public $itemMenu = 16;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu );
    }

    public function index()
    {
        //$pageIcon = 'admin-list';
        //$pageTitulo = 'Receita Rota Itens';
        //$itemMenu = 6;
        //View::share ( 'itemMenu', $itemMenu );

        return view('passivoop.index');
    }
    public function passivoopList()
    {
        $sql = "SELECT PASSIVOOP.ID AS ID,
        PASSIVOOP.ANO AS ANO,
        PASSIVOOP.MES AS MES,
        ROUND(PASSIVOOP.VALOR , 2) AS VALOR,
        PASSIVOOP.CODCGA AS CODCGA,
        PASSIVOOP.CODUNN AS CODUNN,
        RODCGA.DESCRI AS FILIAL,
        RODUNN.DESCRI AS UNIDADE
        FROM PASSIVOOP
        INNER JOIN RODCGA ON PASSIVOOP.CODCGA = RODCGA.CODCGA
        INNER JOIN RODUNN ON PASSIVOOP.CODUNN = RODUNN.CODUNN
        WHERE RODCGA.SITUAC = 'A' ORDER BY PASSIVOOP.ANO DESC";
        $lista = DB::select($sql);

       // dd($lista);
        return datatables()->of($lista)
        ->addIndexColumn()
        ->addColumn('action', function($lista){

               $btn =  '<a href="'.route('passivoop.edit',['ID'=>$lista->ID]).'" class="btn btn-sm btn-info pull-left" style="margin-right: 3px;">Editar</a>';

                return $btn;
        })
        ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {

        $listaRodcga = DB::table('RODCGA')->select('CODCGA','DESCRI AS FILIAL')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();
        $listaRodunn = DB::table('RODUNN')->select('CODUNN','DESCRI AS UNIDADE')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();
        return view('passivoop.create',['listaRodcga'=>$listaRodcga,'listaRodunn'=>$listaRodunn]);
    }

    public function store(Request $request)
    {
        //dd($request);
        $VALOR = str_replace(',','.', str_replace('.','', $request->VALOR));
        $VALOR = str_replace('R$','',$VALOR);

       $val = ['CODCGA'=>$request->CODCGA, 'CODUNN'=>$request->CODUNN, 'ANO'=>$request->ANO, 'MES'=>$request->MES, 'VALOR'=>$VALOR];
       $insertVal = DB::table('PASSIVOOP')->insert($val);
       //dd($insertVal);

        return redirect()->route('passivoop.index')
            ->with('flash_message',
             'Registro Adicionado.');
    }

    public function show($id)
    {

    }

    public function edit(Request $request,$id)
    {


        $listaRodcga = DB::table('RODCGA')->select('CODCGA','DESCRI AS FILIAL')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();
        $listaRodunn = DB::table('RODUNN')->select('CODUNN','DESCRI AS UNIDADE')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();

        if($id){
            $passivoop = DB::table('PASSIVOOP')->select('*')->where('id','=',$id)->first();
        }else{
            return redirect()->route('passivoop.index')
            ->with('flash_message',
             'Registro não encotrado.');
        }

        return view('passivoop.edit',['listaRodcga'=>$listaRodcga,'listaRodunn'=>$listaRodunn,'id'=>$id,'passivoop'=>$passivoop]);
    }

    public function update(Request $request, $id)
    {
        $VALOR = str_replace(',','.', str_replace('.','', $request->VALOR));
        $VALOR = str_replace('R$','',$VALOR);
        $val = array('CODCGA'=>$request->CODCGA, 'CODUNN'=>$request->CODUNN, 'ANO'=>$request->ANO, 'MES'=>$request->MES, 'VALOR'=>$VALOR);
        $updateVal  = DB::table('PASSIVOOP')
            ->where('ID','=', $id)
            ->update($val);

        return redirect()->route('passivoop.index')
            ->with('flash_message',
             'Registro atualizado.');

    }

    public function destroy($id)
    {
        DB::table('PASSIVOOP')->where('ID', '=', $id)->delete();

        return redirect()->route('passivoop.index')
            ->with('flash_message',
             'Registro excluido.');
    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;
use App\Rodcga;
use App\Rodcus;
use App\Rodgru;
use App\Rodunn;
use App\Pagcla;
use Session;
use Carbon\Carbon;
use Auth;
use App\Imports\ItemImporta;
use App\Imports\MultImport;
use Maatwebsite\Excel\Facades\Excel;
use Response;
//use Orcamento;
use App\Http\Controllers\Orcamento;
//use MenuController;

class PrevisaoorcamentariaController extends Controller
{
    //
    public $arrayQlp = array();
    public $sqlServerDb = 'sqlsrv';
    public $cont = 0;
    public $itemMenu = 1;
    public $created_at = '';
    public $updated_at = '';
    public $arrayMes = array('jan'=>'01','fev'=>'02','mar'=>'03','abr'=>'04','mai'=>'05','jun'=>'06','jul'=>'07','ago'=>'08','set'=>'09','out'=>'10','nov'=>'11','dez'=>'12');
    public $arrayMesF =  array('01'=>'JANEIRO',
                               '02'=>'FEVEREIRO',
                               '03'=>'MARÇO',
                               '04'=>'ABRIL',
                               '05'=>'MAIO',
                               '06'=>'JUNHO',
                               '07'=>'JUNHO',
                               '08'=>'AGOSTO',
                               '09'=>'SETEMBRO',
                               '10'=>'OUTUBRO',
                               '11'=>'NOVEMBRO',
                               '12'=>'DEZEMBRO');

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu );
        $this->created_at = Carbon::now()->format('Y-m-d H:i:s');
        $this->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        //$sql = "SELECT COUNT(*) FROM ORCLAN_FOR";
        //$x = DB::select($sql);


    }


    public function atualizadre(){

        $datainicio = date('Y-m-d H:i:s');

        $sql = "SELECT CODCGA,MES,ANO,CODGER,CODGRU,CODCUS,CODUNN,SINTET,ANALIT,ANALITICA,SINTETICA,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRAJUS) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2, SUM(VLRREA) AS VLRREA, SUM(VLRFOR) AS VLRFOR FROM VW_ORCLAN GROUP BY CODCGA,MES,ANO,CODGER,CODGRU,CODCUS,CODUNN,SINTET,ANALIT,ANALITICA,SINTETICA";
        $origem = db::select($sql);
        //dd(count($origem),$origem);
        $tot = 0;
        $status = 'DADOS DA VW_ORCLAN IMPORTADOS COM SUCESSO';
        if(count($origem)>0){

            db::table('ORCLAN_DRE')->truncate();
            try{

                foreach($origem as $o){

                    $MESANO = $o->MES.'/'.$o->ANO;

                    DB::table('ORCLAN_DRE')->insert(
                        ['CODCGA'=>$o->CODCGA,'MES'=>$o->MES,'ANO'=>$o->ANO,'MESANO'=>$MESANO,'CODGER'=>$o->CODGER,'CODGRU'=>$o->CODGRU,'CODCUS'=>$o->CODCUS,'CODUNN'=>$o->CODUNN,'SINTET'=>$o->SINTET,'ANALIT'=>$o->ANALIT,'ANALITICA'=>$o->ANALITICA,'SINTETICA'=>$o->SINTETICA,'VLRIMP'=>$o->VLRIMP , 'VLRINI'=>$o->VLRINI , 'VLRAJUS'=>$o->VLRAJUS , 'VLRAJUS2'=>$o->VLRAJUS2, 'VLRREA'=>$o->VLRREA,'VLRFOR'=>$o->VLRFOR]
                    );
                    $tot++;

                }

            } catch (Exception $e) {

                $status = $e->getMessage();

            }


        }else{
            $status = 'NENHUM DADO ENCONTRADO PARA IMPORTAÇÃO';
        }
        $daatFim = date('Y-m-d H:i:s');

        DB::table('LOGIMPORT')->insert(
            ['STATUS'=>$status ,'REGTOTAL'=>$tot,'INICIO'=>$datainicio,'FIM'=>$daatFim]
        );

    }

    public function getEbitda($ANO,$CODCGA,$CODUNN){

        $sql = "select MES, sum(VLRINI) as VAL from VW_ORCLAN where CODGER between 0 and 9 and ANO='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA." GROUP BY MES";

        //$sql = "select sum(VLRINI) as VAL from VW_ORCLAN where CODGER between 0 and 9 and ANO='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA;
       // $rs = DB::select($sql);

        //$itemSint[5]=array(0=>'15',1=>'725',2=>'528',3=>'153',4=>'307',5=>'772');
        //$itemSint[6]=array(0=>'15',1=>'519',2=>'5',3=>'20',4=>'573',5=>'136',6=>'590',7=>'600',8=>'638',9=>'535',10=>'147',11=>'733',12=>'703',13=>'14',14=>'138');

        //$CODCGA = 27; $CODUNN = 15;

        $despOpSql  = "SELECT MES, sum(VLRINI) AS VAL FROM ORCLAN_DRE WHERE CODGER = 5  AND SINTET IN(15,725,528,153,307,772) AND ANO ='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA." GROUP BY MES";
        $despOp = DB::select($despOpSql);

        $despAdmSql = "SELECT MES, sum(VLRINI) AS VAL FROM ORCLAN_DRE WHERE CODGER = 6 AND SINTET IN(15,519,5,20,573,136,590,600,638,535,147,733,703,14,138) AND ANO ='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA." GROUP BY MES";
        $despAdm = DB::select($despAdmSql);

        ///$despAdmSqlx = "SELECT MES, count(ANALIT) AS VAL FROM ORCLAN_DRE WHERE SINTET IN(15,519,5,20,573,136,590,600,638,535,147,733,703,14,138) AND ANO ='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA." GROUP BY MES";
       /// $despAdmx = DB::select($despAdmSql);
/*
       2 = 108,135,671,663,654,248,105,379,562,168
       3 = 119,509,145,114,632,49,688,106,730
       4 = 583,168

       0 = 154
       1 = 645
       9 = 646
*/
$sql0 = "SELECT MES, sum(VLRINI) AS VAL FROM ORCLAN_DRE WHERE CODGER = 0  AND SINTET IN(154) AND ANO ='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA." GROUP BY MES ORDER BY MES";
$soma0 = DB::select($sql0);

$sql1 = "SELECT MES, sum(VLRINI) AS VAL FROM ORCLAN_DRE WHERE CODGER = 1  AND SINTET IN(645) AND ANO ='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA." GROUP BY MES  ORDER BY MES";
$soma1 = DB::select($sql1);

$sql9 = "SELECT MES, sum(VLRINI) AS VAL FROM ORCLAN_DRE WHERE CODGER = 9  AND SINTET IN(646) AND ANO ='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA." GROUP BY MES  ORDER BY MES";
$soma9 = DB::select($sql9);

$sql2 = "SELECT MES, sum(VLRINI) AS VAL FROM ORCLAN_DRE WHERE CODGER = 2 AND SINTET IN(108,135,671,663,654,248,105,379,562,168) AND ANO ='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA." GROUP BY MES ORDER BY MES";
$soma2 = DB::select($sql2);

$sql3 = "SELECT MES, sum(VLRINI) AS VAL FROM ORCLAN_DRE WHERE CODGER = 3  AND SINTET IN(119,509,145,114,632,49,688,106,730) AND ANO ='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA." GROUP BY MES  ORDER BY MES";
$soma3 = DB::select($sql3);

$sql4 = "SELECT MES, sum(VLRINI) AS VAL FROM ORCLAN_DRE WHERE CODGER = 4  AND SINTET IN(583,168) AND ANO ='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA." GROUP BY MES ORDER BY MES";
$soma4 = DB::select($sql4);


       // dd($despOp,$despAdm);


        $eb['01'] = 0;
        $eb['02'] = 0;
        $eb['03'] = 0;
        $eb['04'] = 0;
        $eb['05'] = 0;
        $eb['06'] = 0;
        $eb['07'] = 0;
        $eb['08'] = 0;
        $eb['09'] = 0;
        $eb['10'] = 0;
        $eb['11'] = 0;
        $eb['12'] = 0;

        $mes[0]='01';
        $mes[1]='02';
        $mes[2]='03';
        $mes[3]='04';
        $mes[4]='05';
        $mes[5]='06';
        $mes[6]='07';
        $mes[7]='08';
        $mes[8]='09';
        $mes[9]='10';
        $mes[10]='11';
        $mes[11]='12';


        for($x = 0;$x < 12; $x++){

            $v0 = isset($soma0[$x]->VAL) ? $soma0[$x]->VAL : 0;
            $v1 = isset($soma1[$x]->VAL) ? $soma1[$x]->VAL : 0;
            $v9 = isset($soma9[$x]->VAL) ? $soma9[$x]->VAL : 0;

            $v2 = isset($soma2[$x]->VAL) ? $soma2[$x]->VAL : 0;
            $v3 = isset($soma3[$x]->VAL) ? $soma3[$x]->VAL : 0;
            $v4 = isset($soma4[$x]->VAL) ? $soma4[$x]->VAL : 0;

            $receitaOpLiquida = $v0 - (abs($v1) + abs($v9));
            $lucruBruto = $receitaOpLiquida +  $v2 + $v3 + $v4;

            $despOpVal = isset($despOp[$x]->VAL) ? $despOp[$x]->VAL : 0;
            $despAdmVal = isset($despAdm[$x]->VAL) ? $despAdm[$x]->VAL : 0;

            $eb[$mes[$x]] = $lucruBruto - (abs($despOpVal) + abs($despAdmVal));

        }
        /*
        if(count($rs) >0){
            foreach($rs as $r){

                $eb[$r->MES] = $r->VAL;

            }
        }*/
/*
        $eb2['01'] = $eb['01'];
        $eb2['02'] = $eb2['01'] + $eb['02'];
        $eb2['03'] = $eb2['02'] + $eb['03'];
        $eb2['04'] = $eb2['03'] + $eb['04'];
        $eb2['05'] = $eb2['04'] + $eb['05'];
        $eb2['06'] = $eb2['05'] + $eb['06'];
        $eb2['07'] = $eb2['06'] + $eb['07'];
        $eb2['08'] = $eb2['07'] + $eb['08'];
        $eb2['09'] = $eb2['08'] + $eb['09'];
        $eb2['10'] = $eb2['09'] + $eb['10'];
        $eb2['11'] = $eb2['10'] + $eb['11'];
        $eb2['12'] = $eb2['11'] + $eb['12'];
*/
        $eb2['01'] = $eb['01'];
        $eb2['02'] = $eb['02'];
        $eb2['03'] = $eb['03'];
        $eb2['04'] = $eb['04'];
        $eb2['05'] = $eb['05'];
        $eb2['06'] = $eb['06'];
        $eb2['07'] = $eb['07'];
        $eb2['08'] = $eb['08'];
        $eb2['09'] = $eb['09'];
        $eb2['10'] = $eb['10'];
        $eb2['11'] = $eb['11'];
        $eb2['12'] = $eb['12'];
        $eb2['total'] = array_sum($eb);

        return $eb2;
    }

    public function getEbitdaMenu($ANO){

        $sqlMenu = "SELECT CODCGA,CODUNN FROM MENUFILIAL ORDER BY CODCGA,CODUNN ASC";
        $rsMenu = DB::select($sqlMenu);
        $matrix = array();
        foreach($rsMenu as $rsM){
            $val =  $this->getEbitda($ANO,$rsM->CODCGA,$rsM->CODUNN);
            $matrix[$rsM->CODCGA][$rsM->CODUNN] = $val['total'];
        }

        $lista = $matrix;
        /*
        $sql = "SELECT CODCGA,CODUNN, sum(VLRINI) as VAL from VW_ORCLAN where CODGER between 0 and 9 and ANO='".$ANO."' GROUP BY CODCGA,CODUNN";
        //$sql = "select sum(VLRINI) as VAL from VW_ORCLAN where CODGER between 0 and 9 and ANO='".$ANO."' AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA;
        $rs = DB::select($sql);

        foreach($rs as $r){
            $lista[$r->CODCGA][$r->CODUNN] = $r->VAL;
        }*/
        return $lista;

    }


    public function getEbitdaMes($MESX,$ANO,$CODCGA,$CODUNN){

        $mes[0]='01';
        $mes[1]='02';
        $mes[2]='03';
        $mes[3]='04';
        $mes[4]='05';
        $mes[5]='06';
        $mes[6]='07';
        $mes[7]='08';
        $mes[8]='09';
        $mes[9]='10';
        $mes[10]='11';
        $mes[11]='12';

        //$sql = "select sum(VLRINI) as VAL from VW_ORCLAN where CODGER between 0 and 9 and ANO='".$ANO."' and cast(MES as unsigned) <= ".$MES." AND CODUNN =".$CODUNN." AND CODCGA=".$CODCGA;
        //$r = DB::select($sql);

        $val =  $this->getEbitda($ANO,$CODCGA,$CODUNN);
        //dd($sql);
        return $val[$mes[$MESX]];
    }

    public function fixOrclan(){

        /// SETA SINTETICA E ANALITICA NA ORCLAN_FOR
        $sqlForSintet = "UPDATE ORCLAN_FOR F
        LEFT JOIN PAGCLA S ON
            F.SINTET = S.CODCLAP
        SET
            F.SINTETICA = S.DESCRI
        WHERE F.SINTETICA IS NULL";

        $sqlForAnalit = "UPDATE ORCLAN_FOR F
        LEFT JOIN PAGCLA S ON
            F.ANALIT = S.CODCLAP
        SET
            F.ANALITICA = S.DESCRI
        WHERE F.ANALITICA IS NULL";

        DB::select($sqlForSintet);
        DB::select($sqlForAnalit);


        /// SETA SINTETICA E ANALITICA NA ORCLAN_RAT
        $sqlRatSintet = "UPDATE ORCLAN_RAT F
        LEFT JOIN PAGCLA S ON
            F.SINTET = S.CODCLAP
        SET
            F.SINTETICA = S.DESCRI
        WHERE F.SINTETICA IS NULL";

        $sqlRatAnalit = "UPDATE ORCLAN_RAT F
        LEFT JOIN PAGCLA S ON
            F.ANALIT = S.CODCLAP
        SET
            F.ANALITICA = S.DESCRI
        WHERE F.ANALITICA IS NULL";

        DB::select($sqlRatSintet);
        DB::select($sqlRatAnalit);


        /// SETA O CODGER
        $sqlForCodger = "UPDATE ORCLAN_FOR F
        LEFT JOIN PAGCLA S ON
            F.ANALIT = S.CODCLAP
        SET
            F.CODGER = S.CODGER
        WHERE F.CODGER IS NULL";

        $sqlRatCodger = "UPDATE ORCLAN_RAT F
        LEFT JOIN PAGCLA S ON
            F.ANALIT = S.CODCLAP
        SET
            F.CODGER = S.CODGER
        WHERE F.CODGER IS NULL";

        DB::select($sqlForCodger);
        DB::select($sqlRatCodger);
    }

    public function iniciaorclanfor(Request $request){


        $arrayMesNotIn = array(0=>'01',1=>'02',2=>'03',3=>'04',4=>'05',5=>'06');

        $items = array();

        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;

        $sql1 = "SELECT DISTINCT CODCGA FROM MENUFILIAL ORDER BY CODCGA ASC";
        $codcga = DB::select($sql1);
        $item = array();
        foreach($codcga as $c){

            foreach($arrayMesNotIn as $mes){
                $MESANO = $mes.'/'.$ano;

                $sqlOrclan = "SELECT PAGCLA.CODGRU,ORCLAN.MESANO,ORCLAN.CODCGA,ORCLAN.CODUNN,ORCLAN.SINTET,ORCLAN.ANALIT,ORCLAN.CMVLRE FROM ORCLAN
                INNER JOIN PAGCLA ON ORCLAN.ANALIT = PAGCLA.CODCLAP WHERE CODCGA = ".$c->CODCGA." AND MESANO = '".$MESANO."' ORDER BY ORCLAN.SINTET ASC";

                $orclan = DB::connection('sqlsrv')->select($sqlOrclan);

                foreach($orclan as $o){

                DB::table('ORCLAN_FOR')
                ->where('ANALIT','=',$o->ANALIT)
                ->where('CODCGA','=',$o->CODCGA)
                ->where('CODUNN','=',$o->CODUNN)
                ->where('MESANO','=',$o->MESANO)
                ->update(['VALORFOR' => $o->CMVLRE]);

                }
            }

        }
        //dd($item);


        dd('PROCESSO FINALIZADO');
    }

    public function combinacoesDe($k, $xs){
        if ($k === 0)
            return array(array());
        if (count($xs) === 0)
            return array();
        $x = $xs[0];

        $xs1 = array_slice($xs,1,count($xs)-1);

        $res1 = combinacoesDe($k-1,$xs1);

        for ($i = 0; $i < count($res1); $i++) {
            array_splice($res1[$i], 0, 0, $x);
        }
        $res2 = combinacoesDe($k,$xs1);

        return array_merge($res1, $res2);
    }

    public function index(Request $request){

       // $dezenas =array_diff($numeros,$exclusao);
       // sort($dezenas);
       // dd($x);
       $this->fixOrclan();

        return view('previsaoorcamentaria.index');
    }

    public function menu(Request $request){

        return view('previsaoorcamentaria.menu');
    }

    public function importar(Request $request){

        $file_path = public_path("uploads/teste.xlsx");

        $array = Excel::toArray(new ItemImporta, $file_path);

        $collection = Excel::toCollection(new ItemImporta, $file_path);


        $import = new MultImport();
        $import->onlySheets('Worksheet1', 'Worksheet2');

        $teste  = Excel::toArray($import, $file_path);

        dd($array,$collection,$teste);

    }

    public function pegaQlp($CODCGA,$CODUNN,$ano,$mes){

        if($CODUNN == 15){
            $itens = '2,5,16,17,18,19,20';
        }elseif($CODUNN == 18){
            $itens = '43,46,55';
        }elseif($CODUNN == 19){
            $itens = '29';
        }elseif($CODUNN == 20){
            $itens = '36';
        }elseif($CODUNN == 21){
            $itens = '61,65';
        }elseif($CODUNN == 23){
            $itens = '79,83';
        }elseif($CODUNN == 22){
            $itens = '93,97';
        }else{
            return 'UNIDADE NÃO RECONHECIDA';
        }

        $sql = "SELECT SUM(VALOR) AS QLP FROM RECEITA_ROTA_CAD WHERE CODCGA =".$CODCGA." AND CODUNN = ".$CODUNN." AND ANO='".$ano."' AND MES='".$mes."' AND ITEN IN(".$itens.")";
        //dd();
        $r = DB::select($sql);
        //dd($r[0]->QLP);
        return $r[0]->QLP;
    }

    public function menupacote(Request $request){
        View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li>Pacote</li>' );
        $CODCGA = '';
        $CODUNN = '';
        $id =  Auth::user()->id;
        $usuario_pacote  = DB::table('USU_PACOTE')->select('CODIGO')->where('ID_USUARIO','=',$id)->pluck('CODIGO')->toArray();

        $listaPacote = DB::table('MENUPACOTE')->select('MENUPACOTE.CODIGO','RODGRU.DESCRI')->join('RODGRU','MENUPACOTE.CODIGO','=','RODGRU.CODIGO')->get();


        if($request->CODCGA && $request->CODUNN){
            $unidadeFilial = (array) DB::table('MENUFILIAL')
            ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
            ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
            ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
            ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
            ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
            ->first();
            $CODCGA = $request->CODCGA;
            $CODUNN = $request->CODUNN;
           //dd($unidadeFilial);
        }

        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;


        $ebitda = $this->getEbitda($ano,$CODCGA,$CODUNN);


        return view('previsaoorcamentaria.menupacote',compact('ebitda','unidadeFilial','CODCGA','CODUNN','listaPacote','usuario_pacote'));
    }

    public function menufilial(Request $request){
        $this->fixOrclan();

        View::share ( 'subMenu', '<li>Centro de Gasto</li>' );

        $id =  Auth::user()->id;

        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;
        $listaEbitda = $this->getEbitdaMenu($ano);


        //dump($id);
        $usuario_codcga  = DB::table('USU_CGA')->select('CODCGA')->where('ID_USUARIO','=',$id)->pluck('CODCGA')->toArray();
        $usuario_unidade = DB::table('USU_UNIDADE')->select('CODUNN')->where('ID_USUARIO','=',$id)->pluck('CODUNN')->toArray();
        $usuario_pacote  = DB::table('USU_PACOTE')->select('CODIGO')->where('ID_USUARIO','=',$id)->pluck('CODIGO')->toArray();

        $listaFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.CODCGA','RODCGA.DESCRI AS CENTRODEGASTO')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->whereIn('MENUFILIAL.CODCGA', $usuario_codcga)
        ->distinct()->get();
        //dd($listaFilial);

        $listaUnidade = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->whereIn('MENUFILIAL.CODUNN', $usuario_unidade)
        ->get();

        return view('previsaoorcamentaria.menufilial',compact('listaFilial','listaUnidade','listaEbitda'));
    }

    public function pacoterateio(Request $request){

        //$listaCentroDeCusto = DB::table('RODCUS')->select('CODCUS','DESCRI')->whereIn('CODCUS', [19,20,21,22,23,24,25,26,27,29,30,32,33,35,37,40])->get();

        //dd($listaCentroDeCusto);
        $permissaoeditarR = DB::table('USU_ACAO')->select('EDITAR')->where('ID_USUARIO','=',Auth::user()->id)->first();

        if($permissaoeditarR == null){
            $permissaoeditar = 'N';
        }else{
            $permissaoeditar = $permissaoeditarR->EDITAR;
        }

        $arrayMes = $this->arrayMes;

        if($request->codgru && $request->CODCGA && $request->CODUNN){
            session(['codgru' => $request->codgru]);
            session(['CODCGA' => $request->CODCGA]);
            session(['CODUNN' => $request->CODUNN]);
            session(['ratCODCLAP' => $request->ratCODCLAP]);
            session(['ratPAICLAP' => $request->ratPAICLAP]);
        }
            $codgru = session('codgru');
            $CODCGA = session('CODCGA');
            $CODUNN = session('CODUNN');
            $ratCODCLAP = session('ratCODCLAP');
            $ratPAICLAP = session('ratPAICLAP');
            $ANALIT = $ratCODCLAP;
            $SINTET = $ratPAICLAP;

            $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
            $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
            $ano = $orc->ano;
            $items = array();

        $idTrechoInsert = Carbon::now()->format('YmdHisu');
        //dd($idTrechoInsert);

        $unidadeFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->where('MENUFILIAL.CODCGA', '=', $CODCGA)
        ->where('MENUFILIAL.CODUNN', '=', $CODUNN)
        ->first();

        if($request->addrateio == 'ok'){

            $items = array();
            $CODCUS = $request->ratCODCUS;

            if($CODCUS !== "0"){

            foreach ($this->arrayMes as $key => $value){

                $mes = $value;
                $MESANO = $mes."/".$ano;
                $valor = 0;
                $items[] = ['ANO' => $ano,'MES' => $mes,'MESANO' => $MESANO,'CODCUS'=>$CODCUS, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET,'VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];
            }
            $q = ['ANO' => $ano,'CODCUS'=>$CODCUS, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET];

            $r = DB::table('ORCLAN_RAT')->select('*')
                    ->where('ANO','=', $ano)
                    ->where('CODCUS','=',$CODCUS)
                    ->where('CODCGA','=', $CODCGA)
                    ->where('CODGRU','=',$codgru)
                    ->where('CODUNN','=', $CODUNN)
                    ->where('ANALIT','=', $ANALIT)
                    ->where('SINTET','=', $SINTET)
                    ->get();

            $tot = count($r);

            //sleep(2);

            if($tot < 1){
                $id = DB::table('ORCLAN_RAT')->insert($items);
            }
        }

        }

        if($request->removerRateio == 'ok'){

           $delRateio = DB::table('ORCLAN_RAT')
           ->where('ANO','=', $ano)
           ->where('CODCUS','=',$request->remover_rateio_id)
           ->where('CODCGA','=', $CODCGA)
           ->where('CODGRU','=',$codgru)
           ->where('CODUNN','=', $CODUNN)
           ->where('ANALIT','=', $ANALIT)
           ->where('SINTET','=', $SINTET)->delete();
           //dump($ano,$request->remover_rateio_id,$CODCGA,$codgru,$CODUNN,$ANALIT,$SINTET);
           //dd($delRateio);
        }

        if($request->salvar == 'ok'){


                    $v = $request->all();
                    $items = array();
                    $jus = array();
                    $justificativa = '';

                    foreach ($v as $key => $value){

                        $r = explode('_',$key);

                        if($r[0] == 'just'){


                            $ANALIT = $r[1];
                            $SINTET = $r[2];

                            if($r[3] == ''){
                                $CODCUS = 0;
                            }else{
                                $CODCUS = $r[3];
                            }


                            $justificativa = $value; // pega a justificativa
                            $analit_jus = $r[1];

                            $itemsAnalitBuscaj = ['ANO' => $ano,'CODCUS'=>$CODCUS , 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET];
                            $itemsAnalitValuej = ['JUSTIFICATIVA' =>  $justificativa, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                            DB::table('ORCLAN_RAT')->updateOrInsert(
                                $itemsAnalitBuscaj,
                                $itemsAnalitValuej
                                );

                        }

                        if(array_key_exists($r[0],$arrayMes)){ // VERIFICA ITEM E MES

                            $ANALIT = $r[1];
                            $SINTET = $r[2];

                            if($r[3] == ''){
                                $CODCUS = 0;
                            }else{
                                $CODCUS = $r[3];
                            }


                            $mes = $arrayMes[$r[0]];


                            $MESANO = $mes."/".$ano;

                            $valor = str_replace(',','.', str_replace('.','', $value));

                            //$items[] = ['ANO' => $ano,'MES' => $mes,'MESANO' => $MESANO,'CODCUS'=>$CODCUS, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET,'VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];

                            $itemsAnalitBusca = ['ANO' => $ano,'MES' => $mes,'MESANO' => $MESANO,'CODCUS'=>$CODCUS , 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET];
                            $itemsAnalitValue = ['VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];
                            //dump($itemsAnalitBusca,$itemsAnalitValue);

                            DB::table('ORCLAN_RAT')->updateOrInsert(
                                $itemsAnalitBusca,
                                $itemsAnalitValue
                                );


                        }
                    }
//dd();
            $this->fixOrclan();
        }

        $sqlAnalit = "SELECT P.*,R.CODCUS, R.DESCRI AS CUSTO ,PA.DESCRI AS ANALITICA,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL1,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL2,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL3,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL4,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL5,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL6,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL7,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL8,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL9,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL10,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL11,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12' AND P.CODCUS = C.CODCUS AND P.ANALIT = C.ANALIT),'0.00') AS VAL12
        FROM ORCLAN_RAT P
        LEFT JOIN RODCUS R ON R.CODCUS = P.CODCUS
        LEFT JOIN PAGCLA PA ON PA.CODCLAP = P.ANALIT
        WHERE P.ANO = ".$ano." AND P.MES = '01' AND P.ANALIT = ".$ANALIT." AND P.CODCGA = ".$CODCGA." AND P.CODUNN = ".$CODUNN." AND P.CODGRU = ".$codgru." ORDER BY P.ID DESC";

        //dump($sqlAnalit);

        $listaAnalitRateio = DB::select($sqlAnalit);

        //dd($sqlAnalit,$listaAnalitRateio);

        $classificacaoRateio = DB::table('PAGCLA')->select('DESCRI')->where('CODCLAP','=',$request->ratCODCLAP)->first();

        // menu centro de custo
        $listaCodcus = DB::table('ORCLAN_RAT')->select('CODCUS')->where(['ANO' => $ano,'MES' =>'01','CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET])->get();
        $itemsCusto = array();

        if(count($listaCodcus) > 0){
            foreach($listaCodcus as $lc){
                $itemsCusto[] = $lc->CODCUS;
            }
        }else{
            $itemsCusto[] = 0;
        }
        $itemsCustoBusca = [0,19,20,21,22,23,24,25,26,27,29,30,32,33,35,37,40];
        $arrayCusto = array_diff($itemsCustoBusca, $itemsCusto);
        $listaCentroDeCusto = DB::table('RODCUS')->select('CODCUS','DESCRI')->whereIn('CODCUS', $arrayCusto)->get();

      View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li>Pacote - Rateio Por Centro de Custo | Classificação: <b>'.$request->ratCODCLAP.' - '.$classificacaoRateio->DESCRI.'</b></li>' );
      return view('previsaoorcamentaria.pacoterateio',['permissaoeditar'=>$permissaoeditar,'listaAnalitRateio'=>$listaAnalitRateio,'ratCODCLAP'=>$ratCODCLAP,'ratPAICLAP'=>$ratPAICLAP,'listaCentroDeCusto'=>$listaCentroDeCusto,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'pacote'=>$pacote->DESCRI,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);


    }

    public function simuladorqlpadm(Request $request){

    }

    public function simuladorepi(Request $request){

        $arrayMes = $this->arrayMes;


        $permissaoeditarR = DB::table('USU_ACAO')->select('EDITAR')->where('ID_USUARIO','=',Auth::user()->id)->first();

        if($permissaoeditarR == null){
            $permissaoeditar = 'N';
        }else{
            $permissaoeditar = $permissaoeditarR->EDITAR;
        }

        //dump($request->mesepi);

        if($request->ajax == 'p1'){
            $id = $request->id;
            $lsta = DB::select('SELECT EPI_SMLD_ITEM.EPI_SMLD_PROD_ID as id,EPI_SMLD_PROD.DESCRI as text FROM EPI_SMLD_ITEM INNER JOIN EPI_SMLD_PROD ON EPI_SMLD_PROD.ID = EPI_SMLD_ITEM.EPI_SMLD_PROD_ID WHERE EPI_SMLD_ITEM.ANALIT ='.$id.' GROUP BY EPI_SMLD_ITEM.EPI_SMLD_PROD_ID ,EPI_SMLD_PROD.DESCRI');
            //$v = json_encode($lsta);
            return Response::json($lsta);
        }

        if($request->ajax == 'p2'){
            $id = $request->id;
            $tipo = DB::select('SELECT TIPO FROM EPI_SMLD_ITEM WHERE EPI_SMLD_ITEM.ANALIT = '.$id.' LIMIT 1');
            $t = $tipo[0]->TIPO;
            $lsta = DB::select('SELECT EPI_SMLD_ITEM.EPI_SMLD_CARGO_ID as id,EPI_SMLD_CARGO.DESCRI as text FROM EPI_SMLD_ITEM INNER JOIN EPI_SMLD_CARGO ON EPI_SMLD_CARGO.ID = EPI_SMLD_ITEM.EPI_SMLD_CARGO_ID WHERE EPI_SMLD_ITEM.TIPO ="'.$t.'" GROUP BY EPI_SMLD_ITEM.EPI_SMLD_CARGO_ID ,EPI_SMLD_CARGO.DESCRI');
            //$v = json_encode($lsta);
            return Response::json($lsta);
        }

        if($request->ajax == 'p3'){
            $id = $request->id;
            $tipo = DB::select('SELECT TIPO FROM EPI_SMLD_ITEM WHERE EPI_SMLD_ITEM.ANALIT = '.$id.' LIMIT 1');
            $t = $tipo[0]->TIPO;

            if($t == 'OP'){
                $itemsCustoBusca = [35];
            }else{
                $itemsCustoBusca = [19,20,21,22,23,24,25,26,27,29,30,32,33,37,40];
            }


            $lsta = DB::table('RODCUS')->select('CODCUS  as id','DESCRI as text')->whereIn('CODCUS', $itemsCustoBusca)->get();
            //$lsta = DB::select('SELECT EPI_SMLD_ITEM.EPI_SMLD_CARGO_ID as id,EPI_SMLD_CARGO.DESCRI as text FROM EPI_SMLD_ITEM INNER JOIN EPI_SMLD_CARGO ON EPI_SMLD_CARGO.ID = EPI_SMLD_ITEM.EPI_SMLD_CARGO_ID WHERE EPI_SMLD_ITEM.TIPO ="'.$t.'" GROUP BY EPI_SMLD_ITEM.EPI_SMLD_CARGO_ID ,EPI_SMLD_CARGO.DESCRI');
            //$v = json_encode($lsta);
            return Response::json($lsta);
        }

        if($request->codgru && $request->CODCGA && $request->CODUNN){
            session(['codgru' => $request->codgru]);
            session(['CODCGA' => $request->CODCGA]);
            session(['CODUNN' => $request->CODUNN]);
            session(['mesepi' => $request->mesepi]);
        }

            $codgru = session('codgru');
            $CODCGA = session('CODCGA');
            $CODUNN = session('CODUNN');
            $mesepi = session('mesepi');

            //dd( $mesepi );
            if($mesepi == null){
                session(['mesepi' => '01']);
                $mesepi = session('mesepi');
            }

           /// dump($request->mesepi,session('mesepi'),$mesepi);

            $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
            $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
            $ano = $orc->ano;
            $items = array();

            $unidadeFilial = DB::table('MENUFILIAL')
            ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
            ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
            ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
            ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
            ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
            ->first();


        if($request->addepi){

            //dd($request);

            $listaProd = DB::table('EPI_SMLD_PROD')->select('*')->orderBy('DESCRI','ASC')->get();
            $listaCargo = DB::table('EPI_SMLD_CARGO')->select('*')->orderBy('DESCRI','ASC')->get();
            $listaAnalit = DB::table('EPI_SMLD_ANALIT')->select('EPI_SMLD_ANALIT.ANALIT','PAGCLA.DESCRI AS ANALITICA')
            ->join('PAGCLA', 'EPI_SMLD_ANALIT.ANALIT', '=', 'PAGCLA.CODCLAP')->orderBy('PAGCLA.DESCRI','ASC')->get();

            $mesepi = $request->addMes;
            $nomeMes = $this->arrayMesF[$mesepi];

            View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li>Simulador EPI </li><li><b>'.$nomeMes.'</b></li>' );
            return view('previsaoorcamentaria.simuladorepiadd',['permissaoeditar'=>$permissaoeditar,'mesepi'=>$mesepi,'arrayMes'=>$this->arrayMes,'arrayMesF'=>$this->arrayMesF,'listaProd'=>$listaProd,'listaCargo'=>$listaCargo,'listaAnalit'=>$listaAnalit,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'pacote'=>$pacote->DESCRI,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);

        }

        if($request->salvarEpi){ // salva toda lista de EPIs do Mes selecionado
            //dd($request);

            $mesepi = $request->mesepiLista;

            foreach ($request->ID as $key => $value) {
                //dump($key,$value);
                $ID = $value;

                $TOTALQLP = str_replace(',','.',$request->TOTALQLP[$key]);
                $VALORUNITARIO = str_replace(',','.',$request->VALORUNITARIO[$key]);
                $QTDITEM = str_replace(',','.',$request->QTDITEM[$key]);
                $ESTOQMIN = str_replace(',','.',$request->ESTOQMIN[$key]);

                //dd($QTDITEM,$TOTALQLP,$ESTOQMIN,$VALORUNITARIO);

                $VALORTOTAL = (($QTDITEM*$TOTALQLP)+$ESTOQMIN)*$VALORUNITARIO;
                DB::table('EPI_SMLD_CAD')
                ->where('ID', $ID)
                ->update(['TOTALQLP'=>$TOTALQLP,'VALORUNITARIO' => $VALORUNITARIO,'QTDITEM'=>$QTDITEM,'ESTOQMIN'=>$ESTOQMIN,'VALORTOTAL'=>$VALORTOTAL]);

                $this->atualizaEpi($ID);

            }
//die;
            return redirect()->route('previsaoorcamentaria/simuladorepi', ['permissaoeditar'=>$permissaoeditar,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'mesepi'=>$mesepi])->with('message', 'Item adicionado!');

        }

        if($request->salvar == "ok"){ // adiciona Um EPI no Mes selecionado

            //dd($request);

            $ANALIT = $request->ANALIT;
            $EPI_SMLD_PROD_ID = $request->EPI_SMLD_PROD_ID;
            $EPI_SMLD_CARGO_ID = $request->EPI_SMLD_CARGO_ID;
            $CODCUS = $request->CODCUS;

            $tipo = DB::select('SELECT TIPO FROM EPI_SMLD_ITEM WHERE EPI_SMLD_ITEM.ANALIT = '.$ANALIT.' LIMIT 1');
            $t = $tipo[0]->TIPO;

            if($t == 'OP'){
                $totalQlp = $this->pegaQlp($CODCGA,$CODUNN,$ano,$mesepi);
            }else{
                $totalQlp = 0;
            }

            $itens = ['ANALIT'=>$ANALIT, 'EPI_SMLD_CARGO_ID'=>$EPI_SMLD_CARGO_ID, 'EPI_SMLD_PROD_ID'=>$EPI_SMLD_PROD_ID, 'CODCGA'=> $CODCGA,'CODCUS'=>$CODCUS, 'CODUNN'=> $CODUNN, 'CODGRU'=> $codgru, 'TOTALQLP'=> $totalQlp, 'VALORUNITARIO'=>0, 'QTDITEM' =>0, 'ESTOQMIN'=> 0, 'VALORTOTAL'=> 0, 'ANO'=> $ano, 'MES'=> $mesepi, 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];
            DB::table('EPI_SMLD_CAD')->insert($itens);
            return redirect()->route('previsaoorcamentaria/simuladorepi', ['permissaoeditar'=>$permissaoeditar,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'mesepi'=>$mesepi])->with('message', 'Item adicionado!');
        }

        if($request->removerEpi == "ok"){
            $mesepi = $request->mesepiRemover;
            $delitem = DB::table('EPI_SMLD_CAD')->where('ID', '=', $request->remover_id)->delete();
            return redirect()->route('previsaoorcamentaria/simuladorepi', ['permissaoeditar'=>$permissaoeditar,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'mesepi'=>$mesepi])->with('message', 'Item adicionado!');
        }


        $epi = DB::table('EPI_SMLD_CAD')
        ->select('EPI_SMLD_CAD.*','VW_EPI_TIPO.TIPO','EPI_SMLD_PROD.DESCRI AS PRODUTO','RODCUS.DESCRI AS CUSTO','EPI_SMLD_CARGO.DESCRI AS CARGO','PAGCLA.DESCRI AS ANALITICA')
        ->join('EPI_SMLD_PROD','EPI_SMLD_PROD.ID','EPI_SMLD_CAD.EPI_SMLD_PROD_ID')
        ->join('EPI_SMLD_CARGO','EPI_SMLD_CARGO.ID','EPI_SMLD_CAD.EPI_SMLD_CARGO_ID')
        ->join('PAGCLA', 'EPI_SMLD_CAD.ANALIT', '=', 'PAGCLA.CODCLAP')
        ->join('RODCUS', 'EPI_SMLD_CAD.CODCUS', '=', 'RODCUS.CODCUS')
        ->leftJoin('VW_EPI_TIPO','EPI_SMLD_CAD.ANALIT','=','VW_EPI_TIPO.ANALIT')
        ->where('EPI_SMLD_CAD.CODCGA','=',$CODCGA)
        ->where('EPI_SMLD_CAD.CODUNN','=',$CODUNN)
        ->where('EPI_SMLD_CAD.codgru','=',$codgru)
        ->where('EPI_SMLD_CAD.ANO','=',$ano)
        ->where('EPI_SMLD_CAD.MES','=',$mesepi)
        ->get();

        //dd($epi);

        //C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09'

        $listaProd = DB::table('EPI_SMLD_PROD')->select('*')->orderBy('DESCRI','ASC')->get();
        $listaCargo = DB::table('EPI_SMLD_CARGO')->select('*')->orderBy('DESCRI','ASC')->get();
        $listaAnalit = DB::table('EPI_SMLD_ANALIT')->select('EPI_SMLD_ANALIT.ANALIT','PAGCLA.DESCRI AS ANALITICA')
        ->join('PAGCLA', 'EPI_SMLD_ANALIT.ANALIT', '=', 'PAGCLA.CODCLAP')->orderBy('PAGCLA.DESCRI','ASC')->get();


        //dd($epi,$listaProd,$listaCargo,$listaAnalit);

        View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li>Simulador EPI</li>' );
        return view('previsaoorcamentaria.simuladorepi',['permissaoeditar'=>$permissaoeditar,'mesepi'=>$mesepi,'arrayMes'=>$this->arrayMes,'arrayMesF'=>$this->arrayMesF,'listaProd'=>$listaProd,'listaCargo'=>$listaCargo,'listaAnalit'=>$listaAnalit,'epi'=>$epi,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'pacote'=>$pacote->DESCRI,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);

    }

    public function atualizaEpi($id){

       $r = DB::table('EPI_SMLD_CAD')->select('EPI_SMLD_CAD.*','PAGCLA.PAICLAP AS SINTET')->join('PAGCLA','PAGCLA.CODCLAP','=','EPI_SMLD_CAD.ANALIT')->where('ID','=',$id)->first();

      $ANALIT = $r->ANALIT;
      $SINTET = $r->SINTET;
      $CODCGA = $r->CODCGA;
      $CODUNN = $r->CODUNN;
      $CODGRU = $r->CODGRU;
      $CODCUS = $r->CODCUS;
      //$VALORFOR = $r->VALORTOTAL;
      $ANO = $r->ANO;
      $MES = $r->MES;
      $MESANO = $r->MES."/".$r->ANO;

      $sqlSum = "SELECT SUM(VALORTOTAL) AS VALORFOR,CODCUS FROM EPI_SMLD_CAD WHERE CODUNN = ".$CODUNN." AND ANALIT =".$ANALIT." AND CODCGA = ".$CODCGA." AND CODGRU = ".$CODGRU." AND CODCUS = ".$CODCUS." AND ANO = ".$ANO." AND MES = '".$MES."' GROUP BY CODCUS";
      $epiSum = DB::select($sqlSum);
      $VALORFOR = $epiSum[0]->VALORFOR;
      //dump($sqlSum,$epiSum);
      //dd($CODCUS);
      $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $MES,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $CODGRU, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET,'CODCUS' => $CODCUS];
      $itemsAnalitValue = ['VALORFOR' =>  $VALORFOR, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

      $retorno = DB::table('ORCLAN_FOR')->updateOrInsert(
          $itemsAnalitBusca,
          $itemsAnalitValue
      );
      //dd($r,$itemsAnalitBusca,$itemsAnalitValue,$retorno);
      //die;
    }

    public function dre(Request $request){

        $orcamento = new Orcamento;

        if($request->busca == "busca"){

            //dd($request);
            $ano = $request->ano;
            $mes = $request->mes;
            $orcamento->gasto =$request->gasto;
            $orcamento->unidade = $request->unidade;
            $orcamento->ano = $ano;
            $orcamento->mes = $mes;
            $orcamento->opcaoview = 'COMPETENCIA';
            $orcamento->opcao = 'I';

            if(isset($orcamento->gasto[0]) == NULL){
                $orcamentoGasto = 9;
            }else{
                $orcamentoGasto = $orcamento->gasto[0];
            }
            $sqlConf = "SELECT * FROM IMPOSTOS WHERE ano = ".$orcamento->ano." AND filial=".$orcamentoGasto;
            $configImposto = db::select($sqlConf);
            $configuracao = $configImposto[0];

            $orcamento->indiceConfis = $configuracao->confis;
            $orcamento->indicePis = $configuracao->pis;
            $orcamento->indiceIss = $configuracao->indiceiss;
            $orcamento->contribuicaoServ = $configuracao->contribuicao_serv;
            $orcamento->contribuicaoTrans = $configuracao->contribuicao_trans;
            $orcamento->irpjServ = $configuracao->irpj_serv;
            $orcamento->irpjTrans = $configuracao->irpj_trans;
            $orcamento->indiceCPRB = $configuracao->cprb;
            $orcamento->despcorp = $configuracao->despcorp;

        }else{

            $ano = '';
            $mes = [];
            $orcamento->gasto = [];
            $orcamento->unidade = [];

        }
        $pagina = '';


        $filtroMes = $orcamento->boxMes('mes',$mes);
        $filtroGasto = $orcamento->boxGasto();
        $filtroUnidade = $orcamento->boxUnidade();
        $filtroAno = $orcamento->boxAno($ano);
        $boxFiltro = $orcamento->boxFiltro($pagina,$filtroAno, $filtroMes, $filtroGasto,$filtroUnidade,'Relatório Demonstrativo de Resultado - DRE ');

        if($request->busca == "busca"){

            $orcamento->processaItem('RECEBIMENTOS',0, 154);
            $orcamento->processaItem('IMPOSTOS S/ VENDAS',1,645);
            $orcamento->processaItem('COMBUSTÍVEIS',2,108);
            $orcamento->processaItem('FRETES / SERVIÇOS DE TERCEIROS',2,135);
            $orcamento->processaItem('MANUTENÇÃO DE CAMINHÃO E CARROCERIA - MCC',2,671);
            $orcamento->processaItem('MANUTENÇÃO DE CARRETA - MCAR',2,663);
            $orcamento->processaItem('MANUTENÇÃO DE CAVALO - MCAV',2,654);
            $orcamento->processaItem('MATERIAL DE CONSUMO',2,248);
            $orcamento->processaItem('PNEUS',2,105);
            $orcamento->processaItem('EMPILHADEIRAS',2,379);
            $orcamento->processaItem('DESPESAS DE VIAGEM - OP',2,562);
            $orcamento->processaItem('SEGUROS',2,168);
            $orcamento->processaItem('ACESSÓRIOS',3,119);
            $orcamento->processaItem('BENEFÍCIOS - OP',3,509);
            $orcamento->processaItem('CARRINHOS DE ENTREGA',3,145);
            $orcamento->processaItem('E.P.I.s - OP',3,114);
            $orcamento->processaItem('ENCARGOS - OP',3,632);
            $orcamento->processaItem('LICENCIAMENTOS DE VEÍCULOS',3,49);
            $orcamento->processaItem('REMUNERAÇÃO - OP',3, 688);
            $orcamento->processaItem('GERENCIAMENTO DE RISCOS',3,106);
            //$orcamento->processaItem('REMUNERAÇÃO - ADM',3,703);
            $orcamento->processaItem('MEDICINA OCUPACIONAL - OP',3,730);
            $orcamento->processaItem('DESPESAS JURÍDICAS  - OP',4,583);
            $orcamento->processaItem('SEGUROS',4,168);
            $orcamento->processaItem('ALUGUEL',5,15);
            $orcamento->processaItem('DESPESAS COM FROTA LEVE',5,725);
            $orcamento->processaItem('FORMAÇÃO/MOTIVACIONAL-OP',5,528);
            $orcamento->processaItem('MARKETING',5,153);
            $orcamento->processaItem('PREJUIZO',5,307);
            $orcamento->processaItem('MULTAS',5,772);
            $orcamento->processaItem('ALUGUEL',6,15);
            $orcamento->processaItem('BENEFICIOS - ADM',6,519);
            $orcamento->processaItem('CONSULTORIAS',6,5);
            $orcamento->processaItem('DESPESAS ADMINISTRATIVAS',6,20);
            $orcamento->processaItem('DESPESAS DE VIAGEM – ADM',6,573);
            $orcamento->processaItem('IMPOSTOS E TAXAS',6, 136);
            $orcamento->processaItem('DESPESAS JURÍDICAS  - ADM',6,590);
            $orcamento->processaItem('E.P.I.s –ADM',6, 600);
            $orcamento->processaItem('ENCARGOS-ADM',6, 638);
            $orcamento->processaItem('FORMAÇÃO/MOTIVACIONAL- ADM',6,535);
            $orcamento->processaItem('MANUTENÇÃO PREDIAL',6,147);
            $orcamento->processaItem('MEDICINA OCUPACIONAL - ADM',6,733);
            $orcamento->processaItem('REMUNERAÇÃO – ADM',6,703);
            $orcamento->processaItem('TECNOLOGIA DA INFORMAÇÃO',6,14);
            $orcamento->processaItem('DESPESAS COM MOBILIZADO',6,138);
            $orcamento->processaItem('DEPRECIAÇÃO',10, 746);
            $orcamento->processaItem('DESPESAS BANCÁRIAS',7,107);
            $orcamento->processaItem('IMPOSTOS E TAXAS',7,136);
            $orcamento->processaItem('JUROS/ENCARGOS',7,547);
            $orcamento->processaItem('TRANSFERÊNCIAS DE NUMERÁRIOS',7,157);
            //$orcamento->processaItem('RECEBIMENTOS',8,63);
            $orcamento->processaItem('RECEBIMENTOS',8,154);
            $orcamento->processaItem('RECEITA FINANCEIRA',8,720);
            $orcamento->processaItem('RECEITA NÃO OPERACIONAL',8,736);
            $orcamento->processaItem('IMPOSTOS S/ LUCROS',9,646);
            $orcamento->processaItem('(-) FINANCIAMENTOS DE VEÍCULOS',14,149);
            $orcamento->processaItem('(-) AQUISIÇÃO DE VEÍCULOS',14,115);
           // dd('fez');
           $orcamento->pegaDados();


           $orcamento->processaGrupo(0,154,'grupox');
           $orcamento->processaGrupo(1,645,'grupo2');
           $orcamento->processaGrupo(2,108,'grupo3');
           $orcamento->processaGrupo(2,135,'grupo4');
           $orcamento->processaGrupo(2,671,'grupo5');
           $orcamento->processaGrupo(2,663,'grupo6');
           $orcamento->processaGrupo(2,654,'grupo7');
           $orcamento->processaGrupo(2,248,'grupo8');
           $orcamento->processaGrupo(2,105,'grupo9');
           $orcamento->processaGrupo(2,379,'grupo10');
           $orcamento->processaGrupo(2,562,'grupo11');
           $orcamento->processaGrupo(2,168,'grupo12');
           $orcamento->processaGrupo(3,119,'grupo13');
           $orcamento->processaGrupo(3,509,'grupo14');
           $orcamento->processaGrupo(3,145,'grupo15');
           $orcamento->processaGrupo(3,114,'grupo16');
           $orcamento->processaGrupo(3,632,'grupo17');
            $orcamento->processaGrupo(3,49,'grupo18');
           $orcamento->processaGrupo(3,688,'grupo19');
           $orcamento->processaGrupo(3,106,'grupo20');
         //$orcamento->processaGrupo(3,703,'grupo1');
           $orcamento->processaGrupo(3,730,'grupo21');
           $orcamento->processaGrupo(4,583,'grupo22');
           $orcamento->processaGrupo(4,168,'grupo23');
            $orcamento->processaGrupo(5,15,'grupo24');
           $orcamento->processaGrupo(5,725,'grupo25');
           $orcamento->processaGrupo(5,528,'grupo26');
           $orcamento->processaGrupo(5,153,'grupo27');
           $orcamento->processaGrupo(5,307,'grupo28');
           $orcamento->processaGrupo(5,772,'grupo29');
            $orcamento->processaGrupo(6,15,'grupo30');
           $orcamento->processaGrupo(6,519,'grupo31');
             $orcamento->processaGrupo(6,5,'grupo32');
            $orcamento->processaGrupo(6,20,'grupo33');
           $orcamento->processaGrupo(6,573,'grupo34');
           $orcamento->processaGrupo(6,136,'grupo35');
           $orcamento->processaGrupo(6,590,'grupo36');
           $orcamento->processaGrupo(6,600,'grupo37');
           $orcamento->processaGrupo(6,638,'grupo38');
           $orcamento->processaGrupo(6,535,'grupo39');
           $orcamento->processaGrupo(6,147,'grupo40');
           $orcamento->processaGrupo(6,733,'grupo41');
           $orcamento->processaGrupo(6,703,'grupo42');
            $orcamento->processaGrupo(6,14,'grupo43');
           $orcamento->processaGrupo(6,138,'grupo44');
          $orcamento->processaGrupo(10,746,'xxx');
           $orcamento->processaGrupo(7,107,'grupo47');
           $orcamento->processaGrupo(7,136,'grupo48');
           $orcamento->processaGrupo(7,547,'grupo49');
           $orcamento->processaGrupo(7,157,'grupo50');
          //$orcamento->processaGrupo(8,63,'grupo1');
           $orcamento->processaGrupo(8,154,'grupo51');
           $orcamento->processaGrupo(8,720,'grupo52');
           $orcamento->processaGrupo(8,736,'grupo53');
           $orcamento->processaGrupo(9,646,'grupo54');
          $orcamento->processaGrupo(14,149,'grupo45');
          $orcamento->processaGrupo(14,115,'grupo46');

           $orcamento->somaGrupoGerencial(0);
           $orcamento->somaGrupoGerencial(1);
           $orcamento->somaGrupoGerencial(2);
           $orcamento->somaGrupoGerencial(3);
           $orcamento->somaGrupoGerencial(4);
           $orcamento->somaGrupoGerencial(5);
           $orcamento->somaGrupoGerencial(6);
           $orcamento->somaGrupoGerencial(7);
           $orcamento->somaGrupoGerencial(8);
           $orcamento->somaGrupoGerencial(9);
           $orcamento->somaGrupoGerencial(10);
           $orcamento->somaGrupoGerencial(14);

           $orcamento->calculaFormulas();

           $itemSint[0]=array(0=>'154');
           $itemSint[1]=array(0=>'645');
           $itemSint[2]=array(0=>'108',1=>'135',2=>'671',3=>'663',4=>'654',5=>'248',6=>'105',7=>'379',8=>'562',9=>'168');
           //$itemSint[2]=array(0=>'108',1=>'135',2=>'671',3=>'654',4=>'248',5=>'105',6=>'379',7=>'562',8=>'168');
           $itemSint[3]=array(0=>'119',1=>'509',2=>'145',3=>'114',4=>'632',5=>'49',6=>'688',7=>'106',8=>'730');
           $itemSint[4]=array(0=>'583',1=>'168');
           $itemSint[5]=array(0=>'15',1=>'725',2=>'528',3=>'153',4=>'307',5=>'772');
           $itemSint[6]=array(0=>'15',1=>'519',2=>'5',3=>'20',4=>'573',5=>'136',6=>'590',7=>'600',8=>'638',9=>'535',10=>'147',11=>'733',12=>'703',13=>'14',14=>'138');
           $itemSint[7]=array(0=>'107',1=>'136',2=>'547',3=>'157');
           $itemSint[8]=array(0=>'154',1=>'720',2=>'736');
           $itemSint[9]=array(0=>'646');
           $itemSint[10]=array(0=>'746');
           $itemSint[14]=array(0=>'149',1=>'115');

           $data = '';
           //$openTable = '<table id="table2excel" border="0" class="tableid table table-bordered table-hover">';
           $openTable = '<table id="fixTable" class="table table-bordered">';


               $data.= $openTable;
               $cabecalho = $orcamento->cabeMes();
               $data.= $cabecalho;
               $data.='<tboty>';
               $data.= $orcamento->boxSomaGrupoReceita(0,'( = ) RECEITA OPERACIONAL BRUTA');
               $data.= $orcamento->boxGrupoReceita(0,154,'grupox');
               $data.= $orcamento->boxGrupo(1,645,'grupo2');
               //$data.= $orcamento->boxSomaGrupo(9,'( - ) IMPOSTOS S/ LUCROS');
               $data.= $orcamento->boxGrupo(9,646,'grupo54');
               $data.= $orcamento->boxDeducoes('( - ) DEDUÇÕES SOBRE RECEITA');
               $data.= $orcamento->opLiquido('( = ) RECEITA OPERACIONAL LIQUIDA');
               $data.= $orcamento->boxSomaGrupo(2,'( - ) CUSTO DIRETO VARIÁVEL');
               $data.= $orcamento->boxGrupo(2,108,'grupo3');
               $data.= $orcamento->boxGrupo(2,135,'grupo4');
               $data.= $orcamento->boxGrupo(2,671,'grupo5');
               $data.= $orcamento->boxGrupo(2,663,'grupo6');
               $data.= $orcamento->boxGrupo(2,654,'grupo7');
               $data.= $orcamento->boxGrupo(2,248,'grupo8');
               $data.= $orcamento->boxGrupo(2,105,'grupo9');
               $data.= $orcamento->boxGrupo(2,379,'grupo10');
               $data.= $orcamento->boxGrupo(2,562,'grupo11');
               $data.= $orcamento->boxGrupo(2,168,'grupo12');
               $data.= $orcamento->boxMargemContribuicao('( % ) MARGEM DE CONTRIBUIÇÃO');
               $data.= $orcamento->boxSomaGrupo(3,'( - ) CUSTO DIRETO FIXO');
               $data.= $orcamento->boxGrupo(3,119,'grupo13');
               $data.= $orcamento->boxGrupo(3,509,'grupo14');
               $data.= $orcamento->boxGrupo(3,145,'grupo15');
               $data.= $orcamento->boxGrupo(3,114,'grupo16');
               $data.= $orcamento->boxGrupo(3,632,'grupo17');
               $data.=  $orcamento->boxGrupo(3,49,'grupo18');
               $data.= $orcamento->boxGrupo(3,688,'grupo19');
               $data.= $orcamento->boxGrupo(3,106,'grupo20');
               //$data.= $orcamento->boxGrupo(3,703);
               $data.= $orcamento->boxGrupo(3,730,'grupo21');
               $data.= $orcamento->boxSomaGrupo(4,'( - ) CUSTO INDIRETO');
               $data.= $orcamento->boxGrupo(4,583,'grupo22');
               $data.= $orcamento->boxPassivo(' PASSIVO OPERACIONAL');
               $data.= $orcamento->boxGrupo(4,168,'grupo23');
               $data.= $orcamento->boxLucroBruto('( = ) LUCRO BRUTO');
               $data.= $orcamento->boxLucroBrutoPerc('( % ) LUCRO BRUTO');
               $data.= $orcamento->boxSomaGrupo(5,'( - ) DESPESAS OPERACIONAIS');
               $data.=  $orcamento->boxGrupo(5,15,'grupo24');
               $data.= $orcamento->boxGrupo(5,725,'grupo25');
               $data.= $orcamento->boxGrupo(5,528,'grupo26');
               $data.= $orcamento->boxGrupo(5,153,'grupo27');
               $data.= $orcamento->boxGrupo(5,307,'grupo28');
               $data.= $orcamento->boxGrupo(5,772,'grupo29');
               $data.= $orcamento->boxSomaGrupo(6,'( - ) DESPESAS ADMINISTRATIVAS');
               $data.=  $orcamento->boxGrupo(6,15,'grupo30');
               $data.= $orcamento->boxGrupo(6,519,'grupo31');
               $data.=   $orcamento->boxGrupo(6,5,'grupo32');
               $data.=  $orcamento->boxGrupo(6,20,'grupo33');
               $data.= $orcamento->boxGrupo(6,573,'grupo34');
               $data.= $orcamento->boxGrupo(6,136,'grupo35');
               $data.= $orcamento->boxGrupo(6,590,'grupo36');
               $data.= $orcamento->boxGrupo(6,600,'grupo37');
               $data.= $orcamento->boxGrupo(6,638,'grupo38');
               $data.= $orcamento->boxGrupo(6,535,'grupo39');
               $data.= $orcamento->boxGrupo(6,147,'grupo40');
               $data.= $orcamento->boxGrupo(6,733,'grupo41');
               $data.= $orcamento->boxGrupo(6,703,'grupo42');
               $data.=  $orcamento->boxGrupo(6,14,'grupo43');
               $data.= $orcamento->boxGrupo(6,138,'grupo44');
               $data.= $orcamento->boxABITDA('( = ) EBITDA (Lucro Antes do Juros, Impostos,<br/> Depreciação e AmortIzação)');
               $data.= $orcamento->boxABITDAPerc2('( % ) EBITDA (Lucro Antes do Juros, Impostos,<br/> Depreciação e AmortIzação)');
               $data.= $orcamento->boxGrupo(14,149,'grupo45');
               $data.= $orcamento->boxGrupo(14,115,'grupo46');
               $data.= $orcamento->boxABITDAVei('( = ) EBITDA - FINANCIAMENTOS');
               $data.= $orcamento->boxABITDAPerc2Vei('( % ) EBITDA - FINANCIAMENTOS');
               //$data.= $orcamento->boxSomaGrupo(10,'( - ) DEPRECIAÇÃO');
               //$data.= $orcamento->boxGrupo(10,746);
              // $data.= $orcamento->boxLucroOperacional('( = ) LUCRO OPERACIONAL');
              // $data.= $orcamento->boxLucroOperacionalPerc('( % ) LUCRO OPERACIONAL');
               $data.= $orcamento->boxSomaGrupo(7,'( - ) DESPESAS FINANCEIRAS');
               $data.= $orcamento->boxGrupo(7,107,'grupo47');
               $data.= $orcamento->boxGrupo(7,136,'grupo48');
               $data.= $orcamento->boxGrupo(7,547,'grupo49');
               $data.= $orcamento->boxGrupo(7,157,'grupo50');
           /*
               if($orcamento->ac == null){
                   $data.= $orcamento->boxDespesasCorporativas('( - ) DESPESAS CORPORATIVAS');
               }else{
                   $data.= $orcamento->boxDespesasCorporativasAC('( - ) DESPESAS CORPORATIVAS');
               }
               */

               //$data.= $orcamento->boxFundoDeReserva('( - ) FUNDO DE RESERVA');
               $data.= $orcamento->boxSomaGrupo(8,'( + ) RECEITA NÃO OPERACIONAL');
               $data.= $orcamento->boxGrupo(8,154,'grupo51');
               $data.= $orcamento->boxGrupo(8,720,'grupo52');
               $data.= $orcamento->boxGrupo(8,736,'grupo53');
               //$data.= $orcamento->boxLair('( = ) LAIR ( Lucro Anterior ao Imposto de Renda<br/> e Contribuição Social )');
               //$data.= $orcamento->boxLairPerc('( % )LAIR ( Lucro Anterior ao Imposto de Renda<br/> e Contribuição Social )');

               $data.= $orcamento->boxLucroLiquido('( = ) FLUXO DE CAIXA OPERACIONAL');
               $data.= $orcamento->boxLucroLiquidoPerc('( % ) FLUXO DE CAIXA OPERACIONAL');


               /*
               if($gerente == true){
                   $data.= $orcamento->boxSomaGrupoPositivo(10,'( + ) DEPRECIAÇÃO');
                   $data.= $orcamento->boxInvestimento('( - ) INVESTIMENTOS / PROJETOS');
                   $data.= $orcamento->boxCaixaLivre('( = ) FLUXO DE CAIXA LIVRE');
                   $data.= $orcamento->boxGrupo(14,149);
                   $data.= $orcamento->boxGrupo(14,115);
                   $data.= $orcamento->boxCaixaAcionista('( = ) FLUXO CAIXA DO ACIONISTA');
                   $data.= $orcamento->boxCaixaAcionistaPerc('( % ) FLUXO CAIXA DO ACIONISTA');
               }*/
               $closeTable = $orcamento->rodape().'</tbody></table>';
               $data.= $closeTable;
               //echo $data;

        }else{
            $data = '';
        }
        ///dd($boxFiltro);

        View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li>Pacote</li>' );
        return view('previsaoorcamentaria.dre',['boxFiltro'=>$boxFiltro,'data'=>$data]);
    }

    public function pacote(Request $request){


        //$listaCentroDeCusto = DB:table()->

        $permissaoeditarR = DB::table('USU_ACAO')->select('EDITAR')->where('ID_USUARIO','=',Auth::user()->id)->first();

        if($permissaoeditarR == null){
            $permissaoeditar = 'N';
        }else{
            $permissaoeditar = $permissaoeditarR->EDITAR;
        }


        $analitBoqueia = DB::table('ANALIT_AUTO')->select('ANALIT')->pluck('ANALIT')->toArray();
        //dd($analitBoqueia);

        $arrayMes = $this->arrayMes;

        $arrayMesNotIn = array(0=>'01',1=>'02',2=>'03',3=>'04',4=>'05',5=>'06');

        $codgru = $request->codgru;
        $CODCGA = $request->CODCGA;
        $CODUNN = $request->CODUNN;

        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;


        $ebitda = $this->getEbitda($ano,$CODCGA,$CODUNN);
/*
        $mesEbitda[1]  = $this->getEbitdaMes(1,$ano,$CODCGA,$CODUNN);
        $mesEbitda[2]  = $this->getEbitdaMes(2,$ano,$CODCGA,$CODUNN);
        $mesEbitda[3]  = $this->getEbitdaMes(3,$ano,$CODCGA,$CODUNN);
        $mesEbitda[4]  = $this->getEbitdaMes(4,$ano,$CODCGA,$CODUNN);
        $mesEbitda[5]  = $this->getEbitdaMes(5,$ano,$CODCGA,$CODUNN);
        $mesEbitda[6]  = $this->getEbitdaMes(6,$ano,$CODCGA,$CODUNN);
        $mesEbitda[7]  = $this->getEbitdaMes(7,$ano,$CODCGA,$CODUNN);
        $mesEbitda[8]  = $this->getEbitdaMes(8,$ano,$CODCGA,$CODUNN);
        $mesEbitda[9]  = $this->getEbitdaMes(9,$ano,$CODCGA,$CODUNN);
        $mesEbitda[10] = $this->getEbitdaMes(10,$ano,$CODCGA,$CODUNN);
        $mesEbitda[11] = $this->getEbitdaMes(11,$ano,$CODCGA,$CODUNN);
        $mesEbitda[12] = $this->getEbitdaMes(12,$ano,$CODCGA,$CODUNN);
*/

        if(isset($_REQUEST['salvar'])){ // PEGA DADOS POSTADOS


            $del = ['ANO' => $ano,'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN];
            //DB::table('ORCLAN_FOR')->where($del)->whereNotIn('MES',$arrayMesNotIn)->whereNotIn('ANALIT',$analitBoqueia)->delete();
            DB::table('ORCLAN_FOR')->where($del)->delete();
            //DB::table('ORCLAN_FOR')->where($del)->whereNotIn('MES',$arrayMesNotIn)->delete();
            DB::table('JUSTIFICATIVA')->where($del)->delete();


            $v = $request->all();
            $items = array();
            $jus = array();
            $justificativa = '';

            foreach ($v as $key => $value){

                $r = explode('_',$key);

                if($r[0] == 'just'){
                    $justificativa = $value; // pega a justificativa
                    $analit_jus = $r[1];
                    $jus[] = ['ANO' => $ano,'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$analit_jus,'TEXTO'=>$justificativa, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];
                }

                if(array_key_exists($r[0],$arrayMes)){ // VERIFICA ITEM E MES

                    $mes = $arrayMes[$r[0]];
                    $ANALIT = $r[1];
                    $SINTET = $r[2];

                    if($r[3] == ''){
                        $CODCUS = 0;
                    }else{
                        $CODCUS = $r[3];
                    }

                    $MESANO = $mes."/".$ano;

                    $valor = str_replace(',','.', str_replace('.','', $value));

                    $items[] = ['ANO' => $ano,'MES' => $mes,'MESANO' => $MESANO,'CODCUS'=>$CODCUS, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET,'VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];

                }
            }

       // dd($items);
        DB::table('ORCLAN_FOR')->insert($items);
        DB::table('JUSTIFICATIVA')->insert($jus);
        //dd($jus);
        $this->fixOrclan();

        }

        $unidadeFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
        ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
        ->first();
        //echo "SELECT PC.PAICLAP FROM PAGCLA PC WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' GROUP BY PC.PAICLAP";
        //$listaSintet = DB::select("SELECT PC.PAICLAP FROM PAGCLA PC WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' GROUP BY PC.PAICLAP");
        //dd($listaSintet);


        $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
        $sqlSintet = "SELECT P.CODCLAP,P.DESCRI,
		IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01' AND P.CODCLAP = C.SINTET),0) AS VAL1,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02' AND P.CODCLAP = C.SINTET),0) AS VAL2,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03' AND P.CODCLAP = C.SINTET),0) AS VAL3,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04' AND P.CODCLAP = C.SINTET),0) AS VAL4,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05' AND P.CODCLAP = C.SINTET),0) AS VAL5,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06' AND P.CODCLAP = C.SINTET),0) AS VAL6,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07' AND P.CODCLAP = C.SINTET),0) AS VAL7,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08' AND P.CODCLAP = C.SINTET),0) AS VAL8,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09' AND P.CODCLAP = C.SINTET),0) AS VAL9,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10' AND P.CODCLAP = C.SINTET),0) AS VAL10,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11' AND P.CODCLAP = C.SINTET),0) AS VAL11,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12' AND P.CODCLAP = C.SINTET),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12' AND P.CODCLAP = C.SINTET),0) AS VAL12
        FROM PAGCLA P WHERE P.CODCLAP IN(SELECT PC.PAICLAP FROM PAGCLA PC WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' GROUP BY PC.PAICLAP)";

        $sqlSintetSoma = "SELECT P.CODGRU,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01'),0) AS VAL1,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02'),0) AS VAL2,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03'),0) AS VAL3,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04'),0) AS VAL4,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05'),0) AS VAL5,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06'),0) AS VAL6,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07'),0) AS VAL7,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08'),0) AS VAL8,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09'),0) AS VAL9,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10'),0) AS VAL10,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11'),0) AS VAL11,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12'),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12'),0) AS VAL12
        FROM PAGCLA P WHERE P.CODGRU = ".$codgru." AND P.SITUAC = 'A' GROUP BY P.CODGRU";

//dd($sqlSintetSoma);
         //dd($sqlSintet);
        //$sqlAnalit = "SELECT P.*, IFNULL(F.VALORFOR,'00,0') AS VAL FROM PAGCLA P
        //              LEFT JOIN ORCLAN_FOR F ON P.CODGRU = F.CODGRU
        //              WHERE P.CODGRU = ".$codgru." AND F.CODGRU = ".$codgru." AND P.SITUAC = 'A' AND F.ANO = $ano AND F.CODCGA = $CODCGA AND F.CODUNN  = $CODUNN ORDER BY P.PAICLAP ASC;";
        /*
         $sqlAnalit = "SELECT P.*,J.TEXTO, CC.OBRIGARATEIO, R.CODCUS, R.DESCRI AS CUSTO ,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL1,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL2,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL3,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL4,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL5,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL6,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL7,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL8,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL9,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL10,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL11,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL12
         FROM PAGCLA P
         LEFT JOIN CUSTO_CLASSIFICACAO CC ON P.CODCLAP = CC.CODCLAP
         LEFT JOIN RODCUS R ON CC.CODCUS = R.CODCUS
         LEFT JOIN JUSTIFICATIVA J ON P.CODCLAP = J.ANALIT AND J.CODCGA = ".$CODCGA." AND J.CODUNN = ".$CODUNN." AND  J.ANO =  ".$ano."
         WHERE P.CODGRU = ".$codgru." AND P.SITUAC = 'A'  ORDER BY P.PAICLAP ASC";
        */

         $sqlAnalit = "SELECT P.*,J.TEXTO, CC.OBRIGARATEIO, R.CODCUS, R.DESCRI AS CUSTO ,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01' AND P.CODCLAP = C.ANALIT),0)  AS VAL1,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02' AND P.CODCLAP = C.ANALIT),0) AS VAL2,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03' AND P.CODCLAP = C.ANALIT),0) AS VAL3,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04' AND P.CODCLAP = C.ANALIT),0) AS VAL4,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05' AND P.CODCLAP = C.ANALIT),0) AS VAL5,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06' AND P.CODCLAP = C.ANALIT),0) AS VAL6,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07' AND P.CODCLAP = C.ANALIT),0) AS VAL7,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08' AND P.CODCLAP = C.ANALIT),0) AS VAL8,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09' AND P.CODCLAP = C.ANALIT),0) AS VAL9,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10' AND P.CODCLAP = C.ANALIT),0) AS VAL10,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11' AND P.CODCLAP = C.ANALIT),0) AS VAL11,
         IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12' AND P.CODCLAP = C.ANALIT),0)+IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_RAT C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12' AND P.CODCLAP = C.ANALIT),0) AS VAL12
         FROM PAGCLA P
         LEFT JOIN CUSTO_CLASSIFICACAO CC ON P.CODCLAP = CC.CODCLAP
         LEFT JOIN RODCUS R ON CC.CODCUS = R.CODCUS
         LEFT JOIN JUSTIFICATIVA J ON P.CODCLAP = J.ANALIT AND J.CODCGA = ".$CODCGA." AND J.CODUNN = ".$CODUNN." AND  J.ANO =  ".$ano."
         WHERE P.CODGRU = ".$codgru." AND P.SITUAC = 'A'  ORDER BY P.PAICLAP ASC";

        //$sqlAnalit = "SELECT PC.*,CUS.CODCUS,CUS.DESCRI AS CUSTO FROM PAGCLA PC INNER JOIN RODCUS CUS ON CUS.CODGRU = PC.CODGRU WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' AND CUS.SITUAC = 'A' ORDER BY PC.PAICLAP ASC";
        //dump($codgru);
        $pacoteAnaliticas = DB::select($sqlAnalit);



        $pacoteSinteticas = DB::select($sqlSintet);
        $pacoteSinteticasSoma = DB::select($sqlSintetSoma);
        //dump($pacoteSinteticasSoma);
        //dump($pacoteSinteticas,$pacoteAnaliticas,$sqlAnalit);
        //dd($sqlAnalit,$pacoteAnaliticas);
        //dump($pacoteSinteticas,$pacoteSinteticasSoma);
       // dd($pacoteAnaliticas);
        //$pacoteAnaliticas = DB::table('PAGCLA')->select('*')->where('CODGRU','=',$codgru)->where('SITUAC','=','A')->orderBy('PAICLAP','ASC')->get();
        //dd($sqlAnalit,$pacoteSinteticas,$pacoteAnaliticas);
        //$mesEbitda
        View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li>Pacote</li>' );
        return view('previsaoorcamentaria.pacote',['permissaoeditar'=>$permissaoeditar,'ebitda'=>$ebitda,'pacoteSinteticasSoma'=>$pacoteSinteticasSoma,'analitBoqueia'=>$analitBoqueia,'pacoteSinteticas'=>$pacoteSinteticas,'pacoteAnaliticas'=>$pacoteAnaliticas,'pacote'=>$pacote->DESCRI,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);
    }

    public function variaveis(Request $request){

        $arrayMes = $this->arrayMes;

        $codgru = $request->codgru;
        $CODCGA = $request->CODCGA;
        $CODUNN = $request->CODUNN;

        $funcoes = DB::table('VARIAVEIS_ITENS')->select('*')->where(['TIPO'=>'F','CODUNN'=>$CODUNN])->orderBy('ORDEMFUNC')->get();
        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;

        if(isset($_REQUEST['salvar'])){
            $v = $request->all();

            //dd($v);
            DB::table('VARIAVEIS_CAD')->where(['ANO' => $ano, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN])->delete();

            $items = array();
            //$t = 0;

            foreach ($v as $key => $value){
             /*
             echo "<pre>";
             var_dump($key,$value);
             echo "</pre>";
             */
                $r = explode('_',$key);
                if(array_key_exists($r[0],$arrayMes)){

                    $mes = $arrayMes[$r[0]];
                    $item = $r[1];
                    $valor = str_replace(',','.', str_replace('.','', $value));

                    $items[] = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$item,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];

                }




            }
            DB::table('VARIAVEIS_CAD')->insert($items);

            if(count($funcoes)>0){
                foreach ($arrayMes as $key => $value){
                    $mes = $value;
                    $this->aplicaFuncao($funcoes,$ano,$mes,$CODCGA,$CODUNN);
                }
            }






            /*
             echo "<pre>";
             var_dump($valor);
             echo "</pre>";
             */
        }
        //   dd($request);



        $unidadeFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
        ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
        ->first();

        $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
/*
        $sqlSintet = "SELECT P.ID,P.DESCRI FROM VARIAVEIS_ITENS P WHERE P.ID IN(SELECT PC.IDPAI FROM VARIAVEIS_ITENS PC WHERE PC.CODUNN = ".$CODUNN." GROUP BY PC.IDPAI);";
        $sqlAnalit = "SELECT * FROM VARIAVEIS_ITENS I
                      LEFT JOIN  VARIAVEIS_CAD C ON  I.ID = C.ITEN
                      WHERE I.CODUNN = ".$CODUNN." AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO = ".$ano." ORDER BY I.ORDEM ASC;";
*/


/*
$sqlSintet = "SELECT P.ID,P.DESCRI FROM VARIAVEIS_ITENS P WHERE P.ID IN(SELECT PC.IDPAI FROM VARIAVEIS_ITENS PC GROUP BY PC.IDPAI);";
$sqlAnalit = "SELECT * FROM VARIAVEIS_ITENS I
              LEFT JOIN  VARIAVEIS_CAD C ON  I.ID = C.ITEN
              WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO = ".$ano." ORDER BY I.ORDEM ASC;";

dd($sqlSintet,$sqlAnalit);
*/
        //$sqlAnalit = "SELECT PC.*,CUS.CODCUS,CUS.DESCRI AS CUSTO FROM PAGCLA PC INNER JOIN RODCUS CUS ON CUS.CODGRU = PC.CODGRU WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' AND CUS.SITUAC = 'A' ORDER BY PC.PAICLAP ASC";
        $sqlAnalit = "SELECT I.* ,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and I.ID = C.ITEN),'0.00') AS VAL1,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and I.ID = C.ITEN),'0.00') AS VAL2,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and I.ID = C.ITEN),'0.00') AS VAL3,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and I.ID = C.ITEN),'0.00') AS VAL4,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and I.ID = C.ITEN),'0.00') AS VAL5,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and I.ID = C.ITEN),'0.00') AS VAL6,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and I.ID = C.ITEN),'0.00') AS VAL7,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and I.ID = C.ITEN),'0.00') AS VAL8,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and I.ID = C.ITEN),'0.00') AS VAL9,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and I.ID = C.ITEN),'0.00') AS VAL10,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and I.ID = C.ITEN),'0.00') AS VAL11,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and I.ID = C.ITEN),'0.00') AS VAL12
         FROM VARIAVEIS_ITENS I ORDER BY I.ORDEM ASC";
//echo $sqlAnalit;
//dd();
        $sqlv = "SELECT * FROM UND_VARIAVEIS WHERE SITUAC = 'A' ORDER BY ID ASC";

        $pacoteAnaliticas = DB::select($sqlAnalit);
        $pacoteSinteticas = DB::select($sqlv);

        //$pacoteAnaliticas = DB::table('PAGCLA')->select('*')->where('CODGRU','=',$codgru)->where('SITUAC','=','A')->orderBy('PAICLAP','ASC')->get();
       // dd($pacoteSinteticas,$pacoteAnaliticas);
       // View::share ( 'subMenu', '<li>Centro de Gasto</li><li>Pacote</li><li>Metas</li>' );
        View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li>Pacote</li>' );
        return view('previsaoorcamentaria.variaveis',['pacoteSinteticas'=>$pacoteSinteticas,'pacoteAnaliticas'=>$pacoteAnaliticas,'pacote'=>$pacote->DESCRI,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);
    }



    public function calculaAnalitica($ANALITICA,$CODCGA,$CODUNN,$ANO){

        //42	COFINS ( soma )
        $cg = DB::table('PAGCLA')->select('CODGRU','PAICLAP')->where('CODCLAP','=',$ANALITICA)->first();
        $codgru = $cg->CODGRU;
        $SINTET = $cg->PAICLAP;
        $itemsAnalit = array();

        // carrega tabela de impostos
        $impostos = DB::select("SELECT * FROM IMPOSTOS WHERE filial = ".$CODCGA." AND ANO = ".$ANO);
        //dd(count($impostos));

        if(count($impostos) > 0){
            /*
            $confins = $impostos[0]->confis/100;
            $pis = $impostos[0]->pis/100;
            $iss = $impostos[0]->indiceiss/100;
            $icms = $impostos[0]->icms/100;
            */
            $confins = $impostos[0]->confis;
            $pis = $impostos[0]->pis;
            $iss = $impostos[0]->indiceiss;
            $icms = $impostos[0]->icms;

        }else{
            $confins = 0;
            $pis = 0;
            $iss = 0;
            $icms = 0;
        }


        if($ANALITICA == 155){ // RECEBIMENTOS

            foreach ($this->arrayMes as $key => $value){
                $mes = $value;
                $MESANO = $mes."/".$ANO;
                $r = DB::select("SELECT SUM(VALOR) as total FROM RECEITA_ROTA_CAD WHERE CODCGA = ".$CODCGA." AND CODUNN = ".$CODUNN." AND ANO = ".$ANO." AND MES = '".$mes."' AND ITEN IN(1,4,7,12,13,21,22,23,42,45,48,57,58,60,64,68,69,70,71,72,125,126,104,79)");
                $s = DB::select("SELECT VALOR as icms FROM RECEITA_ROTA_CAD WHERE  CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ITEN IN(15,54,118)");
                //$aliq = DB::select("SELECT VALOR as aliquota FROM RECEITA_ROTA_CAD WHERE CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ITEN = 75");
                if(count($r)>0){
                    $valorTot = $r[0]->total;
                }else{
                    $valorTot = 0;
                }
                if(count($s)>0){
                    $valorIcms = $s[0]->icms/100;
                }else{
                    $valorIcms = 0;
                }

                if($valorIcms == 0){
                    $valor = $valorTot;
                }else{
                    $valor = ($valorTot * $valorIcms);
                }

                //dump($valor,$valorTot,$valorIcms);

                $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
                $itemsAnalitValue = ['VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                DB::table('ORCLAN_FOR')->updateOrInsert(
                    $itemsAnalitBusca,
                    $itemsAnalitValue
                    );
            }


        }elseif($ANALITICA == 164){ // RECEBIMENTOS

            foreach ($this->arrayMes as $key => $value){
                $mes = $value;
                $MESANO = $mes."/".$ANO;
                $r = DB::select("SELECT SUM(VALOR) as total FROM RECEITA_ROTA_CAD WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ITEN IN(1,4,7,12,13,21,22,23,125,126)");
                $s = DB::select("SELECT VALOR as iss FROM RECEITA_ROTA_CAD WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ITEN IN(14,53)");

                if(count($r)>0){
                    $valorTot = $r[0]->total;
                }else{
                    $valorTot = 0;
                }
                if(count($s)>0){
                    $valorIss = $s[0]->iss/100;
                }else{
                    $valorIss  = 0;
                }

                if($valorIss  == 0){
                    $valor = $valorTot;
                }else{
                    $valor = ($valorTot * $valorIss);//*$iss;
                }

                $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
                $itemsAnalitValue = ['VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                DB::table('ORCLAN_FOR')->updateOrInsert(
                    $itemsAnalitBusca,
                    $itemsAnalitValue
                    );
            }
        }elseif($ANALITICA == 42){ // TRIBUTOS // COFINS


                foreach ($this->arrayMes as $key => $value){
                    $mes = $value;
                    $MESANO = $mes."/".$ANO;
                    $r = DB::select("SELECT SUM(VALORFOR) as total FROM ORCLAN_FOR WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ANALIT IN(155,164,165,218)");
                    if(count($r)>0){
                    $valor = $r[0]->total;
                    }else{
                    $valor = 0;
                    }

                    $valor = $valor * $confins;

                    $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
                    $itemsAnalitValue = ['VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                    DB::table('ORCLAN_FOR')->updateOrInsert(
                        $itemsAnalitBusca,
                        $itemsAnalitValue
                    );
                }

        }elseif($ANALITICA == 44 ){ // TRIBUTOS // ICMS

            $unidadesTrechos = array(0=>21,1=>23,2=>22,3=>26); // UNIDADES COM CALCULO DE TRECHO

            if(in_array($CODUNN, $unidadesTrechos)){


                foreach ($this->arrayMes as $key => $value){
                    $mes = $value;
                    $MESANO = $mes."/".$ANO;

                    $r = DB::select("SELECT SUM(VALOR) as total FROM RECEITA_ROTA_CAD WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ITEN IN(131,132,133,134,137,138,139,140,141,142,143,144,161,162,163,164)");
                    //$s = DB::select("SELECT VALOR as iss FROM RECEITA_ROTA_CAD WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ITEN IN(14,53)");

                    if(count($r)>0){
                        $valorTot = $r[0]->total;
                    }else{
                        $valorTot = 0;
                    }
                    $valor = $valorTot;


                    $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
                    $itemsAnalitValue = ['VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                    DB::table('ORCLAN_FOR')->updateOrInsert(
                        $itemsAnalitBusca,
                        $itemsAnalitValue
                        );
                }


            }else{
                foreach ($this->arrayMes as $key => $value){
                    $mes = $value;
                    $MESANO = $mes."/".$ANO;
                    $r = DB::select("SELECT SUM(VALORFOR) as total FROM ORCLAN_FOR WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ANALIT IN(155)");

                    if(count($r)>0){
                    $valorx = $r[0]->total;
                    }else{
                    $valorx = 0;
                    }

                    $valor = $valorx * $icms;
                    //dump($valorx,$icms,$valor);

                    $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
                    $itemsAnalitValue = ['VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                    DB::table('ORCLAN_FOR')->updateOrInsert(
                        $itemsAnalitBusca,
                        $itemsAnalitValue
                    );
                }

            }
        }elseif($ANALITICA == 46){ // TRIBUTOS // I.R.P.J

            foreach ($this->arrayMes as $key => $value){
                $mes = $value;
                $MESANO = $mes."/".$ANO;
                $r = DB::select("SELECT SUM(VALORFOR) as total FROM ORCLAN_FOR WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ANALIT IN(155,164,165,218)");

                if(count($r)>0){
                $valor = $r[0]->total;
                }else{
                $valor = 0;
                }

                $valor1 = $valor * 0.012;
                $valor2 = $valor * 0.08;
                $val2 = 0;

                if($valor2 < 20000){
                    $val2 = 0;
                }else{
                    $val2 = ($valor2 - 20000) * 0.10;
                }

                $valorfor = $valor1 + $val2;

                //dd($valor1,$valor2,$val2,$valorfor);

                //$valor = $val2;

                $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
                $itemsAnalitValue = ['VALORFOR' =>  $valorfor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                DB::table('ORCLAN_FOR')->updateOrInsert(
                    $itemsAnalitBusca,
                    $itemsAnalitValue
                );
            }
        }elseif($ANALITICA == 43){ // TRIBUTOS // CSLL

            foreach ($this->arrayMes as $key => $value){
                $mes = $value;
                $MESANO = $mes."/".$ANO;
                $r = DB::select("SELECT SUM(VALORFOR) as total FROM ORCLAN_FOR WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ANALIT IN(155,164,165,218)");

                if(count($r)>0){
                $valor = $r[0]->total;
                }else{
                $valor = 0;
                }

                $valorfor = $valor * 0.0108;

                $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
                $itemsAnalitValue = ['VALORFOR' =>  $valorfor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                DB::table('ORCLAN_FOR')->updateOrInsert(
                    $itemsAnalitBusca,
                    $itemsAnalitValue
                );
            }
        }elseif($ANALITICA == 637){ // TRIBUTOS // CSLL

            foreach ($this->arrayMes as $key => $value){
                $mes = $value;
                $MESANO = $mes."/".$ANO;
                $r = DB::select("SELECT SUM(VALORFOR) as total FROM ORCLAN_FOR WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ANALIT IN(155,164,165,218)");

                if(count($r)>0){
                $valor = $r[0]->total;
                }else{
                $valor = 0;
                }

                $valorfor = $valor * 0.015;

                $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
                $itemsAnalitValue = ['VALORFOR' =>  $valorfor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                DB::table('ORCLAN_FOR')->updateOrInsert(
                    $itemsAnalitBusca,
                    $itemsAnalitValue
                );
            }

        }elseif($ANALITICA == 47){ // TRIBUTOS // ISS

            foreach ($this->arrayMes as $key => $value){
                $mes = $value;
                $MESANO = $mes."/".$ANO;
                $r = DB::select("SELECT SUM(VALORFOR) as total FROM ORCLAN_FOR WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ANALIT IN(164)");

                if(count($r)>0){
                $valor = $r[0]->total;
                }else{
                $valor = 0;
                }

                $valor = $valor * $iss;

                $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
                $itemsAnalitValue = ['VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                DB::table('ORCLAN_FOR')->updateOrInsert(
                    $itemsAnalitBusca,
                    $itemsAnalitValue
                );
            }

        }elseif($ANALITICA == 48){ // TRIBUTOS // PIS

            foreach ($this->arrayMes as $key => $value){
                $mes = $value;
                $MESANO = $mes."/".$ANO;
                $r = DB::select("SELECT SUM(VALORFOR) as total FROM ORCLAN_FOR WHERE CODUNN = ".$CODUNN." AND CODCGA = ".$CODCGA." AND ANO = ".$ANO." AND MES = '".$mes."' AND ANALIT IN(155,164,165,218)");

                if(count($r)>0){
                $valor = $r[0]->total;
                }else{
                $valor = 0;
                }

                $valor = $valor * $pis;

                $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
                $itemsAnalitValue = ['VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                DB::table('ORCLAN_FOR')->updateOrInsert(
                    $itemsAnalitBusca,
                    $itemsAnalitValue
                );
            }

    }

    }

    public function printCombination( $arr, $n, $r)
    {
        // A temporary array to store all
        // combination one by one
        $data = array();

        // Print all combination using
        // temprary array 'data[]'
        $this->combinationUtil($arr, $n, $r, 0, $data, 0);
    }

    /* arr[] ---> Input Array
    n ---> Size of input array
    r ---> Size of a combination to be printed
    index ---> Current index in data[]
    data[] ---> Temporary array to store
    current combination
    i ---> index of current element in arr[] */
    public function combinationUtil( $arr, $n, $r, $index,
                        $data, $i)
    {
        // Current cobination is ready, print it
        if ($index == $r) {
            for ( $j = 0; $j < $r; $j++)
                echo $data[$j]," ";
            echo "<BR/>";
            $this->cont++;
            return;
        }

        // When no more elements are there to
        // put in data[]
        if ($i >= $n)
            return;

        // current is included, put next at
        // next location
        $data[$index] = $arr[$i];
        $this->combinationUtil($arr, $n, $r, $index + 1,
                                  $data, $i + 1);

        // current is excluded, replace it with
        // next (Note that i+1 is passed, but
        // index is not changed)
        $this->combinationUtil($arr, $n, $r, $index,
                                $data, $i + 1);
    }

    public function montaSete($arrayRemover){

        $array25 = array('01', '02', '03', '04', '05','06', '07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25');

        $diffVal = array_diff($array25, $arrayRemover);
        array_unshift($diffVal, "00");

        $n = array_values($diffVal);

        $k[1]  = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[13],$n[16],$n[17],$n[18]);
        $k[2]  = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[11],$n[12],$n[14],$n[15],$n[16],$n[17],$n[18]);
        $k[3]  = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[9 ],$n[10],$n[11],$n[12],$n[14],$n[15],$n[16],$n[18]);
        $k[4]  = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[10],$n[12],$n[13],$n[14],$n[15],$n[16],$n[17],$n[18]);
        $k[5]  = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[5 ],$n[6 ],$n[8 ],$n[9 ],$n[12],$n[13],$n[14],$n[15],$n[16],$n[17],$n[18]);
        $k[6]  = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[5 ],$n[6 ],$n[8 ],$n[10],$n[11],$n[12],$n[13],$n[14],$n[15],$n[16],$n[18]);
        $k[7]  = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[5 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[12],$n[13],$n[15],$n[16],$n[17]);
        $k[8]  = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[5 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[13],$n[14],$n[15],$n[17],$n[18]);
        $k[9]  = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[12],$n[13],$n[14],$n[15],$n[16],$n[18]);
        $k[10] = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[6 ],$n[7 ],$n[9 ],$n[11],$n[12],$n[13],$n[14],$n[15],$n[16],$n[17],$n[18]);
        $k[11] = array($n[1 ],$n[2 ],$n[3 ],$n[4 ],$n[6 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[12],$n[14],$n[15],$n[16],$n[17],$n[18]);
        $k[12] = array($n[1 ],$n[2 ],$n[3 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[12],$n[14],$n[15],$n[16],$n[17],$n[18]);
        $k[13] = array($n[1 ],$n[2 ],$n[3 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[11],$n[12],$n[13],$n[14],$n[15],$n[16],$n[18]);
        $k[14] = array($n[1 ],$n[2 ],$n[3 ],$n[5 ],$n[6 ],$n[9 ],$n[10],$n[11],$n[12],$n[13],$n[14],$n[15],$n[16],$n[17],$n[18]);
        $k[15] = array($n[1 ],$n[2 ],$n[3 ],$n[6 ],$n[7 ],$n[8 ],$n[10],$n[11],$n[12],$n[13],$n[14],$n[15],$n[16],$n[17],$n[18]);
        $k[16] = array($n[1 ],$n[2 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[12],$n[13],$n[14],$n[17],$n[18]);
        $k[17] = array($n[1 ],$n[2 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[13],$n[14],$n[15],$n[16],$n[17]);
        $k[18] = array($n[1 ],$n[3 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[12],$n[13],$n[14],$n[15],$n[17]);
        $k[19] = array($n[1 ],$n[3 ],$n[4 ],$n[5 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[12],$n[13],$n[14],$n[16],$n[17],$n[18]);
        $k[20] = array($n[1 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[12],$n[13],$n[15],$n[16],$n[17],$n[18]);
        $k[21] = array($n[2 ],$n[3 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[12],$n[13],$n[14],$n[16],$n[17]);
        $k[22] = array($n[2 ],$n[3 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[12],$n[13],$n[15],$n[17],$n[18]);
        $k[23] = array($n[2 ],$n[4 ],$n[5 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[12],$n[13],$n[14],$n[15],$n[16],$n[17],$n[18]);
        $k[24] = array($n[3 ],$n[4 ],$n[5 ],$n[6 ],$n[7 ],$n[8 ],$n[9 ],$n[10],$n[11],$n[13],$n[14],$n[15],$n[16],$n[17],$n[18]);

        return $k;

    //   $ar = array('19','20','21','22','23','24','25');
    //    $ar = array('07','13','15','16','19','20','24');
    //   dd($this->montaSete($ar));

    }

    public function trechoAux($CODCGA,$CODUNN,$ano){
            /// PEGA VALORES PARA ADICIONAR NA CLASSIFICAÇÃO

            //$itemVal = array();
            foreach ($this->arrayMes as $key => $value){
                $mes = $value;
                //$valor = 0;

                $where1 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 1];
                $where2 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 2];
                $where3 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 3];
                $where4 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 4];

                $valor_item_1 = DB::table('TRECHOS_CAD')->select('VALOR')->where($where1)->get();
                $valor_item_2 = DB::table('TRECHOS_CAD')->select('VALOR')->where($where2)->get();
                $valor_item_3 = DB::table('TRECHOS_CAD')->select('VALOR')->where($where3)->get();
                $valor_item_4 = DB::table('TRECHOS_CAD')->select('VALOR')->where($where4)->get();

                //dd($valor_item_1,$valor_item_2);

                $valorReceitaRota = 0;
                $valorReceitaRotaImposto = 0;
                $total = count($valor_item_1);
                $valorTotalRemunerado = 0;

                for ($i = 0; $i < $total; $i++) {
                    // dd($i);
                    $valorReceitaRota = $valorReceitaRota + ($valor_item_1[$i]->VALOR * $valor_item_2[$i]->VALOR);
                    $valorReceitaRotaImposto = $valorReceitaRotaImposto + ($valor_item_1[$i]->VALOR * $valor_item_2[$i]->VALOR * ($valor_item_3[$i]->VALOR/100));
                    $valorTotalRemunerado = $valorTotalRemunerado + ($valor_item_1[$i]->VALOR * $valor_item_4[$i]->VALOR);
                }

                if($CODUNN == 21){
                    $item = 72;
                    $item2 = 128;
                }elseif($CODUNN == 23){
                    $item = 79;
                    $item2 = 81;
                }elseif($CODUNN == 22){
                    $item = 104;
                    $item2 = 129;
                }

                $unidadesTrechos = array(0=>21,1=>23,2=>22);

                    if(in_array($CODUNN, $unidadesTrechos)){

                        $itemsAnalitBusca = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEN'=> $item];
                        $itemsAnalitValue = ['VALOR' =>  $valorReceitaRota, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                        DB::table('RECEITA_ROTA_CAD')->updateOrInsert(
                            $itemsAnalitBusca,
                            $itemsAnalitValue
                            );

                        $itemsAnalitBusca2 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEN'=> $item2];
                        $itemsAnalitValue2 = ['VALOR' =>  $valorTotalRemunerado, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

                        DB::table('RECEITA_ROTA_CAD')->updateOrInsert(
                            $itemsAnalitBusca2,
                            $itemsAnalitValue2
                            );
                    }
            }
    }


    public function orcaAux($CODCGA,$CODUNN,$ano){

        $itemsAnalitBusca = ['ANO' => $ANO,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALITICA,'SINTET' => $SINTET];
        $itemsAnalitValue = ['VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];

        DB::table('ORCLAN_FOR')->updateOrInsert(
            $itemsAnalitBusca,
            $itemsAnalitValue
            );

    }

    public function simuladorqlp(Request $request){

        //inicializa array de dados do simulador
        for($x = 2; $x <=58;$x++){
            for($y = 1; $y <=47;$y++){

                $this->arrayQlp[$x][$y] = 0.00;

            }
        }

        $arrayMes = $this->arrayMes;

        if($request->codgru && $request->CODCGA && $request->CODUNN){
            session(['codgru' => $request->codgru]);
            session(['CODCGA' => $request->CODCGA]);
            session(['CODUNN' => $request->CODUNN]);
        }
            $codgru = session('codgru');
            $CODCGA = session('CODCGA');
            $CODUNN = session('CODUNN');

        //dd($codgru,$CODCGA,$CODUNN);

        $idTrechoInsert = Carbon::now()->format('YmdHisu');
        //dd($idTrechoInsert);

        $unidadeFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->where('MENUFILIAL.CODCGA', '=', $CODCGA)
        ->where('MENUFILIAL.CODUNN', '=', $CODUNN)
        ->first();

        $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;
        $items = array();

        if($request->addtrecho == 'ok'){
            //dd($request);
            $items = array();
            foreach ($this->arrayMes as $key => $value){
                $mes = $value;
                $valor = 0;
                $items[] = ['TRECHO_ID'=>$idTrechoInsert,'ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEM'=>1,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];
                $items[] = ['TRECHO_ID'=>$idTrechoInsert,'ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEM'=>2,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];
                $items[] = ['TRECHO_ID'=>$idTrechoInsert,'ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEM'=>3,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];
                $items[] = ['TRECHO_ID'=>$idTrechoInsert,'ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEM'=>4,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];

         }

            DB::table('TRECHOS_CAD')->insert($items);
        }


        if($request->salvar == 'ok'){
            //dd($request);
            $v = $request->all();
            foreach ($v as $key => $value){

                $r = explode('_',$key);
                //dd($r);
                $t = count($r);

                if($t > 2 ){

                    $mes = $r[1];
                    $item = $r[2];
                    $valor = str_replace(',','.', str_replace('.','', $value));
                    $trecho_id = $r[3];

                    $items = ['VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'updated_at' => $this->updated_at ];
                    $itemsWhere = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEM'=>$item,'TRECHO_ID'=>$trecho_id];

                    DB::table('TRECHOS_CAD')->where($itemsWhere)->update($items);

                }
            }

            $this->trechoAux($CODCGA,$CODUNN,$ano);
        }

        $sqlItens = "SELECT * FROM QLP_ITENS ORDER BY ID";
        $sqlTipos = "SELECT * FROM QLP_TIPOS WHERE CODUNN =".$CODUNN."  ORDER BY ID";



        $listaQlpItens = DB::select($sqlItens);
        $listaQlpTipos = DB::select($sqlTipos);
        $mes = '1';

        foreach($listaQlpTipos as $tipos){
            foreach($listaQlpItens as $itens){
                $this->calculosQLP($tipos->ID,$itens->ID,$CODCGA,$CODUNN,$ano,$mes);
            }
        }

/*
        $sqltrechoid = "SELECT DISTINCT TC.TRECHO_ID,TC.ITEM,TI.DESCRI,TI.TIPO,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEM = TC.ITEM),'0.00') AS VAL1,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEM = TC.ITEM),'0.00') AS VAL2,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEM = TC.ITEM),'0.00') AS VAL3,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEM = TC.ITEM),'0.00') AS VAL4,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEM = TC.ITEM),'0.00') AS VAL5,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEM = TC.ITEM),'0.00') AS VAL6,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEM = TC.ITEM),'0.00') AS VAL7,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEM = TC.ITEM),'0.00') AS VAL8,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEM = TC.ITEM),'0.00') AS VAL9,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEM = TC.ITEM),'0.00') AS VAL10,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEM = TC.ITEM),'0.00') AS VAL11,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEM = TC.ITEM),'0.00') AS VAL12
        FROM TRECHOS_CAD TC
        INNER JOIN TRECHO_ITENS TI ON TI.ID = TC.ITEM
        WHERE TC.ANO = ".$ano." AND TC.CODCGA =".$CODCGA." AND TC.CODUNN = ".$CODUNN." ORDER BY TC.TRECHO_ID,TC.ITEM";

        $listaTrecho = DB::select($sqltrechoid);
*/

      View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li>Simulador QLP</li>' );
      return view('previsaoorcamentaria.simuladorqlp',['arrayQlp'=>$this->arrayQlp,'listaQlpItens'=>$listaQlpItens,'listaQlpTipos'=>$listaQlpTipos,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'pacote'=>$pacote->DESCRI,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);


    }

    public function valorItemReceita($item,$CODCGA,$CODUNN,$ano,$mes){

        $sql = "SELECT VALOR FROM RECEITA_ROTA_CAD WHERE ITEN = ".$item." AND CODCGA = ".$CODCGA." AND CODUNN = ".$CODUNN." AND MES = ".$mes." AND ANO =".$ano;
        $r = DB::select($sql);
        if(count($r)){
            return $r[0]->VALOR;
        }else{
            return 0;
        }

    }

    public function valorItemVariavel($item,$CODCGA,$CODUNN,$ano,$mes){
        $sql = "SELECT VALOR FROM VARIAVEIS_CAD WHERE ITEN = ".$item." AND CODCGA = ".$CODCGA." AND CODUNN = ".$CODUNN." AND MES = ".$mes." AND ANO =".$ano;
        $r = DB::select($sql);
        if(count($r)){
            return $r[0]->VALOR;
        }else{
            return 0;
        }
    }



    public function calculosQLP($tipo,$item,$CODCGA,$CODUNN,$ano,$mes){

        // QUANTIDADE
        if($tipo == 2 && $item == 1){ // MOTORISTA - EQUIPE ATIVA

            $idReceita1 = 2; //
            $this->arrayQlp[$tipo][$item] = $this->valorItemReceita($idReceita1,$CODCGA,$CODUNN,$ano,$mes);
        }

        if($tipo == 3 && $item == 1){ // MOTORISTA - FERISTAS

            $idVariavel1 = 62; // % META TURN OVER - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[2][1] * (1 - ($m/100))/12, 2);
        }

        if($tipo == 4 && $item == 1){ // MOTORISTA - RESERVAS

            $idVariavel1 = 64; // % ABSENTEÍSMO - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[2][1] * ($m/100), 2);
        }

        if($tipo == 5 && $item == 1){ // AJUDANTE - EQUIPE ATIVA

            $idVariavel1 = 35; // FATOR AJUDANTE POR CAMINHÃO - DISTRIBUIÇÃO URBANA
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[2][1] * $m, 2);
        }

        if($tipo == 6 && $item == 1){ // AJUDANTE - FERISTAS

            $idVariavel1 = 62; // % META TURN OVER - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[5][1] * (1 - ($m/100))/12, 2);
        }

        if($tipo == 7 && $item == 1){ // AJUDANTE - RESERVAS

            $idVariavel1 = 64; // % ABSENTEÍSMO - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[5][1] * ($m/100), 2);
        }

        if($tipo == 8 && $item == 1){ // MOTORISTA - EQUIPE ATIVA - NOTURNA

            $idReceita1 = 20; //QUANTIDADE - EQUIPE NOTUNA

            $this->arrayQlp[$tipo][$item] = $this->valorItemReceita($idReceita1,$CODCGA,$CODUNN,$ano,$mes);
        }

        if($tipo == 9 && $item == 1){ // MOTORISTA - EQUIPE FERISTA - NOTURNA

            $idVariavel1 = 62; // % META TURN OVER - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[8][1] * (1 - ($m/100))/12, 2);
        }
        if($tipo == 10 && $item == 1){ // MOTORISTA - EQUIPE RESERVA - NOTURNA

            $idVariavel1 = 64; // % ABSENTEÍSMO - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[8][1] * ($m/100), 2);
        }

        if($tipo == 11 && $item == 1){ // AJUDANTE - EQUIPE ATIVA - NOTURNA

            $idReceita1 = 20; //QUANTIDADE - EQUIPE NOTUNA

            $this->arrayQlp[$tipo][$item] = $this->valorItemReceita($idReceita1,$CODCGA,$CODUNN,$ano,$mes);
        }
        if($tipo == 12 && $item == 1){ // AJUDANTE - EQUIPE FERISTA - NOTURNA

            $idVariavel1 = 62; // % META TURN OVER - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[10][1] * (1 - ($m/100))/12, 2);
        }

        if($tipo == 13 && $item == 1){ // AJUDANTE - EQUIPE RESERVA - NOTURNA

            $idVariavel1 = 64; // % ABSENTEÍSMO - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[10][1] * ($m/100), 2);
        }

        if($tipo == 39 && $item == 1){ // MOTORISTA - ELD - FAD

            $idReceita1 = 16; //QUANTIDADE MOTORISTA - ELD - FAD;
            $this->arrayQlp[$tipo][$item] = $this->valorItemReceita($idReceita1,$CODCGA,$CODUNN,$ano,$mes);
        }

        if($tipo == 40 && $item == 1){ // MOTORISTA - 12X36 - FAD

            $idReceita1 = 18; // QUANTIDADE EQUIPE - FAD - 12X 36 MOTORISTAS;
            $this->arrayQlp[$tipo][$item] = $this->valorItemReceita($idReceita1,$CODCGA,$CODUNN,$ano,$mes);
        }

        if($tipo == 41 && $item == 1){ // AJUDANTE - 12X36 - FAD

            $idReceita1 = 19; // QUANTIDADE EQUIPE - FAD - 12X 36 AJUDANTES;
            $this->arrayQlp[$tipo][$item] = $this->valorItemReceita($idReceita1,$CODCGA,$CODUNN,$ano,$mes);
        }

        if($tipo == 42 && $item == 1){ // MOTORISTA - ELD - FAD - FERISTAS
            $idVariavel1 = 62; // % META TURN OVER - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[39][1] * (1 - ($m/100))/12, 2);
        }

        if($tipo == 43 && $item == 1){ // MOTORISTA - 12X36 - FAD - FERISTAS

            $idVariavel1 = 62; // % META TURN OVER - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[40][1] * (1 - ($m/100))/12, 2);
        }

        if($tipo == 44 && $item == 1){ // AJUDANTE - 12X36 - FAD - FERISTAS

            $idVariavel1 = 62; // % META TURN OVER - OP
            $m = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $this->arrayQlp[$tipo][$item] = number_format( $this->arrayQlp[41][1] * (1 - ($m/100))/12, 2);
        }



        // SALARIO BASE 860907


        elseif($tipo == 2 && $item == 2){

            $idVariavel1 = 36;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[2][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }

        elseif($tipo == 3 && $item == 2){

            $idVariavel1 = 36;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[3][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }

        elseif($tipo == 4 && $item == 2){

            $idVariavel1 = 36;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[4][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }


        elseif($tipo == 5 && $item == 2){

            $idVariavel1 = 38;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[5][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }

        elseif($tipo == 6 && $item == 2){

            $idVariavel1 = 38;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[6][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }

        elseif($tipo == 7 && $item == 2){

            $idVariavel1 = 38;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[7][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }
        elseif($tipo == 8 && $item == 2){ //MOTORISTA - EQUIPE ATIVA - NOTURNA

            $idVariavel1 = 37;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[8][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }
        elseif($tipo == 9 && $item == 2){ //AJUDANTE - EQUIPE ATIVA - NOTURNA

            $idVariavel1 = 39;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[9][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }
        elseif($tipo == 10 && $item == 2){ //AJUDANTE - EQUIPE FERISTA - NOTURNA

            $idVariavel1 = 39;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[10][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }
        elseif($tipo == 11 && $item == 2){ //MOTORISTA - EQUIPE RESERVA - NOTURNA

            $idVariavel1 = 39;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[11][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }

        elseif($tipo == 39 && $item == 2){ //MOTORISTA - ELD - FAD

            $idVariavel1 = 41;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[39][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }
        elseif($tipo == 40 && $item == 2){ //MOTORISTA - 12X36 - FAD

            $idVariavel1 = 40;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[40][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }
        elseif($tipo == 41 && $item == 2){ //AJUDANTE - 12X36 - FAD

            $idVariavel1 = 48;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[41][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }
        elseif($tipo == 42 && $item == 2){ //MOTORISTA - ELD - FAD - FERISTAS

            $idVariavel1 = 40;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[42][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }
        elseif($tipo == 43 && $item == 2){ //MOTORISTA - 12X36 - FAD - FERISTAS

            $idVariavel1 = 40;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[43][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }
        elseif($tipo == 44 && $item == 2){ //AJUDANTE - 12X36 - FAD

            $idVariavel1 = 48;
            $idVariavel2 = 58;

            $v1 = $this->valorItemVariavel($idVariavel1,$CODCGA,$CODUNN,$ano,$mes);
            $v2 = $this->valorItemVariavel($idVariavel2,$CODCGA,$CODUNN,$ano,$mes);

            if($this->arrayQlp[44][1] > 0){
                $t = $v1 * ($v2/100);
            }else{
                $t = 0.00;
            }
            $this->arrayQlp[$tipo][$item] = $t;
        }


    }


    public function trechos(Request $request){

        $permissaoeditarR = DB::table('USU_ACAO')->select('EDITAR')->where('ID_USUARIO','=',Auth::user()->id)->first();

        if($permissaoeditarR == null){
            $permissaoeditar = 'N';
        }else{
            $permissaoeditar = $permissaoeditarR->EDITAR;
        }

        $arrayMes = $this->arrayMes;

        if($request->codgru && $request->CODCGA && $request->CODUNN){
            session(['codgru' => $request->codgru]);
            session(['CODCGA' => $request->CODCGA]);
            session(['CODUNN' => $request->CODUNN]);
        }
            $codgru = session('codgru');
            $CODCGA = session('CODCGA');
            $CODUNN = session('CODUNN');

        $idTrechoInsert = Carbon::now()->format('YmdHisu');
        //dd($idTrechoInsert);

        $unidadeFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->where('MENUFILIAL.CODCGA', '=', $CODCGA)
        ->where('MENUFILIAL.CODUNN', '=', $CODUNN)
        ->first();

        $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;
        $items = array();

        if($request->addtrecho == 'ok'){
            //dd($request);
            $items = array();
            foreach ($this->arrayMes as $key => $value){
                $mes = $value;
                $valor = 0;
                $items[] = ['TRECHO_ID'=>$idTrechoInsert,'ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEM'=>1,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];
                $items[] = ['TRECHO_ID'=>$idTrechoInsert,'ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEM'=>2,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];
                $items[] = ['TRECHO_ID'=>$idTrechoInsert,'ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEM'=>3,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];
                $items[] = ['TRECHO_ID'=>$idTrechoInsert,'ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEM'=>4,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];

         }

            DB::table('TRECHOS_CAD')->insert($items);
        }

        if($request->removertrecho == 'ok'){
           $delTrecho = DB::table('TRECHOS_CAD')->where('TRECHO_ID', '=', $request->trecho)->delete();
            //$delTrecho = DB::table('TRECHOS_CAD')->where('TRECHO_ID', '=', 9999)->delete();
            //dd($delTrecho);
            if($delTrecho > 0){
                $this->trechoAux($CODCGA,$CODUNN,$ano);
            }

        }

        if($request->salvar == 'ok'){
            //dd($request);
            $v = $request->all();
            foreach ($v as $key => $value){

                $r = explode('_',$key);
                //dd($r);
                $t = count($r);

                if($t > 2 ){

                    $mes = $r[1];
                    $item = $r[2];
                    $valor = str_replace(',','.', str_replace('.','', $value));
                    $trecho_id = $r[3];
                    $descritrecho = $request->DESCRITRECHO[$trecho_id];
                    //dd($descritrecho);

                    $items = ['DESCRITRECHO'=> $descritrecho,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'updated_at' => $this->updated_at ];
                    $itemsWhere = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEM'=>$item,'TRECHO_ID'=>$trecho_id];

                    DB::table('TRECHOS_CAD')->where($itemsWhere)->update($items);

                }
            }

            $this->trechoAux($CODCGA,$CODUNN,$ano);
        }


        $sqltrechoid = "SELECT DISTINCT TC.TRECHO_ID,TC.DESCRITRECHO,TC.ITEM,TI.DESCRI,TI.TIPO,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEM = TC.ITEM),'0.00') AS VAL1,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEM = TC.ITEM),'0.00') AS VAL2,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEM = TC.ITEM),'0.00') AS VAL3,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEM = TC.ITEM),'0.00') AS VAL4,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEM = TC.ITEM),'0.00') AS VAL5,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEM = TC.ITEM),'0.00') AS VAL6,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEM = TC.ITEM),'0.00') AS VAL7,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEM = TC.ITEM),'0.00') AS VAL8,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEM = TC.ITEM),'0.00') AS VAL9,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEM = TC.ITEM),'0.00') AS VAL10,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEM = TC.ITEM),'0.00') AS VAL11,
        IFNULL((SELECT C.VALOR FROM TRECHOS_CAD C WHERE C.TRECHO_ID=TC.TRECHO_ID AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEM = TC.ITEM),'0.00') AS VAL12
        FROM TRECHOS_CAD TC
        INNER JOIN TRECHO_ITENS TI ON TI.ID = TC.ITEM
        WHERE TC.ANO = ".$ano." AND TC.CODCGA =".$CODCGA." AND TC.CODUNN = ".$CODUNN." ORDER BY TC.TRECHO_ID,TC.ITEM";

        $listaTrecho = DB::select($sqltrechoid);


      View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li>Trechos</li>' );
      return view('previsaoorcamentaria.trechos',['permissaoeditar'=>$permissaoeditar,'listaTrecho'=>$listaTrecho,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'pacote'=>$pacote->DESCRI,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);

    }


    public function atualizaReceitaRota(){
/*
        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;

        $filiaisSql = 'SELECT CODCGA,CODUNN FROM MENUFILIAL ORDER BY CODCGA,CODUNN';
        $filiais = db::select($filiais);

        foreach($filiais as $filial){

           // PROCESSA TRECHOS

           $CODCGA = $filial->CODCGA;
           $CODUNN = $filial->CODUNN;

           $this->trechoAux($CODCGA,$CODUNN,$ano);


        }

        $filiaisSql = 'SELECT CODCGA,CODUNN FROM MENUFILIAL ORDER BY CODCGA,CODUNN';
        $filiais = db::select($filiais);

        foreach($filiais as $filial){

           // PROCESSA TRECHOS

           $CODCGA = $filial->CODCGA;
           $CODUNN = $filial->CODUNN;

           $funcoes = DB::table('RECEITA_ROTA_ITENS')->select('*')->where(['TIPO'=>'F','CODUNN'=>$CODUNN])->orderBy('ORDEMFUNC')->get();

           if(count($funcoes)>0){
               foreach ($arrayMes as $key => $value){
                   $mes = $value;
                   $this->aplicaFuncao($funcoes,$ano,$mes,$CODCGA,$CODUNN);
               }
           }

           $this->calculaAnalitica(155,$CODCGA,$CODUNN,$ano);
           $this->calculaAnalitica(164,$CODCGA,$CODUNN,$ano);

           $this->calculaAnalitica(42,$CODCGA,$CODUNN,$ano);
           $this->calculaAnalitica(44,$CODCGA,$CODUNN,$ano);
           $this->calculaAnalitica(47,$CODCGA,$CODUNN,$ano);
           $this->calculaAnalitica(48,$CODCGA,$CODUNN,$ano);
           $this->calculaAnalitica(46,$CODCGA,$CODUNN,$ano);
           $this->calculaAnalitica(43,$CODCGA,$CODUNN,$ano);
           $this->calculaAnalitica(637,$CODCGA,$CODUNN,$ano);

        }






            $epis = DB::table('EPI_SMLD_CAD')->select('*')->where(['CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'ANO'=>$ano])-get();

            if(count($epis > 0)){
                foreach($epis as $epi){
                    $this->atualizaEpi($epi->ID);
                }
            }
*/
    }

    public function receitarota(Request $request){


        $permissaoeditarR = DB::table('USU_ACAO')->select('EDITAR')->where('ID_USUARIO','=',Auth::user()->id)->first();

        if($permissaoeditarR == null){
            $permissaoeditar = 'N';
        }else{
            $permissaoeditar = $permissaoeditarR->EDITAR;
        }

        $arrayMes = $this->arrayMes;

        $codgru = $request->codgru;
        $CODCGA = $request->CODCGA;
        $CODUNN = $request->CODUNN;

        $funcoes = DB::table('RECEITA_ROTA_ITENS')->select('*')->where(['TIPO'=>'F','CODUNN'=>$CODUNN])->orderBy('ORDEMFUNC')->get();
        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;

        if(isset($_REQUEST['salvar'])){
            $v = $request->all();

            //dd($v);
            DB::table('RECEITA_ROTA_CAD')->where(['ANO' => $ano, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN])->delete();

            $items = array();
            //$t = 0;

            foreach ($v as $key => $value){

                $r = explode('_',$key);
                if(array_key_exists($r[0],$arrayMes)){

                    $mes = $arrayMes[$r[0]];
                    $item = $r[1];
                    $valor = str_replace(',','.', str_replace('.','', $value));

                    $items[] = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$item,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];

                }

            }
            DB::table('RECEITA_ROTA_CAD')->insert($items);
            //dd($funcoes);
            if(count($funcoes)>0){
                foreach ($arrayMes as $key => $value){
                    $mes = $value;
                    $this->aplicaFuncao($funcoes,$ano,$mes,$CODCGA,$CODUNN);
                }
            }

            $this->calculaAnalitica(155,$CODCGA,$CODUNN,$ano);
            $this->calculaAnalitica(164,$CODCGA,$CODUNN,$ano);

            $this->calculaAnalitica(42,$CODCGA,$CODUNN,$ano);
            $this->calculaAnalitica(44,$CODCGA,$CODUNN,$ano);
            $this->calculaAnalitica(47,$CODCGA,$CODUNN,$ano);
            $this->calculaAnalitica(48,$CODCGA,$CODUNN,$ano);
            $this->calculaAnalitica(46,$CODCGA,$CODUNN,$ano);
            $this->calculaAnalitica(43,$CODCGA,$CODUNN,$ano);
            $this->calculaAnalitica(637,$CODCGA,$CODUNN,$ano);



            /*
             echo "<pre>";
             var_dump($valor);
             echo "</pre>";
             */
        }
        //   dd($request);



        $unidadeFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
        ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
        ->first();

        $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();

        $sqlSintet = "SELECT P.ID,P.DESCRI FROM RECEITA_ROTA_ITENS P WHERE P.ID IN(SELECT PC.IDPAI FROM RECEITA_ROTA_ITENS PC WHERE PC.CODUNN = ".$CODUNN." GROUP BY PC.IDPAI);";
        $sqlAnalit = "SELECT * FROM RECEITA_ROTA_ITENS I
                      LEFT JOIN  RECEITA_ROTA_CAD C ON  I.ID = C.ITEN
                      WHERE I.CODUNN = ".$CODUNN." AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO = ".$ano." ORDER BY I.ORDEM ASC;";
        //$sqlAnalit = "SELECT PC.*,CUS.CODCUS,CUS.DESCRI AS CUSTO FROM PAGCLA PC INNER JOIN RODCUS CUS ON CUS.CODGRU = PC.CODGRU WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' AND CUS.SITUAC = 'A' ORDER BY PC.PAICLAP ASC";
        $sqlAnalit = "SELECT I.* ,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and I.ID = C.ITEN),'0.00') AS VAL1,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and I.ID = C.ITEN),'0.00') AS VAL2,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and I.ID = C.ITEN),'0.00') AS VAL3,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and I.ID = C.ITEN),'0.00') AS VAL4,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and I.ID = C.ITEN),'0.00') AS VAL5,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and I.ID = C.ITEN),'0.00') AS VAL6,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and I.ID = C.ITEN),'0.00') AS VAL7,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and I.ID = C.ITEN),'0.00') AS VAL8,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and I.ID = C.ITEN),'0.00') AS VAL9,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and I.ID = C.ITEN),'0.00') AS VAL10,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and I.ID = C.ITEN),'0.00') AS VAL11,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and I.ID = C.ITEN),'0.00') AS VAL12
        FROM RECEITA_ROTA_ITENS I WHERE I.CODUNN = ".$CODUNN." ORDER BY I.ORDEM ASC";

            $in = '0';
        if($CODUNN == 15){
            $in = '1,4,7,12,13,21,22';
        }elseif($CODUNN == 18){
            $in = '42,45,48,56,57,58,59';
        }elseif($CODUNN == 19){
            $in = '31,32,33,34,35'; // 77*24 + 25*26 + 27*28 + 29*30
        }elseif($CODUNN == 20){
            $in = '38,39,40,41,40,74'; // 36*37
        }elseif($CODUNN == 21){
            $in = '60,64,68,69,70,71,72';
        }elseif($CODUNN == 22){
            $in = '92,96,100,101,102,103,104';
        }elseif($CODUNN == 25){
            $in = '106,109,112,120,121,122,123';
        }elseif($CODUNN == 23){
            $in = '78,82,86,87,88,89,90';
        }elseif($CODUNN == 26){
            $in = '145,149,153,154,154,155,156,158';
        }elseif($CODUNN == 6){
            $in = '0';
        }

        $sqlAnalitSum = "";

        if( $CODUNN == 15 || $CODUNN == 18 || $CODUNN == 21 || $CODUNN == 22 || $CODUNN == 23 || $CODUNN == 25 || $CODUNN == 26){

            $sqlAnalitSum = "SELECT I.CODUNN ,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN IN(".$in.")),'0.00') AS VAL1,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN IN(".$in.")),'0.00') AS VAL2,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN IN(".$in.")),'0.00') AS VAL3,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN IN(".$in.")),'0.00') AS VAL4,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN IN(".$in.")),'0.00') AS VAL5,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN IN(".$in.")),'0.00') AS VAL6,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN IN(".$in.")),'0.00') AS VAL7,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN IN(".$in.")),'0.00') AS VAL8,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN IN(".$in.")),'0.00') AS VAL9,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN IN(".$in.")),'0.00') AS VAL10,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN IN(".$in.")),'0.00') AS VAL11,
            IFNULL((SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN IN(".$in.")),'0.00') AS VAL12
            FROM RECEITA_ROTA_CAD I WHERE I.CODCGA = ".$CODCGA." AND I.CODUNN = ".$CODUNN." GROUP BY I.CODUNN";

        }elseif($CODUNN == 19){

            $sqlAnalitSum = "SELECT I.CODUNN ,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN IN(".$in.")),'0.00') AS VAL1,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN IN(".$in.")),'0.00') AS VAL2,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN IN(".$in.")),'0.00') AS VAL3,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN IN(".$in.")),'0.00') AS VAL4,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN IN(".$in.")),'0.00') AS VAL5,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN IN(".$in.")),'0.00') AS VAL6,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN IN(".$in.")),'0.00') AS VAL7,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN IN(".$in.")),'0.00') AS VAL8,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN IN(".$in.")),'0.00') AS VAL9,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN IN(".$in.")),'0.00') AS VAL10,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN IN(".$in.")),'0.00') AS VAL11,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN = 77 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN = 24 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN = 25 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN = 26 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN = 27 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN = 28 ))+
            ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN = 29 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN = 30 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN IN(".$in.")),'0.00') AS VAL12
            FROM RECEITA_ROTA_CAD I WHERE I.CODCGA = ".$CODCGA." AND I.CODUNN = ".$CODUNN." GROUP BY I.CODUNN";

        }elseif($CODUNN == 20){
            $sqlAnalitSum = "SELECT I.CODUNN ,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and C.ITEN IN(".$in.")),'0.00') AS VAL1,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and C.ITEN IN(".$in.")),'0.00') AS VAL2,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and C.ITEN IN(".$in.")),'0.00') AS VAL3,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and C.ITEN IN(".$in.")),'0.00') AS VAL4,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and C.ITEN IN(".$in.")),'0.00') AS VAL5,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and C.ITEN IN(".$in.")),'0.00') AS VAL6,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and C.ITEN IN(".$in.")),'0.00') AS VAL7,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and C.ITEN IN(".$in.")),'0.00') AS VAL8,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and C.ITEN IN(".$in.")),'0.00') AS VAL9,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and C.ITEN IN(".$in.")),'0.00') AS VAL10,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and C.ITEN IN(".$in.")),'0.00') AS VAL11,
            IFNULL( ((SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN = 36 ) * (SELECT (C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN = 37 ))+
            (SELECT SUM(C.VALOR) FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and C.ITEN IN(".$in.")),'0.00') AS VAL12
            FROM RECEITA_ROTA_CAD I WHERE I.CODCGA = ".$CODCGA." AND I.CODUNN = ".$CODUNN." GROUP BY I.CODUNN";
        }
//dd($sqlAnalitSum);


        $pacoteAnaliticas = DB::select($sqlAnalit);
        $pacoteSinteticas = DB::select($sqlSintet);
        $pacoteAnaliticasSum = DB::select($sqlAnalitSum);
       // dd($pacoteAnaliticasSum);
        //$pacoteAnaliticas = DB::table('PAGCLA')->select('*')->where('CODGRU','=',$codgru)->where('SITUAC','=','A')->orderBy('PAICLAP','ASC')->get();
        //dd($pacoteSinteticas,$pacoteAnaliticas);
       // View::share ( 'subMenu', '<li>Centro de Gasto</li><li>Pacote</li><li>Metas</li>' );
        View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li>Pacote</li>' );
        return view('previsaoorcamentaria.receitarota',['permissaoeditar'=>$permissaoeditar,'pacoteAnaliticasSum'=>$pacoteAnaliticasSum,'pacoteSinteticas'=>$pacoteSinteticas,'pacoteAnaliticas'=>$pacoteAnaliticas,'pacote'=>$pacote->DESCRI,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);
    }

    public function aplicaFuncao($funcoes,$ano,$mes,$CODCGA,$CODUNN){

        foreach($funcoes as $f){

            $func = explode(';',$f->OBJTIPO);

            if(count($func) > 1){

                $funcao = array_shift($func);
                $parametros = $func;

                    if($funcao == 'mult'){

                        $q =  ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN];
                        $q3 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        $v  = DB::table('RECEITA_ROTA_CAD')->select(DB::raw('round(EXP(SUM(LOG(VALOR))),2) AS total'))->where($q)->whereIn('ITEN',$parametros)->first();

                        DB::table('RECEITA_ROTA_CAD')
                        ->where($q3)
                        ->update(['VALOR' => $v->total]);

                    }
                    elseif($funcao == 'multpff'){
                        //dump($parametros);
                        $q3 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        $p1 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[0]];
                        $p2 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[1]];
                        $p3 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[2]];

                        $v1  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p1)->first();
                        $v2  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p2)->first();
                        $v3  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p3)->first();

                        if($v1 == null){
                            $v1 = 0;
                        }
                        if($v2 == null){
                            $v2 = 0;
                        }
                        if($v3 == null){
                            $v3 = 0;
                        }

                        $valor = ($v1->VALOR * $v2->VALOR) * ($v3->VALOR/100);
                        //dump($v1,$v2,$v3,$valor);
                        //dd($v1->VALOR,$v2->VALOR,$v3->VALOR,$valor,$f->ID);

                        DB::table('RECEITA_ROTA_CAD')
                        ->where($q3)
                        ->update(['VALOR' => $valor]);
                    }
                    elseif($funcao == 'multp'){

                        $q =  ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN];
                        $q3 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        $p1 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[0]];
                        $p2 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[1]];
                        $p3 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[2]];

                        $v1  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p1)->first();
                        $v2  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p2)->first();
                        $v3  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p3)->first();

                        //dd($v1,$p1,$v2,$p2,$v3,$p3);
                        $v1Val = isset($v1) ? $v1->VALOR : 0;
                        $v2Val = isset($v2) ? $v2->VALOR : 0;
                        $v3Val = isset($v3) ? $v3->VALOR : 0;

                        $valor = ($v1Val * $v2Val) * ($v3Val/100);
                        //dump($valor);
                        //dd($v1->VALOR,$v2->VALOR,$v3->VALOR,$valor,$f->ID);

                        DB::table('RECEITA_ROTA_CAD')
                        ->where($q3)
                        ->update(['VALOR' => $valor]);

                    }
                    elseif($funcao == 'somatrecho'){



                    }
                    elseif($funcao == 'mediakm'){

                        $itemID = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        $p1 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[0]];
                        $p2 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[1]];

                        $v1  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p1)->first();
                        $v2  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p2)->first();

                        $v1Val = isset($v1) ? $v1->VALOR : 0;
                        $v2Val = isset($v2) ? $v2->VALOR : 1;

                        if($v2Val == 0){ $v2Val = 1;}

                        $valor = ($v1Val/$v2Val);

                        DB::table('RECEITA_ROTA_CAD')
                        ->where($itemID)
                        ->update(['VALOR' => $valor]);

                    }
                    elseif($funcao == 'imposto1'){

                        $itemID = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        $p1 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[0]];
                        $p2 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[1]];

                        $v1  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p1)->first();
                        $v2  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p2)->first();

                        $v1Val = isset($v1) ? $v1->VALOR : 0;
                        $v2Val = isset($v2) ? $v2->VALOR : 0;

                        $valor = ($v1Val * ($v2Val/100));
                        //dd($valor,$p1,$p2,$v1,$v2);
                        DB::table('RECEITA_ROTA_CAD')
                        ->where($itemID)
                        ->update(['VALOR' => $valor]);

                    }
                    elseif($funcao == 'imposto2'){

                        $itemID = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        $p1 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[0]];
                        $p2 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[1]];

                        $v1  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p1)->first();
                        $v2  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p2)->first();

                        $v1Val = isset($v1) ? $v1->VALOR : 0;
                        $v2Val = isset($v2) ? $v2->VALOR : 0;

                        $valor = ($v1Val * ($v2Val/100));

                        DB::table('RECEITA_ROTA_CAD')
                        ->where($itemID)
                        ->update(['VALOR' => $valor]);

                    }
                    elseif($funcao == 'imposto3'){

                        $itemID = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        $p1 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[0]];
                        $p2 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[1]];
                        $p3 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[2]];
                        $p4 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[3]];
                        $p5 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[4]];

                        $v1  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p1)->first();
                        $v2  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p2)->first();
                        $v3  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p3)->first();
                        $v4  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p4)->first();
                        $v5  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p5)->first();

                        $v1Val = isset($v1) ? $v1->VALOR : 0;
                        $v2Val = isset($v2) ? $v2->VALOR : 0;
                        $v3Val = isset($v3) ? $v3->VALOR : 0;
                        $v4Val = isset($v4) ? $v4->VALOR : 0;
                        $v5Val = isset($v5) ? $v5->VALOR : 0;

                        $valor = ($v1Val+$v2Val+$v3Val+$v4Val) * ($v5Val/100);

                        DB::table('RECEITA_ROTA_CAD')
                        ->where($itemID)
                        ->update(['VALOR' => $valor]);

                    }
                    elseif($funcao == 'imposto4'){

                        $where1 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 1];
                        $where2 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 2];
                        $where3 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 3];
                        $where4 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 4];

                        $valor_item_1 = DB::table('TRECHOS_CAD')->select('*')->where($where1)->get();
                        $valor_item_2 = DB::table('TRECHOS_CAD')->select('*')->where($where2)->get();
                        $valor_item_3 = DB::table('TRECHOS_CAD')->select('*')->where($where3)->get();
                        $valor_item_4 = DB::table('TRECHOS_CAD')->select('*')->where($where4)->get();

                        $t = count($valor_item_1);
                        $valorTrecho = 0;

                        for($x = 0;$x < $t; $x++){
                            $valorTrecho = $valorTrecho + (($valor_item_1[$x]->VALOR * $valor_item_2[$x]->VALOR) * ($valor_item_3[$x]->VALOR/100));
                        }

                        $itemID = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];
                        $p1 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[0]];

                        $v1  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p1)->first();
                        $v1Val = isset($v1) ? $v1->VALOR : 0;

                        $valor = ($valorTrecho) * ($v1Val/100);

                        DB::table('RECEITA_ROTA_CAD')
                        ->where($itemID)
                        ->update(['VALOR' => $valor]);

                        ///dd(count($valor_item_1),$valor_item_1,$valor_item_2,$valor_item_3,$valor_item_4);
                    }
                    elseif($funcao == 'impostospot'){

                        $where1 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 1];
                        $where2 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 2];
                        $where3 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 3];
                        $where4 = ['MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ANO'=>$ano,'ITEM'=> 4];

                        $valor_item_1 = DB::table('TRECHOS_CAD')->select('*')->where($where1)->get();
                        $valor_item_2 = DB::table('TRECHOS_CAD')->select('*')->where($where2)->get();
                        $valor_item_3 = DB::table('TRECHOS_CAD')->select('*')->where($where3)->get();
                        $valor_item_4 = DB::table('TRECHOS_CAD')->select('*')->where($where4)->get();

                        $t = count($valor_item_1);
                        $valorTrecho = 0;

                        for($x = 0;$x < $t; $x++){
                            $valorTrecho = $valorTrecho + (($valor_item_1[$x]->VALOR * $valor_item_2[$x]->VALOR) * ($valor_item_3[$x]->VALOR/100));
                        }

                        $itemID = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];
                        //$p1 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[0]];

                        //$v1  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p1)->first();
                        //$v1Val = isset($v1) ? $v1->VALOR : 0;

                        //$valor = ($valorTrecho) * ($v1Val/100);

                        //dd($valorTrecho,$itemID);

                        DB::table('RECEITA_ROTA_CAD')
                        ->where($itemID)
                        ->update(['VALOR' => $valorTrecho]);

                        ///dd(count($valor_item_1),$valor_item_1,$valor_item_2,$valor_item_3,$valor_item_4);
                    }
                    elseif($funcao == 'multtotal'){

                        $p = array();
                        $v = array();
                        $vl = array();

                        $qID = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        for($x = 0;$x <= 12;$x++){
                            $p[$x] = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[$x]];
                        }

                        for($x = 0;$x <= 12;$x++){
                            $v[$x]  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p[$x])->first();
                        }

                        for($x = 0;$x <= 12;$x++){
                            if($v[$x] == null){
                                $vl[$x] = 0;
                            }else{
                                $vl[$x] = $v[$x]->VALOR;
                            }
                        }
                        //dd($vl);
                        $valor = ($vl[0]*$vl[1]) + ($vl[2]*$vl[3]) + ($vl[4]*$vl[5]) + ($vl[6]*$vl[7]) + ($vl[8] + $vl[9] + $vl[10] + $vl[11] + $vl[12]);
                        //dd($v);
                        DB::table('RECEITA_ROTA_CAD')
                        ->where($qID)
                        ->update(['VALOR' => $valor]);

                    }
                    elseif($funcao == 'multtotal2'){

                        $p = array();
                        $v = array();
                        $vl = array();

                        $qID = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        for($x = 0;$x <= 6;$x++){
                            $p[$x] = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[$x]];
                        }

                        for($x = 0;$x <= 6;$x++){
                            $v[$x]  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p[$x])->first();
                        }

                        for($x = 0;$x <= 6;$x++){
                            if($v[$x] == null){
                                $vl[$x] = 0;
                            }else{
                                $vl[$x] = $v[$x]->VALOR;
                            }
                        }
                        //dd($vl);
                        $valor = ($vl[0]*$vl[1]) + ($vl[2] + $vl[3] + $vl[4] + $vl[5] + $vl[6]);
                        //dd($v);
                        DB::table('RECEITA_ROTA_CAD')
                        ->where($qID)
                        ->update(['VALOR' => $valor]);
                    }


            }
       }
    }

    public function operacional(Request $request){
        $CODCGA = $request->CODCGA;
        $CODUNN = $request->CODUNN;
        $codgru = $request->codgru;


        $CODCGA = '';
        $CODUNN = '';

        if($request->CODCGA && $request->CODUNN){
            $unidadeFilial = (array) DB::table('MENUFILIAL')
            ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
            ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
            ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
            ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
            ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
            ->first();
            $CODCGA = $request->CODCGA;
            $CODUNN = $request->CODUNN;
        }


        //View::share ( 'subMenu', '<li>Centro de Gasto</li><li>Pacote</li>' );
        View::share ( 'subMenu', '<li><a href="'.route('previsaoorcamentaria/menufilial').'">Centro de Gasto</a></li><li><a href="'.route('previsaoorcamentaria/operacional',['CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru]).'">Pacote</a></li>' );
        return view('previsaoorcamentaria.menupacotefrota',compact('CODCGA','CODUNN','codgru','unidadeFilial'));
    }

    public function gente(Request $request){
        $CODCGA = $request->CODCGA;
        $CODUNN = $request->CODUNN;
        $codgru = $request->codgru;


        return view('previsaoorcamentaria.gente',compact('CODCGA','CODUNN','codgru'));
    }


}

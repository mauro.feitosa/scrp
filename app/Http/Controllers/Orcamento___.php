<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Auth;

class Orcamento extends Controller
{
    public $ano;
    public $mes;
    public $gasto;
    public $con;
    public $conMy;
    public $item;
    public $arrayVLRIMP;
    public $arrayVLRINI;
    public $arrayVLRREA;
    public $arrayVLRAJUS;
    public $arrayVLRAJUS2;
    public $J;
    public $J2;
    public $mArrayVLRIMP;
    public $mArrayVLRINI;
    public $mArrayVLRAJUS;
    public $mArrayVLRAJUS2;

    public $grupo;
    public $busca;
    public $somaGrupo = array(0=>0,1=>0,2=>0,3=>0);
    public $somaGrupoi = array(0=>0,1=>0,2=>0,3=>0);
    public $somaGrupoJ = array(0=>0,1=>0,2=>0,3=>0);
    public $somaGrupoJ2 = array(0=>0,1=>0,2=>0,3=>0);

    public $receitaOpLiquida = array();
    public $receitaOpLiquidai= array();
    public $receitaOpLiquidaJ= array();
    public $receitaOpLiquidaJ2= array();

    public $margemContribuicao;
    public $margemContribuicaoi;
    public $margemContribuicaoJ;
    public $margemContribuicaoJ2;

    public $margemContribuicaoAnual;
    public $lucroBruto;
    public $lucroBrutoi;
    public $lucroBrutoJ;
    public $lucroBrutoJ2;


    public $lucroBrutoPerc;
    public $lucroBrutoAnual;
    public $lucroBrutoAnualPerc;
    public $chart;
    public $chartLine;

    public $ABITDA = array();
    public $ABITDAi = array();
    public $ABITDAJ = array();
    public $ABITDAJ2 = array();

    public $ABITDAAnual;
    public $ABITDAPerc;
    public $ABITDAPercAnual;


    public $valLucroBruto;
    public $valDespesasOp;
    public $valDespesasAdm;

    public $ABITDA_VEI = array();
    public $ABITDAi_VEI = array();
    public $ABITDAJ_VEI = array();
    public $ABITDAJ2_VEI = array();

    public $ABITDAAnual_VEI;
    public $ABITDAPerc_VEI;
    public $ABITDAPercAnual_VEI;

    public $lucroOperacionalAnual;
    public $lucroOperacional;
    public $lucroOperacionalPerc;
    public $lucroOperacionalPercAnual;

    public $lair;
    public $lairAnual;
    public $lairPerc;
    public $lairPercAnual;

    public $lucroLiquido = array();
    public $lucroLiquidoi = array();
    public $lucroLiquidoJ = array();
    public $lucroLiquidoJ2 = array();

    public $lucroLiquidoAnual;
    public $lucroLiquidoPerc;
    public $lucroLiquidoPerci;
    public $lucroLiquidoPercJ;
    public $lucroLiquidoPercJ2;

    public $lucroLiquidoAnualPerc;

    public $despesasCorporativas;
    public $despesasCorporativasAnual;
    public $despesasCorporativasAC;
    public $despesasCorporativasAnualAC;

    public $despcorp;


    public $unidade;
    public $indiceConfis;
    public $indicePis;
    public $indiceIss;

    public $contribuicaoServ;
    public $contribuicaoTrans;
    public $irpjServ;
    public $irpjTrans;
    public $impNotas;
    public $impNotasJ;
    public $impNotasJ2;
    public $indiceCPRB;
    public $xxx;

    public $investimentos;
    public $fundoDeReserva;

    public $investimentosJ;
    public $fundoDeReservaJ;

    public $investimentosJ2;
    public $fundoDeReservaJ2;

    public $gastosMatriz;
    public $gastosMatrizJ;
    public $gastosMatrizJ2;
    public $fatLiqTotal;
    public $opcaoview;
    public $opcao;
    public $ac;
    public $fr;
    public $presumidoreal;

    public $deducoes;
    public $deducoesi;
    public $deducoesJ;
    public $deducoesJ2;
    public $deducoesAnual;

    public $passivo;
    public $passivoi;
    public $passivoJ;
    public $passivoJ2;
    public $passivoAnual;

    public function __construct()
    {
        for($x=0;$x < 100;$x++){

            $this->somaGrupo[$x] = array();
            $this->somaGrupoi[$x] = array();
            $this->somaGrupoJ[$x] = array();
            $this->somaGrupoJ2[$x] = array();


            $this->lucroBruto[$x] = array();
            $this->lucroBrutoi[$x] = array();
            $this->lucroBrutoJ[$x] = array();
            $this->lucroBrutoJ2[$x] = array();
        }
    }

    public function testeDb(){

       // $t = DB::select('SELECT COUNT(*) FROM ORCLAN_FOR');
       // dd($t);

    }

    public function listaMes(){
        $arrayMes[1]  = 'Janeiro';
        $arrayMes[2]  = 'Fevereiro';
        $arrayMes[3]  = 'Março';
        $arrayMes[4]  = 'Abril';
        $arrayMes[5]  = 'Maio';
        $arrayMes[6]  = 'Junho';
        $arrayMes[7]  = 'Julho';
        $arrayMes[8]  = 'Agosto';
        $arrayMes[9]  = 'Setembro';
        $arrayMes[10] = 'Outubro';
        $arrayMes[11] = 'Novembro';
        $arrayMes[12] = 'Dezembro';
        return $arrayMes;
    }

    public function listaMesAbrev(){
        $arrayMes[1]  = 'JAN';
        $arrayMes[2]  = 'FEV';
        $arrayMes[3]  = 'MAR';
        $arrayMes[4]  = 'ABR';
        $arrayMes[5]  = 'MAI';
        $arrayMes[6]  = 'JUN';
        $arrayMes[7]  = 'JUL';
        $arrayMes[8]  = 'AGO';
        $arrayMes[9]  = 'SET';
        $arrayMes[10] = 'OUT';
        $arrayMes[11] = 'NOV';
        $arrayMes[12] = 'DEZ';
        return $arrayMes;
    }
    public function rodape(){

        $selMes = $this->mes;
        $tot = count($selMes);
        $conteudo = '';
        $conteudo.='<tr style="background-color:#000;color:#fff;">';
        $conteudo.='<th id="siz1"><img scr="espaco.php" width="300px" heigth="30px"></th>';
        $conteudo.='<th></th>';
        $conteudo.='<th></td>';
        $conteudo.='<th></th>';
        $conteudo.='<th></th>';
        for($x=0;$x < $tot;$x++){
            $conteudo.='<th></th>';
            $conteudo.='<th></th>';
            $conteudo.='<th></th>';
            $conteudo.='<th></th>';
        }
        $conteudo.='</tr>';


        return $conteudo;

    }

    function boxAno($selAno){

        $ano = (int) Date('Y');
        $selAno = (int) $selAno;

       // if($_SESSION['USUTIPO'] == 'ADM'){
           $xAno = 2020;
       // }else{
        //   $xAno = 2017;
       // }

        //$arrayAno = $this->listaAno();

        $conteudo = '<label for="ano">Ano</label><select id="ano" name="ano" class="form-control">';

        for($x=$xAno; $x <= $ano; $x++){
            if( $x == $selAno) {
                $conteudo.='<option selected="selected" value="'.$x.'">'.$x.'</option>';
            }else{
                $conteudo.='<option value="'.$x.'">'.$x.'</option>';
            }
        }

        $conteudo.='</select>';
        return $conteudo;
    }

    public function cabeMes(){

        if($this->opcao == "J"){
            $tit = 'FOR';
        }elseif($this->opcao == "J2"){
            $tit = 'PAJ';
        }else{
            $tit = 'INI';
        }

        $selMes = $this->mes;
        $arrayMes = $this->listaMes();

        $conteudo='<thead>';
        $conteudo.= '<tr>';
        //$conteudo.= '<th><a href="xls.php" role="button" class="btn btn-success btn-xs">EXPORTAR <i class="fa fa-fw fa-table"></i></a></th>';
        $conteudo.= '<th class="cabClass">DRE</th>';
        $conteudo.= '<th class="cabMes" colspan="4" style="text-align:center">ANO</th>';
        $tot = count($selMes);

            for($x=0;$x < $tot;$x++){
                $conteudo.= '<th class="cabMes" colspan="4" style="text-align:center">'.strtoupper($arrayMes[$selMes[$x]]).'</th>';
            }
        $conteudo.='</tr>';
        $conteudo.='<tr style="background-color:#444;color:#fff;">';
        $conteudo.='<th class="cabMes"><img scr="espaco.php" width="300px" heigth="30px"></th>';
        $conteudo.='<th class="cabMes"><b>ORC('.$tit.')</b></th>';
        $conteudo.='<th class="cabMes"><b>REAL</b></td>';
        $conteudo.='<th class="cabMes"><b>DISP(R$)</b></th>';
        $conteudo.='<th class="cabMes"><b>DISP(%)</b></th>';
        for($x=0;$x < $tot;$x++){
            $conteudo.='<th class="cabMes"><b>ORC('.$tit.')</b></th>';
            $conteudo.='<th class="cabMes"><b>REAL</b></th>';
            $conteudo.='<th class="cabMes"><b>DISP(R$)</b></th>';
            $conteudo.='<th class="cabMes"><b>DISP(%)</b></th>';
        }
        $conteudo.='</tr>';

      //  if($_REQUEST['debug']){
      //      $conteudo.='<input id="debug" name="debug" value="ok" type="hidden">';
      //  }

        $conteudo.='</thead>';

        return $conteudo;

    }


    public function processaItem($titulo,$codger,$paiclap){

        $sql = "SELECT * FROM PAGCLA WHERE CODGER = ".$codger." AND SITUAC = 'A' AND TIPCLA = 2 AND PAICLAP =".$paiclap;
        //var_dump($sql);
        /*
        if($codger == 2 && $paiclap == 135){
        echo "<pre>";
        echo $sql;
        echo "</pre>";
        }*/

        $rs = DB::select($sql);
        $itemId[] = array();
        $itemDesc[] = array();
        $x = 0;

         foreach ($rs as $row) {
             $itemId[$x] = $row->CODCLAP;
             $itemDesc[$x] = $row->CODCLAP.' - '.$row->DESCRI;
             $x++;
         }

        if($paiclap == 154 && $codger <> 8){
             $itemId[$x] = '1111';
             $itemDesc[$x] = 'ENCONTRO DE CONTAS';
        }

        $this->item[$codger][$paiclap]['TITULO'] =  $titulo;
        $this->item[$codger][$paiclap]['ID'] =  $itemId;
        $this->item[$codger][$paiclap]['DESC'] =  $itemDesc;


        /*
        if($paiclap == 154){
            echo "<pre>";
            var_dump($this->item[$codger]);
            echo "</pre>";
        }
        */
    }

    public function pegaDados(){

        //dd('ok');

        $totMes = count($this->mes);
        $uniGasto = implode(", ", $this->gasto);
        $ano = $this->ano;
        $SQL_UNIDADE='';
        $this->deducoes = array();
        $this->deducoesi = array();
        $this->deducoesJ = array();
        $this->deducoesJ2 = array();

        $this->passivo = array();
        $this->passivoi = array();
        $this->passivoJ = array();
        $this->passivoJ2 = array();
        //echo "<pre>";
        //var_dump($this->ano);
        //echo "</pre>";
        $this->deducoesAnual = 0;
        $this->passivoAnual = 0;
        $SQL_UNIDADE_IMP = " AND CODUNN IN(19,20) ";

        if(count($this->unidade) > 0){
          $unidades = implode(", ", $this->unidade);
          $SQL_UNIDADE = " AND CODUNN IN(".$unidades.") ";
        }

        $fontedadosImp  = 'VW_ORCLAN_SP';
        $fonteDados     = 'VW_ORCLAN_SP';

        if($uniGasto == 12){
            $SQL_UNIDADE = " AND CODUNN NOT IN(16) ";
        }

        //dd($totMes,$uniGasto,$ano);

        for($x=0;$x < $totMes;$x++){

        $sqlAll[$x] = "SELECT SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA, SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE  CODCGA IN(".$uniGasto.")".$SQL_UNIDADE." AND ANO = ".$ano." AND ANALIT <> 165 AND MES = ".$this->mes[$x]." AND CODCUS NOT IN(13) GROUP BY SINTET, ANALIT";
        $sqlAllMatriz[$x] = "SELECT  SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA ,SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE SINTET IN(154,645) AND CODCGA NOT IN(12,10,26,39)".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." AND CODCUS NOT IN(13) GROUP BY SINTET, ANALIT";
        $sqlimp[$x] = "SELECT  SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA ,SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE  CODCGA IN(".$uniGasto.")".$SQL_UNIDADE_IMP." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." AND CODCUS NOT IN(13) GROUP BY SINTET, ANALIT";
        $sqldesp1[$x] = "SELECT  SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA ,SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE  CODCGA IN(".$uniGasto.")".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." AND CODCUS = 11 GROUP BY SINTET, ANALIT";
        $sqldesp2[$x] = "SELECT  SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA ,SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE  CODCGA IN(".$uniGasto.")".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." AND CODCUS = 13 GROUP BY SINTET, ANALIT";
        $sqlMatriz[$x] = "SELECT  SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA ,SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE  CODCGA IN(12)".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." AND CODCUS NOT IN(13) GROUP BY SINTET, ANALIT";
        $sqlFatLiqTotal[$x] = "SELECT  SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA ,SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE CODCGA NOT IN(12)".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." AND CODCUS NOT IN(13) AND SINTET IN(154,645) GROUP BY SINTET, ANALIT";
        $sqlImpostosCaixa[$x] = "SELECT  SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA ,SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fontedadosImp." WHERE CODCGA IN(".$uniGasto.")".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." AND CODCUS NOT IN(13) AND SINTET IN(646,645) GROUP BY SINTET, ANALIT";
        $sqlImpostosCaixaMatriz[$x] = "SELECT  SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA ,SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fontedadosImp." WHERE CODCGA NOT IN(12)".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." AND CODCUS NOT IN(13) AND SINTET IN(645) GROUP BY SINTET, ANALIT";
        $sqlDeducoes[$x] = "SELECT MES,SUM(VALOR) AS VALORDEDUCAO FROM DEDUCAO WHERE  CODCGA IN(".$uniGasto.")".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." GROUP BY MES";
        $sqlPassivo[$x] = "SELECT MES,SUM(VALOR) AS VALORPASSIVO FROM PASSIVOOP WHERE  CODCGA IN(".$uniGasto.")".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." GROUP BY MES";
        $sqlEncontro[$x] = "SELECT MES,SUM(VALOR) AS VALORENCONTRO FROM ENCONTRO WHERE  CODCGA IN(".$uniGasto.")".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$x]." GROUP BY MES";

       //dd($sqlAll[$x]);
//if(Auth::user()) == 23){
 //       dump($sqlAll);
//}

        // calcula liquido para pegar 4,5% quando escolhida filal 12
//dd($sqlAll);


        if($uniGasto == 12){

            $rsAllMatriz[$x]  = DB::select($sqlAllMatriz[$x]);

            foreach ($rsAllMatriz[$x] as $row){

             $this->mArrayVLRIMP[$x][$row->ANALIT] = $row->VLRIMP;
             $this->mArrayVLRINI[$x][$row->ANALIT] = $row->VLRINI;
             $this->mArrayVLRREA[$x][$row->ANALIT] = $row->VLRREA;

             $this->mArrayVLRAJUS[$x][$row->ANALIT] = $row->VLRAJUS;
             $this->mArrayVLRAJUS2[$x][$row->ANALIT] = $row->VLRAJUS2;

            }


            $this->mArrayVLRREA[$x][155]  = isset($this->mArrayVLRREA[$x][155]) ? $this->mArrayVLRREA[$x][155] : 0;
            $this->mArrayVLRREA[$x][165]  = isset($this->mArrayVLRREA[$x][165]) ? $this->mArrayVLRREA[$x][165] : 0;
            $this->mArrayVLRREA[$x][164]  = isset($this->mArrayVLRREA[$x][164]) ? $this->mArrayVLRREA[$x][164] : 0;

            $this->mArrayVLRINI[$x][155]  = isset($this->mArrayVLRINI[$x][155]) ? $this->mArrayVLRINI[$x][155] : 0;
            $this->mArrayVLRINI[$x][165]  = isset($this->mArrayVLRINI[$x][165]) ? $this->mArrayVLRINI[$x][165] : 0;
            $this->mArrayVLRINI[$x][164]  = isset($this->mArrayVLRINI[$x][164]) ? $this->mArrayVLRINI[$x][164] : 0;

            $this->mArrayVLRAJUS[$x][155]  = isset($this->mArrayVLRAJUS[$x][155]) ? $this->mArrayVLRAJUS[$x][155] : 0;
            $this->mArrayVLRAJUS[$x][165]  = isset($this->mArrayVLRAJUS[$x][165]) ? $this->mArrayVLRAJUS[$x][165] : 0;
            $this->mArrayVLRAJUS[$x][164]  = isset($this->mArrayVLRAJUS[$x][164]) ? $this->mArrayVLRAJUS[$x][164] : 0;

            $this->mArrayVLRAJUS2[$x][155]  = isset($this->mArrayVLRAJUS2[$x][155]) ? $this->mArrayVLRAJUS2[$x][155] : 0;
            $this->mArrayVLRAJUS2[$x][165]  = isset($this->mArrayVLRAJUS2[$x][165]) ? $this->mArrayVLRAJUS2[$x][165] : 0;
            $this->mArrayVLRAJUS2[$x][164]  = isset($this->mArrayVLRAJUS2[$x][164]) ? $this->mArrayVLRAJUS2[$x][164] : 0;

            $v155  = isset($this->mArrayVLRREA[$x][155]) ? $this->mArrayVLRREA[$x][155] : 0;
            $v165  = isset($this->mArrayVLRREA[$x][165]) ? $this->mArrayVLRREA[$x][165] : 0;
            $v164  = isset($this->mArrayVLRREA[$x][164]) ? $this->mArrayVLRREA[$x][164] : 0;
            $v155imp  = isset($this->mArrayVLRIMP[$x][44]) ? $this->mArrayVLRIMP[$x][44] : 0;

            $v155i = isset($this->mArrayVLRINI[$x][155]) ? $this->mArrayVLRINI[$x][155] : 0;
            $v165i  = isset($this->mArrayVLRINI[$x][165]) ? $this->mArrayVLRINI[$x][165] : 0;
            $v164i  = isset($this->mArrayVLRINI[$x][164]) ? $this->mArrayVLRINI[$x][164] : 0;

            $v155J  = isset($this->mArrayVLRAJUS[$x][155]) ? $this->mArrayVLRAJUS[$x][155] : 0;
            $v165J  = isset($this->mArrayVLRAJUS[$x][165]) ? $this->mArrayVLRAJUS[$x][165] : 0;
            $v164J  = isset($this->mArrayVLRAJUS[$x][164]) ? $this->mArrayVLRAJUS[$x][164] : 0;
            //$v155impJ  = isset($this->mArrayVLRAJUS[$x][155]) ? $this->mArrayVLRAJUS[$x][155] : 0;

            $v155J2  = isset($this->mArrayVLRAJUS2[$x][155]) ? $this->mArrayVLRAJUS2[$x][155] : 0;
            $v165J2  = isset($this->mArrayVLRAJUS2[$x][165]) ? $this->mArrayVLRAJUS2[$x][165] : 0;
            $v164J2  = isset($this->mArrayVLRAJUS2[$x][164]) ? $this->mArrayVLRAJUS2[$x][164] : 0;
            //$v155impJ2  = isset($this->mArrayVLRAJUS2[$x][155]) ? $this->mArrayVLRAJUS2[$x][155] : 0;

            $temp = $v155 + $v165 + $v164;
            $tempJ = $v155J + $v165J + $v164J;
            $tempJ2 = $v155J2 + $v165J2 + $v164J2;
            $tempi = $v155i + $v165i + $v164i;

           // echo "<pre> impostos ini 1:<BR/>";
            //var_dump( abs($this->mArrayVLRREA[$x][155]), abs($this->mArrayVLRREA[$x][165]), abs($this->mArrayVLRREA[$x][164])    );
           // var_dump( abs($this->mArrayVLRINI[$x][42]),abs($this->mArrayVLRINI[$x][44]),abs($this->mArrayVLRINI[$x][47]),abs($this->mArrayVLRINI[$x][48])    );
           // echo "</pre>";

            /*
            echo "<pre>";
            var_dump($this->opcaoview);
            var_dump($this->presumidoreal);
            echo "</pre>";
            */
            if($this->opcaoview == 'CAIXA' && $this->presumidoreal=='REAL'){

            }elseif($this->opcaoview == 'COMPETENCIA' && $this->presumidoreal=='REAL'){

                $rsImpostosCaixaMatriz[$x]  = DB::select($sqlImpostosCaixaMatriz[$x]);

                //echo $sqlImpostosCaixaMatriz[$x];

                    foreach ($rsImpostosCaixaMatriz[$x] as $row){

                        $this->mArrayVLRIMP[$x][$row->ANALIT] = $row->VLRIMP;
                        $this->mArrayVLRINI[$x][$row->ANALIT] = $row->VLRINI;
                        $this->mArrayVLRREA[$x][$row->ANALIT] = $row->VLRREA;
                        $this->mArrayVLRAJUS[$x][$row->ANALIT] = $row->VLRAJUS;
                        $this->mArrayVLRAJUS2[$x][$row->ANALIT] = $row->VLRAJUS2;

                    }
                    //echo "<pre> impostos ini 2:<BR/>";
                    //var_dump( abs($this->mArrayVLRREA[$x][155]), abs($this->mArrayVLRREA[$x][165]), abs($this->mArrayVLRREA[$x][164])    );
                   // var_dump( abs($this->mArrayVLRINI[$x][42]),abs($this->mArrayVLRINI[$x][44]),abs($this->mArrayVLRINI[$x][47]),abs($this->mArrayVLRINI[$x][48])    );
                  //  echo "</pre>";

            }else{

                $this->mArrayVLRREA[$x][42] = $temp * $this->indiceConfis *(-1); // confis
                $this->mArrayVLRREA[$x][44] = $v155imp *(-1); // icms
                $this->mArrayVLRREA[$x][47] = $v164 * $this->indiceIss * (-1); // iss
                $this->mArrayVLRREA[$x][48] = $temp * $this->indicePis * (-1); // pis
 /*
                echo "<pre> indices:<BR/>";
                var_dump($this->indiceConfis);
                var_dump($this->indiceIss);
                var_dump($this->indicePis);
                echo "</pre>";

                echo "<pre> impostos ini 3:<BR/>";
               // var_dump( abs($this->mArrayVLRREA[$x][155]), abs($this->mArrayVLRREA[$x][165]), abs($this->mArrayVLRREA[$x][164])    );
                    var_dump( abs($this->mArrayVLRINI[$x][42]),
                    abs($this->mArrayVLRINI[$x][44]),
                    abs($this->mArrayVLRINI[$x][47]),
                    abs($this->mArrayVLRINI[$x][48])    );
               echo "</pre>";
*/
                /*
                $this->mArrayVLRINI[$x][42] = $tempi * $this->indiceConfis *(-1); // confis
                $this->mArrayVLRINI[$x][44] = $v155imp*(-1); // icms
                $this->mArrayVLRINI[$x][47] = $v164i * $this->indiceIss * (-1); // iss
                $this->mArrayVLRINI[$x][48] = $tempi * $this->indicePis * (-1); // pis

                $this->mArrayVLRAJUS[$x][42] = $tempJ * $this->indiceConfis *(-1); // confis
                $this->mArrayVLRAJUS[$x][44] = $v155imp *(-1); // icms
                $this->mArrayVLRAJUS[$x][47] = $v164J * $this->indiceIss * (-1); // iss
                $this->mArrayVLRAJUS[$x][48] = $tempJ * $this->indicePis * (-1); // pis

                $this->mArrayVLRAJUS2[$x][42] = $tempJ2 * $this->indiceConfis *(-1); // confis
                $this->mArrayVLRAJUS2[$x][44] = $v155imp *(-1); // icms
                $this->mArrayVLRAJUS2[$x][47] = $v164J2 * $this->indiceIss * (-1); // iss
                $this->mArrayVLRAJUS2[$x][48] = $tempJ2 * $this->indicePis * (-1); // pis
                */
                //echo "<pre> impostos ini 3:<BR/>";
                //var_dump( abs($this->mArrayVLRREA[$x][155]), abs($this->mArrayVLRREA[$x][165]), abs($this->mArrayVLRREA[$x][164])    );
                //var_dump( abs($this->mArrayVLRINI[$x][42]),abs($this->mArrayVLRINI[$x][44]),abs($this->mArrayVLRINI[$x][47]),abs($this->mArrayVLRINI[$x][48])    );
               // echo "</pre>";
            }

           // echo "<pre> impostos ini 4:<BR/>";
           // var_dump( abs($this->mArrayVLRINI[$x][42]),abs($this->mArrayVLRINI[$x][44]),abs($this->mArrayVLRINI[$x][47]),abs($this->mArrayVLRINI[$x][48])    );

          //  echo "</pre>";


            $recebimentos  = abs($this->mArrayVLRREA[$x][155])+abs($this->mArrayVLRREA[$x][165])+abs($this->mArrayVLRREA[$x][164]);
            $impostosVendas = abs($this->mArrayVLRREA[$x][42])+abs($this->mArrayVLRREA[$x][44])+abs($this->mArrayVLRREA[$x][47])+abs($this->mArrayVLRREA[$x][48]);

            $recebimentosi  = abs($this->mArrayVLRINI[$x][155])+abs($this->mArrayVLRINI[$x][165])+abs($this->mArrayVLRINI[$x][164]);
            $impostosVendasi = abs($this->mArrayVLRINI[$x][42])+abs($this->mArrayVLRINI[$x][44])+abs($this->mArrayVLRINI[$x][47])+abs($this->mArrayVLRINI[$x][48]);

            $recebimentosJ  = abs($this->mArrayVLRAJUS[$x][155])+abs($this->mArrayVLRAJUS[$x][165])+abs($this->mArrayVLRAJUS[$x][164]);
            $impostosVendasJ = abs($this->mArrayVLRAJUS[$x][42])+abs($this->mArrayVLRAJUS[$x][44])+abs($this->mArrayVLRAJUS[$x][47])+abs($this->mArrayVLRAJUS[$x][48]);

            $recebimentosJ2  = abs($this->mArrayVLRAJUS2[$x][155])+abs($this->mArrayVLRAJUS2[$x][165])+abs($this->mArrayVLRAJUS2[$x][164]);
            $impostosVendasJ2 = abs($this->mArrayVLRAJUS2[$x][42])+abs($this->mArrayVLRAJUS2[$x][44])+abs($this->mArrayVLRAJUS2[$x][47])+abs($this->mArrayVLRAJUS2[$x][48]);


            //die;
            $receitaLiquida = $recebimentos - $impostosVendas;
/*
            echo "<pre>recebimentos<br>";
            var_dump( abs($this->mArrayVLRREA[$x][155]),abs($this->mArrayVLRREA[$x][165]),abs($this->mArrayVLRREA[$x][164]));
            echo "<pre>impostosVendas<br>";
            var_dump( abs($this->mArrayVLRREA[$x][42]),abs($this->mArrayVLRREA[$x][44]),abs($this->mArrayVLRREA[$x][47]),abs($this->mArrayVLRREA[$x][48]));

            //var_dump($recebimentos,$impostosVendas,$receitaLiquida);
            //var_dump($recebimentos * 0.045);
            echo "</pre>";
*/
            $receitaLiquida2 =  $receitaLiquida * 0.045;// 0.05;

            $receitaLiquidai = $recebimentosi - $impostosVendasi;
            $receitaLiquida2i = $receitaLiquidai * 0.045;// 0.05;
            $receitaLiquida2i12 = $receitaLiquidai;// * 0.045;// 0.05;

            $receitaLiquidaJ = $recebimentosJ - $impostosVendasJ;
            $receitaLiquida2J = $receitaLiquidaJ * 0.045;// 0.05;

            $receitaLiquidaJ2 = $recebimentosJ2 - $impostosVendasJ2;
            $receitaLiquida2J2 = $receitaLiquidaJ2 * 0.045;// 0.05;

         // echo "<pre> ->receitaliquida:<br/>";

         // var_dump($receitaLiquida,$receitaLiquida2);
            //var_dump($receitaLiquidai,$receitaLiquida2i);
           // var_dump($receitaLiquidaj,$receitaLiquida2j);
           // var_dump($receitaLiquidaj2,$receitaLiquida2j2);
           // var_dump($recebimentos,$impostosVendas,$receitaLiquida,$receitaLiquida2);
         // var_dump($recebimentosi,$impostosVendasi,$receitaLiquidai,$receitaLiquida2i);
        //  echo "</pre>";
//die;
            if($uniGasto == 12){
                $impostosVendas = 0;
                $impostosVendasi = 0;
                $impostosVendasJ = 0;
                $impostosVendasJ2 = 0;
            }



        }


        $rsDeducoes[$x] = DB::select($sqlDeducoes[$x]);
        //$deducao = $rsDeducoes[$x]->fetch();
        $deducao = $rsDeducoes[$x];

        $rsPassivo[$x] = DB::select($sqlPassivo[$x]);
        //$passivo = $rsPassivo[$x]->fetch();
        $passivo = $rsPassivo[$x];

        $valorDeducao = isset($deducao->VALORDEDUCAO) ? $deducao->VALORDEDUCAO : 0;
        $valorDeducao = (float)$valorDeducao;

        $valorPassivo = isset($passivo->VALORPASSIVO) ? $passivo->VALORPASSIVO : 0;
        $valorPassivo = (float)$valorPassivo;

        $this->deducoes[$x] = $valorDeducao;
        $this->deducoesi[$x] = 0;
        $this->deducoesJ[$x] = 0;
        $this->deducoesJ2[$x] = 0;
        $this->deducoesAnual = $this->deducoesAnual + $valorDeducao;


        $this->passivo[$x] = $valorPassivo;
        $this->passivoi[$x] = 0;
        $this->passivoJ[$x] = 0;
        $this->passivoJ2[$x] = 0;
        $this->passivoAnual = $this->passivoAnual + $valorPassivo;


        $rsFatLiqTotal[$x] = DB::select($sqlFatLiqTotal[$x]);

        $tempVLRREA[$x][154][155] = 0;
        $tempVLRREA[$x][154][164] = 0;
        $tempVLRREA[$x][154][218] = 0;
        $tempVLRREA[$x][154][1111] = 0;
        $tempVLRREA[$x][645][42] = 0;
        $tempVLRREA[$x][645][44] = 0;
        $tempVLRREA[$x][645][47] = 0;
        $tempVLRREA[$x][645][48] = 0;
		$tempVLRREA[$x][645][184] = 0;

        $tempVLRINI[$x][154][155] = 0;
        $tempVLRINI[$x][154][164] = 0;
        $tempVLRINI[$x][154][218] = 0;
        $tempVLRINI[$x][154][1111] = 0;
        $tempVLRINI[$x][645][42] = 0;
        $tempVLRINI[$x][645][44] = 0;
        $tempVLRINI[$x][645][47] = 0;
        $tempVLRINI[$x][645][48] = 0;
		$tempVLRINI[$x][645][184] = 0;

        $tempVLRAJUS[$x][154][155] = 0;
        $tempVLRAJUS[$x][154][164] = 0;
        $tempVLRAJUS[$x][154][218] = 0;
        $tempVLRAJUS[$x][154][1111] = 0;
        $tempVLRAJUS[$x][645][42] = 0;
        $tempVLRAJUS[$x][645][44] = 0;
        $tempVLRAJUS[$x][645][47] = 0;
        $tempVLRAJUS[$x][645][48] = 0;
		$tempVLRAJUS[$x][645][184] = 0;

        $tempVLRAJUS2[$x][154][155] = 0;
        $tempVLRAJUS2[$x][154][164] = 0;
        $tempVLRAJUS2[$x][154][218] = 0;
        $tempVLRAJUS2[$x][154][1111] = 0;
        $tempVLRAJUS2[$x][645][42] = 0;
        $tempVLRAJUS2[$x][645][44] = 0;
        $tempVLRAJUS2[$x][645][47] = 0;
        $tempVLRAJUS2[$x][645][48] = 0;
		$tempVLRAJUS2[$x][645][184] = 0;


        $rslEncontro[$x] = DB::select($sqlEncontro[$x]);
        //$encontro = $rslEncontro[$x]->fetch();
        $encontro = $rslEncontro[$x];



        $valorEncontro = isset($encontro->VALORENCONTRO) ? $encontro->VALORENCONTRO : 0;
        $valorEncontro = (float)$valorEncontro;

        $this->arrayVLRIMP[$x][1111] = 0;
        $this->arrayVLRINI[$x][1111] = $valorEncontro;
        $this->arrayVLRREA[$x][1111] = $valorEncontro;
        $this->arrayVLRAJUS[$x][1111] = $valorEncontro;
        $this->arrayVLRAJUS2[$x][1111] = $valorEncontro;

            foreach ($rsFatLiqTotal[$x] as $row){

             //$this->gastosMatriz[$x] = $this->gastosMatriz[$x] + $row->VLRREA;
                $tempVLRREA[$x][$row->SINTET][$row->ANALIT] = $row->VLRREA;
                $tempVLRINI[$x][$row->SINTET][$row->ANALIT] = $row->VLRINI;
                $tempVLRAJUS[$x][$row->SINTET][$row->ANALIT] = $row->VLRAJUS;
                $tempVLRAJUS2[$x][$row->SINTET][$row->ANALIT] = $row->VLRAJUS2;

            }

            $this->fatLiqTotal[$x] = 0;

            $xyz  = array_sum($tempVLRREA[$x][154]);
            $zzz  = array_sum($tempVLRREA[$x][645]);
            $this->fatLiqTotal[$x] = $xyz - abs($zzz);

            $xyzi  = array_sum($tempVLRINI[$x][154]);
            $zzzi  = array_sum($tempVLRINI[$x][645]);
            $this->fatLiqTotali[$x] = $xyzi - abs($zzzi);

            $xyzJ  = array_sum($tempVLRAJUS[$x][154]);
            $zzzJ  = array_sum($tempVLRAJUS[$x][645]);
            $this->fatLiqTotalJ[$x] = $xyzJ - abs($zzzJ);

            $xyzJ2  = array_sum($tempVLRAJUS2[$x][154]);
            $zzzJ2  = array_sum($tempVLRAJUS2[$x][645]);
            $this->fatLiqTotalJ2[$x] = $xyzJ2 - abs($zzzJ2);

        $this->gastosMatriz[$x] = 0;
        $this->gastosMatrizi[$x] = 0;
        $this->gastosMatrizJ[$x] = 0;
        $this->gastosMatrizJ2[$x] = 0;

        $rsMatriz[$x] = DB::select($sqlMatriz[$x]);

            foreach ($rsMatriz[$x] as $row){

             $this->gastosMatriz[$x] = $this->gastosMatriz[$x] + $row->VLRREA;
             $this->gastosMatrizi[$x] = $this->gastosMatrizi[$x] + $row->VLRINI;
             $this->gastosMatrizJ[$x] = $this->gastosMatrizJ[$x] + $row->VLRAJUS;
             $this->gastosMatrizJ2[$x] = $this->gastosMatrizJ2[$x] + $row->VLRAJUS2;

            }

           // var_dump($this->gastosMatriz);

        $rsAll[$x]  = DB::select($sqlAll[$x]);

            foreach ($rsAll[$x] as $row){

             $this->arrayVLRIMP[$x][$row->ANALIT] = $row->VLRIMP;
             $this->arrayVLRINI[$x][$row->ANALIT] = $row->VLRINI;
             $this->arrayVLRREA[$x][$row->ANALIT] = $row->VLRREA;
             $this->arrayVLRAJUS[$x][$row->ANALIT] = $row->VLRAJUS;
             $this->arrayVLRAJUS2[$x][$row->ANALIT] = $row->VLRAJUS2;

             //if($row->SINTET == 646){
              //  dd($row);
             //}

            }
            if($uniGasto == 12){
               $this->arrayVLRREA[$x][165] = $receitaLiquida2;
               $this->arrayVLRINI[$x][165] = $receitaLiquida2i;
               //$this->arrayVLRINI[$x][165] = $receitaLiquida2i12;
               $this->arrayVLRAJUS[$x][165] = $receitaLiquida2J;
               $this->arrayVLRAJUS2[$x][165] = $receitaLiquida2J2;

            }
            /*
            echo "<pre>";
            var_dump($sqlAll[$x]);
            var_dump($this->arrayVLRIMP[0][155]);
            var_dump($this->arrayVLRINI[0][155]);
            var_dump($this->arrayVLRAJUS[0][155]);
            var_dump($this->arrayVLRAJUS2[0][155]);

            var_dump($this->arrayVLRIMP[$x][155]);
            var_dump($this->arrayVLRINI[$x][155]);
            var_dump($this->arrayVLRAJUS[$x][155]);
            var_dump($this->arrayVLRAJUS2[$x][155]);
            echo "</pre>";
            */
            //$tempVLRREA[$x][154][218] = $receitaLiquida2;

        $rsImp[$x] = DB::select($sqlimp[$x]);

            foreach ($rsImp[$x] as $row){

             $this->impNotas[$x] = $row->VLRREA;
             $this->impNotasi[$x] = $row->VLRINI;
             $this->impNotasJ[$x] = $row->VLRAJUS;
             $this->impNotasJ2[$x] = $row->VLRAJUS2;

            }

        //CODCUS = 13 investimenos

        $rsDesp1[$x]  = DB::select($sqldesp1[$x]);

        //echo "<pre>";
        //echo $sqldesp1[$x];
       // echo "</pre>";

            $t=0;
            $ti=0;
            $tJ=0;
            $tJ2=0;
            foreach ($rsDesp1[$x] as $row){
              $t = $t+ abs($row->VLRREA);
              $ti = $ti+ abs($row->VLRINI);
              $tJ = $tJ+ abs($row->VLRAJUS);
              $tJ2 = $tJ2+ abs($row->VLRAJUS2);
            }
            $this->investimentos[$x] = $t;
            $this->investimentosi[$x] = $ti;
            $this->investimentosJ[$x] = $tJ;
            $this->investimentosJ2[$x] = $tJ2;

        //CODCUS = 11 reserva

        $rsDesp2[$x]  = DB::select($sqldesp2[$x]);

       // echo "<pre>";
       // echo $sqldesp2[$x];
       // echo "</pre>";

            $t=0;
            $ti=0;
            $tJ=0;
            $tJ2=0;

            // EXIBE OU NÃO VALORES PARA FUNDO DE RESERVA
            if($this->fr <> null){
                foreach ($rsDesp2[$x] as $row){
                  $t = $t+ abs($row->VLRREA);
                  $ti = $ti+ abs($row->VLRINI);
                  $tJ = $tJ+ abs($row->VLRAJUS);
                  $tJ2 = $tJ2+ abs($row->VLRAJUS2);
                }
            }
             $this->fundoDeReserva[$x] = $t;
             $this->fundoDeReservai[$x] = $ti;
             $this->fundoDeReservaJ[$x] = $tJ;
             $this->fundoDeReservaJ2[$x] = $tJ2;
        }

        for($x=0;$x < $totMes;$x++){

            $v155  = isset($this->arrayVLRREA[$x][155]) ? $this->arrayVLRREA[$x][155] : 0;
            $v165  = isset($this->arrayVLRREA[$x][165]) ? $this->arrayVLRREA[$x][165] : 0;
            $v164  = isset($this->arrayVLRREA[$x][164]) ? $this->arrayVLRREA[$x][164] : 0;

            $v155i  = isset($this->arrayVLRINI[$x][155]) ? $this->arrayVLRINI[$x][155] : 0;
            $v165i  = isset($this->arrayVLRINI[$x][165]) ? $this->arrayVLRINI[$x][165] : 0;
            $v164i  = isset($this->arrayVLRINI[$x][164]) ? $this->arrayVLRINI[$x][164] : 0;

            $v155J  = isset($this->arrayVLRAJUS[$x][155]) ? $this->arrayVLRAJUS[$x][155] : 0;
            $v165J  = isset($this->arrayVLRAJUS[$x][165]) ? $this->arrayVLRAJUS[$x][165] : 0;
            $v164J  = isset($this->arrayVLRAJUS[$x][164]) ? $this->arrayVLRAJUS[$x][164] : 0;

            $v155J2  = isset($this->arrayVLRAJUS2[$x][155]) ? $this->arrayVLRAJUS2[$x][155] : 0;
            $v165J2  = isset($this->arrayVLRAJUS2[$x][165]) ? $this->arrayVLRAJUS2[$x][165] : 0;
            $v164J2  = isset($this->arrayVLRAJUS2[$x][164]) ? $this->arrayVLRAJUS2[$x][164] : 0;
            //$v1111  = isset($this->arrayVLRREA[$x][1111]) ? $this->arrayVLRREA[$x][1111] : 0;

            $v155imp  = isset($this->arrayVLRIMP[$x][155]) ? $this->arrayVLRIMP[$x][155] : 0;
            $v155impi  = isset($this->arrayVLRINI[$x][44]) ? $this->arrayVLRINI[$x][44] : 0;
            $v155impJ  = isset($this->arrayVLRAJUS[$x][44]) ? $this->arrayVLRAJUS[$x][44] : 0;
            $v155impJ2  = isset($this->arrayVLRAJUS2[$x][44]) ? $this->arrayVLRAJUS2[$x][44] : 0;

/*
            echo "<pre>";
            var_dump($v164);
            var_dump($v164J);
            var_dump($v164J2);
            var_dump($v164i);
            echo '-------------<br/>';
            var_dump($v164 * $this->indiceIss * (-1));
            var_dump($v164J * $this->indiceIss * (-1));
            var_dump($v164J2 * $this->indiceIss * (-1));
            var_dump($v164i * $this->indiceIss * (-1));
            echo "</pre>";
            */
            /*
            echo "<pre>";
            var_dump($v155impi);
            var_dump($v155impJ);
            var_dump($v155impJ2);
            var_dump($v155imp);
            echo "</pre>";
            */
            //var_dump($v155);

            $temp = $v155 + $v165 + $v164;
            //dd($v155 , $v165 , $v164);

            $tempi = $v155i + $v165i + $v164i;
            //dd($v155i , $v165i , $v164i);

            $tempJ = $v155J + $v165J + $v164J;
            $tempJ2 = $v155J2 + $v165J2 + $v164J2;

            //var_dump($temp);
            if($this->opcaoview == 'CAIXA' && $this->presumidoreal=='REAL'){

            }elseif($this->opcaoview == 'COMPETENCIA' && $this->presumidoreal=='REAL'){

                $rsImpostosCaixa[$x]  = DB::select($sqlImpostosCaixa[$x]);

                    foreach ($rsImpostosCaixa[$x] as $row){

                        $this->arrayVLRIMP[$x][$row->ANALIT] = $row->VLRIMP;
                        $this->arrayVLRINI[$x][$row->ANALIT] = $row->VLRINI;
                        $this->arrayVLRREA[$x][$row->ANALIT] = $row->VLRREA;
                        $this->arrayVLRAJUS[$x][$row->ANALIT] = $row->VLRAJUS;
                        $this->arrayVLRAJUS2[$x][$row->ANALIT] = $row->VLRAJUS2;

                    }

            }else{

                $iss = $this->calculaIss($x);
                /*
                echo "<pre>";
                var_dump($iss);
                echo "</pre>";
                */
                $this->arrayVLRREA[$x][42] = $temp * $this->indiceConfis *(-1); // confis
                                             //dd($temp,$this->indiceConfis);

                $this->arrayVLRREA[$x][44] = -abs($v155imp);// *(-1); // icms
                $this->arrayVLRREA[$x][47] = $iss[$x]['VLRREA']; // $v164 * $this->indiceIss * (-1); // iss
                $this->arrayVLRREA[$x][48] = $temp * $this->indicePis * (-1); // pis

                $this->arrayVLRINI[$x][42] = $tempi * $this->indiceConfis *(-1); // confis


                //dd($tempi,$this->indiceConfis ,    $this->arrayVLRINI[$x][42]);

                $this->arrayVLRINI[$x][44] = -abs($v155impi);// *(-1); // icms
                $this->arrayVLRINI[$x][47] = $iss[$x]['VLRINI'];//$v164i * $this->indiceIss * (-1); // iss
                $this->arrayVLRINI[$x][48] = $tempi * $this->indicePis * (-1); // pis

                $this->arrayVLRAJUS[$x][42] = $tempJ * $this->indiceConfis *(-1); // confis
                $this->arrayVLRAJUS[$x][44] = -abs($v155impJ);// *(-1); // icms
                $this->arrayVLRAJUS[$x][47] = $iss[$x]['VLRAJUS']; //$v164J * $this->indiceIss * (-1); // iss
                $this->arrayVLRAJUS[$x][48] = $tempJ * $this->indicePis * (-1); // pis

                $this->arrayVLRAJUS2[$x][42] = $tempJ2 * $this->indiceConfis *(-1); // confis
                $this->arrayVLRAJUS2[$x][44] = -abs($v155impJ2);// *(-1); // icms
                $this->arrayVLRAJUS2[$x][47] = $iss[$x]['VLRAJUS2']; //$v164J2 * $this->indiceIss * (-1); // iss
                $this->arrayVLRAJUS2[$x][48] = $tempJ2 * $this->indicePis * (-1); // pis


                $this->calculaIss($x);
            }

            if(isset($this->impNotas[$x]) == null){
               $this->impNotas[$x] = 0;
               $this->impNotasi[$x] = 0;
               $this->impNotasJ[$x] = 0;
               $this->impNotasJ2[$x] = 0;
            }

            $notasImpostas = $this->impNotas[$x];
            $notasImpostasi = $this->impNotasi[$x];
            $notasImpostasJ = $this->impNotasJ[$x];
            $notasImpostasJ2 = $this->impNotasJ2[$x];

            $servicoContribuicao = ($this->impNotas[$x])*($this->contribuicaoServ);
            $servicoContribuicaoi = ($this->impNotasi[$x])*($this->contribuicaoServ);
            $servicoContribuicaoJ = ($this->impNotasJ[$x])*($this->contribuicaoServ);
            $servicoContribuicaoJ2 = ($this->impNotasJ2[$x])*($this->contribuicaoServ);
            //var_dump($rsImp);

            $servico = ($this->impNotas[$x]);
            $servicoi = ($this->impNotasi[$x]);
            $servicoJ = ($this->impNotasJ[$x]);
            $servicoJ2 = ($this->impNotasJ2[$x]);

            $transporteContribuicao = ($v155 + $v164 - $this->impNotas[$x]) * ($this->contribuicaoTrans);
            $transporteContribuicaoi = ($v155i + $v164i - $this->impNotasi[$x]) * ($this->contribuicaoTrans);
            $transporteContribuicaoJ = ($v155J + $v164J - $this->impNotasJ[$x]) * ($this->contribuicaoTrans);
            $transporteContribuicaoJ2 = ($v155J2 + $v164J2 - $this->impNotasJ2[$x]) * ($this->contribuicaoTrans);

            $this->xxxx = "transporte = (".$v155." + ".$v164." - ".$this->impNotas[$x].") * (".$this->contribuicaoTrans.")";
            //var_dump($this->xxxx);
            //var_dump($transporteContribuicao);
            //var_dump($this->impNotas[$x]);

            $transporte = ($v155 + $v164 - $this->impNotas[$x]);
            $transportei = ($v155i + $v164i - $this->impNotasi[$x]);
            $transporteJ = ($v155J + $v164J - $this->impNotasJ[$x]);
            $transporteJ2 = ($v155J2 + $v164J2 - $this->impNotasJ2[$x]);

            $csll = $servicoContribuicao+$transporteContribuicao;
            $cslli = $servicoContribuicaoi+$transporteContribuicaoi;
            $cslli = 0; // ZERA CSLL 43

            /*echo "<pre>";
            var_dump($servicoContribuicaoi);
            var_dump($transporteContribuicaoi);
            echo "</pre>";
            */
            $csllJ = $servicoContribuicaoJ+$transporteContribuicaoJ;
            $csllJ2 = $servicoContribuicaoJ2+$transporteContribuicaoJ2;

            if($this->opcaoview == 'CAIXA' && $this->presumidoreal=='REAL'){

            }elseif($this->opcaoview == 'COMPETENCIA' && $this->presumidoreal=='REAL'){

            }else{
                $this->arrayVLRREA[$x][43] =  -abs($csll); // CSLL
              /*
              $this->arrayVLRREA[$x][43] = $csll; // CSLL
              $this->arrayVLRINI[$x][43] = $cslli; // CSLL
              $this->arrayVLRAJUS[$x][43] = $csllJ; // CSLL
              $this->arrayVLRAJUS2[$x][43] = $csllJ2; // CSLL
              */
            }

            $this->arrayVLRREA[$x][43] = 0; // CSLL
            $this->arrayVLRINI[$x][43] = 0; // CSLL
            $this->arrayVLRAJUS[$x][43] = 0; // CSLL
            $this->arrayVLRAJUS2[$x][43] = 0; // CSLL

            /*
            echo "<pre>";
            var_dump($csll);
            var_dump($cslli);
            var_dump($csllJ);
            var_dump($csllJ2);

            var_dump($this->arrayVLRREA[$x][43]);
            var_dump($this->arrayVLRINI[$x][43]);
            var_dump($this->arrayVLRAJUS[$x][43]);
            var_dump($this->arrayVLRAJUS2[$x][43]);

            echo "</pre>";
            */



            $t = ($transporte * 0.08) + ($servico * 0.32);
            $ti = ($transportei * 0.08) + ($servicoi * 0.32);
            $tJ = ($transporteJ * 0.08) + ($servicoJ * 0.32);
            $tJ2 = ($transporteJ2 * 0.08) + ($servicoJ2 * 0.32);
            //var_dump()

           // $this->irpjTrans = 0.012;
           // $this->irpjServ = 0.048;

            $pt = 0.08;
            $ps = 0.32;
            $pc = 0.10;
            /*echo "<pre>";
              var_dump('transporte:'.$transporte);
              var_dump('servico:'.$servico);
            echo "</pre>";*/
            if($t < 20000){
              $t = 0;

            }else{
              //$t2 = ($transporte * $this->irpjTrans ) + ($servico * $this->irpjServ);
              $t2 = 0;
              $t = (($transporte * $pt) + ($servico * $ps) - 20000) * ($pc);
              $t = $t+$t2;
            }

            if($ti < 20000){
              $ti = 0;

            }else{
              //$t2i = ($transportei * $this->irpjTrans ) + ($servicoi * $this->irpjServ);
              $t2i = 0;
              $ti = (($transportei * $pt) + ($servicoi * $ps) - 20000) * ($pc);
              $ti = $ti+$t2i;
            }

            if($tJ < 20000){
              $tJ = 0;

            }else{
              $tJ = (($transporteJ * $pt) + ($servicoJ * $ps) - 20000) * ($pc);
            }

            if($tJ2 < 20000){
              $tJ2 = 0;

            }else{
              $tJ2 = (($transporteJ2 * $pt) + ($servicoJ2 * $ps) - 20000) * ($pc);
            }



            $irpj = (($transporte*($this->irpjTrans)) + ($servico*($this->irpjServ))) + $t;
            $irpji = (($transportei*($this->irpjTrans)) + ($servicoi*($this->irpjServ))) + $ti;
            $irpjJ = (($transporteJ*($this->irpjTrans)) + ($servicoJ*($this->irpjServ))) + $tJ;
            $irpjJ2 = (($transporteJ2*($this->irpjTrans)) + ($servicoJ2*($this->irpjServ))) + $tJ2;

            $irpj = 0; // ZERA IRPJ
            $irpji = 0; // ZERA IRPJ
            $irpjJ = 0; // ZERA IRPJ
            $irpjJ2 = 0; // ZERA IRPJ

            $this->arrayVLRREA[$x][46] = $irpj; // I.R.P.J.
            $this->arrayVLRINI[$x][46] = $irpji; // I.R.P.J.
            $this->arrayVLRAJUS[$x][46] = $irpjJ; // I.R.P.J.
            $this->arrayVLRAJUS2[$x][46] = $irpjJ2; // I.R.P.J.


            if($this->opcaoview == 'CAIXA' && $this->presumidoreal=='REAL'){

            }elseif($this->opcaoview == 'COMPETENCIA' && $this->presumidoreal=='REAL'){

            }else{

               $irpj = ($this->calculaIRPJ($x));
               $irpj = 0; // ZERA IRPJ

                //$this->arrayVLRREA[$x][46] = -abs($irpj); // I.R.P.J.
               $this->arrayVLRREA[$x][46] = -abs($irpj[$x]); // I.R.P.J.

           // $teste = ($this->calculaIRPJ($x));
           //$irpj = ($this->calculaIRPJ($x));
            //echo "<pre>";
           // var_dump($irpj[$x]);
           /// echo "</pre>";

               // $this->arrayVLRREA[$x][46] = $this->calculaIRPJ($x); // I.R.P.J.


                /*
                $this->arrayVLRINI[$x][46] = $irpji; // I.R.P.J.
                $this->arrayVLRAJUS[$x][46] = $irpjJ; // I.R.P.J.
                $this->arrayVLRAJUS2[$x][46] = $irpjJ2; // I.R.P.J.
                */
              /*
              $this->arrayVLRREA[$x][46] = $irpj; // I.R.P.J.
              $this->arrayVLRINI[$x][46] = $irpji; // I.R.P.J.
              $this->arrayVLRAJUS[$x][46] = $irpjJ; // I.R.P.J.
              $this->arrayVLRAJUS2[$x][46] = $irpjJ2; // I.R.P.J.
               *
               */
            }



            $faturamentoBruto = $v155 + $v165 + $v164;

/*            echo "<pre>";
                var_dump($v155,$v165,$v164,$faturamentoBruto,($faturamentoBruto * ($this->indiceCPRB) * -1));
            echo "---------------------------------------------------------------</pre>";
            echo "<pre>";
                var_dump($v155i,$v165i,$v164i,$faturamentoBrutoi,($faturamentoBrutoi * ($this->indiceCPRB) * -1));
            echo "----------------------------------------------------------------</pre>";
            echo "<pre>";
                var_dump($v155J,$v165J,$v164J,$faturamentoBrutoJ,($faturamentoBrutoJ * ($this->indiceCPRB) * -1));
            echo "----------------------------------------------------------------</pre>";
            echo "<pre>";
                var_dump($v155J2,$v165J2,$v164J2,$faturamentoBrutoJ2,($faturamentoBrutoJ2 * ($this->indiceCPRB) * -1));
            echo "----------------------------------------------------------------</pre>";
*/
            $this->arrayVLRREA[$x][637] = $faturamentoBruto * ($this->indiceCPRB) * -1;

            $faturamentoBrutoi = $v155i + $v165i + $v164i;
            //$this->arrayVLRINI[$x][637] = $faturamentoBrutoi * ($this->indiceCPRB) * -1;

            $faturamentoBrutoJ = $v155J + $v165J + $v164J;
            //$this->arrayVLRAJUS[$x][637] = $faturamentoBrutoJ * ($this->indiceCPRB) * -1;

            $faturamentoBrutoJ2 = $v155J2 + $v165J2 + $v164J2;
            //$this->arrayVLRAJUS2[$x][637] = 0;
            //$this->arrayVLRAJUS2[$x][637] = $faturamentoBrutoJ2 * ($this->indiceCPRB) * -1;


            if($uniGasto == "12"){
                // SE 12 ZERA CPRB
                $this->arrayVLRREA[$x][637]  =  0;
                $this->arrayVLRINI[$x][637]  = 0;
                $this->arrayVLRAJUS[$x][637]  = 0;
                $this->arrayVLRAJUS2[$x][637]  = 0;
               // echo "Entrou";
            }
            //echo "<pre>";
           // var_dump($uniGasto,$this->arrayVLRREA[$x][637],$this->mArrayVLRINI[$x][637],$this->mArrayVLRAJUS[$x][637],$this->mArrayVLRAJUS2[$x][637]   );
           // echo "</pre>";
            /*
            echo "<pre>";
            var_dump($v155J2);
            var_dump($v165J2);
            var_dump($v164J2);
            var_dump($faturamentoBrutoJ2);
            var_dump($faturamentoBrutoJ2* ($this->indiceCPRB) * -1);
            echo "</pre>";
         */
        }


      //  $_SESSION['VLRIMP'] = $this->arrayVLRIMP;
      //  $_SESSION['VLRINI'] = $this->arrayVLRINI;
      //  $_SESSION['VLRREA'] = $this->arrayVLRREA;
    }

    function processaGrupo($codger,$sintet,$grupox){

        //$grp = $this->item[$codger][$sintet];
        $itemId = $this->item[$codger][$sintet]['ID'];
        $itemDesc = $this->item[$codger][$sintet]['DESC'];
        $itemTitulo = $this->item[$codger][$sintet]['TITULO'];

        $chartTitulo = $itemTitulo;
        $chartAno = $this->ano;

        //return $grupo;
        $idChart = $codger.'-'.$sintet;

        $conteudo = '';
        $calculo = 0;
        $orcado = 0;

        $totItem = count($itemId);
        $totMes = count($this->mes);

        $js = array();

        $totalVLRREA = array();
        $totalVLRINI = array();
        $totalVLRIMP = array();
        $totalVLRAJUS = array();
        $totalVLRAJUS2 = array();

        for($x = 0; $x < $totMes; $x++){
          $totalVLRREA[$x] = 0;
          $totalVLRINI[$x] = 0;
          $totalVLRIMP[$x] = 0;
          $totalVLRAJUS[$x] = 0;
          $totalVLRAJUS2[$x] = 0;
        }

        for($y = 0;$y < $totItem; $y++){

                if (is_string($itemDesc[$y])) {
                 $val = ($itemDesc[$y]);
                } else {
                 $val = 'nulo';
                }
            $data['label'] =  $val;
            $conteudo.='<tr class="'.$grupox.'">';
            $conteudo.='<td>'.$val.'</td>';

            $ta = 0;
            $tp = 0;
            $ti = 0;
            $tJ = 0;
            $tJ2 = 0;
            $c = '';
            $a = '';
            $a2 = '';
            $d2 = '';
            $dp2 = '';

            for($x = 0; $x < $totMes; $x++){

                if($sintet == 646){
                   /// print_r($this->arrayVLRINI[$x]);
                    //dump($this->arrayVLRINI[$x][$itemId[$y]]);
                }

                if(is_array($itemId[$y])){
                    $VLRREA  = 0;
                    $VLRIMP  = 0;
                    $VLRINI  = 0;
                    $VLRAJUS = 0;
                    $VLRAJUS2 = 0;
                 }else{
                    $VLRREA  = isset($this->arrayVLRREA[$x][$itemId[$y]]) ? $this->arrayVLRREA[$x][$itemId[$y]] : 0;
                    $VLRIMP  = isset($this->arrayVLRIMP[$x][$itemId[$y]]) ? $this->arrayVLRIMP[$x][$itemId[$y]] : 0;
                    $VLRINI  = isset($this->arrayVLRINI[$x][$itemId[$y]]) ? $this->arrayVLRINI[$x][$itemId[$y]] : 0;
                    $VLRAJUS  = isset($this->arrayVLRAJUS[$x][$itemId[$y]]) ? $this->arrayVLRAJUS[$x][$itemId[$y]] : 0;
                    $VLRAJUS2  = isset($this->arrayVLRAJUS2[$x][$itemId[$y]]) ? $this->arrayVLRAJUS2[$x][$itemId[$y]] : 0;
                }
/*
                $c.='<td>'.number_format($VLRREA, 2, ',', '.').'</td>';
                $c.='<td>'.number_format($VLRINI, 2, ',', '.').'</td>';
                $c.='<td>'.number_format($VLRAJUS, 2, ',', '.').'</td>';
                $c.='<td>'.number_format($VLRAJUS2, 2, ',', '.').'</td>';*/

           $percentual = 0;
           $valperc = 0;

           if($sintet == 154){

                if($this->opcao == 'I'){
                   $orcado = $VLRINI;
                   $c.='<td>'.number_format($VLRINI, 2, ',', '.').'</td>';
                   $calculo = abs($VLRREA) - abs($VLRINI);
                   $valperc = abs($VLRINI);

                }

                if($this->opcao == 'J'){
                   $orcado = $VLRAJUS;
                   $c.='<td>'.number_format($VLRAJUS, 2, ',', '.').'</td>';
                   $calculo =  abs($VLRREA) - abs($VLRAJUS);
                   $valperc = abs($VLRAJUS);
                }

                if($this->opcao == 'J2'){
                   $orcado = $VLRAJUS2;
                   $c.='<td>'.number_format($VLRAJUS2, 2, ',', '.').'</td>';
                   $calculo = abs($VLRREA) - abs($VLRAJUS2);
                   $valperc = abs($VLRAJUS2);
                }

           }else{

                if($this->opcao == 'I'){
                   $orcado = $VLRINI;
                   $c.='<td>'.number_format($VLRINI, 2, ',', '.').'</td>';
                   if($VLRINI < 0){
                        $calculo =  $VLRREA - $VLRINI;  // negativo
                        if($VLRREA > $VLRINI){
                           $calculo = abs($calculo);
                        }
                   }else{
                        $calculo = $VLRREA - $VLRINI; // positivo
                   }
                   $valperc = abs($VLRINI);

                }

                if($this->opcao == 'J'){
                   $orcado = $VLRAJUS;
                   $c.='<td>'.number_format($VLRAJUS, 2, ',', '.').'</td>';
                   if($VLRAJUS < 0){
                        $calculo =  $VLRREA -$VLRAJUS;
                        if($VLRREA > 0){
                           $calculo = abs($calculo);
                        }
                   }else{
                        $calculo = $VLRREA - $VLRAJUS;
                   }
                   $valperc = abs($VLRAJUS);
                }

                if($this->opcao == 'J2'){
                   $c.='<td>'.number_format($VLRAJUS2, 2, ',', '.').'</td>';
                   $orcado = $VLRAJUS2;
                   if($VLRAJUS2 < 0 ){
                        $calculo =  $VLRREA - $VLRAJUS2;
                        if($VLRREA > 0){
                           $calculo = abs($calculo);
                        }
                   }else{
                        $calculo = $VLRREA - $VLRAJUS2;
                   }
                   $valperc = abs($VLRAJUS2);
                }
           }

           if($VLRREA == 0 ){
              $formula = 0;
           }elseif($valperc == 0 && $VLRREA <> 0 ){
              $formula = 100;
           }else{
               //$formula = (1-($valperc / $VLRREA))*100;
               $formula = ($calculo / $valperc)*100;
               //$formula  = $this->calculoPercentualDisp($calculo, $orcado, $VLRREA);
               //$formula = (1-($valperc / $VLRINI))*100;
           }
            $cod1= 0;
            if($sintet == 154){
                $cod1 = 154;
            }

           $formula  = $this->calculoPercentualDisp($calculo, $orcado, $VLRREA, $cod1);

            $c.='<td>'.number_format($VLRREA, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';




                //////////////////////////

                $totalVLRREA[$x] = $totalVLRREA[$x] + $VLRREA;
                $totalVLRINI[$x] = $totalVLRINI[$x] + $VLRINI;
                $totalVLRIMP[$x] = $totalVLRIMP[$x] + $VLRIMP;

                $totalVLRAJUS[$x] = $totalVLRAJUS[$x] + $VLRAJUS;
                $totalVLRAJUS2[$x] = $totalVLRAJUS2[$x] + $VLRAJUS2;


                $ta = $ta+$VLRREA;
                $tp = $tp+$VLRIMP;
                $ti = $ti+$VLRINI;
                $tJ = $tJ+$VLRAJUS;
                $tJ2 = $tJ2+$VLRAJUS2;
            }
            //$c.= '<td></td>';


            $data["value"] = $ta;
            $valt = 0;
            //$conteudo.= '<td>'.number_format($ta, 2, ',', '.').'</td>';

            if($this->opcao == 'I'){
                $valt = ($ti);
                $a.= '<td>'.number_format($valt, 2, ',', '.').'</td>';
            }
            if($this->opcao == 'J'){
                $valt = ($tJ);
                $a.= '<td>'.number_format($valt, 2, ',', '.').'</td>';
            }
            if($this->opcao == 'J2'){
                $valt = ($tJ2);
                $a.= '<td>'.number_format($valt, 2, ',', '.').'</td>';
            }

            $cod = 0;
            if($sintet == 154){
                $cod = 154;
                $disp =  abs($ta) - abs($valt);
            }else{
                $disp = $ta -  $valt ;
            }
            //$disp = abs($ta) - abs($valt);
            if($ta == 0){
                $dispPerc = 0;
            }elseif($valt == 0 && $ta <> 0){
                $dispPerc = 100;
            }else{
                $dispPerc = ( $disp / abs($valt))*100;
            }

            $dispPerc  = $this->calculoPercentualDisp($disp, $valt, $ta, $cod);

/*
            echo "<pre>";
            echo $dispPerc.'='.' ('.$disp.'/'.$valt.')*100';
            echo "</pre>";
*/

            $a2.=  '<td>'.number_format($ta, 2, ',', '.').'</td>';
            $d2.=  '<td>'.number_format($disp, 2, ',', '.').'</td>';
            $dp2.= '<td>'.number_format($dispPerc, 2, ',', '.').'%</td>';

            //$dp2.= '<td>'.$dispPerc.'</td>';

            $conteudo.= $a;
            $conteudo.= $a2;
            $conteudo.= $d2;
            $conteudo.= $dp2;
            $conteudo.= $c;

            array_push($js, $data);

          $conteudo.= '</tr>';
          //dd();
        }

        $this->grupo[$codger][$sintet]['itensGrupo'] = $conteudo;
        $this->grupo[$codger][$sintet]['totalGrupo'] = $totalVLRREA;
        $this->grupo[$codger][$sintet]['totalGrupoi'] = $totalVLRINI;
        $this->grupo[$codger][$sintet]['totalGrupoJ'] = $totalVLRAJUS;
        $this->grupo[$codger][$sintet]['totalGrupoJ2'] = $totalVLRAJUS2;
        $this->grupo[$codger][$sintet]['titulo'] = $itemTitulo;

       //$valorJSON = json_encode($js,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

    }

    public function makeChartLine($codger,$sintet){


        $itemId = $this->item[$codger][$sintet]['ID'];
        $itemDesc = $this->item[$codger][$sintet]['DESC'];
        $itemTitulo = $this->item[$codger][$sintet]['TITULO'];

        $totItem = count($itemId);
        $totMes = count($this->mes);
        $txtJSON = '';
        for($y = 0;$y < $totItem; $y++){

            if($y < $totItem-1){
                $separa_y = ',';
            }else{
                $separa_y = '';
            }

                if (is_string($itemDesc[$y])) {
                 $val = ($itemDesc[$y]);
                } else {
                 $val = 'nulo';
                }


            $txtJSON.='{"seriesname": "'.$val.'","data": [';
            $txtCategoria = '';

            for($x = 0; $x < $totMes; $x++){

                if($x < $totMes -1){
                    $separa_x = ',';
                }else{
                    $separa_x = '';
                }

                if(is_array($itemId[$y])){
                    $VLRREA  = 0;
                    $VLRIMP  = 0;
                    $VLRINI  = 0;
                }else{
                    $VLRREA  = isset($this->arrayVLRREA[$x][$itemId[$y]]) ? $this->arrayVLRREA[$x][$itemId[$y]] : 0;
                    $VLRIMP  = isset($this->arrayVLRIMP[$x][$itemId[$y]]) ? $this->arrayVLRIMP[$x][$itemId[$y]] : 0;
                    $VLRINI  = isset($this->arrayVLRINI[$x][$itemId[$y]]) ? $this->arrayVLRINI[$x][$itemId[$y]] : 0;
                }
                $txtJSON.='{"value":"'.abs($VLRREA).'"}'.$separa_x;


            }
            $xt = 0;
            $listaMes = $this->listaMesAbrev();
            foreach($this->mes AS $idMes){

                if($xt < $totMes -1){
                    $separa_x = ',';
                }else{
                    $separa_x = '';
                }
               $xt++;
               $txtCategoria.='{"label":"'.$listaMes[$idMes].'"}'.$separa_x;
            }

            $txtJSON.=']}'.$separa_y;
        }

    $serie["seriesname"]  = "CONHECIMENTOS";
    $serie["data"] = array();

    $x = array('value'=>'888.88');
    $y = array('value'=>'999.99');


    $dataset[0] = array();
    $dataset[1] = array();

    array_push($serie["data"], $x);
    array_push($serie["data"], $y);

    $vv = json_encode($serie,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

    array_push($dataset[0], $vv);
    array_push($dataset[1], $serie);

        $idChart = $codger.'-'.$sintet;
        $this->chartLine[$idChart] = new FusionCharts("msline", "exl".$idChart, "100%", 300, "chartline".$idChart, "json", '{
    "chart": {
        "caption": "",
        "numberprefix": "R$",
        "decimals": "2",
        "formatNumberScale": "0",
        "decimalSeparator": ",",
        "thousandSeparator": ".",
        "plotgradientcolor": "",
        "bgcolor": "FFFFFF",
        "showalternatehgridcolor": "0",
        "divlinecolor": "CCCCCC",
        "showvalues": "0",
        "showcanvasborder": "0",
        "canvasborderalpha": "0",
        "canvasbordercolor": "CCCCCC",
        "canvasborderthickness": "1",
        "captionpadding": "30",
        "linethickness": "3",
        "yaxisvaluespadding": "15",
        "legendshadow": "0",
        "legendborderalpha": "0",
        "palettecolors": "#f8bd19,#008ee4,#33bdda,#e44a00,#6baa01,#583e78",
        "showborder": "0"
    },
    "categories": [
        {
            "category": ['
                .$txtCategoria.
            ']
        }
    ],
    "dataset":['.$txtJSON.']}');
    }

    public function boxGrupo($codger,$sintet,$grupox){

        $totMes = count($this->mes);
        $conteudo = '';
        $t = 0;
        $ti = 0;
        $tJ = 0;
        $tJ2 = 0;
        $c = "";
        $a = "";
        $to = 0;
        $nome = $this->grupo[$codger][$sintet]['titulo'];
        $total = $this->grupo[$codger][$sintet]['totalGrupo'];
        $totali = $this->grupo[$codger][$sintet]['totalGrupoi'];
        $totalJ = $this->grupo[$codger][$sintet]['totalGrupoJ'];
        $totalJ2 = $this->grupo[$codger][$sintet]['totalGrupoJ2'];
        $gridGrupo = $this->grupo[$codger][$sintet]['itensGrupo'];
        $calculo = 0;
/*
if($codger = 2 && $sintet == 135){
echo '<pre>';
var_dump($nome);
var_dump($total);
var_dump($totali);
var_dump($totalJ);
var_dump($totalJ2);
var_dump($gridGrupo);
echo '</pre>';
}*/


        $conteudo.='<tr class="info">';

            for($x = 0; $x < $totMes;$x++){


         $percentual = 0;
           $valperc = 0;

           if($this->opcao == 'I'){
              $c.='<td>'.number_format($totali[$x], 2, ',', '.').'</td>';

              if($totali[$x] < 0 ){
                $calculo =  $total[$x] - $totali[$x];

                if($total[$x] > 0){
                   $calculo = abs($calculo);
                }

              }else{
                $calculo = $total[$x] - $totali[$x];
              }


              $valperc = abs($totali[$x]);

           }

           if($this->opcao == 'J'){
              $c.='<td>'.number_format($totalJ[$x], 2, ',', '.').'</td>';

              if($totalJ[$x] < 0 ){
                 $calculo =  $total[$x] - $totalJ[$x];

                 if($total[$x] > 0){
                   $calculo = abs($calculo);
                 }

              }else{
                 $calculo = $total[$x] - $totalJ[$x];
              }

              $valperc = abs($totalJ[$x]);
           }

           if($this->opcao == 'J2'){
              $c.='<td>'.number_format($totalJ2[$x], 2, ',', '.').'</td>';
              if($totalJ2[$x] < 0 ){
                 $calculo = $total[$x] - $totalJ2[$x];

                 if($total[$x] > 0){
                   $calculo = abs($calculo);
                 }
              }else{
                 $calculo = $total[$x] - $totalJ2[$x];
              }
              $valperc = abs($totalJ2[$x]);
           }

           //if($valperc == 0 ){
            if($total[$x] == 0){

               $formula = 0;
            }elseif($totalJ2[$x] == 0 && $total[$x] <> 0){
               $formula = 100;
           }else{

               //$formula = (1-($valperc / $total[$x]))*100;
               $formula = ($calculo/$valperc)*100;
           }

           $formula = $this->calculoPercentualDisp($calculo, $valperc, $total[$x]);

            $c.='<td>'.number_format($total[$x], 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';

               /*
              $c.='<td>'.number_format($total[$x], 2, ',', '.').'</td>';
              $c.='<td>'.number_format($totali[$x], 2, ',', '.').'</td>';
              $c.='<td>'.number_format($totalJ[$x], 2, ',', '.').'</td>';
              $c.='<td>'.number_format($totalJ2[$x], 2, ',', '.').'</td>';
              */
            $t = $t+$total[$x];

            if($this->opcao == 'I'){
                $to = $to+$totali[$x];
                $a='<td>'.number_format($to, 2, ',', '.').'</td>';
            }
            if($this->opcao == 'J'){
                $to = $to+$totalJ[$x];
                $a='<td>'.number_format($to, 2, ',', '.').'</td>';
            }
            if($this->opcao == 'J2'){
                $to = $to+$totalJ2[$x];
                $a='<td>'.number_format($to, 2, ',', '.').'</td>';
            }

            }
        //var_dump($to);
        //$totDesp = abs($to)-abs($t);
        $totDesp = $t  - $to;
        //var_dump($to);
        if($t == 0){
          $totDespPerc  = 0;
        }elseif($to  == 0 && $t <> 0){
          $totDespPerc  = 100;
        }else{
          $totDespPerc = ($totDesp/abs($to))*100;
        }
        $totDespPerc = $this->calculoPercentualDisp($totDesp, $to, $t);

        $conteudo.='<td class="'.$grupox.'">'.$nome.' </td>';
        $conteudo.= $a;
        $conteudo.='<td>'.number_format($t, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($totDesp, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($totDespPerc, 2, ',', '.').'%</td>';
        /*
        $conteudo.='<td>'.number_format($ti, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($tJ, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($tJ2, 2, ',', '.').'</td>';
         */
        $conteudo.= $c;
        $conteudo.='</tr>';
        //$conteudo.='<tr>';
        //$conteudo.='<td colspan="'.(6+8).'">';
        $conteudo.= $gridGrupo;
        //$conteudo.='</td>';
        //$conteudo.='</tr>';

        return $conteudo;
    }

    public function boxGrupoReceita($codger,$sintet,$grupox){

        $totMes = count($this->mes);
        $conteudo = '';
        $t = 0;
        $ti = 0;
        $tJ = 0;
        $tJ2 = 0;
        $c = "";
        $a = "";
        $to = 0;
        $nome = $this->grupo[$codger][$sintet]['titulo'];
        $total = $this->grupo[$codger][$sintet]['totalGrupo'];
        $totali = $this->grupo[$codger][$sintet]['totalGrupoi'];
        $totalJ = $this->grupo[$codger][$sintet]['totalGrupoJ'];
        $totalJ2 = $this->grupo[$codger][$sintet]['totalGrupoJ2'];
        $gridGrupo = $this->grupo[$codger][$sintet]['itensGrupo'];
        $calculo = 0;

        $conteudo.='<tr class="info">';

            for($x = 0; $x < $totMes;$x++){


         $percentual = 0;
           $valperc = 0;

           if($this->opcao == 'I'){
              $c.='<td>'.number_format($totali[$x], 2, ',', '.').'</td>';
              $calculo = ($total[$x]) - ($totali[$x]);
              $valperc = abs($totali[$x]);

           }

           if($this->opcao == 'J'){
              $c.='<td>'.number_format($totalJ[$x], 2, ',', '.').'</td>';
              $calculo = ($total[$x]) - ($totalJ[$x]);
              $valperc = abs($totalJ[$x]);
           }

           if($this->opcao == 'J2'){
              $c.='<td>'.number_format($totalJ2[$x], 2, ',', '.').'</td>';
              $calculo = ($total[$x]) - ($totalJ2[$x]);
              $valperc = abs($totalJ2[$x]);
           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               //$formula = (1-($valperc / $total[$x]))*100;
               $formula = ($calculo/$valperc)*100;
           }

            $c.='<td>'.number_format($total[$x], 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';

               /*
              $c.='<td>'.number_format($total[$x], 2, ',', '.').'</td>';
              $c.='<td>'.number_format($totali[$x], 2, ',', '.').'</td>';
              $c.='<td>'.number_format($totalJ[$x], 2, ',', '.').'</td>';
              $c.='<td>'.number_format($totalJ2[$x], 2, ',', '.').'</td>';
              */
            $t = $t+$total[$x];

            if($this->opcao == 'I'){
                $to = $to+$totali[$x];
                $a='<td>'.number_format($to, 2, ',', '.').'</td>';
            }
            if($this->opcao == 'J'){
                $to = $to+$totalJ[$x];
                $a='<td>'.number_format($to, 2, ',', '.').'</td>';
            }
            if($this->opcao == 'J2'){
                $to = $to+$totalJ2[$x];
                $a='<td>'.number_format($to, 2, ',', '.').'</td>';
            }

            }

        $totDesp = ($t) - ($to);

        if($to == 0){
          $totDespPerc  = 0;
        }else{
          $totDespPerc = ($totDesp/abs($to))*100;
        }

        $conteudo.='<td class="'.$grupox.'">'.$nome.' </td>';
        $conteudo.= $a;
        $conteudo.='<td>'.number_format($t, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($totDesp, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($totDespPerc, 2, ',', '.').'%</td>';
        /*
        $conteudo.='<td>'.number_format($ti, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($tJ, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($tJ2, 2, ',', '.').'</td>';
         */
        $conteudo.= $c;
        $conteudo.='</tr>';
        //$conteudo.='<tr>';
       // $conteudo.='<td colspan="'.(6+8).'">';
        $conteudo.= $gridGrupo;
        //$conteudo.='</td>';
       // $conteudo.='</tr>';

        return $conteudo;
    }

    function boxMes($nome, $selMes){

        $arrayMes = $this->listaMes();

        $conteudo = '<label for="mes">Mes</label><select id="mes" name="'.$nome.'[]" data-selected-text-format="count > 1" data-actions-box="true" class="form-control selectpicker" data-none-selected-text="Selecione..." multiple>';
        for($x=1; $x <= 12; $x++){
            if (in_array($x, $selMes)) {
                $conteudo.='<option selected="selected" value="'.$x.'">'.$arrayMes[$x].'</option>';
            }else{
                $conteudo.='<option value="'.$x.'">'.$arrayMes[$x].'</option>';
            }
        }
        $conteudo.='</select>';
        return $conteudo;
    }

    function boxGasto(){

        $gasto = $this->gasto;

        //if($_SESSION['USUTIPO']=='USU'){
            $sqlConf = "SELECT CODCGA FROM USU_CGA  WHERE ID_USUARIO = ".Auth::user()->id;
            $rsConf = DB::select($sqlConf);
            foreach ($rsConf as $rs) {
                $codcga[] = $rs->CODCGA;
            }
            $uniGasto = implode(", ", $codcga);
            ///var_dump($codcga);
            $sqlGasto = "SELECT CODCGA, DESCRI FROM RODCGA WHERE SITUAC = 'A' AND CODCGA IN(".$uniGasto.")";
        ///}else{
        //    $sqlGasto = "SELECT CODCGA, DESCRI FROM RODCGA WHERE SITUAC = 'A' AND CODCGA NOT IN(41)";
        //}
        $rsGasto = DB::select($sqlGasto);

        $tituloGasto = "";
        $sel = '<label for="gasto">Centro de Gasto</label><select name="gasto[]" class="form-control selectpicker" data-selected-text-format="count > 1" data-actions-box="true" data-none-selected-text="Selecione..." multiple>';
        foreach ($rsGasto as $rsG) {
            if(in_array($rsG->CODCGA, $gasto)){
                $tituloGasto = ($rsG->DESCRI);
                $sel.='<option selected="selected" value="'.$rsG->CODCGA.'">'.$rsG->CODCGA.' - '.($rsG->DESCRI).'</option>';
            }else{
                $sel.='<option value="'.$rsG->CODCGA.'">'.$rsG->CODCGA.' - '.($rsG->DESCRI).'</option>';
            }
        }
        $sel.='</select>';
        return $sel;
    }

    function boxUnidade(){

        $unidade = $this->unidade;

        //if($_SESSION['USUTIPO']=='USU'){
            $sqlConf = "SELECT CODUNN FROM USU_UNIDADE  WHERE ID_USUARIO = ".Auth::user()->id;
            $rsConf = DB::select($sqlConf);
            foreach ($rsConf as $rs) {
                $coduni[] = $rs->CODUNN;
            }
            $uni = implode(", ", $coduni);
           // var_dump($uni);


            //$sqlGasto = "SELECT CODCGA, DESCRI FROM RODCGA WHERE SITUAC = 'A' AND CODCGA IN(".$uni.")";
            $sql = "SELECT CODUNN,DESCRI FROM RODUNN WHERE SITUAC = 'A' AND CODUNN IN(".$uni.") ORDER BY CODUNN";
       // }else{
           // $sqlGasto = "SELECT CODCGA, DESCRI FROM RODCGA WHERE SITUAC = 'A' AND CODCGA NOT IN(41)";
        //    $sql = "SELECT CODUNN,DESCRI FROM RODUNN WHERE SITUAC = 'A' ORDER BY CODUNN";
       // }

        $rs = DB::select($sql);

        $sel = '<label for="unidade">Unidade</label><select id="unidade" name="unidade[]" class="form-control selectpicker" data-selected-text-format="count > 1" data-actions-box="true" data-none-selected-text="Selecione..." multiple>';
        foreach ($rs as $rsUni) {
            if(in_array($rsUni->CODUNN, $unidade)){
                $tituloGasto = ($rsUni->DESCRI);
                $sel.='<option selected="selected" value="'.$rsUni->CODUNN.'">'.$rsUni->CODUNN.' - '.($rsUni->DESCRI).'</option>';
            }else{
                $sel.='<option value="'.$rsUni->CODUNN.'">'.$rsUni->CODUNN.' - '.($rsUni->DESCRI).'</option>';
            }
        }
        $sel.='</select>';
        return $sel;
    }

    function boxFiltro($pagina,$filtroAno,$filtroMes,$filtroGasto,$filtroUnidade,$nomeFiltro){

        $ativaOpt1 = '';
        $ativaOpt2 = '';
        $ativaOpt3 = '';
        $ativaOpt4 = '';
        $ativaOpt5 = '';
        $erro = false;
        $txtAno = '';
        $txtMes = '';
        $txtGasto = '';
        $txtErro = '';

        if($this->ano == ""){
           $txtAno = 'Informe o ANO, ';
           $erro = true;
        }

        if(count($this->mes) == 0){
           $txtMes = 'Informe um Mes, ';
           $erro = true;
        }

        if(count($this->gasto) == 0){
           $txtGasto = 'Informe um Centro de Gasto.';
           $erro = true;
        }

        if($this->busca <> NULL && $erro == true){
            $txtErro ='<div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          Por favor, '.$txtAno.$txtMes.$txtGasto.
          '</div>';
        }
        $ativaCheck = '';
        $ativaCheckFR = '';
        if($this->ac <> NULL){
            $ativaCheck ='checked="checked"';
        }
        if($this->fr <> NULL){
            $ativaCheckFR ='checked="checked"';
        }
        if($this->opcaoview == "COMPETENCIA"){
            $ativaOpt1 ='checked="checked"';
        }else{
            $ativaOpt2 ='checked="checked"';
        }

        if($this->opcao == "J"){
            $ativaOpt3 ='checked="checked"';
            $tit = 'JUS';
        }elseif($this->opcao == "J2"){
            $ativaOpt4 ='checked="checked"';
            $tit = 'JUS2';
        }else{
            $ativaOpt5 ='checked="checked"';
            $tit = 'INI';
        }

            $addFiltro = '';

        $url ='previsaoorcamentaria/dre';

        $formFiltro ='<form action="'.route($url).'" method="post">
                        <input type="hidden" name="busca" value="busca">
                        '.csrf_field().'
                        <div class="row">
                          <div class="col-xs-1">
                          '.$filtroAno.'
                          </div>

                          <div class="col-xs-2">
                            '.$filtroMes.
                         ' </div>

                          <div class="col-xs-4">
                            '.$filtroGasto.
                          '</div>

                          <div class="col-xs-4">
                            '.$filtroUnidade.
                          '</div>

                          <div class="col-xs-1">
                          <label for="gasto">Açao</label>
                          <button id="submit" name="submit" type="submit" class="btn btn-success form-control">Filtrar</button>
                          </div>
                            <input type="hidden" name="opcaoview" id="optionsRadios1" value="COMPETENCIA">
                            <input type="hidden" name="opcao" id="optionsRadios4" value="I">

                    </form>';

        $conteudo ='<div class="box">
                        <div class="box-body">'
                          .$formFiltro.
                        '</div><!-- /.box-body -->
                    </div><!-- /.box -->
                    '.$txtErro;

    return $conteudo;
    }

    function somaGrupo($nome,$arrayGrupo){

        $tot = count($arrayGrupo);
        $totMes = count($mes);

        for($x = 0; $x < $totMes; $x++){
          $total[$mes[$x]] = 0;
        }

        for($x = 0;$x < $tot; $x++ ){
            for($y = 0;$y < $totMes; $y++ ){
              $total[$mes[$y]] =  $total[$mes[$y]] + ($arrayGrupo[$x][$mes[$y]]);
            }
        }

        $conteudo = '<tr style="background-color: #aaa; color:#000;">';
        $conteudo.= '<th>'.$nome.'</th>';
        $conteudo.= '<th>'.number_format(array_sum($total), 2, ',', '.').'</th>';

        for($x = 0; $x < $totMes; $x++){
          /// $conteudo.='<th>'.number_format($total[$mes[$x]], 2, ',', '.').'</th>';
        }
        $conteudo.= '';

        return $conteudo;
    }

    function somaGrupoGerencial($codGerencial){

        $grupo = $this->grupo[$codGerencial];
        $totMes = count($this->mes);

        for($y = 0 ; $y < $totMes;$y++){
           $this->somaGrupo[$codGerencial][$y] = 0;
           $this->somaGrupoi[$codGerencial][$y] = 0;
           $this->somaGrupoJ[$codGerencial][$y] = 0;
           $this->somaGrupoJ2[$codGerencial][$y] = 0;
        }

        foreach($grupo as $key => $value)
        {
            for($y = 0 ; $y < $totMes;$y++){
               //var_dump($grupo[$key]['totalGrupo'][$y]);
               $this->somaGrupo[$codGerencial][$y] = $this->somaGrupo[$codGerencial][$y] + $grupo[$key]['totalGrupo'][$y];
               $this->somaGrupoi[$codGerencial][$y] = $this->somaGrupoi[$codGerencial][$y] + $grupo[$key]['totalGrupoi'][$y];
               $this->somaGrupoJ[$codGerencial][$y] = $this->somaGrupoJ[$codGerencial][$y] + $grupo[$key]['totalGrupoJ'][$y];
               $this->somaGrupoJ2[$codGerencial][$y] = $this->somaGrupoJ2[$codGerencial][$y] + $grupo[$key]['totalGrupoJ2'][$y];

            }

        }
        //echo "<pre>";
        //var_dump($this->grupo[0]);
        //$this->grupo[0][154]['totalGrupo'];
        //var_dump($this->somaGrupo);
        //echo "</pre>";
    }

    function boxSomaGrupo($codGerencial,$titulo){

        $grupo = $this->somaGrupo[$codGerencial];
        $grupoi = $this->somaGrupoi[$codGerencial];
        $grupoJ = $this->somaGrupoJ[$codGerencial];
        $grupoJ2 = $this->somaGrupoJ2[$codGerencial];
        $tot = 0;
        $a = $this->passivo;
        $b = $this->passivoi;
        $c = $this->passivoJ;
        $d = $this->passivoJ2;
        $calculo = 0;

        $a = array_map(function($el) { return $el * -1; }, $a);
        $b = array_map(function($el) { return $el * -1; }, $b);
        $c = array_map(function($el) { return $el * -1; }, $c);
        $d = array_map(function($el) { return $el * -1; }, $d);

        if($codGerencial == 4){

            $grupo = array_merge($this->somaGrupo[$codGerencial],$a);
            $grupoi = array_merge($this->somaGrupoi[$codGerencial],$b);
            $grupoJ = array_merge($this->somaGrupoJ[$codGerencial],$c);
            $grupoJ2 = array_merge($this->somaGrupoJ2[$codGerencial],$d);

        }

        if($grupo == null){
            $valGrupo = 0;
        }else{
            $valGrupo = array_sum($grupo);
        }

        $totMes = count($this->mes);
        $conteudo= '<tr class="active" style="font-weight:bold; color:#000;">';
        $conteudo.= '<td>'.$titulo.'</td>';

        if($this->opcao == 'I'){
            $conteudo.= '<td>'.number_format(array_sum($grupoi), 2, ',', '.').'</td>';
            $tot = array_sum($grupoi);
        }

        if($this->opcao == 'J'){
            $conteudo.= '<td>'.number_format(array_sum($grupoJ), 2, ',', '.').'</td>';
            $tot = array_sum($grupoJ);

            if($codGerencial == 5){
                $this->valDespesasOp = array_sum($grupoJ);
            }

            if($codGerencial == 6){
                $this->valDespesasAdm = array_sum($grupoJ);
            }

        }

        if($this->opcao == 'J2'){
            $conteudo.= '<td>'.number_format(array_sum($grupoJ2), 2, ',', '.').'</td>';
            $tot = array_sum($grupoJ2);
        }

        //$totDisp =  abs($tot) - abs($valGrupo);
        $totDisp = $this->calcDisp($tot, $valGrupo);
        $totDisp = $this->calcDisp($valGrupo ,$tot);
        //$totDisp =  $tot - $valGrupo;

        if($tot == 0){
            $topDispPerc = 0;
        }else{
            $topDispPerc = ($totDisp / abs($tot))*100;
            //$topDispPerc = ($totDisp / $tot)*100;
        }

        $topDispPerc = $this->calculoPercentualDisp($totDisp, $tot, $valGrupo);

        $conteudo.= '<td>'.number_format($valGrupo, 2, ',', '.').'</td>';
        $conteudo.= '<td>'.number_format($totDisp, 2, ',', '.').'</td>';
        $conteudo.= '<td>'.number_format($topDispPerc, 2, ',', '.').'%</td>';

        for($y = 0 ; $y < $totMes;$y++){
           $valy = $grupo[$y];
           $valyi = $grupoi[$y];
           $valyJ = $grupoJ[$y];
           $valyJ2 = $grupoJ2[$y];

           if($codGerencial == 4){
            $valy = $grupo[$y] + $a[$y];
            $valyi = $grupoi[$y] + $b[$y];
            $valyJ = $grupoJ[$y] + $c[$y];
            $valyJ2 = $grupoJ2[$y] + $d[$y];
           }

           $percentual = 0;
           $valperc = 0;

           if($this->opcao == 'I'){
              $conteudo.='<td>'.number_format($valyi, 2, ',', '.').'</td>';
              $calculo = $this->calcDisp($valyi, $valy);
              $calculo = $this->calcDisp($valy ,$valyi );
              $valperc = abs($valyi);

           }

           if($this->opcao == 'J'){
              $conteudo.='<td>'.number_format($valyJ, 2, ',', '.').'</td>';
              //$calculo = abs($valyJ) - abs($valy);
              $calculo = $this->calcDisp($valyJ, $valy);
              $calculo = $this->calcDisp($valy ,$valyJ);
              //$calculo = $valyJ - $valy;
              //$calculo = abs($valy) - abs($valyJ);
              $valperc = abs($valyJ);
              //$valperc = $valyJ;
           }

           if($this->opcao == 'J2'){
              $conteudo.='<td>'.number_format($valyJ2, 2, ',', '.').'</td>';
              $calculo = $this->calcDisp($valyJ2, $valy);
              $calculo = $this->calcDisp($valy,$valyJ2);
              $orcado = $valyJ2;
              $valperc = abs($valyJ2);

           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = ($calculo / $valperc)*100;
           }


            $formula = $this->calculoPercentualDisp($calculo, $valperc, $valy);

            $conteudo.='<td>'.number_format($valy, 2, ',', '.').'</td>';
            $conteudo.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $conteudo.='<td>'.number_format($formula, 2, ',', '.').'</td>';

        }

        $conteudo.='</tr>';
        return $conteudo;
    }

    function boxSomaGrupoReceita($codGerencial,$titulo){

        $grupo = $this->somaGrupo[$codGerencial];
        $grupoi = $this->somaGrupoi[$codGerencial];
        $grupoJ = $this->somaGrupoJ[$codGerencial];
        $grupoJ2 = $this->somaGrupoJ2[$codGerencial];
        $tot = 0;
        $calculo = 0;

        if($grupo == null){
            $valGrupo = 0;
        }else{
            $valGrupo = array_sum($grupo);
        }

        $totMes = count($this->mes);

        $conteudo= '<tr class="active" style="font-weight:bold; color:#000;">';
        $conteudo.= '<td>'.$titulo.'</td>';

        if($this->opcao == 'I'){
            $conteudo.= '<td>'.number_format(array_sum($grupoi), 2, ',', '.').'</td>';
            $tot = array_sum($grupoi);
        }

        if($this->opcao == 'J'){
            $conteudo.= '<td>'.number_format(array_sum($grupoJ), 2, ',', '.').'</td>';
            $tot = array_sum($grupoJ);
        }

        if($this->opcao == 'J2'){
            $conteudo.= '<td>'.number_format(array_sum($grupoJ2), 2, ',', '.').'</td>';
            $tot = array_sum($grupoJ2);
        }

        $totDisp = $valGrupo - $tot;

        if($tot == 0){
            $topDispPerc = 0;
        }else{
            $topDispPerc = ($totDisp / $tot)*100;
        }


        $conteudo.= '<td>'.number_format($valGrupo, 2, ',', '.').'</td>';
        $conteudo.= '<td>'.number_format($totDisp, 2, ',', '.').'</td>';
        $conteudo.= '<td>'.number_format($topDispPerc, 2, ',', '.').'%</td>';

        for($y = 0 ; $y < $totMes;$y++){
           $valy = $grupo[$y];
           $valyi = $grupoi[$y];
           $valyJ = $grupoJ[$y];
           $valyJ2 = $grupoJ2[$y];

           $percentual = 0;
           $valperc = 0;

           if($this->opcao == 'I'){
              $conteudo.='<td>'.number_format($valyi, 2, ',', '.').'</td>';
              $calculo = abs($valy) - abs($valyi);
              $valperc = abs($valyi);

           }

           if($this->opcao == 'J'){
              $conteudo.='<td>'.number_format($valyJ, 2, ',', '.').'</td>';
              $calculo = abs($valy) - abs($valyJ);
              $valperc = abs($valyJ);
           }

           if($this->opcao == 'J2'){
              $conteudo.='<td>'.number_format($valyJ2, 2, ',', '.').'</td>';
              $calculo =  abs($valy) - abs($valyJ2);
              $valperc = abs($valyJ2);
           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = ($calculo / $valperc)*100;
           }

            $conteudo.='<td>'.number_format($valy, 2, ',', '.').'</td>';
            $conteudo.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $conteudo.='<td>'.number_format($formula, 2, ',', '.').'%</td>';
          // }
        }

        $conteudo.='</tr>';
        return $conteudo;
    }

    function boxSomaGrupoPositivo($codGerencial,$titulo){

        $grupo = $this->somaGrupo[$codGerencial];
        $grupoi = $this->somaGrupoi[$codGerencial];
        $grupoJ = $this->somaGrupoJ[$codGerencial];
        $grupoJ2 = $this->somaGrupoJ2[$codGerencial];
        $valGrupo = array_sum($grupo);
        $calculo = 0;


        $totMes = count($this->mes);
        $conteudo ='';
        $conteudo.= '<tr style="background-color: #ccc; color:#000;">';
        $conteudo.= '<th>'.$titulo.'</th>';

        if($this->opcao == 'I'){
            $conteudo.= '<th>'.number_format(abs(array_sum($grupoi)), 2, ',', '.').'</th>';
            $tot = array_sum($grupoi);
        }

        if($this->opcao == 'J'){
            $conteudo.= '<th>'.number_format(abs(array_sum($grupoJ)), 2, ',', '.').'</th>';
            $tot = array_sum($grupoJ);
        }

        if($this->opcao == 'J2'){
            $conteudo.= '<th>'.number_format(abs(array_sum($grupoJ2)), 2, ',', '.').'</th>';
            $tot = array_sum($grupoJ2);
        }

        $totDisp = $tot - $valGrupo;

        if(array_sum($grupo) == 0){
            $topDispPerc = 0;
        }else{
            $topDispPerc = ($totDisp / $tot)*100;
        }

        $conteudo.= '<th>'.number_format(abs($valGrupo), 2, ',', '.').'</th>';
        $conteudo.= '<th>'.number_format(abs($totDisp), 2, ',', '.').'</th>';
        $conteudo.= '<th>'.number_format(abs($topDispPerc), 2, ',', '.').'%</th>';

        for($y = 0 ; $y < $totMes;$y++){
           $valy = $grupo[$y];
           $valyi = $grupoi[$y];
           $valyJ = $grupoJ[$y];
           $valyJ2 = $grupoJ2[$y];

           $percentual = 0;
           $valperc = 0;

           if($this->opcao == 'I'){
              $conteudo.='<th>'.number_format(abs($valyi), 2, ',', '.').'</th>';
              $calculo = abs($valyi) - abs($valy);
              $valperc = abs($valyi);

           }

           if($this->opcao == 'J'){
              $conteudo.='<th>'.number_format(abs($valyJ), 2, ',', '.').'</th>';
              $calculo = abs($valyJ) - abs($valy);
              $valperc = abs($valyJ);
           }

           if($this->opcao == 'J2'){
              $conteudo.='<th>'.number_format(abs($valyJ2), 2, ',', '.').'</th>';
              $calculo = abs($valyJ2) - abs($valy);
              $valperc = abs($valyJ2);
           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = ($calculo / $valperc)*100;
           }

            $conteudo.='<th>'.number_format(abs($valy), 2, ',', '.').'</th>';
            $conteudo.='<th>'.number_format(abs($calculo), 2, ',', '.').'</th>';
            $conteudo.='<th>'.number_format(abs($formula), 2, ',', '.').'%</th>';

        }

        $conteudo.='</th>';
        return $conteudo;

    }

    function calculaFormulas(){
        $uniGasto = implode(", ", $this->gasto);
        // =Recebimentos - Impostos s/ vendas (Grupo 1) - deduções sobre receita
        $x = 0; // recebimetos
        $y = 1; // impostos / vendas
        $z = 9;

        $grupox = $this->somaGrupo[$x];
        $grupoy = $this->somaGrupo[$y];
        $grupoz = $this->somaGrupo[$z];

        $grupoxi = $this->somaGrupoi[$x];
        $grupoyi = $this->somaGrupoi[$y];
        $grupozi = $this->somaGrupoi[$z];



        $grupoxJ = $this->somaGrupoJ[$x];
        $grupoyJ = $this->somaGrupoJ[$y];
        $grupozJ = $this->somaGrupoJ[$z];

        $grupoxJ2 = $this->somaGrupoJ2[$x];
        $grupoyJ2 = $this->somaGrupoJ2[$y];
        $grupozJ2 = $this->somaGrupoJ2[$z];

        $totMes = count($this->mes);

        for($k = 0 ; $k < $totMes;$k++){


            $val = $grupox[$k] - abs($grupoy[$k]) - ($this->deducoes[$k]);
            $vali = $grupoxi[$k] - (abs($grupoyi[$k]) + abs($grupozi[$k])) - ($this->deducoesi[$x]);

           // if($request->debug == 'ok'){
              //  dd($grupoxi[$k],($grupoyi[$k]),$grupozi[$k],$vali);


           // dd($grupoxi[$k],$grupoyi[$k],$this->deducoesi[$x]);

            $valJ = $grupoxJ[$k] - abs($grupoyJ[$k]) - ($this->deducoesJ[$x]);
            $valJ2 = $grupoxJ2[$k] - abs($grupoyJ2[$k]) - ($this->deducoesJ2[$x]);

            $this->receitaOpLiquida[$k] = $val; // armazena -> RECEITA OPERACIONAL LIQUIDA
            $this->receitaOpLiquidai[$k] = $vali; // armazena -> RECEITA OPERACIONAL LIQUIDA

            $this->receitaOpLiquidaJ[$k] = $valJ; // armazena -> RECEITA OPERACIONAL LIQUIDA
            $this->receitaOpLiquidaJ2[$k] = $valJ2; // armazena -> RECEITA OPERACIONAL LIQUIDA

        }



        $g2 = 2;

        $grupo2 = $this->somaGrupo[$g2];
        $grupo2i = $this->somaGrupoi[$g2];
        $grupo2J = $this->somaGrupoJ[$g2];
        $grupo2J2 = $this->somaGrupoJ2[$g2];

        $t1 = 0;
        $t2 = 0;

        $t1i = 0;
        $t2i = 0;

        $t1J = 0;
        $t2J = 0;

        $t1J2 = 0;
        $t2J2 = 0;


        for($k = 0 ; $k < $totMes;$k++){

            if($this->receitaOpLiquida[$k] == 0){
                $val = 0;
            }else{
                $val = abs($grupo2[$k]) / $this->receitaOpLiquida[$k];

            }

            if($this->receitaOpLiquidai[$k] == 0){
                $vali = 0;
            }else{
                $vali = abs($grupo2i[$k]) / $this->receitaOpLiquidai[$k];
            }

            if($this->receitaOpLiquidaJ[$k] == 0){
                $valJ = 0;
            }else{
                $valJ = abs($grupo2J[$k]) / $this->receitaOpLiquidaJ[$k];
            }

            if($this->receitaOpLiquidaJ2[$k] == 0){
                $valJ = 0;
            }else{
                $valJ2 = abs($grupo2J2[$k]) / $this->receitaOpLiquidaJ2[$k];
            }

            $this->margemContribuicao[$k] = $val; // armazena ->  ( % ) MARGEM DE CONTRIBUIÇÃO
            $t1 = $t1+abs($grupo2[$k]);
            $t2 = $t2+$this->receitaOpLiquida[$k];

            $this->margemContribuicaoi[$k] = $vali; // armazena ->  ( % ) MARGEM DE CONTRIBUIÇÃO
            $t1i = $t1i+abs($grupo2i[$k]);
            $t2i = $t2i+$this->receitaOpLiquidai[$k];

            $this->margemContribuicaoJ[$k] = $valJ; // armazena ->  ( % ) MARGEM DE CONTRIBUIÇÃO
            $t1J = $t1J+abs($grupo2J[$k]);
            $t2J = $t2J+$this->receitaOpLiquidaJ[$k];

            $this->margemContribuicaoJ2[$k] = $valJ2; // armazena ->  ( % ) MARGEM DE CONTRIBUIÇÃO
            $t1J2 = $t1J2+abs($grupo2J2[$k]);
            $t2J2 = $t2J2+$this->receitaOpLiquidaJ2[$k];
        }
        if($t2 ==0){
            $this->margemContribuicaoAnual =  0;
        }else{
            $this->margemContribuicaoAnual = $t1 / $t2; //aramazena ( % ) MARGEM DE CONTRIBUIÇÃO anual
        }
        if($t2i ==0){
            $this->margemContribuicaoAnuali =  0;
        }else{
            $this->margemContribuicaoAnuali = $t1i / $t2i; //aramazena ( % ) MARGEM DE CONTRIBUIÇÃO anual
        }
        if($t2J ==0){
            $this->margemContribuicaoAnualJ =  0;
        }else{
            $this->margemContribuicaoAnualJ = $t1J / $t2J; //aramazena ( % ) MARGEM DE CONTRIBUIÇÃO anual
        }
        if($t2J2 ==0){
            $this->margemContribuicaoAnualJ2 =  0;
        }else{
            $this->margemContribuicaoAnualJ2 = $t1J2 / $t2J2; //aramazena ( % ) MARGEM DE CONTRIBUIÇÃO anual
        }

        // CALCULA LUCRO BRUTO
        $t = 0;
        $tp = 0;

        $ti = 0;
        $tpi = 0;

        $tJ = 0;
        $tpJ = 0;

        $tJ2 = 0;
        $tpJ2 = 0;

        for($k = 0 ; $k < $totMes;$k++){

            $val = $this->receitaOpLiquida[$k] + $this->somaGrupo[2][$k] +  $this->somaGrupo[3][$k] + $this->somaGrupo[4][$k];

            $val = $val + $this->passivo[$k]*(-1);
            $this->lucroBruto[$k] = $val;

            //$vali = $this->receitaOpLiquidai[$k] - abs($this->somaGrupoi[2][$k]) - abs($this->somaGrupoi[3][$k]) -  abs($this->somaGrupoi[4][$k]);
            $vali = $this->receitaOpLiquidai[$k] + $this->somaGrupoi[2][$k] + $this->somaGrupoi[3][$k] + $this->somaGrupoi[4][$k];

            $vali = $vali + $this->passivoi[$k]*(-1);
            $this->lucroBrutoi[$k] = $vali;

            //$valJ = $this->receitaOpLiquidaJ[$k] - abs($this->somaGrupoJ[2][$k]) -  abs($this->somaGrupoJ[3][$k]) -  abs($this->somaGrupoJ[4][$k]);
            $valJ = $this->receitaOpLiquidaJ[$k] + $this->somaGrupoJ[2][$k] + $this->somaGrupoJ[3][$k] + $this->somaGrupoJ[4][$k];

            $valJ = $valJ + $this->passivoJ[$k]*(-1);
            $this->lucroBrutoJ[$k] = $valJ;

            $valJ2 = $valJ2 + $this->passivoJ2[$k]*(-1);
            $valJ2 = $this->receitaOpLiquidaJ2[$k] + $this->somaGrupoJ2[2][$k] + $this->somaGrupoJ2[3][$k] + $this->somaGrupoJ2[4][$k];
            $this->lucroBrutoJ2[$k] = $valJ2;

            if($this->receitaOpLiquida[$k] == 0){
                $valPerc = 0;
            }else{
                $valPerc = $val /  $this->receitaOpLiquida[$k];
            }

            if($this->receitaOpLiquidai[$k] == 0){
                $valPerci = 0;
            }else{
                $valPerci = $vali /  $this->receitaOpLiquidai[$k];
            }

            if($this->receitaOpLiquidaJ[$k] == 0){
                $valPercJ = 0;
            }else{
                $valPercJ = $valJ /  $this->receitaOpLiquidaJ[$k];
            }

            if($this->receitaOpLiquidaJ2[$k] == 0){
                $valPercJ2 = 0;
            }else{
                $valPercJ2 = $valJ2 /  $this->receitaOpLiquidaJ2[$k];
            }

            $this->lucroBrutoPerc[$k] = $valPerc;
            $t = $t + $val;
            $tp = $tp + $this->receitaOpLiquida[$k];

            $this->lucroBrutoPerci[$k] = $valPerci;
            $ti = $ti + $vali;
            $tpi = $tpi + $this->receitaOpLiquidai[$k];

            $this->lucroBrutoPercJ[$k] = $valPercJ;
            $tJ = $tJ + $valJ;
            $tpJ = $tpJ + $this->receitaOpLiquidaJ[$k];

            $this->lucroBrutoPercJ2[$k] = $valPercJ2;
            $tJ2 = $tJ2 + $valJ2;
            $tpJ2 = $tpJ2 + $this->receitaOpLiquidaJ2[$k];
        }

        if($tp ==0){
            $this->lucroBrutoAnualPerc = 0;
        }else{
            $this->lucroBrutoAnualPerc = $t / $tp;
        }

        if($tpi ==0){
            $this->lucroBrutoAnualPerci = 0;
        }else{
            $this->lucroBrutoAnualPerci = $ti / $tpi;
        }

        if($tpJ ==0){
            $this->lucroBrutoAnualPercJ = 0;
        }else{
            $this->lucroBrutoAnualPercJ = $tJ / $tpJ;
        }

        if($tpJ2 ==0){
            $this->lucroBrutoAnualPercJ2 = 0;
        }else{
            $this->lucroBrutoAnualPercJ2 = $tJ2 / $tpJ2;
        }

        $this->lucroBrutoAnual = $t;
        $this->lucroBrutoAnuali = $ti;
        $this->lucroBrutoAnualJ = $tJ;
        $this->lucroBrutoAnualJ2 = $tJ2;


       //CALCULA ABITDA
        $t = 0;
        $ti = 0;
        $tJ = 0;
        $tJ2 = 0;

        $t_VEI = 0;
        $ti_VEI = 0;
        $tJ_VEI = 0;
        $tJ2_VEI = 0;


        for($k = 0 ; $k < $totMes;$k++){

            $val =  ($this->lucroBruto[$k]) + (($this->somaGrupo[5][$k])  + ($this->somaGrupo[6][$k]));

            $this->ABITDA[$k] = ($val);
            $t = $t+$val;

            $this->ABITDA_VEI[$k] = ($val) - abs($this->somaGrupo[14][$k]);
            $t_VEI = $t+$val - abs($this->somaGrupo[14][$k]);
            //--------------------------------------
            $vali =  ($this->lucroBrutoi[$k]) + (($this->somaGrupoi[5][$k]) + ($this->somaGrupoi[6][$k]));

            $this->ABITDAi[$k] = ($vali);
            //dump($this->lucroBrutoi[$k]);
            //dump($this->somaGrupoi[5][$k]);
            //dump($this->somaGrupoi[6][$k]);
            //dd();
            $ti = $ti+$vali;

            $this->ABITDAi_VEI[$k] = ($vali) - abs($this->somaGrupoi[14][$k]);
            $ti_VEI = $ti+$vali - abs($this->somaGrupoi[14][$k]);
            // --------------------------------------
            $valJ =  ($this->lucroBrutoJ[$k]) + (($this->somaGrupoi[5][$k]) + ($this->somaGrupoJ[6][$k]));

            $this->ABITDAJ[$k] = ($valJ);
            $tJ = $tJ+$valJ;

            $this->ABITDAJ_VEI[$k] = ($valJ) - abs($this->somaGrupoJ[14][$k]);
            $tJ_VEI = $tJ+$valJ - abs($this->somaGrupoJ[14][$k]);
            //-----------------------------------------
            $valJ2 =  ($this->lucroBrutoJ2[$k]) + (($this->somaGrupoJ2[5][$k]) + ($this->somaGrupoJ2[6][$k]));

            $this->ABITDAJ2[$k] = ($valJ2);
            $tJ2 = $tJ2+$valJ2;

            $this->ABITDAJ2_VEI[$k] = ($valJ2) - abs($this->somaGrupoJ2[14][$k]);
            $tJ2_VEI = $tJ2+$valJ2 - abs($this->somaGrupoJ2[14][$k]);

        }

        $this->ABITDAAnual = $t;
        $this->ABITDAAnuali = $ti;
        $this->ABITDAAnualJ = $tJ;
        $this->ABITDAAnualJ2 = $tJ2;

        $this->ABITDAAnual_VEI = $t_VEI;
        $this->ABITDAAnuali_VEI = $ti_VEI;
        $this->ABITDAAnualJ_VEI = $tJ_VEI;
        $this->ABITDAAnualJ2_VEI = $tJ2_VEI;

        //CALCULA ABITDA %
        $t1=0;
        $t2=0;
        $t1i=0;
        $t2i=0;
        $t1J=0;
        $t2J=0;
        $t1J2=0;
        $t2J2=0;


        $t1_VEI=0;
        $t2_VEI=0;
        $t1i_VEI=0;
        $t2i_VEI=0;
        $t1J_VEI=0;
        $t2J_VEI=0;
        $t1J2_VEI=0;
        $t2J2_VEI=0;

        for($k = 0 ; $k < $totMes;$k++){

            if($this->receitaOpLiquida[$k] == 0){
                $this->ABITDAPerc[$k] = 0;
            }else{
                $this->ABITDAPerc[$k] = $this->ABITDA[$k] / abs($this->receitaOpLiquida[$k]);
            }
            $t1 = $t1 + abs($this->ABITDA[$k]);
            $t2 = $t2 + abs($this->receitaOpLiquida[$k]);

            if($this->receitaOpLiquidai[$k] == 0){
                $this->ABITDAPerci[$k] = 0;
            }else{
                $this->ABITDAPerci[$k] = $this->ABITDAi[$k] / abs($this->receitaOpLiquidai[$k]);
            }
            $t1i = $t1i + abs($this->ABITDAi[$k]);
            $t2i = $t2i + abs($this->receitaOpLiquidai[$k]);

            if($this->receitaOpLiquidaJ[$k] == 0){
                $this->ABITDAPercJ[$k] = 0;
            }else{
                $this->ABITDAPercJ[$k] = $this->ABITDAJ[$k] / abs($this->receitaOpLiquidaJ[$k]);
            }
            $t1J = $t1J + abs($this->ABITDAJ[$k]);
            $t2J = $t2J + abs($this->receitaOpLiquidaJ[$k]);

            if($this->receitaOpLiquidaJ2[$k] == 0){
                $this->ABITDAPercJ2[$k] = 0;
            }else{
                $this->ABITDAPercJ2[$k] = $this->ABITDAJ2[$k] / abs($this->receitaOpLiquidaJ2[$k]);
            }
            $t1J2 = $t1J2 + abs($this->ABITDAJ2[$k]);
            $t2J2 = $t2J2 + abs($this->receitaOpLiquidaJ2[$k]);
        //--------------------------------------------------------------------------------------------
            if($this->receitaOpLiquida[$k] == 0){
                $this->ABITDAPerc_VEI[$k] = 0;
            }else{
                $this->ABITDAPerc_VEI[$k] = $this->ABITDA_VEI[$k] / abs($this->receitaOpLiquida[$k]);
            }
            $t1_VEI = $t1_VEI + abs($this->ABITDA_VEI[$k]);
            $t2_VEI = $t2_VEI + abs($this->receitaOpLiquida[$k]);

            if($this->receitaOpLiquidai[$k] == 0){
                $this->ABITDAPerci_VEI[$k] = 0;
            }else{
                $this->ABITDAPerci_VEI[$k] = $this->ABITDAi_VEI[$k] / abs($this->receitaOpLiquidai[$k]);
            }
            $t1i_VEI = $t1i_VEI + abs($this->ABITDAi_VEI[$k]);
            $t2i_VEI = $t2i_VEI + abs($this->receitaOpLiquidai[$k]);

            if($this->receitaOpLiquidaJ[$k] == 0){
                $this->ABITDAPercJ_VEI[$k] = 0;
            }else{
                $this->ABITDAPercJ_VEI[$k] = $this->ABITDAJ_VEI[$k] / abs($this->receitaOpLiquidaJ[$k]);
            }
            $t1J_VEI = $t1J_VEI + abs($this->ABITDAJ_VEI[$k]);
            $t2J_VEI = $t2J_VEI + abs($this->receitaOpLiquidaJ[$k]);

            if($this->receitaOpLiquidaJ2[$k] == 0){
                $this->ABITDAPercJ2_VEI[$k] = 0;
            }else{
                $this->ABITDAPercJ2_VEI[$k] = $this->ABITDAJ2_VEI[$k] / abs($this->receitaOpLiquidaJ2[$k]);
            }
            $t1J2_VEI = $t1J2_VEI + abs($this->ABITDAJ2_VEI[$k]);
            $t2J2_VEI = $t2J2_VEI + abs($this->receitaOpLiquidaJ2[$k]);

        }

        if($t2 == 0){
            $this->ABITDAPercAnual = 0;
        }else{
            $this->ABITDAPercAnual = $t1 / $t2;
        }

        if($t2i == 0){
            $this->ABITDAPercAnuali = 0;
        }else{
            $this->ABITDAPercAnuali = $t1i / $t2i;
        }

        if($t2J == 0){
            $this->ABITDAPercAnualJ = 0;
        }else{
            $this->ABITDAPercAnualJ = $t1J / $t2J;
        }

        if($t2J2 == 0){
            $this->ABITDAPercAnualJ2 = 0;
        }else{
            $this->ABITDAPercAnualJ2 = $t1J2 / $t2J2;
        }
        //--------------------------------------------------
        if($t2_VEI == 0){
            $this->ABITDAPercAnual_VEI = 0;
        }else{
            $this->ABITDAPercAnual_VEI = $t1_VEI / $t2_VEI;
        }

        if($t2i_VEI == 0){
            $this->ABITDAPercAnuali_VEI = 0;
        }else{
            $this->ABITDAPercAnuali_VEI = $t1i_VEI / $t2i_VEI;
        }

        if($t2J_VEI == 0){
            $this->ABITDAPercAnualJ_VEI = 0;
        }else{
            $this->ABITDAPercAnualJ_VEI = $t1J_VEI / $t2J_VEI;
        }

        if($t2J2_VEI == 0){
            $this->ABITDAPercAnualJ2_VEI = 0;
        }else{
            $this->ABITDAPercAnualJ2_VEI = $t1J2_VEI / $t2J2_VEI;
        }

        //CALCULA LUCRO OPERACIONAL
        $t = 0;
        $ti = 0;
        $tJ = 0;
        $tJ2 = 0;

        for($k = 0 ; $k < $totMes;$k++){
            //$val = abs($this->ABITDA[$k]) -  abs($this->somaGrupo[10][$k]);
            $val = ($this->ABITDA[$k]) ;//-abs($this->somaGrupo[10][$k]);
            $val = ($this->ABITDA_VEI[$k]) ;//-abs($this->somaGrupo[10][$k]);
            $this->lucroOperacional[$k] = $val;
            $t = $t+$val;

            $vali = ($this->ABITDAi[$k]);// -abs($this->somaGrupoi[10][$k]);
            $vali = ($this->ABITDAi_VEI[$k]);// -abs($this->somaGrupoi[10][$k]);
            $this->lucroOperacionali[$k] = $vali;
            $ti = $ti+$vali;

            $valJ = ($this->ABITDAJ[$k]);// -abs($this->somaGrupoJ[10][$k]);
            $valJ = ($this->ABITDAJ_VEI[$k]);// -abs($this->somaGrupoJ[10][$k]);
            $this->lucroOperacionalJ[$k] = $valJ;
            $tJ = $tJ+$valJ;

            $valJ2 = ($this->ABITDAJ2[$k]);// -abs($this->somaGrupoJ2[10][$k]);
            $valJ2 = ($this->ABITDAJ2_VEI[$k]);// -abs($this->somaGrupoJ2[10][$k]);
            $this->lucroOperacionalJ2[$k] = $valJ2;
            $tJ2 = $tJ2+$valJ2;

        }

        $this->lucroOperacionalAnual = $t;
        $this->lucroOperacionalAnuali = $ti;
        $this->lucroOperacionalAnualJ = $tJ;
        $this->lucroOperacionalAnualJ2 = $tJ2;

        //CALCULA LUCRO OPERACIONAL %
        $t1=0;
        $t2=0;
        $t1i=0;
        $t2i=0;
        $t1J=0;
        $t2J=0;
        $t1J2=0;
        $t2J2=0;

        for($k = 0 ; $k < $totMes;$k++){

            if($this->receitaOpLiquida[$k] == 0){
                $this->lucroOperacionalPerc[$k] = 0;
            }else{
                $this->lucroOperacionalPerc[$k] = $this->lucroOperacional[$k] / ($this->receitaOpLiquida[$k]);
            }

            if($this->receitaOpLiquidai[$k] == 0){
                $this->lucroOperacionalPerci[$k] = 0;
            }else{
                $this->lucroOperacionalPerci[$k] = $this->lucroOperacionali[$k] / ($this->receitaOpLiquidai[$k]);
            }

            if($this->receitaOpLiquidaJ[$k] == 0){
                $this->lucroOperacionalPercJ[$k] = 0;
            }else{
                $this->lucroOperacionalPercJ[$k] = $this->lucroOperacionalJ[$k] / ($this->receitaOpLiquidaJ[$k]);
            }

            if($this->receitaOpLiquidaJ2[$k] == 0){
                $this->lucroOperacionalPercJ2[$k] = 0;
            }else{
                $this->lucroOperacionalPercJ2[$k] = $this->lucroOperacionalJ2[$k] / ($this->receitaOpLiquidaJ2[$k]);
            }

            $t1 = $t1 + ($this->lucroOperacional[$k]);
            $t2 = $t2 + ($this->receitaOpLiquida[$k]);

            $t1i = $t1i + ($this->lucroOperacionali[$k]);
            $t2i = $t2i + ($this->receitaOpLiquidai[$k]);

            $t1J = $t1J + ($this->lucroOperacionalJ[$k]);
            $t2J = $t2J + ($this->receitaOpLiquidaJ[$k]);

            $t1J2 = $t1J2 + ($this->lucroOperacionalJ2[$k]);
            $t2J2 = $t2J2 + ($this->receitaOpLiquidaJ2[$k]);
        }

        if($t2 == 0){
            $this->lucroOperacionalPercAnual = 0;
        }else{
            $this->lucroOperacionalPercAnual = $t1 / $t2;
        }

        if($t2i == 0){
            $this->lucroOperacionalPercAnuali = 0;
        }else{
            $this->lucroOperacionalPercAnuali = $t1i / $t2i;
        }

        if($t2J == 0){
            $this->lucroOperacionalPercAnualJ = 0;
        }else{
            $this->lucroOperacionalPercAnualJ = $t1J / $t2J;
        }

        if($t2J2 == 0){
            $this->lucroOperacionalPercAnualJ2 = 0;
        }else{
            $this->lucroOperacionalPercAnualJ2 = $t1J2 / $t2J2;
        }

        // ( + ) DESPESAS CORPORATIVAS
        $t = 0;
        $ti = 0;
        $tJ = 0;
        $tJ2 = 0;

         for($k = 0 ; $k < $totMes;$k++){

            $v = $this->receitaOpLiquida[$k] * $this->despcorp;
           // var_dump($this->receitaOpLiquida[$k]);
            $this->despesasCorporativas[$k] = $v;
            $t = $t + abs($v);

            $vi = $this->receitaOpLiquidai[$k] * $this->despcorp;
            $this->despesasCorporativasi[$k] = $vi;
            $ti = $ti + abs($vi);

            $vJ = $this->receitaOpLiquidaJ[$k] * $this->despcorp;
            $this->despesasCorporativasJ[$k] = $vJ;
            $tJ = $tJ + abs($vJ);

            $vJ2 = $this->receitaOpLiquidaJ2[$k] * $this->despcorp;
            $this->despesasCorporativasJ2[$k] = $vJ2;
            $tJ2 = $tJ2 + abs($vJ2);
        }

        $this->despesasCorporativasAnual = $t;
        $this->despesasCorporativasAnuali = $ti;
        $this->despesasCorporativasAnualJ = $tJ;
        $this->despesasCorporativasAnualJ2 = $tJ2;

        // ( + ) DESPESAS CORPORATIVAS (AC)

        $t = 0;
        $ti = 0;
        $tJ = 0;
        $tJ2 = 0;

         for($k = 0 ; $k < $totMes;$k++){

            if($this->fatLiqTotal[$k] == 0){
               $v = 0;
            }else{
               $v = $this->gastosMatriz[$k] / $this->fatLiqTotal[$k];
            }

            if($this->fatLiqTotali[$k] == 0){
               $vi = 0;
            }else{
               $vi = $this->gastosMatrizi[$k] / $this->fatLiqTotali[$k];
            }

            if($this->fatLiqTotalJ[$k] == 0){
               $vJ = 0;
            }else{
               $vJ = $this->gastosMatrizJ[$k] / $this->fatLiqTotalJ[$k];
            }

            if($this->fatLiqTotalJ2[$k] == 0){
               $vJ2 = 0;
            }else{
               $vJ2 = $this->gastosMatrizJ2[$k] / $this->fatLiqTotalJ2[$k];
            }

            $this->despesasCorporativasAC[$k] = 0;
            $t = $t + abs($this->despesasCorporativasAC[$k]);

            $this->despesasCorporativasACi[$k] = 0;
            $ti = $ti + abs($this->despesasCorporativasACi[$k]);

            $this->despesasCorporativasACJ[$k] = 0;
            $tJ = $tJ + abs($this->despesasCorporativasACJ[$k]);

            $this->despesasCorporativasACJ2[$k] = 0;
            $tJ2 = $tJ2 + abs($this->despesasCorporativasACJ2[$k]);

        }
        $this->despesasCorporativasAnualAC = $t;
        $this->despesasCorporativasAnualACi = $ti;
        $this->despesasCorporativasAnualACJ = $tJ;
        $this->despesasCorporativasAnualACJ2 = $tJ2;

        if($uniGasto == 12){

           $this->despesasCorporativasAnualAC = 0;
           $this->despesasCorporativasAnual = 0;

           $this->despesasCorporativasAnualACi = 0;
           $this->despesasCorporativasAnuali = 0;

           $this->despesasCorporativasAnualACJ = 0;
           $this->despesasCorporativasAnualJ = 0;

           $this->despesasCorporativasAnualACJ2 = 0;
           $this->despesasCorporativasAnualJ2 = 0;

           $t=0;
           $ti=0;
           $tJ=0;
           $tJ2=0;

           for($k = 0 ; $k < $totMes;$k++){

             $this->despesasCorporativas[$k] = 0;
             $this->despesasCorporativasAC[$k] = 0;

             $this->despesasCorporativasi[$k] = 0;
             $this->despesasCorporativasACi[$k] = 0;

             $this->despesasCorporativasJ[$k] = 0;
             $this->despesasCorporativasACJ[$k] = 0;

             $this->despesasCorporativasJ2[$k] = 0;
             $this->despesasCorporativasACJ2[$k] = 0;

           }

        }

        // LAIR
        $t=0;
        for($k = 0 ; $k < $totMes;$k++){

           if($this->ac == null){
            $desp = $this->despesasCorporativas[$k]*-1;
           }else{
            $desp = $this->despesasCorporativasAC[$k]*-1;
           }

           if($this->ac == null){
            $despi = $this->despesasCorporativasi[$k]*-1;
           }else{
            $despi = $this->despesasCorporativasACi[$k]*-1;
           }

           if($this->ac == null){
            $despJ = $this->despesasCorporativasJ[$k]*-1;
           }else{
            $despJ = $this->despesasCorporativasACJ[$k]*-1;
           }

           if($this->ac == null){
            $despJ2 = $this->despesasCorporativasJ2[$k]*-1;
           }else{
            $despJ2 = $this->despesasCorporativasACJ2[$k]*-1;
           }

          $inve = $this->investimentos[$k];
          $fund = $this->fundoDeReserva[$k]*-1;

          $invei = $this->investimentosi[$k];
          $fundi = $this->fundoDeReservai[$k]*-1;

          $inveJ = $this->investimentosJ[$k];
          $fundJ = $this->fundoDeReservaJ[$k]*-1;

          $inveJ2 = $this->investimentosJ2[$k];
          $fundJ2 = $this->fundoDeReservaJ2[$k]*-1;

          $inve = 0;
          $invei = 0;
          $inveJ = 0;
          $inveJ2 = 0;

          if($this->fr == NULL){
            $fund = 0;
            $fundi = 0;
            $fundJ = 0;
            $fundJ2 = 0;
          }

          $v = $this->lucroOperacional[$k] + $this->somaGrupo[7][$k] +$desp +$inve +$fund - $this->somaGrupo[8][$k];

          $this->lair[$k] = $v;
          $t = $t+$v;

          $vi = $this->lucroOperacionali[$k] + $this->somaGrupoi[7][$k] +$despi +$invei +$fundi - $this->somaGrupoi[8][$k];

          $this->lairi[$k] = $vi;
          $ti = $ti+$vi;

          $vJ = $this->lucroOperacionalJ[$k] + $this->somaGrupoJ[7][$k] +$despJ +$inveJ +$fundJ - $this->somaGrupoJ[8][$k];
          $this->lairJ[$k] = $vJ;
          $tJ = $tJ+$vJ;

          $vJ2 = $this->lucroOperacionalJ2[$k] + $this->somaGrupoJ2[7][$k] +$despJ2 +$inveJ2 +$fundJ2 - $this->somaGrupoJ2[8][$k];
          $this->lairJ2[$k] = $vJ2;
          $tJ2 = $tJ2+$vJ2;


        }
        $this->lairAnual = $t;
        $this->lairAnuali = $ti;
        $this->lairAnualJ = $tJ;
        $this->lairAnualJ2 = $tJ2;

        // LAIR %
        $t1= 0;
        $t2 = 0;
        $t1i= 0;
        $t2i = 0;
        $t1J= 0;
        $t2J = 0;
        $t1J2= 0;
        $t2J2 = 0;

        for($k = 0 ; $k < $totMes;$k++){

             if($this->receitaOpLiquida[$k] == 0){
               $v = 0;
             }else{
               $v = $this->lair[$k] / $this->receitaOpLiquida[$k];
             }

             if($this->receitaOpLiquidai[$k] == 0){
               $vi = 0;
             }else{
               $vi = $this->lairi[$k] / $this->receitaOpLiquidai[$k];
             }

             if($this->receitaOpLiquidaJ[$k] == 0){
               $vJ = 0;
             }else{
               $vJ = $this->lairJ[$k] / $this->receitaOpLiquidaJ[$k];
             }

             if($this->receitaOpLiquidaJ2[$k] == 0){
               $vJ2= 0;
             }else{
               $vJ2 = $this->lairJ2[$k] / $this->receitaOpLiquidaJ2[$k];
             }

            $this->lairPerc[$k] = $v;
            $t1 = $t1 + abs($this->lair[$k]);
            $t2 = $t2 + abs($this->receitaOpLiquida[$k]);

            $this->lairPerci[$k] = $vi;
            $t1i = $t1i + abs($this->lairi[$k]);
            $t2i = $t2i + abs($this->receitaOpLiquidai[$k]);

            $this->lairPercJ[$k] = $vJ;
            $t1J = $t1J + abs($this->lairJ[$k]);
            $t2J = $t2J + abs($this->receitaOpLiquidaJ[$k]);

            $this->lairPercJ2[$k] = $vJ2;
            $t1J2 = $t1J2 + abs($this->lairJ2[$k]);
            $t2J2 = $t2J2 + abs($this->receitaOpLiquidaJ2[$k]);
        }

        if($t2 == 0){
            $this->lairPercAnual = 0;
        }else{
            $this->lairPercAnual = $t1 / $t2;
        }

        if($t2i == 0){
            $this->lairPercAnuali = 0;
        }else{
            $this->lairPercAnuali = $t1i / $t2i;
        }

        if($t2J == 0){
            $this->lairPercAnualJ = 0;
        }else{
            $this->lairPercAnualJ = $t1J / $t2J;
        }

        if($t2J2 == 0){
            $this->lairPercAnualJ2 = 0;
        }else{
            $this->lairPercAnualJ2 = $t1J2 / $t2J2;
        }

        // LUCRO LIQUIDO
        $t=0;
        $ti=0;
        $tJ=0;
        $tJ2=0;
        for($k = 0 ; $k < $totMes;$k++){

          $v = $this->lair[$k] - abs($this->somaGrupo[9][$k]);
          $this->lucroLiquido[$k] = $v;
          $t = $t+$v;

          $vi = $this->lairi[$k] - abs($this->somaGrupoi[9][$k]);
          $this->lucroLiquidoi[$k] = $vi;
          $ti = $ti+$vi;

          $vJ = $this->lairJ[$k] - abs($this->somaGrupoJ[9][$k]);
          $this->lucroLiquidoJ[$k] = $vJ;
          $tJ = $tJ+$vJ;

          $vJ2 = $this->lairJ2[$k] - abs($this->somaGrupoJ2[9][$k]);
          $this->lucroLiquidoJ2[$k] = $vJ2;
          $tJ2 = $tJ2+$vJ2;
        }

        $this->lucroLiquidoAnual = $t;
        $this->lucroLiquidoAnuali = $ti;
        $this->lucroLiquidoAnualJ = $tJ;
        $this->lucroLiquidoAnualJ2 = $tJ2;

        $t1= 0;
        $t2 = 0;
        $t1i= 0;
        $t2i = 0;
        $t1J= 0;
        $t2J = 0;
        $t1J2= 0;
        $t2J2 = 0;

        for($k = 0 ; $k < $totMes;$k++){

             if($this->receitaOpLiquida[$k] == 0){
               $v = 0;
             }else{
               $v = $this->lucroLiquido[$k] / $this->receitaOpLiquida[$k];
             }

             if($this->receitaOpLiquidai[$k] == 0){
               $vi = 0;
             }else{
               $vi = $this->lucroLiquidoi[$k] / $this->receitaOpLiquidai[$k];
             }

             if($this->receitaOpLiquidaJ[$k] == 0){
               $vJ = 0;
             }else{
               $vJ = $this->lucroLiquidoJ[$k] / $this->receitaOpLiquidaJ[$k];
             }

             if($this->receitaOpLiquidaJ2[$k] == 0){
               $vJ2 = 0;
             }else{
               $vJ2 = $this->lucroLiquidoJ2[$k] / $this->receitaOpLiquidaJ2[$k];
             }

            $this->lucroLiquidoPerc[$k] = $v;
            $t1 = $t1 + abs($this->lucroLiquido[$k]);
            $t2 = $t2 + abs($this->receitaOpLiquida[$k]);

            $this->lucroLiquidoPerci[$k] = $vi;
            $t1i = $t1i + abs($this->lucroLiquidoi[$k]);
            $t2i = $t2i + abs($this->receitaOpLiquidai[$k]);

            $this->lucroLiquidoPercJ[$k] = $vJ;
            $t1J = $t1J + abs($this->lucroLiquidoJ[$k]);
            $t2J = $t2J + abs($this->receitaOpLiquidaJ[$k]);

            $this->lucroLiquidoPercJ2[$k] = $vJ2;
            $t1J2 = $t1J2 + abs($this->lucroLiquidoJ2[$k]);
            $t2J2 = $t2J2 + abs($this->receitaOpLiquidaJ2[$k]);
        }

        if($t2 == 0){
            $this->lucroLiquidoAnualPerc = 0;
        }else{
            $this->lucroLiquidoAnualPerc = $t1 / $t2;
        }

        if($t2i == 0){
            $this->lucroLiquidoAnualPerci = 0;
        }else{
            $this->lucroLiquidoAnualPerci = $t1i / $t2i;
        }

        if($t2J == 0){
            $this->lucroLiquidoAnualPercJ = 0;
        }else{
            $this->lucroLiquidoAnualPercJ = $t1J / $t2J;
        }

        if($t2J2 == 0){
            $this->lucroLiquidoAnualPercJ2 = 0;
        }else{
            $this->lucroLiquidoAnualPercJ2 = $t1J2 / $t2J2;
        }

    }

    public function opLiquido($titulo){

        $totMes = count($this->mes);
        $val = $this->receitaOpLiquida;
        $vali = $this->receitaOpLiquidai;
        $valJ = $this->receitaOpLiquidaJ;
        $valJ2 = $this->receitaOpLiquidaJ2;
        $calculo = 0;

        $ano = 0;
        $conteudo= '<tr class="active" style="font-weight:bold; color:#000;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $t=0;
        $a = 0;
        for($k = 0 ; $k < $totMes;$k++){

           $percentual = 0;
           $valperc = 0;
           $t = $t + $val[$k];

           if($this->opcao == 'I'){
              $a = $a + $vali[$k];
              $c.='<td>'.number_format($vali[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $vali[$k];
              $valperc = $vali[$k];

           }

           if($this->opcao == 'J'){
              $a = $a + $valJ[$k];
              $c.='<td>'.number_format($valJ[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ[$k];
              $valperc = $valJ[$k];
           }

           if($this->opcao == 'J2'){
              $a = $a + $valJ2[$k];
              $c.='<td>'.number_format($valJ2[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ2[$k];
              $valperc = $valJ2[$k];
           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = ($calculo/$valperc)*100;
           }




            $c.='<td>'.number_format($val[$k], 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';

        }
        $disp =  $t - $a;
        if($t ==  0){
            $dispPerc = 0;
        }elseif($a == 0){
            $dispPerc = 100;
        }else{
            $dispPerc = ($disp / $a) * 100;
        }

        $conteudo.='<td>'.number_format($a, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($t, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($disp, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($dispPerc, 2, ',', '.').'%</td>';
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }

    function boxMargemContribuicao($titulo){

        $totMes = count($this->mes);
        $val = $this->margemContribuicao;
        $vali = $this->margemContribuicaoi;
        $valJ = $this->margemContribuicaoJ;
        $valJ2 = $this->margemContribuicaoJ2;
        $va = 0;

       /* $vala = array_sum($this->margemContribuicao);
        $valia = array_sum($this->margemContribuicaoi);
        $valJa = array_sum($this->margemContribuicaoJ);
        $valJ2a = array_sum($this->margemContribuicaoJ2);
        */
        $vala = $this->margemContribuicaoAnual;
        $valia = $this->margemContribuicaoAnuali;
        $valJa = $this->margemContribuicaoAnualJ;
        $valJ2a = $this->margemContribuicaoAnualJ2;



        $grupo2 = $this->somaGrupo[2];
        $grupo2i = $this->somaGrupoi[2];
        $grupo2J = $this->somaGrupoJ[2];
        $grupo2J2 = $this->somaGrupoJ2[2];

        $ano = 0;

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $c1 = '';
        $c2 = '';
        $c1a = '';
        $c2a = '';
        for($k = 0 ; $k < $totMes;$k++){

            $v = 100 - ($val[$k]*100);
            $va = 100 - ($vala*100);
            $c1 = '';
            $c2 = '';
            $c1a = '';
            $c2a = '';

            if($this->opcao == 'I'){
                $a = 100 - ($vali[$k]*100);
                $aa = 100 - ($valia*100);

                $c1.='<td>'.number_format($a, 2, ',', '.').' %</td>';


                $vi = 100 - (($val[$k]-$vali[$k])*100);
                $vi =  $v - $a;
                $c2.='<td>'.number_format(($vi), 2, ',', '.').' %</td>';

                $c1a ='<td>'.number_format($aa, 2, ',', '.').' %</td>';

                $via = 100 - (($vala - $valia)*100);
                $via = $va - $aa;
                $c2a ='<td>'.number_format(($via), 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J'){
                $a = 100 - ($valJ[$k]*100);
                $c1.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $vJ = 100 - (($val[$k] - $valJ[$k])*100);
                $vJ =  $v - $a;
                $c2.='<td>'.number_format(($vJ), 2, ',', '.').' %</td>';

                 //ano
                $aa = 100 - ($valJa*100);
                $c1a ='<td>'.number_format($aa, 2, ',', '.').' %</td>';

                $vJa = 100 - (($vala - $valJa)*100);
                $vJa =  $va - $aa;
                $c2a ='<td>'.number_format(($vJa), 2, ',', '.').' %</td>';
            }
            if($this->opcao == 'J2'){
                $a = 100 - ($valJ2[$k]*100);
                $c1.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $vJ2 = 100 - (($val[$k] - $valJ2[$k])*100);
                $vJ2 =  $v-$a;
                $c2.='<td>'.number_format(($vJ2), 2, ',', '.').' %</td>';

                //ano

                $aa = 100 - ($valJ2a*100);
                $c1a ='<td>'.number_format($aa, 2, ',', '.').' %</td>';

                $vJ2a = 100 - (($vala - $valJ2a)*100);
                $vJ2a =  $va - $aa;
                $c2a ='<td>'.number_format(($vJ2a), 2, ',', '.').' %</td>';
            }





            $va = 100 - ($vala*100);




            $c.=$c1;
            $c.='<td>'.number_format($v, 2, ',', '.').' %</td>';
            $c.=$c2;
            $c.=$c2;
            //$ano = $ano + $val[$k];
        }



        $ano = 100 - ($this->margemContribuicaoAnual * 100);
        ///$conteudo.= '<th>'.number_format($ano, 2, ',', '.').' %</th>';
        $conteudo.=$c1a;
        $conteudo.='<td>'.number_format($va, 2, ',', '.').' %</td>';
        $conteudo.=$c2a;
        $conteudo.=$c2a;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }
    public function boxLucroBruto($titulo){

        $totMes = count($this->mes);
        $val = $this->lucroBruto;
        $vali = $this->lucroBrutoi;
        $valJ = $this->lucroBrutoJ;
        $valJ2 = $this->lucroBrutoJ2;

        $vala = array_sum($this->lucroBruto);
        $valia = array_sum($this->lucroBrutoi);
        $valJa = array_sum($this->lucroBrutoJ);
        $valJ2a = array_sum($this->lucroBrutoJ2);

        $percentual = 0;
        $valperc = 0;
        $calculo = 0;

        $percentuala = 0;
        $valperca = 0;
        $calculoa = 0;

        $this->valLucroBruto = $valJa;

        //$ano = array_sum($val);

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';

        for($k = 0 ; $k < $totMes;$k++){


           $percentual = 0;
           $valperc = 0;
           $calculo = 0;

           $percentuala = 0;
           $valperca = 0;
           $calculoa = 0;

           if($this->opcao == 'I'){


              $c.='<td>'.number_format($vali[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $vali[$k];
              $valperc = $vali[$k];

              $ca='<td>'.number_format($valia, 2, ',', '.').'</td>';
              $calculoa = $vala - $valia;
              $valperca = $valia;
           }

           if($this->opcao == 'J'){
              $c.='<td>'.number_format($valJ[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ[$k];
              $valperc = $valJ[$k];

              $ca='<td>'.number_format($valJa, 2, ',', '.').'</td>';
              $calculoa = $vala - $valJa;
              $valperca = $valJa;
           }

           if($this->opcao == 'J2'){
              $c.='<td>'.number_format($valJ2[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ2[$k];
              $valperc = $valJ2[$k];

              $ca='<td>'.number_format($valJ2a, 2, ',', '.').'</td>';
              $calculoa = $vala - $valJ2a;
              $valperca = $valJ2a;
           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = ($calculo / $valperc)*100;
           }



            $c.='<td>'.number_format($val[$k], 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';

        }

          if($valperca == 0 ){

               $formulaa = 0;

           }else{

               $formulaa = ($calculoa / $valperca)*100;
           }



        $ca.='<td>'.number_format($vala, 2, ',', '.').'</td>';
        $ca.='<td>'.number_format($calculoa, 2, ',', '.').'</td>';
        $ca.='<td>'.number_format($formulaa, 2, ',', '.').'%</td>';

        ///$conteudo.= '<th>'.number_format($this->lucroBrutoAnual, 2, ',', '.').'</th>';
        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }

    public function boxLucroBrutoPerc($titulo){
        $totMes = count($this->mes);

        $lucroBruto = $this->lucroBruto;
        $lucroBrutoi = $this->lucroBrutoi;
        $lucroBrutoJ = $this->lucroBrutoJ;
        $lucroBrutoJ2 = $this->lucroBrutoJ2;

        $receitaOpLiquida= $this->receitaOpLiquida;
        $receitaOpLiquidai = $this->receitaOpLiquidai;
        $receitaOpLiquidaJ = $this->receitaOpLiquidaJ;
        $receitaOpLiquidaJ2 = $this->receitaOpLiquidaJ2;


        $valy = array_sum($this->lucroBruto);
        $valiy = array_sum($this->lucroBrutoi);
        $valJy = array_sum($this->lucroBrutoJ);
        $valJ2y = array_sum($this->lucroBrutoJ2);

        $valar = array_sum($this->receitaOpLiquida);
        $valiar = array_sum($this->receitaOpLiquidai);
        $valJar = array_sum($this->receitaOpLiquidaJ);
        $valJ2ar = array_sum($this->receitaOpLiquidaJ2);

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $aaaa = 0;
        $aaaa1 = 0;
        $a = 0;
        $a1 = 0;
        $a2 = 0;
        $aa = 0;
        $ca = 0;
        $c = '';
        $b1 = 0;
        $b2 =0;
        $disp = 0;
        $b = 0;
        $b1 = 0;
        $dispA = 0;

        for($k = 0 ; $k < $totMes;$k++){

            $percentual = 0;
            $valperc = 0;

            $percentuala = 0;
            $valperca = 0;


            if($valar == 0 && $valy == 0){
                $aaaa = 0;
            }elseif($valar == 0){
                $aaaa  = 100;
            }else{
                $aaaa  = (($valy) / ($valar))*100;
            }

            if($receitaOpLiquida[$k] == 0 && $lucroBruto[$k] == 0){
                $aaaa1 = 0;
            }elseif($receitaOpLiquida[$k] == 0){
                $aaaa1  = 100;
            }else{
                $aaaa1  = (($lucroBruto[$k]) / ($receitaOpLiquida[$k]))*100;
            }
            //$aaaa1  = ($lucroBruto[$k] / $receitaOpLiquida[$k])*100;

            if($this->opcao == 'I'){
                $a =   ($receitaOpLiquidai[$k]   == 0) ? 0 : ($lucroBrutoi[$k] / $receitaOpLiquidai[$k])*100;
                //$a  = ($lucroBrutoi[$k] / $receitaOpLiquidai[$k])*100;
                $a1 = $lucroBrutoi[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidai[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a2)  == 0) ? 0 : (abs($a1) / abs($a2))*100;
                //$disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valiar  == 0) ? 0 : ($valiy / $valiar)*100;
                $b1 = ($valiy-$valy);
                $b2 = ($valiar-$valar);

                $dispA  = (abs($b2) == 0) ? 0 : (abs($b1) / abs($b2))*100;
                $dispA = $aaaa - $aa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J'){

                $a  = ($lucroBrutoJ[$k] / $receitaOpLiquidaJ[$k])*100;
                $a1 = $lucroBrutoJ[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidaJ[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valJy / $valJar)*100;
                $b1 = ($valJy-$valy);
                $b2 = ($valJar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA = $aaaa - $aa;
                //$dispA = $aa - $aaaa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J2'){

                $a  = ($lucroBrutoJ2[$k] / $receitaOpLiquidaJ2[$k])*100;
                $a1 = $lucroBrutoJ2[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidaJ2[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valJ2y / $valJ2ar)*100;
                $b1 = abs($valJ2y-$valy);
                $b2 = abs($valJ2ar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA =  $aaaa - $aa;
                //$dispA = $aa - $aaaa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }

            if($receitaOpLiquida[$k] == 0 && $lucroBruto[$k] == 0){
                $b = 0;
            }elseif($receitaOpLiquida[$k] == 0){
                $b  = 100;
            }else{
                $b = ($lucroBruto[$k] / $receitaOpLiquida[$k])*100;
            }

            //$disp = $a - $b;

            $c.='<td>'.number_format($b, 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($disp), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($disp), 2, ',', '.').' %</td>';
        }

            $ca.='<td>'.number_format($aaaa, 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($dispA), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($dispA), 2, ',', '.').' %</td>';

        ///$conteudo.= '<th>'.number_format(($this->lucroBrutoAnualPerc*100), 2, ',', '.').' %</th>';
        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }

    public function boxABITDAx($titulo){



    }

    public function boxABITDA($titulo){
        $totMes = count($this->mes);
        $val = $this->ABITDA;
        $vali = $this->ABITDAi;
        $valJ = $this->ABITDAJ;
        $valJ2 = $this->ABITDAJ2;

        $vala = array_sum($this->ABITDA);
        $valia = array_sum($this->ABITDAi);
        $valJa = array_sum($this->ABITDAJ);
        $valJ2a = array_sum($this->ABITDAJ2);

        $valJa = $this->valLucroBruto + ($this->valDespesasOp) + ($this->valDespesasAdm);


        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculoa = 0;
        $calculo = 0;
        $valperc = 0;
        $valperca = 0;
        $formula  = 0;
        $formulaa = 0;
        $valperca = 0;

        for($k = 0 ; $k < $totMes;$k++){


           $percentual = 0;
           $valperc = 0;

           if($this->opcao == 'I'){
              $c.='<td>'.number_format($vali[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $vali[$k];
              $valperc = $vali[$k];

              $ca='<td>'.number_format($valia, 2, ',', '.').'</td>';
              $calculoa = $vala - $valia;
              $valperca = $valia;

           }

           if($this->opcao == 'J'){
              $c.='<td>'.number_format($valJ[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ[$k];
              $valperc = $valJ[$k];

              $ca='<td>'.number_format($valJa, 2, ',', '.').'</td>';
              $calculoa = $vala - $valJa;
              $valperca = $valJa;
           }

           if($this->opcao == 'J2'){
              $c.='<td>ddd'.number_format($valJ2[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ2[$k];
              $valperc = $valJ2[$k];

              $ca='<td>ddda'.number_format($valJ2a, 2, ',', '.').'</td>';
              $calculoa = $vala - $valJ2a;
              $valperca = $valJ2a;
           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = ($calculo / $valperc)*100;
           }

           if($valperca == 0 ){

               $formulaa = 0;

           }else{

               $formulaa = ($calculoa / $valperca)*100;
           }
           // mes
            $c.='<td>'.number_format($val[$k], 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';

        }
        // ano real
        $ca.='<td>'.number_format($vala, 2, ',', '.').'</td>';
        $ca.='<td>'.number_format($calculoa, 2, ',', '.').'</td>';
        $ca.='<td>'.number_format($formulaa, 2, ',', '.').'%</td>';

        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }


    public function boxABITDAVei($titulo){

        $totMes = count($this->mes);
        $val = $this->ABITDA_VEI;
        $vali = $this->ABITDAi_VEI;
        $valJ = $this->ABITDAJ_VEI;
        $valJ2 = $this->ABITDAJ2_VEI;

        $vala = array_sum($this->ABITDA_VEI);
        $valia = array_sum($this->ABITDAi_VEI);
        $valJa = array_sum($this->ABITDAJ_VEI);
        $valJ2a = array_sum($this->ABITDAJ2_VEI);

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;


        for($k = 0 ; $k < $totMes;$k++){


           $percentual = 0;
           $valperc = 0;

           if($this->opcao == 'I'){
              $c.='<td>'.number_format($vali[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $vali[$k];
              $valperc = $vali[$k];

              $ca='<td>'.number_format($valia, 2, ',', '.').'</td>';
              $calculoa = $vala - $valia;
              $valperca = $valia;

           }

           if($this->opcao == 'J'){
              $c.='<td>'.number_format($valJ[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ[$k];
              $valperc = $valJ[$k];

              $ca='<td>'.number_format($valJa, 2, ',', '.').'</td>';
              $calculoa = $vala - $valJa;
              $valperca = $valJa;
           }

           if($this->opcao == 'J2'){
              $c.='<td>'.number_format($valJ2[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ2[$k];
              $valperc = $valJ2[$k];

              $ca='<td>'.number_format($valJ2a, 2, ',', '.').'</td>';
              $calculoa = $vala - $valJ2a;
              $valperca = $valJ2a;
           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = ($calculo / $valperc)*100;
           }

           if($valperca == 0 ){

               $formulaa = 0;

           }else{

               $formulaa = ($calculoa / $valperca)*100;
           }

            $c.='<td>'.number_format($val[$k], 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';

        }

        $ca.='<td>'.number_format($vala, 2, ',', '.').'</td>';
        $ca.='<td>'.number_format($calculoa, 2, ',', '.').'</td>';
        $ca.='<td>'.number_format($formulaa, 2, ',', '.').'%</td>';

        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }

    public function boxABITDAPercVei($titulo){

        $totMes = count($this->mes);
        $val = $this->ABITDAPerc_VEI;
        $vali = $this->ABITDAPerci_VEI;
        $valJ = $this->ABITDAPercJ_VEI;
        $valJ2 = $this->ABITDAPercJ2_VEI;

        $vala = array_sum($this->ABITDAPerc_VEI);
        $valia = array_sum($this->ABITDAPerci_VEI);
        $valJa = array_sum($this->ABITDAPercJ_VEI);
        $valJ2a = array_sum($this->ABITDAPercJ2_VEI);

        $conteudo.= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;

        for($k = 0 ; $k < $totMes;$k++){


            $percentual = 0;
            $valperc = 0;

            $percentuala = 0;
            $valperca = 0;

            if($this->opcao == 'I'){
                $a = $vali[$k]*100;
                $c.='<td>'.number_format(($a), 2, ',', '.').' %</td>';
                $valperc = $vali[$k];

                $aa = $valia*100;
                $ca ='<td>'.number_format(($aa), 2, ',', '.').' %</td>';
                $valperca = $valia;

            }
            if($this->opcao == 'J'){
                $a = $valJ[$k]*100;
                $c.='<td>'.number_format(($a), 2, ',', '.').' %</td>';
                $valperc = $valJ[$k];

                $aa = $valJa*100;
                $ca='<td>'.number_format(($aa), 2, ',', '.').' %</td>';
                $valperca = $valJa;

            }
            if($this->opcao == 'J2'){
                $a = $valJ2[$k]*100;
                $c.='<td>'.number_format(($a), 2, ',', '.').' %</td>';
                $valperc = $valJ2[$k];

                $aa = $valJ2a*100;
                $ca='<td>'.number_format(($aa), 2, ',', '.').' %</td>';
                $valperca = $valJ2a;
            }
            $b = $val[$k]*100;
            $ab = $b-$a;
            $c.='<td>'.number_format(($b), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($ab), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($ab), 2, ',', '.').' %</td>';

        }

            $ba = $vala*100;
            $aba = $ba-$aa;
            $ca.='<td>'.number_format(($ba), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($aba), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($aba), 2, ',', '.').' %</td>';

        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }

    public function boxABITDAPerc($titulo){

        $totMes = count($this->mes);
        $val = $this->ABITDAPerc;
        $vali = $this->ABITDAPerci;
        $valJ = $this->ABITDAPercJ;
        $valJ2 = $this->ABITDAPercJ2;

        $vala = array_sum($this->ABITDAPerc);
        $valia = array_sum($this->ABITDAPerci);
        $valJa = array_sum($this->ABITDAPercJ);
        $valJ2a = array_sum($this->ABITDAPercJ2);

        $conteudo.= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;

        for($k = 0 ; $k < $totMes;$k++){


            $percentual = 0;
            $valperc = 0;

            $percentuala = 0;
            $valperca = 0;

            if($this->opcao == 'I'){
                $a = $vali[$k]*100;
                $c.='<td>'.number_format(($a), 2, ',', '.').' %</td>';
                $valperc = $vali[$k];

                $aa = $valia*100;
                $ca ='<td>'.number_format(($aa), 2, ',', '.').' %</td>';
                $valperca = $valia;

            }
            if($this->opcao == 'J'){
                $a = $valJ[$k]*100;
                $c.='<td>'.number_format(($a), 2, ',', '.').' %</td>';
                $valperc = $valJ[$k];

                $aa = $valJa*100;
                $ca='<td>'.number_format(($aa), 2, ',', '.').' %</td>';
                $valperca = $valJa;

            }
            if($this->opcao == 'J2'){
                $a = $valJ2[$k]*100;
                $c.='<td>'.number_format(($a), 2, ',', '.').' %</td>';
                $valperc = $valJ2[$k];

                $aa = $valJ2a*100;
                $ca='<td>'.number_format(($aa), 2, ',', '.').' %</td>';
                $valperca = $valJ2a;
            }
            $b = $val[$k]*100;
            $ab = $b-$a;
            $c.='<td>'.number_format(($b), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($ab), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($ab), 2, ',', '.').' %</td>';

        }

            $ba = $vala*100;
            $aba = $ba-$aa;
            $ca.='<td>'.number_format(($ba), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($aba), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($aba), 2, ',', '.').' %</td>';

        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }


    public function boxLucroOperacional($titulo){

        $totMes = count($this->mes);
        $val = $this->lucroOperacional;
        $vali = $this->lucroOperacionali;
        $valJ = $this->lucroOperacionalJ;
        $valJ2 = $this->lucroOperacionalJ2;

        $vala = array_sum($this->lucroOperacional);
        $valia = array_sum($this->lucroOperacionali);
        $valJa = array_sum($this->lucroOperacionalJ);
        $valJ2a = array_sum($this->lucroOperacionalJ2);

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;

        for($k = 0 ; $k < $totMes;$k++){


           $percentual = 0;
           $valperc = 0;

           $percentuala = 0;
           $valperca = 0;

           if($this->opcao == 'I'){
              $c.='<td>'.number_format($vali[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $vali[$k];
              $valperc = $vali[$k];

              $ca='<td>'.number_format($valia, 2, ',', '.').'</td>';
              $calculoa = $vala - $valia;
              $valperca = $valia;

           }

           if($this->opcao == 'J'){
              $c.='<td>'.number_format($valJ[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ[$k];
              $valperc = $valJ[$k];

              $ca='<td>'.number_format($valJa, 2, ',', '.').'</td>';
              $calculoa = $vala - $valJa;
              $valperca = $valJa;
           }

           if($this->opcao == 'J2'){
              $c.='<td>'.number_format($valJ2[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ2[$k];
              $valperc = $valJ2[$k];

              $ca='<td>'.number_format($valJ2a, 2, ',', '.').'</td>';
              $calculoa = $vala - $valJ2a;
              $valperca = $valJ2a;

           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = ($calculo / $valperc)*100;
           }

           if($valperca == 0 ){

               $formulaa = 0;

           }else{

               $formulaa = ($calculoa / $valperca)*100;
           }

            $c.='<td>'.number_format($val[$k], 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';

        }

        $ca.='<td>'.number_format($vala, 2, ',', '.').'</td>';
        $ca.='<td>'.number_format($calculoa, 2, ',', '.').'</td>';
        $ca.='<td>'.number_format($formulaa, 2, ',', '.').'%</td>';

        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }

    public function boxLucroOperacionalPerc($titulo){

        $totMes = count($this->mes);

        $lucroBruto = $this->lucroOperacional;
        $lucroBrutoi = $this->lucroOperacionali;
        $lucroBrutoJ = $this->lucroOperacionalJ;
        $lucroBrutoJ2 = $this->lucroOperacionalJ2;

        $receitaOpLiquida= $this->receitaOpLiquida;
        $receitaOpLiquidai = $this->receitaOpLiquidai;
        $receitaOpLiquidaJ = $this->receitaOpLiquidaJ;
        $receitaOpLiquidaJ2 = $this->receitaOpLiquidaJ2;


        $valy = array_sum($this->lucroOperacional);
        $valiy = array_sum($this->lucroOperacionali);
        $valJy = array_sum($this->lucroOperacionalJ);
        $valJ2y = array_sum($this->lucroOperacionalJ2);

        $valar = array_sum($this->receitaOpLiquida);
        $valiar = array_sum($this->receitaOpLiquidai);
        $valJar = array_sum($this->receitaOpLiquidaJ);
        $valJ2ar = array_sum($this->receitaOpLiquidaJ2);


        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;

        for($k = 0 ; $k < $totMes;$k++){

            $percentual = 0;
            $valperc = 0;

            $percentuala = 0;
            $valperca = 0;


            if($valar == 0 && $valy == 0){
                $aaaa = 0;
            }elseif($valar == 0){
                $aaaa  = 100;
            }else{
                $aaaa  = (($valy) / ($valar))*100;
            }

            if($receitaOpLiquida[$k] == 0 && $lucroBruto[$k] == 0){
                $aaaa1 = 0;
            }elseif($receitaOpLiquida[$k] == 0){
                $aaaa1  = 100;
            }else{
                $aaaa1  = (($lucroBruto[$k]) / ($receitaOpLiquida[$k]))*100;
            }

            if($this->opcao == 'I'){

                $a  = ($lucroBrutoi[$k] / $receitaOpLiquidai[$k])*100;
                $a1 = $lucroBrutoi[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidai[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valiy / $valiar)*100;
                $b1 = ($valiy-$valy);
                $b2 = ($valiar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA = $aaaa - $aa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J'){

                $a  = ($lucroBrutoJ[$k] / $receitaOpLiquidaJ[$k])*100;
                $a1 = $lucroBrutoJ[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidaJ[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valJy / $valJar)*100;
                $b1 = ($valJy-$valy);
                $b2 = ($valJar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA = $aaaa - $aa;

                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J2'){

                $a  = ($lucroBrutoJ2[$k] / $receitaOpLiquidaJ2[$k])*100;
                $a1 = $lucroBrutoJ2[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidaJ2[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valJ2y / $valJ2ar)*100;
                $b1 = abs($valJ2y-$valy);
                $b2 = abs($valJ2ar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA =  $aaaa - $aa;
                //$dispA = $aa - $aaaa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }


            if($receitaOpLiquida[$k] == 0 && $lucroBruto[$k] == 0){
                $b = 0;
            }elseif($receitaOpLiquida[$k] == 0){
                $b  = 100;
            }else{
                $b = ($lucroBruto[$k] / $receitaOpLiquida[$k])*100;
            }


            $c.='<td>'.number_format($b, 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($disp), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($disp), 2, ',', '.').' %</td>';
        }

            $ca.='<td>'.number_format($aaaa, 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($dispA), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($dispA), 2, ',', '.').' %</td>';

        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;

    }

    public function boxLair($titulo){

        $totMes = count($this->mes);
        $val = $this->lair;
        $vali = $this->lairi;
        $valJ = $this->lairJ;
        $valJ2 = $this->lairJ2;

        $vala = array_sum($this->lair);
        $valia = array_sum($this->lairi);
        $valJa = array_sum($this->lairJ);
        $valJ2a = array_sum($this->lairJ2);

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;

        for($k = 0 ; $k < $totMes;$k++){


           $percentual = 0;
           $valperc = 0;

           $percentuala = 0;
           $valperca = 0;

           if($this->opcao == 'I'){
              $c.='<td>'.number_format($vali[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $vali[$k];
              $valperc = $vali[$k];

              $ca='<td>'.number_format($valia, 2, ',', '.').'</td>';
              $calculoa = $vala - $valia;
              $valperca = $valia;

           }

           if($this->opcao == 'J'){
              $c.='<td>'.number_format($valJ[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ[$k];
              $valperc = $valJ[$k];

              $ca='<td>'.number_format($valJa, 2, ',', '.').'</td>';
              $calculoa = $vala - $valJa;
              $valperca = $valJa;
           }

           if($this->opcao == 'J2'){
              $c.='<td>'.number_format($valJ2[$k], 2, ',', '.').'</td>';
              $calculo = $val[$k] - $valJ2[$k];
              $valperc = $valJ2[$k];

              $ca='<td>'.number_format($valJ2a, 2, ',', '.').'</td>';
              $calculoa = $vala - $valJ2a;
              $valperca = $valJ2a;
           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = ($calculo / ($valperc))*100;
           }
           $formula = $this->calculoPercentualDisp($calculo, $valperc, $val[$k]);

            $c.='<td>'.number_format($val[$k], 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';

        }

           if($valperca == 0 ){

               $formulaa= 0;

           }else{

               $formulaa = ($calculoa / ($valperca))*100;
           }
           $formulaa = $this->calculoPercentualDisp($calculoa, $valperca, $vala);


            $ca.='<td>'.number_format($vala, 2, ',', '.').'</td>';
            $ca.='<td>'.number_format($calculoa, 2, ',', '.').'</td>';
            $ca.='<td>'.number_format($formulaa, 2, ',', '.').'%</td>';

       // $conteudo.= '<th>'.number_format($this->lairAnual, 2, ',', '.').'</th>';
        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }


    public function boxLairPerc($titulo){

        $totMes = count($this->mes);

        $lucroBruto = $this->lair;
        $lucroBrutoi = $this->lairi;
        $lucroBrutoJ = $this->lairJ;
        $lucroBrutoJ2 = $this->lairJ2;

        $receitaOpLiquida= $this->receitaOpLiquida;
        $receitaOpLiquidai = $this->receitaOpLiquidai;
        $receitaOpLiquidaJ = $this->receitaOpLiquidaJ;
        $receitaOpLiquidaJ2 = $this->receitaOpLiquidaJ2;


        $valy = array_sum($this->lair);
        $valiy = array_sum($this->lairi);
        $valJy = array_sum($this->lairJ);
        $valJ2y = array_sum($this->lairJ2);

        $valar = array_sum($this->receitaOpLiquida);
        $valiar = array_sum($this->receitaOpLiquidai);
        $valJar = array_sum($this->receitaOpLiquidaJ);
        $valJ2ar = array_sum($this->receitaOpLiquidaJ2);

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;

        for($k = 0 ; $k < $totMes;$k++){

            $percentual = 0;
            $valperc = 0;

            $percentuala = 0;
            $valperca = 0;

            if($valar == 0 && $valy == 0){
                $aaaa = 0;
            }elseif($valar == 0){
                $aaaa  = 100;
            }else{
                $aaaa  = (($valy) / ($valar))*100;
            }

            if($receitaOpLiquida[$k] == 0 && $lucroBruto[$k] == 0){
                $aaaa1 = 0;
            }elseif($receitaOpLiquida[$k] == 0){
                $aaaa1  = 100;
            }else{
                $aaaa1  = (($lucroBruto[$k]) / ($receitaOpLiquida[$k]))*100;
            }



            if($this->opcao == 'I'){

                $a  = ($lucroBrutoi[$k] / $receitaOpLiquidai[$k])*100;
                $a1 = $lucroBrutoi[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidai[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valiy / $valiar)*100;
                $b1 = ($valiy-$valy);
                $b2 = ($valiar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA = $aaaa - $aa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J'){

                $a  = ($lucroBrutoJ[$k] / $receitaOpLiquidaJ[$k])*100;
                $a1 = $lucroBrutoJ[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidaJ[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valJy / $valJar)*100;
                $b1 = ($valJy-$valy);
                $b2 = ($valJar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA = $aaaa - $aa;
                //$dispA = $aa - $aaaa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J2'){

                $a  = ($lucroBrutoJ2[$k] / $receitaOpLiquidaJ2[$k])*100;
                $a1 = $lucroBrutoJ2[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidaJ2[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valJ2y / $valJ2ar)*100;
                $b1 = abs($valJ2y-$valy);
                $b2 = abs($valJ2ar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA =  $aaaa - $aa;
                //$dispA = $aa - $aaaa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }

            //$b = ($lucroBruto[$k] / $receitaOpLiquida[$k])*100;
            if($receitaOpLiquida[$k] == 0 && $lucroBruto[$k] == 0){
                $b = 0;
            }elseif($receitaOpLiquida[$k] == 0){
                $b  = 100;
            }else{
                $b = ($lucroBruto[$k] / $receitaOpLiquida[$k])*100;
            }
            //$disp = $a - $b;

            $c.='<td>'.number_format($b, 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($disp), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($disp), 2, ',', '.').' %</td>';
        }

            $ca.='<td>'.number_format($aaaa, 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($dispA), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($dispA), 2, ',', '.').' %</td>';

        ///$conteudo.= '<th>'.number_format(($this->lucroBrutoAnualPerc*100), 2, ',', '.').' %</th>';
        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }

    public function boxLucroLiquido($titulo){
        $totMes = count($this->mes);
        $val = $this->lucroLiquido;
        $vali = $this->lucroLiquidoi;
        $valJ = $this->lucroLiquidoJ;
        $valJ2 = $this->lucroLiquidoJ2;

        $vala = array_sum($this->lucroLiquido);
        $valia = array_sum($this->lucroLiquidoi);
        $valJa = array_sum($this->lucroLiquidoJ);
        $valJ2a = array_sum($this->lucroLiquidoJ2);

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;

        for($k = 0 ; $k < $totMes;$k++){


           $percentual = 0;
           $valperc = 0;

           $percentuala = 0;
           $valperca = 0;

           if($this->opcao == 'I'){

              $c.='<td>'.number_format($vali[$k], 2, ',', '.').'</td>';
              $calculo = $this->calcDisp($val[$k], $vali[$k]);
              $valperc = $vali[$k];

              $ca='<td>'.number_format($valia, 2, ',', '.').'</td>';
              $calculoa = $this->calcDisp($vala, $valia);

              $valperca = $valia;
           }

           if($this->opcao == 'J'){

             $c.='<td>'.number_format($valJ[$k], 2, ',', '.').'</td>';

              $valperc = $valJ[$k];
              $calculo = $this->calcDisp($val[$k], $valJ[$k]);

              $ca='<td>'.number_format($valJa, 2, ',', '.').'</td>';

              $calculoa = $this->calcDisp($vala, $valJa);
              $valperca = $valJa;

           }

           if($this->opcao == 'J2'){
             $c.='<td>'.number_format($valJ2[$k], 2, ',', '.').'</td>';

              $valperc = $valJ2[$k];

              $calculo = $this->calcDisp($val[$k], $valJ2[$k]);

              $ca='<td>'.number_format($valJ2a, 2, ',', '.').'</td>';

              $calculoa = $this->calcDisp($vala, $valJ2a);

              $valperca = $valJ2a;

           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = (($calculo) / $valperc)*100;
               if($calculo < 0 ){
                  $formula = -abs($formula);
               }

           }

            $c.='<td>'.number_format($val[$k], 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';

        }

           if($valperca == 0 ){

               $formulaa = 0;

           }else{

               $formulaa = (($calculoa) / ($valperca))*100;
               if($calculoa < 0 ){
                  $formulaa = -abs($formulaa);
               }

           }

            $ca.='<td>'.number_format($vala, 2, ',', '.').'</td>';
            $ca.='<td>'.number_format($calculoa, 2, ',', '.').'</td>';
            $ca.='<td>'.number_format($formulaa, 2, ',', '.').'%</td>';

        //$conteudo.= '<th>'.number_format($this->lucroLiquidoAnual, 2, ',', '.').'</th>';
        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }


    public function boxLucroLiquidoPerc($titulo){
        $totMes = count($this->mes);
        $val = $this->lucroLiquidoPerc;
        $vali = $this->lucroLiquidoPerci;
        $valJ = $this->lucroLiquidoPercJ;
        $valJ2 = $this->lucroLiquidoPercJ2;


       // $vala = array_sum($this->lucroLiquidoPerc);

        $vala =   (array_sum($this->receitaOpLiquida)   == 0) ? 0 : array_sum($this->lucroLiquido) / array_sum($this->receitaOpLiquida);
        $valia =  (array_sum($this->receitaOpLiquidai)  == 0) ? 0 : array_sum($this->lucroLiquidoi) / array_sum($this->receitaOpLiquidai);
        $valJa =  (array_sum($this->receitaOpLiquidaJ)  == 0) ? 0 : array_sum($this->lucroLiquidoJ) / array_sum($this->receitaOpLiquidaJ);
        $valJ2a = (array_sum($this->receitaOpLiquidaJ2) == 0) ? 0 : array_sum($this->lucroLiquidoJ2) / array_sum($this->receitaOpLiquidaJ2);

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $a = 0;
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;
        $ba = 0;
        $aba = 0;
        $aa = 0;
        $b = 0;
        $ab = 0;

        for($k = 0 ; $k < $totMes;$k++){

            $percentual = 0;
            $valperc = 0;

            $percentuala = 0;
            $valperca = 0;

            if($this->opcao == 'I'){
                $a = $vali[$k]*100;
                $c.='<td>'.number_format(($a), 2, ',', '.').' %</td>';
                $valperc = $vali[$k];

                $aa = $valia*100;
                $ca='<td>'.number_format(($aa), 2, ',', '.').' %</td>';
                $valperca = $valia;
            }
            if($this->opcao == 'J'){
                $a = $valJ[$k]*100;
                $c.='<td>'.number_format(($a), 2, ',', '.').' %</td>';
                $valperc = $valJ[$k];

                $aa = $valJa*100;
                $ca='<td>'.number_format(($aa), 2, ',', '.').' %</td>';
                $valperca = $valJa;
            }
            if($this->opcao == 'J2'){
                $a = $valJ2[$k]*100;
                $c.='<td>'.number_format(($a), 2, ',', '.').' %</td>';
                $valperc = $valJ2[$k];

                $aa = $valJ2a*100;
                $ca='<td>'.number_format(($aa), 2, ',', '.').' %</td>';
                $valperca = $valJ2a;
            }
            $b = $val[$k]*100;
            $ab = $b-$a;
            $c.='<td>'.number_format(($b), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(abs($ab), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(abs($ab), 2, ',', '.').' %</td>';

        }

            $ba = $vala*100;
            $aba = $ba-$aa;
            $ca.='<td>'.number_format(($ba), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(abs($aba), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(abs($aba), 2, ',', '.').' %</td>';

        //$conteudo.= '<th>'.number_format(($this->lucroLiquidoAnualPerc*100), 2, ',', '.').' %</th>';
        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }
    public function boxDespesasCorporativas($titulo){
        $totMes = count($this->mes);
        $val = $this->despesasCorporativas;
        $vali = $this->despesasCorporativasi;
        $valJ = $this->despesasCorporativasJ;
        $valJ2 = $this->despesasCorporativasJ2;

        $vala = array_sum($this->despesasCorporativas);
        $valia = array_sum($this->despesasCorporativasi);
        $valJa = array_sum($this->despesasCorporativasJ);
        $valJ2a = array_sum($this->despesasCorporativasJ2);

        $conteudo= '<tr class="active" style="font-weight:bold; color:#000;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;

        for($k = 0 ; $k < $totMes;$k++){

           $percentual = 0;
           $valperc = 0;

           $percentuala = 0;
           $valperca = 0;

            $valor = $val[$k];
            if($valor > 0){
                $valor = $valor * -1;
            }

            $valora = $vala;
            if($valora > 0){
                $valora = $valora * -1;
            }

            if($this->opcao == 'I'){
                $valori = $vali[$k];
                if($valori > 0){
                    $valori = $valori * -1;
                }

                $calculo =  $valor -$valori;
                $valperc = $valori;
                $c.='<td>'.number_format($valori, 2, ',', '.').'</td>';

                $valoria = $valia;
                if($valoria > 0){
                    $valoria = $valoria * -1;
                }

                $calculoa =  $valora - $valoria;
                $valperca = $valoria;
                $ca='<td>'.number_format($valoria, 2, ',', '.').'</td>';

            }

            if($this->opcao == 'J'){
                $valorJ = $valJ[$k];
                if($valorJ > 0){
                    $valorJ = $valorJ * -1;
                }
                $calculo =  $valor - $valorJ;
                $valperc = $valorJ;
                $c.='<td>'.number_format($valorJ, 2, ',', '.').'</td>';


                $valorJa = $valJa;
                if($valorJa > 0){
                    $valorJa = $valorJa * -1;
                }
                $calculoa = $valora - $valorJa;
                $valperca = $valorJa;
                $ca='<td>'.number_format($valorJa, 2, ',', '.').'</td>';
            }

            if($this->opcao == 'J2'){
                $valorJ2 = $valJ2[$k];
                if($valorJ2 > 0){
                    $valorJ2 = $valorJ2 * -1;
                }
                $calculo = $valor - $valorJ2;
                $valperc = $valorJ2;
                $c.='<td>'.number_format($valorJ2, 2, ',', '.').'</td>';

                $valorJ2a = $valJ2a;
                if($valorJ2a > 0){
                    $valorJ2a = $valorJ2a * -1;
                }
                $calculoa = $valora - $valorJ2a;
                $valperca = $valorJ2a;
                $ca='<td>'.number_format($valorJ2a, 2, ',', '.').'</td>';
            }

            //$c.='<th>'.number_format($valor, 2, ',', '.').'</th>';

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = ($calculo / $valperc)*100;
           }

           $formula = $this->calculoPercentualDisp($calculo, $valperc, $valor);

            $c.='<td>'.number_format($valor, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';

        }


           if($valperca == 0 ){

               $formulaa = 0;

           }else{

               $formulaa = ($calculoa / $valperca)*100;
           }
           $formulaa = $this->calculoPercentualDisp($calculoa, $valperca, $valora);


            $ca.='<td>'.number_format($valora, 2, ',', '.').'</td>';
            $ca.='<td>'.number_format($calculoa, 2, ',', '.').'</td>';
            $ca.='<td>'.number_format($formulaa, 2, ',', '.').'%</td>';

        //$conteudo.= '<th>'.number_format($valorAno, 2, ',', '.').'</th>';
        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }

    public function boxDespesasCorporativasAC($titulo){
        $totMes = count($this->mes);

        $val = $this->despesasCorporativasAC;
        $vali = $this->despesasCorporativasACi;
        $valJ = $this->despesasCorporativasACJ;
        $valJ2 = $this->despesasCorporativasACJ2;

        $conteudo= '<tr class="active" style="font-weight:bold; color:#000;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;

        for($k = 0 ; $k < $totMes;$k++){
            $valor =  $val[$k];
            $valori =  $vali[$k];
            $valorJ =  $valJ[$k];
            $valorJ2 =  $valJ2[$k];

            if($valor > 0){
              $valor = $valor * -1;
            }
            if($valori > 0){
              $valori = $valori * -1;
            }
            if($valorJ > 0){
              $valorJ = $valorJ * -1;
            }
            if($valorJ2 > 0){
              $valorJ2 = $valorJ2 * -1;
            }
            $c.='<td>'.number_format($valor, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($valori, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($valorJ, 2, ',', '.').'</td>';
            $c.='<td>'.number_format($valorJ2, 2, ',', '.').'</td>';

        }

        $valorAno = $this->despesasCorporativasAnualAC;
        if($valorAno > 0){
          $valorAno = $valorAno * -1;
        }
       // $conteudo.= '<th>'.number_format($valorAno, 2, ',', '.').'</th>';
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }

    public function boxInvestimento($titulo){
        $totMes = count($this->mes);
        $val = $this->investimentos;
        $vali = $this->investimentosi;
        $valJ = $this->investimentosJ;
        $valJ2 = $this->investimentosJ2;

        $conteudo= '<tr class="active" style="font-weight:bold; color:#000;">';
        $conteudo.= '<td>'.$titulo.'</td>';

        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;
                //$t = $this->deducoesAnual;
        $ta = 0;
        $tc = 0;

        //$conteudo.= '<th>'.$t.'</th>';
            for($k = 0 ; $k < $totMes;$k++){

                    $calculo = 0;
                    $valperc = 0;

                if($this->opcao == 'I'){
                    $tc = $tc+$vali[$k];
                    $c.='<td>'.number_format($vali[$k], 2, ',', '.').'</td>';
                    $calculo = $vali[$k] - $val[$k];
                    $valperc = $vali[$k];
                }
                if($this->opcao == 'J'){
                    $tc = $tc+$valJ[$k];
                    $c.='<td>'.number_format($valJ[$k], 2, ',', '.').'</td>';
                    $calculo = $valJ[$k] - $val[$k];
                    $valperc = $valJ[$k];
                }
                if($this->opcao == 'J2'){
                    $tc = $tc+$valJ2[$k];
                    $c.='<td>'.number_format($valJ2[$k], 2, ',', '.').'</td>';
                    $calculo = $valJ2[$k] - $val[$k];
                    $valperc = $valJ2[$k];
                }

                $ta = $ta+$val[$k];
                //$c.='<th>'.number_format($val[$k], 2, ',', '.').'</th>';

                if($valperc == 0 ){

                    $formula = 0;

                }else{

                    $formula = ($calculo / $valperc)*100;
                }

                 $c.='<td>'.number_format($val[$k], 2, ',', '.').'</td>';
                 $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
                 $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';


//$t=$t+$val[$k];
            }
         $disp = $tc - $ta;
         if($tc == 0){
            $dispPerc = 0;
         }else{
            $dispPerc = $disp / $tc;
         }
        //$conteudo.= '<th>'.number_format($t, 2, ',', '.').'</th>';
        $conteudo.='<td>'.number_format($tc, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($ta, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($disp, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($dispPerc, 2, ',', '.').'%</td>';
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }

    public function boxFundoDeReserva($titulo){

        $totMes = count($this->mes);
        $val = $this->fundoDeReserva;
        $vali = $this->fundoDeReservai;
        $valJ = $this->fundoDeReservaJ;
        $valJ2 = $this->fundoDeReservaJ2;

        $conteudo= '<tr class="active" style="font-weight:bold; color:#000;">';
        $conteudo.= '<td>'.$titulo.'</td>';

        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;        //$t = $this->deducoesAnual;
        $ta = 0;
        $tc = 0;

        //$conteudo.= '<th>'.$t.'</th>';
            for($k = 0 ; $k < $totMes;$k++){

                    $calculo = 0;
                    $valperc = 0;

                if($this->opcao == 'I'){
                    $tc = $tc+$vali[$k];
                    $c.='<td>'.number_format($vali[$k], 2, ',', '.').'</td>';
                    $calculo = $vali[$k] - $val[$k];
                    $valperc = $vali[$k];
                }
                if($this->opcao == 'J'){
                    $tc = $tc+$valJ[$k];
                    $c.='<td>'.number_format($valJ[$k], 2, ',', '.').'</td>';
                    $calculo = $valJ[$k] - $val[$k];
                    $valperc = $valJ[$k];
                }
                if($this->opcao == 'J2'){
                    $tc = $tc+$valJ2[$k];
                    $c.='<td>'.number_format($valJ2[$k], 2, ',', '.').'</td>';
                    $calculo = $valJ2[$k] - $val[$k];
                    $valperc = $valJ2[$k];
                }

                $ta = $ta+$val[$k];
                //$c.='<th>'.number_format($val[$k], 2, ',', '.').'</th>';

                if($valperc == 0 ){

                    $formula = 0;

                }else{

                    $formula = ($calculo / $valperc)*100;
                }

                 $c.='<td>'.number_format($val[$k]*-1, 2, ',', '.').'</td>';
                 $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
                 $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';


//$t=$t+$val[$k];
            }
         $disp = $tc - $ta;
         if($tc == 0){
            $dispPerc = 0;
         }else{
            $dispPerc = $disp / $tc;
         }
        //$conteudo.= '<th>'.number_format($t, 2, ',', '.').'</th>';
        $conteudo.='<td>'.number_format($tc*-1, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($ta*-1, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($disp, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($dispPerc, 2, ',', '.').'%</td>';
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;

    }
    public function boxDeducoes($titulo){
        $totMes = count($this->mes);
        $val = $this->deducoes;
        $vali = $this->deducoesi;
        $valJ = $this->deducoesJ;
        $valJ2 = $this->deducoesJ2;

        $conteudo= '<tr class="active" style="font-weight:bold; color:#000;">';
        $conteudo.= '<td>'.$titulo.'</td>';

        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;
        $ta = 0;
        $tc = 0;

            for($k = 0 ; $k < $totMes;$k++){

                    $calculo = 0;
                    $valperc = 0;

                ///if($val[$k] < 0){ $val[$k] = $val[$k] * (-1); }
                $val[$k] = $val[$k] * (-1);

                //
                //$val[$k] = $val[$k] * (-1); bvcn

                if($this->opcao == 'I'){
                    $tc = $tc+$vali[$k];
                    $c.='<td>'.number_format($vali[$k], 2, ',', '.').'</td>';
                    $calculo = $vali[$k] - $val[$k];
                    $valperc = $vali[$k];
                }
                if($this->opcao == 'J'){
                    $tc = $tc+$valJ[$k];
                    $c.='<td>'.number_format($valJ[$k], 2, ',', '.').'</td>';
                    $calculo = $valJ[$k] - $val[$k];
                    $valperc = $valJ[$k];
                }
                if($this->opcao == 'J2'){
                    $tc = $tc+$valJ2[$k];
                    $c.='<td>'.number_format($valJ2[$k], 2, ',', '.').'</td>';
                    $calculo = $valJ2[$k] - $val[$k];
                    $valperc = $valJ2[$k];
                }

                $ta = $ta+$val[$k];

                if($val[$k] == 0 ){
                    $formula = 0;
                }elseif($valperc == 0 && $val[$k] <>0){
                   $formula = 100;
                }else{

                    $formula = ($calculo / $valperc)*100;
                }

                $formula = $this->calculoPercentualDisp($calculo, $valperc, $val[$k]);

                 //$c.='<td>'.number_format(-abs($val[$k]), 2, ',', '.').'</td>';
                 $c.='<td>'.number_format($val[$k], 2, ',', '.').'</td>';
                 $c.='<td>'.number_format($calculo, 2, ',', '.').'</td>';
                 $c.='<td>'.number_format($formula, 2, ',', '.').'%</td>';


            }
         $disp = $tc - $ta;
         if($ta == 0){
         //if($tc == 0){
            $dispPerc = 0;
         }elseif($tc == 0 && $ta <> 0){
            $dispPerc = 100;
         }else{
            $dispPerc = $disp / $tc;
         }

        $dispPerc = $this->calculoPercentualDisp($disp, $tc, $ta);

        $conteudo.='<td>'.number_format($tc, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format(-abs($ta), 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($disp, 2, ',', '.').'</td>';
        $conteudo.='<td>'.number_format($dispPerc, 2, ',', '.').'%</td>';
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }
    public function boxPassivo($titulo){
        $totMes = count($this->mes);
        $val = $this->passivo;
        $vali = $this->passivoi;
        $valJ = $this->passivoJ;
        $valJ2 = $this->passivoJ2;

        $conteudo= '<tr class="active" style="font-weight:bold; color:#000;background-color:#d9edf7!important">';
        $conteudo.= '<td style="background-color:#d9edf7!important">'.$titulo.'</td>';

        $c = '';
        $ca = '';
        $calculo = 0;
        $valperc = 0;
        $calculoa = 0;
        $calculo = 0;
        $valperca =0;
        $formula = 0;
        $formulaa = 0;
        $ta = 0;
        $tc = 0;

            for($k = 0 ; $k < $totMes;$k++){

                    $calculo = 0;
                    $valperc = 0;

                ///if($val[$k] < 0){ $val[$k] = $val[$k] * (-1); }
                $val[$k] = $val[$k] * (-1);

                //
                //$val[$k] = $val[$k] * (-1); bvcn

                if($this->opcao == 'I'){
                    $tc = $tc+$vali[$k];
                    $c.='<td style="background-color:#d9edf7!important">'.number_format($vali[$k], 2, ',', '.').'</td>';
                    $calculo = $vali[$k] - $val[$k];
                    $valperc = $vali[$k];
                }
                if($this->opcao == 'J'){
                    $tc = $tc+$valJ[$k];
                    $c.='<td style="background-color:#d9edf7!important">'.number_format($valJ[$k], 2, ',', '.').'</td>';
                    $calculo = $valJ[$k] - $val[$k];
                    $valperc = $valJ[$k];
                }
                if($this->opcao == 'J2'){
                    $tc = $tc+$valJ2[$k];
                    $c.='<td style="background-color:#d9edf7!important">'.number_format($valJ2[$k], 2, ',', '.').'</td>';
                    $calculo = $valJ2[$k] - $val[$k];
                    $valperc = $valJ2[$k];
                }

                $ta = $ta+$val[$k];

                if($val[$k] == 0 ){
                    $formula = 0;
                }elseif($valperc == 0 && $val[$k] <>0){
                   $formula = 100;
                }else{

                    $formula = ($calculo / $valperc)*100;
                }

                $formula = $this->calculoPercentualDisp($calculo, $valperc, $val[$k]);

                 //$c.='<td>'.number_format(-abs($val[$k]), 2, ',', '.').'</td>';
                 $c.='<td style="background-color:#d9edf7!important">'.number_format($val[$k], 2, ',', '.').'</td>';
                 $c.='<td style="background-color:#d9edf7!important">'.number_format($calculo, 2, ',', '.').'</td>';
                 $c.='<td style="background-color:#d9edf7!important">'.number_format($formula, 2, ',', '.').'%</td>';


            }
         $disp = $tc - $ta;
         if($ta == 0){
         //if($tc == 0){
            $dispPerc = 0;
         }elseif($tc == 0 && $ta <> 0){
            $dispPerc = 100;
         }else{
            $dispPerc = $disp / $tc;
         }

        $dispPerc = $this->calculoPercentualDisp($disp, $tc, $ta);

        $conteudo.='<td style="background-color:#d9edf7!important">'.number_format($tc, 2, ',', '.').'</td>';
        $conteudo.='<td style="background-color:#d9edf7!important">'.number_format(-abs($ta), 2, ',', '.').'</td>';
        $conteudo.='<td style="background-color:#d9edf7!important">'.number_format($disp, 2, ',', '.').'</td>';
        $conteudo.='<td style="background-color:#d9edf7!important">'.number_format($dispPerc, 2, ',', '.').'%</td>';
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;
    }
    public function boxCaixaAcionista($titulo){
        $totMes = count($this->mes);

        $fin = $this->grupo[14][149]['totalGrupo'];
        $fini = $this->grupo[14][149]['totalGrupoi'];
        $finJ = $this->grupo[14][149]['totalGrupoJ'];
        $finJ2 = $this->grupo[14][149]['totalGrupoJ2'];

        $aqui = $this->grupo[14][115]['totalGrupo'];
        $aquii = $this->grupo[14][115]['totalGrupoi'];
        $aquiJ = $this->grupo[14][115]['totalGrupoJ'];
        $aquiJ2 = $this->grupo[14][115]['totalGrupoJ2'];


        $inve = $this->investimentos;
        $invei = $this->investimentosi;
        $inveJ = $this->investimentosJ;
        $inveJ2 = $this->investimentosJ2;

        $depreciacao = $this->grupo[10][746]['totalGrupo'];
        $depreciacaoi = $this->grupo[10][746]['totalGrupoi'];
        $depreciacaoJ = $this->grupo[10][746]['totalGrupoJ'];
        $depreciacaoJ2 = $this->grupo[10][746]['totalGrupoJ2'];

        $val = $this->lucroLiquido;
        $vali = $this->lucroLiquidoi;
        $valJ = $this->lucroLiquidoJ;
        $valJ2 = $this->lucroLiquidoJ2;

        $conteudo ='';
        $conteudo.= '<tr style="background-color: #555555; color:#FFFF00;">';
        $conteudo.= '<th>'.$titulo.'</th>';

        $c = '';
        $ca = '';
        $ta = 0;
        $acaixa = 0;
        $acaixai = 0;
        $acaixaJ = 0;
        $acaixaJ2 = 0;
        $calculo = 0;
        $calculoa = 0;


        for($k = 0 ; $k < $totMes;$k++){


           $percentual = 0;
           $valperc = 0;

           $caixaLivre = ($val[$k]) -abs($inve[$k]) + abs($depreciacao[$k]);
           $caixaLivrei = ($vali[$k]) -abs($invei[$k]) + abs($depreciacaoi[$k]);
           $caixaLivreJ = ($valJ[$k]) -abs($inveJ[$k]) + abs($depreciacaoJ[$k]);
           $caixaLivreJ2 = ($valJ2[$k]) -abs($inveJ2[$k]) + abs($depreciacaoJ2[$k]);

           $caixa = $caixaLivre -abs($fin[$k])-abs($aqui[$k]);
           $caixai = $caixaLivrei -abs($fini[$k])-abs($aquii[$k]);
           $caixaJ = $caixaLivreJ -abs($finJ[$k])-abs($aquiJ[$k]);
           $caixaJ2 = $caixaLivreJ2 -abs($finJ2[$k])-abs($aquiJ2[$k]);

           $acaixa   = $acaixa + $caixa;
           $acaixai  = $acaixai + $caixai;
           $acaixaJ  = $acaixaJ + $caixaJ;
           $acaixaJ2 = $acaixaJ2 + $caixaJ2;

           if($this->opcao == 'I'){
              $c.='<th>'.number_format($caixai, 2, ',', '.').'</th>';
              $calculo = $caixa - $caixai;
              $valperc = $caixai;

           }

           if($this->opcao == 'J'){
              $c.='<th>'.number_format($caixaJ, 2, ',', '.').'</th>';
              $calculo = $caixa - $caixaJ;
              $valperc = $caixaJ;
           }

           if($this->opcao == 'J2'){
              $c.='<th>'.number_format($caixaJ2, 2, ',', '.').'</th>';
              $calculo = $caixa - $caixaJ2;
              $valperc = $caixaJ2;
           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = (($calculo) / ($valperc))*100;
           }

            $c.='<th>'.number_format($caixa, 2, ',', '.').'</th>';
            $c.='<th>'.number_format($calculo, 2, ',', '.').'</th>';
            $c.='<th>'.number_format($formula, 2, ',', '.').'%</th>';
        }


           if($this->opcao == 'I'){
              $ca='<th>'.number_format($acaixai, 2, ',', '.').'</th>';
              $calculoa = $acaixa - $acaixai;
              $valperca = $acaixai;

           }

           if($this->opcao == 'J'){
              $ca='<th>'.number_format($acaixaJ, 2, ',', '.').'</th>';
              $calculoa =  $acaixa - $acaixaJ;
              $valperca = $acaixaJ;
           }

           if($this->opcao == 'J2'){
              $ca='<th>'.number_format($acaixaJ2, 2, ',', '.').'</th>';
              $calculoa = $acaixa - $acaixaJ2;
              $valperca = $acaixaJ2;
           }

           if($valperca == 0 ){

               $formulaa = 0;

           }else{

               $formulaa = (($calculoa) / ($valperca))*100;
           }

        $ca.='<th>'.number_format($acaixa, 2, ',', '.').'</th>';
        $ca.='<th>'.number_format($calculoa, 2, ',', '.').'</th>';
        $ca.='<th>'.number_format($formulaa, 2, ',', '.').'%</th>';

        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='';
        return $conteudo;


    }
    public function boxCaixaAcionistaPerc($titulo){
       $totMes = count($this->mes);

        $fin = $this->grupo[14][149]['totalGrupo'];
        $fini = $this->grupo[14][149]['totalGrupoi'];
        $finJ = $this->grupo[14][149]['totalGrupoJ'];
        $finJ2 = $this->grupo[14][149]['totalGrupoJ2'];

        $inve = $this->investimentos;
        $invei = $this->investimentosi;
        $inveJ = $this->investimentosJ;
        $inveJ2 = $this->investimentosJ2;

        $depreciacao = $this->grupo[10][746]['totalGrupo'];
        $depreciacaoi = $this->grupo[10][746]['totalGrupoi'];
        $depreciacaoJ = $this->grupo[10][746]['totalGrupoJ'];
        $depreciacaoJ2 = $this->grupo[10][746]['totalGrupoJ2'];

        $val = $this->lucroLiquido;
        $vali = $this->lucroLiquidoi;
        $valJ = $this->lucroLiquidoJ;
        $valJ2 = $this->lucroLiquidoJ2;

        $aval = $this->receitaOpLiquida;
        $avali = $this->receitaOpLiquidai;
        $avalJ = $this->receitaOpLiquidaJ;
        $avalJ2 = $this->receitaOpLiquidaJ2;

        $aqui = $this->grupo[14][115]['totalGrupo'];
        $aquii = $this->grupo[14][115]['totalGrupoi'];
        $aquiJ = $this->grupo[14][115]['totalGrupoJ'];
        $aquiJ2 = $this->grupo[14][115]['totalGrupoJ2'];

        $conteudo ='';
        $conteudo.= '<tr style="background-color: #555555; color:#FFFF00;">';
        $conteudo.= '<th>'.$titulo.'</th>';

        $c = '';
        $ca = '';
        $ta = 0;
        $acaixa = 0;
        $acaixai = 0;
        $acaixaJ = 0;
        $acaixaJ2 = 0;
        $calculo = 0;
        $calculoa = 0;

        for($k = 0 ; $k < $totMes;$k++){


           $percentual = 0;
           $valperc = 0;


           $caixaLivre = ($val[$k]) -abs($inve[$k]) + abs($depreciacao[$k]);
           $caixaLivrei = ($vali[$k]) -abs($invei[$k]) + abs($depreciacaoi[$k]);
           $caixaLivreJ = ($valJ[$k]) -abs($inveJ[$k]) + abs($depreciacaoJ[$k]);
           $caixaLivreJ2 = ($valJ2[$k]) -abs($inveJ2[$k]) + abs($depreciacaoJ2[$k]);

           $caixa = $caixaLivre -abs($fin[$k])-abs($aqui[$k]);
           $caixai = $caixaLivrei -abs($fini[$k])-abs($aquii[$k]);
           $caixaJ = $caixaLivreJ -abs($finJ[$k])-abs($aquiJ[$k]);
           $caixaJ2 = $caixaLivreJ2 -abs($finJ2[$k])-abs($aquiJ2[$k]);

           $acaixa   = $acaixa + $caixa;
           $acaixai  = $acaixai + $caixai;
           $acaixaJ  = $acaixaJ + $caixaJ;
           $acaixaJ2 = $acaixaJ2 + $caixaJ2;

           if($this->opcao == 'I'){
              $a = $caixai / $avali[$k];
              $c.='<th>'.number_format($a*100, 2, ',', '.').'%</th>';
              //$calculo = abs($caixai) - abs($caixa);
              //$valperc = abs($caixai);

           }

           if($this->opcao == 'J'){
              $a = $caixaJ / $avalJ[$k];
              $c.='<th>'.number_format($a*100, 2, ',', '.').'%</th>';
              //$calculo = abs($caixaJ) - abs($caixa);
              //$valperc = abs($caixaJ);
           }

           if($this->opcao == 'J2'){
              $a = $caixaJ2 / $avalJ2[$k];
              $c.='<th>'.number_format($a*100, 2, ',', '.').'%</th>';
              //$calculo = abs($caixaJ2) - abs($caixa);
              //$valperc = abs($caixaJ2);
           }
           $b = $caixa / $aval[$k];
           $ab = $b - $a;
           $ab = $ab*100;

            $c.='<th>'.number_format($b*100, 2, ',', '.').'%</th>';
            $c.='<th>'.number_format($ab, 2, ',', '.').'%</th>';
            $c.='<th>'.number_format($ab, 2, ',', '.').'%</th>';
        }

           if($this->opcao == 'I'){
              $aa = $acaixai / array_sum($avali);
              $ca='<th>'.number_format($aa*100, 2, ',', '.').'%</th>';
              //$calculo = abs($caixai) - abs($caixa);
              //$valperc = abs($caixai);

           }

           if($this->opcao == 'J'){
              $aa = $acaixaJ / array_sum($avalJ);
              $ca='<th>'.number_format($aa*100, 2, ',', '.').'%</th>';
              //$calculo = abs($caixaJ) - abs($caixa);
              //$valperc = abs($caixaJ);
           }

           if($this->opcao == 'J2'){
              $aa = $acaixaJ2 / array_sum($avalJ2);
              $ca='<th>'.number_format($aa*100, 2, ',', '.').'%</th>';
              //$calculo = abs($caixaJ2) - abs($caixa);
              //$valperc = abs($caixaJ2);
           }
           $ba = $acaixa / array_sum($aval);
           $aba = $ba - $aa;
           $aba = $aba*100;

            $ca.='<th>'.number_format($ba*100, 2, ',', '.').'%</th>';
            $ca.='<th>'.number_format($aba, 2, ',', '.').'%</th>';
            $ca.='<th>'.number_format($aba, 2, ',', '.').'%</th>';

        //$conteudo.= '<th>'.number_format($this->lucroLiquidoAnual, 2, ',', '.').'</th>';
        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='';
        return $conteudo;
    }


public function boxCaixaLivre($titulo){
        $totMes = count($this->mes);

        $inve = $this->investimentos;
        $invei = $this->investimentosi;
        $inveJ = $this->investimentosJ;
        $inveJ2 = $this->investimentosJ2;

        $depreciacao = $this->grupo[10][746]['totalGrupo'];
        $depreciacaoi = $this->grupo[10][746]['totalGrupoi'];
        $depreciacaoJ = $this->grupo[10][746]['totalGrupoJ'];
        $depreciacaoJ2 = $this->grupo[10][746]['totalGrupoJ2'];


        $val = $this->lucroLiquido;
        $vali = $this->lucroLiquidoi;
        $valJ = $this->lucroLiquidoJ;
        $valJ2 = $this->lucroLiquidoJ2;

        $conteudo ='';
        $conteudo.= '<tr style="background-color: #555555; color:#FFFF00;">';
        $conteudo.= '<th>'.$titulo.'</th>';

        $c = '';
        $ca = '';
        $ta = 0;
        $acaixa = 0;
        $acaixai = 0;
        $acaixaJ = 0;
        $acaixaJ2 = 0;
        $calculo = 0;
        $calculoa = 0;

        for($k = 0 ; $k < $totMes;$k++){


           $percentual = 0;
           $valperc = 0;

           $caixa = ($val[$k]) -abs($inve[$k]) + abs($depreciacao[$k]);
           $caixai = ($vali[$k]) -abs($invei[$k]) + abs($depreciacaoi[$k]);
           $caixaJ = ($valJ[$k]) -abs($inveJ[$k]) + abs($depreciacaoJ[$k]);
           $caixaJ2 = ($valJ2[$k]) -abs($inveJ2[$k]) + abs($depreciacaoJ2[$k]);

           $acaixa   = $acaixa + $caixa;
           $acaixai  = $acaixai + $caixai;
           $acaixaJ  = $acaixaJ + $caixaJ;
           $acaixaJ2 = $acaixaJ2 + $caixaJ2;

           if($this->opcao == 'I'){
              $c.='<th>'.number_format($caixai, 2, ',', '.').'</th>';
              $calculo = $caixa - $caixai;
              $valperc = $caixai;

           }

           if($this->opcao == 'J'){
              $c.='<th>'.number_format($caixaJ, 2, ',', '.').'</th>';
              $calculo = $caixa - $caixaJ;
              $valperc = $caixaJ;
           }

           if($this->opcao == 'J2'){
              $c.='<th>'.number_format($caixaJ2, 2, ',', '.').'</th>';
              $calculo = $caixa - $caixaJ2;
              $valperc = $caixaJ2;
           }

           if($valperc == 0 ){

               $formula = 0;

           }else{

               $formula = (($calculo) / ($valperc))*100;
           }

            $c.='<th>'.number_format($caixa, 2, ',', '.').'</th>';
            $c.='<th>'.number_format($calculo, 2, ',', '.').'</th>';
            $c.='<th>'.number_format($formula, 2, ',', '.').'%</th>';

        }

           if($this->opcao == 'I'){
              $ca='<th>'.number_format($acaixai, 2, ',', '.').'</th>';
              $calculoa = $acaixa - $acaixai;
              $valperca = $acaixai;

           }

           if($this->opcao == 'J'){
              $ca='<th>'.number_format($acaixaJ, 2, ',', '.').'</th>';
              $calculoa = $acaixa - $acaixaJ;
              $valperca = $acaixaJ;
           }

           if($this->opcao == 'J2'){
              $ca='<th>'.number_format($acaixaJ2, 2, ',', '.').'</th>';
              $calculoa = $acaixa - $acaixaJ2;
              $valperca = $acaixaJ2;
           }

           if($valperca == 0 ){

               $formulaa = 0;

           }else{

               $formulaa = (($calculoa) / ($valperca))*100;
           }

        //$conteudo.= '<th>'.number_format($this->lucroLiquidoAnual, 2, ',', '.').'</th>';
        //$conteudo.= $ca;

        $ca.='<th>'.number_format($acaixa, 2, ',', '.').'</th>';
        $ca.='<th>'.number_format($calculoa, 2, ',', '.').'</th>';
        $ca.='<th>'.number_format($formulaa, 2, ',', '.').'%</th>';

        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='';
        return $conteudo;
    }


    public function boxABITDAPerc2($titulo){
        ////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////

        $totMes = count($this->mes);

        $lucroBruto = $this->ABITDA;
        $lucroBrutoi = $this->ABITDAi;
        $lucroBrutoJ = $this->ABITDAJ;
        $lucroBrutoJ2 = $this->ABITDAJ2;

        $receitaOpLiquida= $this->receitaOpLiquida;
        $receitaOpLiquidai = $this->receitaOpLiquidai;
        $receitaOpLiquidaJ = $this->receitaOpLiquidaJ;
        $receitaOpLiquidaJ2 = $this->receitaOpLiquidaJ2;

        $valy = array_sum($this->ABITDA);
        $valiy = array_sum($this->ABITDAi);
        $valJy = array_sum($this->ABITDAJ);
        $valJ2y = array_sum($this->ABITDAJ2);

        $valar = array_sum($this->receitaOpLiquida);
        $valiar = array_sum($this->receitaOpLiquidai);
        $valJar = array_sum($this->receitaOpLiquidaJ);
        $valJ2ar = array_sum($this->receitaOpLiquidaJ2);

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';
        $c = '';
        $ca = '';
        $percentual = 0;
        $valperc = 0;
        $percentuala = 0;
        $valperca = 0;
        $aaaa = 0;
        $aaaa1 = 0;
        $a  = 0;
        $a1 = 0;
        $a2 = 0;
        $disp  = 0;
        $disp  = 0;
        $aa  =0;
        $b1 = 0;
        $b2 = 0;
        $b = 0;

        $dispA = 0;
        $dispA = 0;

        for($k = 0 ; $k < $totMes;$k++){

            $percentual = 0;
            $valperc = 0;

            $percentuala = 0;
            $valperca = 0;

            if($valar == 0 && $valy == 0){
                $aaaa = 0;
            }elseif($valar == 0){
                $aaaa  = 100;
            }else{
                $aaaa  = (($valy) / ($valar))*100;
            }

            if($receitaOpLiquida[$k] == 0 && $lucroBruto[$k] == 0){
                $aaaa1 = 0;
            }elseif($receitaOpLiquida[$k] == 0){
                $aaaa1  = 100;
            }else{
                $aaaa1  = (($lucroBruto[$k]) / ($receitaOpLiquida[$k]))*100;
            }


            if($this->opcao == 'I'){

                $a  = ($receitaOpLiquidai[$k] == 0) ? 0 : ($lucroBrutoi[$k] / $receitaOpLiquidai[$k])*100;
                $a1 = $lucroBrutoi[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidai[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a2) == 0) ? 0 :(abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valiar == 0) ? 0 : ($valiy / $valiar)*100;
                $b1 = ($valiy-$valy);
                $b2 = ($valiar-$valar);

                $dispA  = (abs($b2) == 0) ? 0 :  (abs($b1) / abs($b2))*100;
                $dispA = $aaaa - $aa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J'){

                $a  = ($lucroBrutoJ[$k] / $receitaOpLiquidaJ[$k])*100;
                $a1 = $lucroBrutoJ[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidaJ[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valJy / $valJar)*100;
                $b1 = ($valJy-$valy);
                $b2 = ($valJar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA = $aaaa - $aa;
                //$dispA = $aa - $aaaa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J2'){

                $a  = ($lucroBrutoJ2[$k] / $receitaOpLiquidaJ2[$k])*100;
                $a1 = $lucroBrutoJ2[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidaJ2[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valJ2y / $valJ2ar)*100;
                $b1 = abs($valJ2y-$valy);
                $b2 = abs($valJ2ar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA =  $aaaa - $aa;
                //$dispA = $aa - $aaaa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }

            //$b = ($lucroBruto[$k] / $receitaOpLiquida[$k])*100;
            if($receitaOpLiquida[$k] == 0 && $lucroBruto[$k] == 0){
                $b = 0;
            }elseif($receitaOpLiquida[$k] == 0){
                $b  = 100;
            }else{
                $b = ($lucroBruto[$k] / $receitaOpLiquida[$k])*100;
            }

            //$disp = $a - $b;

            $c.='<td>'.number_format($b, 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($disp), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($disp), 2, ',', '.').' %</td>';
        }

            $ca.='<td>'.number_format($aaaa, 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($dispA), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($dispA), 2, ',', '.').' %</td>';

        ///$conteudo.= '<th>'.number_format(($this->lucroBrutoAnualPerc*100), 2, ',', '.').' %</th>';
        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;


    }

    public function boxABITDAPerc2Vei($titulo){
        ////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////

        $totMes = count($this->mes);

        $lucroBruto = $this->ABITDA_VEI;
        $lucroBrutoi = $this->ABITDAi_VEI;
        $lucroBrutoJ = $this->ABITDAJ_VEI;
        $lucroBrutoJ2 = $this->ABITDAJ2_VEI;

        $receitaOpLiquida= $this->receitaOpLiquida;
        $receitaOpLiquidai = $this->receitaOpLiquidai;
        $receitaOpLiquidaJ = $this->receitaOpLiquidaJ;
        $receitaOpLiquidaJ2 = $this->receitaOpLiquidaJ2;

        $valy = array_sum($this->ABITDA_VEI);
        $valiy = array_sum($this->ABITDAi_VEI);
        $valJy = array_sum($this->ABITDAJ_VEI);
        $valJ2y = array_sum($this->ABITDAJ2_VEI);

        $valar = array_sum($this->receitaOpLiquida);
        $valiar = array_sum($this->receitaOpLiquidai);
        $valJar = array_sum($this->receitaOpLiquidaJ);
        $valJ2ar = array_sum($this->receitaOpLiquidaJ2);

        $conteudo= '<tr style="background-color: #555555; color:#fff;">';
        $conteudo.= '<td>'.$titulo.'</td>';

        $c = '';
        $ca = '';
        $percentual = 0;
        $valperc = 0;
        $percentuala = 0;
        $valperca = 0;
        $aaaa = 0;
        $aaaa1 = 0;
        $a  = 0;
        $a1 = 0;
        $a2 = 0;
        $disp  = 0;
        $disp  = 0;
        $aa  =0;
        $b1 = 0;
        $b2 = 0;
        $b = 0;

        $dispA = 0;
        $dispA = 0;
        $calculo = 0;
        $calculoa = 0;

        for($k = 0 ; $k < $totMes;$k++){

            $percentual = 0;
            $valperc = 0;

            $percentuala = 0;
            $valperca = 0;


            if($valar == 0 && $valy == 0){
                $aaaa = 0;
            }elseif($valar == 0){
                $aaaa  = 100;
            }else{
                $aaaa  = (($valy) / ($valar))*100;
            }

            if($receitaOpLiquida[$k] == 0 && $lucroBruto[$k] == 0){
                $aaaa1 = 0;
            }elseif($receitaOpLiquida[$k] == 0){
                $aaaa1  = 100;
            }else{
                $aaaa1  = (($lucroBruto[$k]) / ($receitaOpLiquida[$k]))*100;
            }


            if($this->opcao == 'I'){

                $a  = ( $receitaOpLiquidai[$k] == 0) ? 0 : ($lucroBrutoi[$k] / $receitaOpLiquidai[$k])*100;
                $a1 = $lucroBrutoi[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidai[$k] - $receitaOpLiquida[$k];
                $disp  = ( abs($a2) == 0) ? 0 :  (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ( $valiar == 0) ? 0 :  ($valiy / $valiar)*100;
                $b1 = ($valiy-$valy);
                $b2 = ($valiar-$valar);

                $dispA  =( abs($b2) == 0) ? 0 :  (abs($b1) / abs($b2))*100;
                $dispA = $aaaa - $aa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J'){

                $a  = ($lucroBrutoJ[$k] / $receitaOpLiquidaJ[$k])*100;
                $a1 = $lucroBrutoJ[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidaJ[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valJy / $valJar)*100;
                $b1 = ($valJy-$valy);
                $b2 = ($valJar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA = $aaaa - $aa;
                //$dispA = $aa - $aaaa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }
            if($this->opcao == 'J2'){

                $a  = ($lucroBrutoJ2[$k] / $receitaOpLiquidaJ2[$k])*100;
                $a1 = $lucroBrutoJ2[$k] - $lucroBruto[$k];
                $a2 = $receitaOpLiquidaJ2[$k] - $receitaOpLiquida[$k];
                $disp  = (abs($a1) / abs($a2))*100;
                $disp  = $aaaa1 - $a;

                $c.='<td>'.number_format($a, 2, ',', '.').' %</td>';

                $aa  = ($valJ2y / $valJ2ar)*100;
                $b1 = abs($valJ2y-$valy);
                $b2 = abs($valJ2ar-$valar);

                $dispA  = (abs($b1) / abs($b2))*100;
                $dispA =  $aaaa - $aa;
                //$dispA = $aa - $aaaa;
                $ca='<td>'.number_format($aa, 2, ',', '.').' %</td>';

            }

            //$b = ($lucroBruto[$k] / $receitaOpLiquida[$k])*100;
            if($receitaOpLiquida[$k] == 0 && $lucroBruto[$k] == 0){
                $b = 0;
            }elseif($receitaOpLiquida[$k] == 0){
                $b  = 100;
            }else{
                $b = ($lucroBruto[$k] / $receitaOpLiquida[$k])*100;
            }

            //$disp = $a - $b;

            $c.='<td>'.number_format($b, 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($disp), 2, ',', '.').' %</td>';
            $c.='<td>'.number_format(($disp), 2, ',', '.').' %</td>';
        }

            $ca.='<td>'.number_format($aaaa, 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($dispA), 2, ',', '.').' %</td>';
            $ca.='<td>'.number_format(($dispA), 2, ',', '.').' %</td>';

        ///$conteudo.= '<th>'.number_format(($this->lucroBrutoAnualPerc*100), 2, ',', '.').' %</th>';
        $conteudo.= $ca;
        $conteudo.= $c;
        $conteudo.='</tr>';
        return $conteudo;


    }

public function calculaIss($mes) {

        $ano = $this->ano;
        $SQL_UNIDADE='';
        $sqlAll = array();
        $iss = array();
        $iss[$mes]['VLRIMP']  =  0;
        $iss[$mes]['VLRINI']  =  0;
        $iss[$mes]['VLRREA']  =  0;
        $iss[$mes]['VLRAJUS']  =  0;
        $iss[$mes]['VLRAJUS2']  =  0;

        if($this->opcaoview == "COMPETENCIA"){
            $fonteDados ='ORCLAN_DRE';

        }else{
            $fonteDados ='VW_ORCAMENTO_CAIXA_V1';
        }

        if(count($this->unidade) > 0){
          $unidades = implode(", ", $this->unidade);
          $SQL_UNIDADE = " AND CODUNN IN(".$unidades.") ";
        }

        $y=0;

            $tot = count($this->gasto);

            for($y=0;$y < $tot;$y++){
                $unidadeGasto = $this->gasto[$y];

                $sqlConf = "SELECT indiceiss FROM IMPOSTOS WHERE ano = ".$this->ano." AND filial=".$unidadeGasto;
                $rsConf = DB::select($sqlConf);
                //$configuracao = $rsConf->fetch();

                if(count($rsConf)>0){
                    $configuracao = $rsConf[0];
                    $configuracaoindiceiss  = $configuracao->indiceiss;
                }else{
                    $configuracaoindiceiss   = 0;
                }

                //dd($configuracao,$configuracao->indiceiss );



                $sqlAll[$y] = "SELECT SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA, SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE ANALIT = 164 AND CODCGA IN(".$unidadeGasto.")".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$mes]." AND CODCUS NOT IN(11,13) GROUP BY SINTET, ANALIT";
                $rs = DB::select($sqlAll[$y]);

                if(count($rs) > 0){
                    $valor = $rs[0];
                    $iss[$mes]['VLRIMP']  =  $iss[$mes]['VLRIMP'] + ($valor->VLRIMP * $configuracaoindiceiss *(-1));
                    $iss[$mes]['VLRINI']  =  $iss[$mes]['VLRINI'] + ($valor->VLRINI * $configuracaoindiceiss *(-1));
                    $iss[$mes]['VLRREA']  =  $iss[$mes]['VLRREA'] + ($valor->VLRREA * $configuracaoindiceiss *(-1));
                    $iss[$mes]['VLRAJUS']  =  $iss[$mes]['VLRAJUS'] + ($valor->VLRAJUS * $configuracaoindiceiss *(-1));
                    $iss[$mes]['VLRAJUS2']  =  $iss[$mes]['VLRAJUS2'] + ($valor->VLRAJUS2 * $configuracaoindiceiss *(-1));
                }



            }

        return $iss;

    }

    public function calcDisp($valorReal,$valorOrcado){
        //real - orc
        $dispercao = $valorReal - $valorOrcado;
        return $dispercao;
    }

    public function calculoDisp($valorReal,$valorOrcado){
        //real - orc
        $dispercao = $valorReal - $valorOrcado;

        if($valorReal < 0 && $valorOrcado < 0){
           $dispercao = -abs($dispercao);
        }

    }

    public function calculoPercentualDisp($dispercao,$orcado,$realizado,$cod = 0){
        //real - orc
        if($orcado == 0 && $realizado <> 0){
          if($cod == 154){
            $percentual = 100;
          }else{
            $percentual = -100;
          }
        }elseif($orcado <> 0 && $realizado == 0) {
          $percentual = 0;
        }elseif($orcado <> 0 && $realizado <> 0) {
          $percentual = ($dispercao/$orcado)*100;
          if($dispercao < 0){
              $percentual = -abs($percentual);
          }elseif($dispercao > 0){
              $percentual = abs($percentual);
          }
        }elseif($orcado == 0 && $realizado == 0){
          $percentual = 0;
        }elseif($orcado == $realizado) {
          $percentual = 100;
        }

        return $percentual;
    }


    public function percentualDisp($valor1,$valor2){


    }

    public function calculaIRPJ($mes) {

        $ano = $this->ano;
        $SQL_UNIDADE='';
        $SQL_UNIDADE_IMP = " AND CODUNN IN(19,20) ";
        $sqlAll = array();
        $iss[$mes]['VLRIMP']  =  0;
        $iss[$mes]['VLRINI']  =  0;
        $iss[$mes]['VLRREA']  =  0;
        $iss[$mes]['VLRAJUS']  =  0;
        $iss[$mes]['VLRAJUS2']  =  0;

        $irpj[$mes] = 0;
        $irpji[$mes] = 0;
        $irpjJ[$mes] = 0;
        $irpjJ2[$mes] = 0;

        if($this->opcaoview == "COMPETENCIA"){
            $fonteDados ='ORCLAN_DRE';

        }else{
            $fonteDados ='VW_ORCAMENTO_CAIXA_V1';
        }

        if(count($this->unidade) > 0){
          $unidades = implode(", ", $this->unidade);
          $SQL_UNIDADE = " AND CODUNN IN(".$unidades.") ";
        }

        $y=0;

            $tot = count($this->gasto);

            for($y=0;$y < $tot;$y++){
                $unidadeGasto = $this->gasto[$y];

                $sqlimp = "SELECT  SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA ,SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE  CODCGA IN(".$unidadeGasto.")".$SQL_UNIDADE_IMP." AND ANO = ".$ano." AND MES = ".$this->mes[$mes]." AND CODCUS NOT IN(11,13) GROUP BY SINTET, ANALIT";
                $rsImp  = DB::select($sqlimp);
                //$valorImp = $rsImp->fetch();

                //dd($sqlimp,$rsImp);
                $valorImp = $rsImp;

                //var_dump($valorImp);
                if(count($rsImp)>0){
                    $valorImp = $rsImp[0];
                    $impNotas = $valorImp->VLRREA;
                    $impNotasi = $valorImp->VLRINI;
                    $impNotasJ = $valorImp->VLRAJUS;
                    $impNotasJ2= $valorImp->VLRAJUS2;
                }else{
                    $impNotas = 0;
                    $impNotasi = 0;
                    $impNotasJ = 0;
                    $impNotasJ2= 0;
                }
                $sqlAll = "SELECT SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA, SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE ANALIT = 155 AND CODCGA IN(".$unidadeGasto.")".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$mes]." AND CODCUS NOT IN(11,13) GROUP BY SINTET, ANALIT";
                $rs = DB::select($sqlAll);
                //$valor155 = $rs->fetch();

                if(count($rs)>0){
                    $valor155 = $rs[0];
                    $v155 = $valor155->VLRREA;
                    $v155i = $valor155->VLRINI;
                    $v155J = $valor155->VLRAJUS;
                    $v155J2 = $valor155->VLRAJUS2;

                }else{
                    $v155 = 0;
                    $v155i = 0;
                    $v155J = 0;
                    $v155J2 = 0;
                }


                $sqlAll = "SELECT SINTET,ANALIT,SUM(VLRIMP) AS VLRIMP , SUM(VLRINI) AS VLRINI , SUM(VLRREA) AS VLRREA, SUM(VLRFOR) AS VLRAJUS , SUM(VLRAJUS2) AS VLRAJUS2  FROM ".$fonteDados." WHERE ANALIT = 164 AND CODCGA IN(".$unidadeGasto.")".$SQL_UNIDADE." AND ANO = ".$ano." AND MES = ".$this->mes[$mes]." AND CODCUS NOT IN(11,13) GROUP BY SINTET, ANALIT";
                $rs = DB::select($sqlAll);
                //$valor164 = $rs->fetch();

                if(count($rs)>0){
                    $valor164 = $rs[0];
                    $v164 = $valor164->VLRREA;
                    $v164i = $valor164->VLRINI;
                    $v164J = $valor164->VLRAJUS;
                    $v164J2 = $valor164->VLRAJUS2;
                }else{
                    $v164 = 0;
                    $v164i = 0;
                    $v164J = 0;
                    $v164J2 = 0;
                }

                $servico = $impNotas;
                $servicoi = $impNotasi;
                $servicoJ = $impNotasJ;
                $servicoJ2 = $impNotasJ2;

                $transporte = ($v155 + $v164 - $impNotas);
                $transportei = ($v155i + $v164i - $impNotasi);
                $transporteJ = ($v155J + $v164J - $impNotasJ);
                $transporteJ2 = ($v155J2 + $v164J2 - $impNotasJ2);

            $t = ($transporte * 0.08) + ($servico * 0.32);
            $ti = ($transportei * 0.08) + ($servicoi * 0.32);
            $tJ = ($transporteJ * 0.08) + ($servicoJ * 0.32);
            $tJ2 = ($transporteJ2 * 0.08) + ($servicoJ2 * 0.32);

            $pt = 0.08;
            $ps = 0.32;
            $pc = 0.10;

            if($t < 20000){
              $t = 0;

            }else{
              //$t2 = ($transporte * $this->irpjTrans ) + ($servico * $this->irpjServ);
              $t2 = 0;
              $t = (($transporte * $pt) + ($servico * $ps) - 20000) * ($pc);
              $t = $t+$t2;
            }

            if($ti < 20000){
              $ti = 0;

            }else{
              //$t2i = ($transportei * $this->irpjTrans ) + ($servicoi * $this->irpjServ);
              $t2i = 0;
              $ti = (($transportei * $pt) + ($servicoi * $ps) - 20000) * ($pc);
              $ti = $ti+$t2i;
            }

            if($tJ < 20000){
              $tJ = 0;

            }else{
              $tJ = (($transporteJ * $pt) + ($servicoJ * $ps) - 20000) * ($pc);
            }

            if($tJ2 < 20000){
              $tJ2 = 0;

            }else{
              $tJ2 = (($transporteJ2 * $pt) + ($servicoJ2 * $ps) - 20000) * ($pc);
            }

            $irpj[$mes] += (($transporte*($this->irpjTrans)) + ($servico*($this->irpjServ))) + $t;
            $irpji[$mes] += (($transportei*($this->irpjTrans)) + ($servicoi*($this->irpjServ))) + $ti;
            $irpjJ[$mes] += (($transporteJ*($this->irpjTrans)) + ($servicoJ*($this->irpjServ))) + $tJ;
            $irpjJ2[$mes] += (($transporteJ2*($this->irpjTrans)) + ($servicoJ2*($this->irpjServ))) + $tJ2;

            }

        return $irpj;


    }

}

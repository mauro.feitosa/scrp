<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;

class MenudreController extends Controller
{
    //
    public $sqlServerDb = 'sqlsrv';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
    }

    public function index(Request $request){

        $itemMenu = 2;

        View::share ( 'itemMenu', $itemMenu );

        $val = 1;

        return view('menudre.index',['val'=>$val]);
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,DB,Config;
use Datatables;
use View;
use Session;

class DeducoesController extends Controller
{
    public $itemMenu = 15;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu );
    }

    public function index()
    {
        //$pageIcon = 'admin-list';
        //$pageTitulo = 'Receita Rota Itens';
        //$itemMenu = 6;
        //View::share ( 'itemMenu', $itemMenu );

        return view('deducoes.index');
    }
    public function deducoesList()
    {
        $sql = "SELECT DEDUCAO.ID AS ID,
        DEDUCAO.ANO AS ANO,
        DEDUCAO.MES AS MES,
        ROUND(DEDUCAO.VALOR , 2) AS VALOR,
        DEDUCAO.CODCGA AS CODCGA,
        DEDUCAO.CODUNN AS CODUNN,
        RODCGA.DESCRI AS FILIAL,
        RODUNN.DESCRI AS UNIDADE
        FROM DEDUCAO
        INNER JOIN RODCGA ON DEDUCAO.CODCGA = RODCGA.CODCGA
        INNER JOIN RODUNN ON DEDUCAO.CODUNN = RODUNN.CODUNN
        WHERE RODCGA.SITUAC = 'A' ORDER BY DEDUCAO.ID DESC";
        $lista = DB::select($sql);

       // dd($lista);
        return datatables()->of($lista)
        ->addIndexColumn()
        ->addColumn('action', function($lista){

               $btn =  '<a href="'.route('deducoes.edit',['ID'=>$lista->ID]).'" class="btn btn-sm btn-info pull-left" style="margin-right: 3px;">Editar</a>';

                return $btn;
        })
        ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {

        $listaRodcga = DB::table('RODCGA')->select('CODCGA','DESCRI AS FILIAL')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();
        $listaRodunn = DB::table('RODUNN')->select('CODUNN','DESCRI AS UNIDADE')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();
        return view('deducoes.create',['listaRodcga'=>$listaRodcga,'listaRodunn'=>$listaRodunn]);
    }

    public function store(Request $request)
    {
        $VALOR = str_replace(',','.', str_replace('.','', $request->VALOR));
        $VALOR = str_replace('R$','',$VALOR);
       // dd($request->ANO);

       $val = array('CODCGA'=>$request->CODCGA, 'CODUNN'=>$request->CODUNN, 'ANO'=>$request->ANO, 'MES'=>$request->MES, 'VALOR'=>$VALOR);
       $insertVal = DB::table('DEDUCAO')->insert($val);

        return redirect()->route('deducoes.index')
            ->with('flash_message',
             'Registro Adicionado.');
    }

    public function show($id)
    {

    }

    public function edit(Request $request,$id)
    {


        $listaRodcga = DB::table('RODCGA')->select('CODCGA','DESCRI AS FILIAL')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();
        $listaRodunn = DB::table('RODUNN')->select('CODUNN','DESCRI AS UNIDADE')->where('SITUAC','=','A')->orderBy('DESCRI','ASC')->get();

        if($id){
            $deducoes = DB::table('DEDUCAO')->select('*')->where('id','=',$id)->first();
        }else{
            return redirect()->route('deducoes.index')
            ->with('flash_message',
             'Registro não encotrado.');
        }

        return view('deducoes.edit',['listaRodcga'=>$listaRodcga,'listaRodunn'=>$listaRodunn,'id'=>$id,'deducoes'=>$deducoes]);
    }

    public function update(Request $request, $id)
    {
        $VALOR = str_replace(',','.', str_replace('.','', $request->VALOR));
        $VALOR = str_replace('R$','',$VALOR);
        $val = array('CODCGA'=>$request->CODCGA, 'CODUNN'=>$request->CODUNN, 'ANO'=>$request->ANO, 'MES'=>$request->MES, 'VALOR'=>$VALOR);
        $updateVal  = DB::table('DEDUCAO')
            ->where('ID','=', $id)
            ->update($val);

        return redirect()->route('deducoes.index')
            ->with('flash_message',
             'Registro atualizado.');

    }

    public function destroy($id)
    {
        DB::table('DEDUCAO')->where('ID', '=', $id)->delete();

        return redirect()->route('deducoes.index')
            ->with('flash_message',
             'Registro excluido.');
    }


}

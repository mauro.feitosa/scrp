<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use View;

class MenuController extends Controller
{
    //
    public $menuTitulo = array();
    public $menuIcon = array();
    public $menuUrl = array();

    public function __construct()
    {
        $this->$menuTitulo[0]= 'Dashboard';
        $this->$menuIcon[0]= 'admin-home';
        $this->$menuUrl[0]= 'home';

        $this->$menuTitulo[1]= 'Previsão Orçamentaria';
        $this->$menuIcon[1]= 'admin-documentation';
        $this->$menuUrl[1]= 'dre/menu';

        $this->$menuTitulo[2]= 'Forecast';
        $this->$menuIcon[2]= 'admin-documentation';
        $this->$menuUrl[2]= 'forecast/menu';

        View::share ( 'menuTitulo', $menuTitulo );
        View::share ( 'menuIcon', $menuIcon );
        View::share ( 'menuUrl', $menuUrl );

    }    
    /*
    public function index(Request $request){

        $pageIcon = 'admin-documentation';
        $pageTitulo = 'Preenchimento DRE';
        $itemMenu = 2;

        View::share ( 'itemMenu', $itemMenu );
        View::share ( 'pageIcon', $pageIcon );
        View::share ( 'pageTitulo', $pageTitulo );
        $val = 1;

        return view('menudre.index',['val'=>$val]);
    }*/

}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Aprovador;

class Verificaaprovador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $aprv = Aprovador::where('user_id','=',Auth::user()->id)->first();
        if($aprv == null){
            Auth::user()->tpaprov = 'N';
        }else{
            Auth::user()->tpaprov = 'S';
        }
    }
}
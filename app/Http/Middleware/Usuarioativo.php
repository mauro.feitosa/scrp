<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Usuarioativo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    public function handle($request, Closure $next)
    {
        if (Auth::user()->ativo !== 'S') {

            return redirect('acessonegado');
        }

        return $next($request);
    }

}

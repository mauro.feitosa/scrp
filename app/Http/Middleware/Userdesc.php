<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Filial;
use App\User;
use App\Setor;
use App\Cargos;
use DB;

class Userdesc
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {   

        if (Auth::guard($guard)->check()) {
            //dd('login');
           // return redirect('/home');
          // echo '<pre>';
          // var_dump(Auth::guard($guard)->check());
          // var_dump(Auth::user());
          // echo '</pre>';

           $infoUser = User::where('username','=', Auth::user()->username)->first();
           //dd($infoUser->id_filial);

           if($infoUser->filial_id == null || $infoUser->setor_id == null){

              return redirect('/register');
           }else{
                if ($request->session()->exists('nomefilial') == false) {

                    $filialUser = Filial::select('abreviacao')->where('id', Auth::user()->filial_id)->first();
                    $setorUser = DB::table('setores')->select('nome')->where('id', Auth::user()->setor_id)->first();

                    // dd($filialUser->abreviacao);
                    $nomesetor = $setorUser->nome;
                    $nomefilial = $filialUser->abreviacao;

                    //$nomesetor = 'Setor';
                    //$nomefilial = 'Filial';

                    $request->session()->put('nomefilial', $nomefilial);
                    $request->session()->put('nomesetor', $nomesetor);
                }                 
           }


        }


 /*       if (Auth::check())
        {
            // $id = Auth::user()->getId();
           // dd($request->session()->exists('nomefilial'));
            //dd(Auth::user()->username);

            $infoUser = User::where('username','=', Auth::user()->username)->first();
            //dd($infoUser->id_filial);

            if($infoUser->id_filial == null || $infoUser->id_setor == null){

                return view('modulos.index', compact('modulos'));
            }

            if ($request->session()->exists('nomefilial') == false) {

                $filialUser = Filial::select('abreviacao')->where('id', Auth::user()->filial_id)->first();
                $setorUser = DB::table('setores')->select('nome')->where('id', Auth::user()->setor_id)->first();

               // dd($filialUser->abreviacao);
               $nomesetor = $setorUser->nome;
               $nomefilial = $filialUser->abreviacao;

               //$nomesetor = 'Setor';
               //$nomefilial = 'Filial';

                $request->session()->put('nomefilial', $nomefilial);
                $request->session()->put('nomesetor', $nomesetor);
            }          

        }

*/
        return $next($request);
    }
}

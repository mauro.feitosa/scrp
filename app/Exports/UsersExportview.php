<?php

namespace App\Exports;

use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;

class UsersExportview implements FromView, WithHeadings, WithTitle
{

    public function view(): View
    {
        $rs = DB::table('users')->select('name','email')->where('setor_id','=',1)->get();

        return view('exports.users', ['usuarios' => $rs]);
    }

    public function headings(): array
    {
        return [
            'nome', 'email'
        ];
    }

    public function title(): string
    {
        return 'Usuarios';
    }

}

<?php

namespace App\Exports;
use App\Exports\UsersExport;
use App\Exports\UsersExport2;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;

class MultExportvr implements WithMultipleSheets
{
    use Exportable;

    protected $year;

    public function __construct(int $ano, int $codunn, int $codcga)
    {
        $this->ano = $ano;
        $this->codunn = $codunn;
        $this->codcga = $codcga;

    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

            $grupo = DB::table('RODGRU_LIST')
            ->select('RODGRU_LIST.CODIGO','RODGRU_LIST.DESCRI')
            ->where('RODGRU_LIST.LISTAR', '=','s')
            ->get();
            //dd($grupo);
            $sheets[] = new VariaveisExport($this->ano,$this->codunn,$this->codcga);
            $sheets[] = new ReceitaExport($this->ano,$this->codunn,$this->codcga);

       return $sheets;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setores extends Model
{
    protected $table = 'setores';
    
    protected $fillable = ['nome','ativo','atendimento'];
    
    public function tipos()
    {
      return $this->hasMany('app\tipochamadosetor');
    }

}
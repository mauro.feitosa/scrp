<?php
namespace App\Imports;
use App\Imports\ItemImporta;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

use Maatwebsite\Excel\Concerns\WithConditionalSheets;

class MultImport implements WithMultipleSheets, SkipsUnknownSheets
{
    use WithConditionalSheets;

    public function conditionalSheets(): array
    {
        return [
            'Worksheet1' => new ItemImporta(),
            'Worksheet2' => new ItemImporta(),
            'Worksheet3' => new ItemImporta(),
        ];
    }

    public function onUnknownSheet($sheetName)
    {

        info("Sheet {$sheetName} nao encontrada");
    }
}

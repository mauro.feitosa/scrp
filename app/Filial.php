<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filial extends Model
{
	protected $table = 'filiais';
	protected $fillable = ['id','codcga','nome','abreviacao','ativo'];


public function users(){
    	return $this->hasMany('App\User');
    }

}

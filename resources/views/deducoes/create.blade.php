@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-center">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-bordered">
                        <form name="frmAdd" method="POST" action="{{route('deducoes.store')}}">
                        {{ csrf_field() }}
                        <div class="panel-body">
                                    <div class="form-group" >
                                        <label for="name">ANO (Ex: 2021)</label>
                                        <input class="form-control" type="text" name="ANO" />
                                    </div>
                                    <div class="form-group" >
                                        <label for="name">MES ( Ex: 12 para Dezembro)</label>
                                        <input class="form-control" type="text" name="MES" />
                                    </div>
                                    <div class="form-group  ">
                                            <label for="name">FILIAL</label>
                                            <select class="form-control select2" name="CODCGA">
                                                @foreach ($listaRodcga as $lr)
                                                <option value="{{$lr->CODCGA}}">{{$lr->CODCGA}} - {{$lr->FILIAL}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <div class="form-group  ">
                                        <label for="name">UNIDADE</label>
                                        <select class="form-control select2" name="CODUNN">
                                            @foreach ($listaRodunn as $lu)
                                            <option value="{{$lu->CODUNN}}">{{$lu->CODUNN}} - {{$lu->UNIDADE}}</option>
                                            @endforeach
                                        </select>
                                  </div>
                                    <div class="form-group" >
                                        <label for="name">VALOR</label>
                                        <input class="form-control currency" type="text" name="VALOR" />
                                    </div>
                        </div>
                        <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Salvar</button>
                        </div>
                       </form>
                    </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
          });
          $(function() {
          $(".currency").maskMoney({prefix:'R$ ', allowZero: true, allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

        });
</script>
@endsection

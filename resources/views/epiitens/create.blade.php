@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-center">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-bordered">
                        <form name="frmAdd" method="POST" action="{{route('epiitens.store')}}">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="form-group  ">
                                <label for="name">Classificação</label>
                                <select class="form-control select2" name="ANALIT">
                                    @foreach ($listaAnalit as $la)
                                    <option value="{{$la->ANALIT}}">{{$la->ANALIT}} - {{$la->ANALITICA}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group  ">
                                <label for="name">Produto</label>
                                <select class="form-control select2" name="EPI_SMLD_PROD_ID">
                                    @foreach ($listaProd as $lp)
                                    <option value="{{$lp->ID}}">{{$lp->DESCRI}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group  ">
                                <label for="name">Cargo</label>
                                <select class="form-control select2" name="EPI_SMLD_CARGO_ID">
                                    @foreach ($listaCargo as $lc)
                                    <option value="{{$lc->ID}}">{{$lc->DESCRI}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group  ">
                                <label for="name">Tipo</label>
                                <select class="form-control select2" name="TIPO">
                                    <option value="OP">OP</option>
                                    <option value="ADM">ADM</option>
                                </select>
                            </div>
                        </div>
                        <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Salvar</button>
                        </div>
                       </form>
                    </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
          });
</script>
@endsection

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
.topoMenu {
    background-color: #337ab7!important;
    color: #fffff!important;
    border-color: #337ab7!important;
}
.topoMenuTitulo {
    background-color: #337ab7!important;
    color: #fffff!important;
    font-size:12px;
    font-weight:bolder;
}
</style>
<div class="page-content container-fluid">
    <!--div class="clearfix container-fluid row"-->
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                            <table id="fixTable" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>FILIAL</th>
                                        <th>REGISTROS</th>
                                        <th>AÇÃO</th>
                                        <th style="width:300px!important;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($filial as $f)
                                    <tr>
                                    <td>{{$f->CODCGA}} - {{$arrayFilialDesc[$f->CODCGA]}}</td>
                                    <td>{{$f->TOTAL}}</td>
                                    <td>
                                    <input onclick="publica({{$f->CODCGA}})" class="btn  btn-success" id="btn{{$f->CODCGA}}" type="button" name="btn{{$f->CODCGA}}" value="PUBLICAR"/>
                                    </td>
                                    <td>
                                    <div class="progress" style="margin:0px">
                                    <div id="progress{{$f->CODCGA}}" class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                    </div>
                                    </div>
                                    </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </diV>
</div>
@endsection
@section('javascript')
<script>

publica = function(codcga) {

    console.log(codcga);

    getProgress(codcga);

    $.ajax({
        type:"post",
        url:"exportador/export",
        data:"codcga="+codcga,
        success:function(data){
            $('#progress'+codcga).css("width", data + '%');
        }
    });
      //  $('#progress'+codcga).css("width", codcga + '%');
    }

    function getProgress(codcga) {
        $.ajax({
            url: "exportador/getprogress",
            type: "post",
            data:"codcga="+codcga,
            success: function (data) {
                console.log(data);
                $('#progress'+codcga).css('width', data+'%');
                if(data!=='100'){
                    setTimeout(getprogress(codcga), 4000);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
                return false;
            }
        });
    }

</script>
@endsection


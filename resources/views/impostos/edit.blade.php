@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-fluid">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-bordered">
                        <form name="frmAdd" method="POST" action="{{route('impostos.update',$id)}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <input id="ano" type="hidden" name="ano" value="{{$impostos->ano}}">
                        <div class="form-group  ">
                            <label for="name">FILIAL</label>
                            <select class="form-control select2" name="filial">
                                @foreach ($listaRodcga as $lr)

                                    @if($lr->CODCGA == $impostos->filial)
                                    <option selected="selected" value="{{$lr->CODCGA}}">{{$lr->CODCGA}} - {{$lr->NOMEFILIAL}}</option>
                                    @else
                                    <option value="{{$lr->CODCGA}}">{{$lr->CODCGA}} - {{$lr->NOMEFILIAL}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                        <div class="panel-body">
                            <div class="form-group" >
                                <label for="name">ICMS</label>
                                <input class="form-control" type="text" name="icms" value="{{$impostos->icms}}"/>
                            </div>

                            <div class="form-group" >
                                <label for="name">CONFIS</label>
                            <input class="form-control" type="text" name="confis" value="{{$impostos->confis}}" />
                            </div>

                            <div class="form-group" >
                                <label for="name">INDICEISS</label>
                                <input class="form-control" type="text" name="indiceiss"  value="{{$impostos->indiceiss}}"/>
                            </div>

                            <div class="form-group" >
                                <label for="name">PIS</label>
                                <input class="form-control" type="text" name="pis"  value="{{$impostos->pis}}"/>
                            </div>

                            <div class="form-group" >
                                <label for="name">CONTRIBUICAO_SERV</label>
                                <input class="form-control" type="text" name="contribuicao_serv"  value="{{$impostos->contribuicao_serv}}"/>
                            </div>

                            <div class="form-group" >
                                <label for="name">CONTRIBUICAO_TRANS</label>
                                <input class="form-control" type="text" name="contribuicao_trans"  value="{{$impostos->contribuicao_trans}}"/>
                            </div>

                            <div class="form-group" >
                                <label for="name">IRPJ_SERV</label>
                                <input class="form-control" type="text" name="irpj_serv"  value="{{$impostos->irpj_serv}}"/>
                            </div>

                            <div class="form-group" >
                                <label for="name">IRPJ_TRANS</label>
                                <input class="form-control" type="text" name="irpj_trans"  value="{{$impostos->irpj_trans}}"/>
                            </div>

                            <div class="form-group" >
                                <label for="name">CPRB</label>
                                <input class="form-control" type="text" name="cprb"  value="{{$impostos->cprb}}"/>
                            </div>

                            <div class="form-group" >
                                <label for="name">DESPCORP</label>
                                <input class="form-control" type="text" name="despcorp"  value="{{$impostos->despcorp}}"/>
                            </div>

                        </div>
                        <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Salvar</button>
                        </div>
                       </form>
                    </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
          });
</script>
@endsection

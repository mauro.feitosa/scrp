@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
<nav class="navbar navbar-default navbar-fixed-top navbar-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="hamburger btn-link">
                <span class="hamburger-inner"></span>
            </button>

            <ol class="breadcrumb hidden-xs" style="text-transform:uppercase!important;">
                <li class="active"><i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}</li>
                @if(isset($subMenu))
                {!!$subMenu!!}
                @endif
            </ol>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown profile">
                <a href="#" class="dropdown-toggle text-right" data-toggle="dropdown" role="button"
                   aria-expanded="false"><img src="http://scrp.grupohorizonte.com.br/uploads/users/default.png" class="profile-img"> <span
                            class="caret"></span></a>
                <ul class="dropdown-menu dropdown-menu-animated">
                    <li class="profile-img">
                        <img src="http://scrp.grupohorizonte.com.br/uploads/users/default.png" class="profile-img">
                        <div class="profile-body">
                            <h5>{{ ucwords(Auth::user()->name) }}</h5>
                            <h6>{{ Auth::user()->email }}</h6>
                        </div>
                    </li>
                    <li class="divider"></li>
                            <!--li class="class-full-of-rum">
                                <a href="http://scrp.grupohorizonte.com.br/admin/profile" ><i class="admin-person"></i>Profile</a>
                            </li>
                            <li ><a href="/" target="_blank">
                                <i class="admin-home"></i>Home</a>
                            </li-->
                        <li>
                            <form action="{{ route('logout') }}" method="POST">
                                    {!! csrf_field() !!}
                            <button type="submit" class="btn btn-danger btn-block"><i class="admin-power"></i>Sair</button>
                        </form>
                    </li>
                </ul>   
            </li>     
        </ul>
    </div>
</nav>

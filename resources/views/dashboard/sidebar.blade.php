@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('home') }}">
                    <div class="logo-icon-container">
                        <img src="http://scrp.grupohorizonte.com.br/assets/images/logo-icon-light.png" alt="Logo Icon">
                    </div>
                    <div class="title">SCRP</div>
                </a>
            </div><!-- .navbar-header -->

            <div class="panel widget center bgimage"
                style="background-image:url(/assets/images/bg.jpg); background-size: cover; background-position: 0px;">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <img src="http://scrp.grupohorizonte.com.br/uploads/users/default.png" class="avatar" alt="mauro.feitosa avatar">
                    <h4>{{ ucwords(Auth::user()->name) }}</h4>
                    <p>{{ Auth::user()->email }}</p>

                    <a href="http://scrp.grupohorizonte.com.br/admin/profile" class="btn btn-primary">Profile</a>
                    <div style="clear:both"></div>
                </div>
            </div>

        </div>
        <ul class="nav navbar-nav">
        <li class="@if($itemMenu == 0) active @endif">
            <a href="{{route($menuUrl[0])}}" target="_self">
                <span class="icon {{$menuIcon[0]}}"></span>
                   <span class="title">{{$menuTitulo[0]}}</span>
            </a>
                </li>
        <li class="@if($itemMenu == 17) active @endif">
            <a href="{{route($menuUrl[17])}}" target="_self">
                <span class="icon {{$menuIcon[17]}}"></span>
                <span class="title">{{$menuTitulo[17]}}</span>
            </a>
        </li>
        <li class="@if($itemMenu == 14) active @endif">
            <a href="{{route($menuUrl[14])}}" target="_self">
                <span class="icon {{$menuIcon[14]}}"></span>
                <span class="title">{{$menuTitulo[14]}}</span>
            </a>
        </li>
        <li class="@if($itemMenu == 1) active @endif">
            <a href="{{route($menuUrl[1])}}" target="_self">
                <span class="icon {{$menuIcon[1]}}"></span>
                <span class="title">{{$menuTitulo[1]}}</span>
            </a>
        </li>
        <!--li class="@if($itemMenu ==  2) active @endif">
        <a href="{{route($menuUrl[2])}}" target="_self">
                    <span class="icon {{$menuIcon[2]}}"></span>
                    <span class="title">{{$menuTitulo[2]}}</span>
                </a>
        </li-->
        @if(Auth::user()->perfil == 'A')
        <li class="dropdown">
            <a href="#tools-dropdown-element" data-toggle="collapse" aria-expanded="false" target="_self">
                <span class="icon {{$menuIcon[3]}}"></span>
                <span class="title">{{$menuTitulo[3]}}</span>
            </a>
                        <div id="tools-dropdown-element" class="panel-collapse collapse ">
                    <div class="panel-body">
                        <ul class="nav navbar-nav">



        <li class="@if($itemMenu ==  4) active @endif">
            <a href="{{route($menuUrl[4])}}" target="_self">
                <span class="icon {{$menuIcon[4]}}"></span>
                <span class="title">{{$menuTitulo[4]}}</span>
            </a>
        </li>
        <li class="@if($itemMenu ==  8) active @endif">
            <a href="{{route($menuUrl[8])}}" target="_self">
                <span class="icon {{$menuIcon[8]}}"></span>
                <span class="title">{{$menuTitulo[8]}}</span>
            </a>
        </li>
        <li class="@if($itemMenu ==  5) active @endif">
            <a href="{{route($menuUrl[5])}}" target="_self">
                <span class="icon {{$menuIcon[5]}}"></span>
                <span class="title">{{$menuTitulo[5]}}</span>
            </a>
        </li>
        <li class="@if($itemMenu ==  6) active @endif">
                <a href="{{route($menuUrl[6])}}" target="_self">
                    <span class="icon {{$menuIcon[6]}}"></span>
                    <span class="title">{{$menuTitulo[6]}}</span>
                </a>
        </li>
        <li class="@if($itemMenu ==  7) active @endif">
            <a href="{{route($menuUrl[7])}}" target="_self">
                <span class="icon {{$menuIcon[7]}}"></span>
                <span class="title">{{$menuTitulo[7]}}</span>
            </a>
        </li>
        <li class="@if($itemMenu ==  9) active @endif">
            <a href="{{route($menuUrl[9])}}" target="_self">
                <span class="icon {{$menuIcon[9]}}"></span>
                <span class="title">{{$menuTitulo[9]}}</span>
            </a>
        </li>
        <li class="@if($itemMenu ==  10) active @endif">
                <a href="{{route($menuUrl[10])}}" target="_self">
                    <span class="icon {{$menuIcon[10]}}"></span>
                    <span class="title">{{$menuTitulo[10]}}</span>
                </a>
            </li>
            <li class="@if($itemMenu ==  11) active @endif">
                <a href="{{route($menuUrl[11])}}" target="_self">
                    <span class="icon {{$menuIcon[11]}}"></span>
                    <span class="title">{{$menuTitulo[11]}}</span>
                </a>
            </li>
            <li class="@if($itemMenu ==  12) active @endif">
                <a href="{{route($menuUrl[12])}}" target="_self">
                    <span class="icon {{$menuIcon[12]}}"></span>
                    <span class="title">{{$menuTitulo[12]}}</span>
                </a>
            </li>
            <li class="@if($itemMenu ==  13) active @endif">
                <a href="{{route($menuUrl[13])}}" target="_self">
                    <span class="icon {{$menuIcon[13]}}"></span>
                    <span class="title">{{$menuTitulo[13]}}</span>
                </a>
            </li>
            <li class="@if($itemMenu ==  15) active @endif">
                <a href="{{route($menuUrl[15])}}" target="_self">
                    <span class="icon {{$menuIcon[15]}}"></span>
                    <span class="title">{{$menuTitulo[15]}}</span>
                </a>
            </li>
            <li class="@if($itemMenu ==  16) active @endif">
                <a href="{{route($menuUrl[16])}}" target="_self">
                    <span class="icon {{$menuIcon[16]}}"></span>
                    <span class="title">{{$menuTitulo[16]}}</span>
                </a>
            </li>
        <!--li class="">
            <a href="https://demo.laraveladminpanel.com/admin/form-designer" target="_self">
                <span class="icon admin-wand"></span>
                <span class="title">Form Designer</span>
            </a>
                </li>

        <li class="">
            <a href="https://demo.laraveladminpanel.com/admin/database" target="_self">
                <span class="icon admin-data"></span>
                <span class="title">Database</span>
            </a>
                </li>

        <li class="">
            <a href="https://demo.laraveladminpanel.com/admin/compass" target="_self">
                <span class="icon admin-compass"></span>
                <span class="title">Compass</span>
            </a>
                </li-->

            </ul>
            </div>
        </li>
        @endif
    </ul>
    </nav>
</div>

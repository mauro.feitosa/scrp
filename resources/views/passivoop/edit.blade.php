@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-fluid">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                        <form name="frmAdd" method="POST" action="{{route('passivoop.update',$id)}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group  ">
                            <div class="form-group" >
                                <label for="name">ANO (Ex: 2021)</label>
                                <input class="form-control" type="text" name="ANO" value="{{$passivoop->ANO}}"/>
                            </div>
                            <div class="form-group" >
                                <label for="name">MES ( Ex: 12 para Dezembro)</label>
                                <input class="form-control" type="text" name="MES" value="{{$passivoop->MES}}"/>
                            </div>
                            <div class="form-group" >
                            <label for="name">FILIAL</label>
                            <select class="form-control select2" name="CODCGA">
                                @foreach ($listaRodcga as $lr)

                                    @if($lr->CODCGA == $passivoop->CODCGA)
                                    <option selected="selected" value="{{$lr->CODCGA}}">{{$lr->CODCGA}} - {{$lr->FILIAL}}</option>
                                    @else
                                    <option value="{{$lr->CODCGA}}">{{$lr->CODCGA}} - {{$lr->FILIAL}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                            <div class="form-group" >
                            <label for="name">UNIDADE</label>
                            <select class="form-control select2" name="CODUNN">
                                @foreach ($listaRodunn as $lu)

                                    @if($lu->CODUNN == $passivoop->CODUNN)
                                    <option selected="selected" value="{{$lu->CODUNN}}">{{$lu->CODUNN}} - {{$lu->UNIDADE}}</option>
                                    @else
                                    <option value="{{$lu->CODUNN}}">{{$lu->CODUNN}} - {{$lu->UNIDADE}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                        </div>

                            <div class="form-group" >
                                <label for="name">VALOR</label>
                            <input class="form-control currency" type="text" name="VALOR" value="{{number_format($passivoop->VALOR, 2, ',', '.')}}" />
                            </div>

                        <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Salvar</button>
                        </div>
                       </form>
                    </div>
                </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
          });

          $(function() {
          $(".currency").maskMoney({prefix:'R$ ', allowZero: true, allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

        });
</script>
@endsection

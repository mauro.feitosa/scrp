<!DOCTYPE html>
<html lang="en">
    <head>
        <head>
            <title>@yield('page_title')</title>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="csrf-token" content="{{ csrf_token() }}"/>

            <!-- Favicon -->
            <link rel="shortcut icon" href="{{ asset('assets/images/logo-icon.png') }}" type="image/x-icon">

            <!-- App CSS -->
            <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">

            @yield('css')

            <!-- Few Dynamic Styles -->
            <style type="text/css">
                .admin .side-menu .navbar-header {
                    background:#22A7F0;
                    border-color:#22A7F0;
                }
                .widget .btn-primary{
                    border-color:#22A7F0;
                }
                .widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
                    background:#22A7F0;
                }
                .admin .breadcrumb a{
                    color:#22A7F0;
                }
            </style>

            @yield('head')

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .code {
                border-right: 2px solid;
                font-size: 26px;
                padding: 0 15px 0 15px;
                text-align: center;
            }

            .message {
                font-size: 18px;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="code" style="color:red">
                <i class="admin-lock"></i><span> ACESSO NEGADO</span>
            </div>

            <div class="message" style="padding: 10px;">
                <b>SOLICITE ACESSO ATRAVÉS DE ABERTURA DE CHAMADO PELO <span style="color:mediumblue">WORKFLOW</span></b>. <a href="{{route('login')}}" class="btn btn-success" > SAIR </a>
            </div>
        </div>
    </body>
</html>

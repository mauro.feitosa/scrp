@extends('errors::minimal')

@section('title', __('Serviço não disponivél'))
@section('code', '503')
@section('message', __($exception->getMessage() ?: 'Serviço não disponivél'))

@extends('master')

@section('page_header')
@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
            <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

        </div>
    </div>
</div>
@endsection

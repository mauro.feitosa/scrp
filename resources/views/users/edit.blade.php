@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-fluid">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-bordered">
                            {{ Form::model($usuario, array('route' => array('users.update', $usuario->id), 'method' => 'PUT','enctype'=> 'multipart/form-data')) }} {{-- Form model binding to automatically populate our fields with user data --}}

                            <div class="form-group" style="padding-left:10px;padding-top:20px;">
                                {{ Form::label('nomeusuario', $usuario->name) }}

                            </div>

                            <div class="form-group" style="padding-left:10px;">
                                {{ Form::label('ativo', 'Ativo') }}
                                {{ Form::select('ativo', ['S' => 'Sim', 'N' => 'Não'], $usuario->ativo,array('class' => 'form-control','style'=>'width:200px;!important')) }}
                            </div>

                            <div class="form-group" style="padding-left:10px;">
                                {{ Form::label('perfil', 'Perfil') }}
                                {{ Form::select('perfil', ['U' => 'Usuário', 'A' => 'Administrador'], $usuario->perfil,array('class' => 'form-control','style'=>'width:200px;!important')) }}
                            </div>

                            <div class="form-group" style="padding-left:10px;">
                                {{ Form::label('permissaoeditar', 'Permissão para Editar') }}
                                {{ Form::select('permissaoeditar', ['N' => 'Não', 'S' => 'Sim'], $permissaoeditar,array('class' => 'form-control','style'=>'width:200px;!important')) }}
                            </div>

                            <div class="form-group">
                                <table>
                                    <tr>
                                        <td colspan="3"><span style="padding-left:10px;"><b>Permissões</b></span></td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <ul class="list-group" style="font-size:10px!important;padding-left:10px;">
                                            <li class="list-group-item active">CENTRO DE GASTO</li>
                                            <li class="list-group-item" style="background-color:lightgrey"><label><input id="checkAll1" type="checkbox"> MARCAR/DESMARCAR TODOS </label></li>
                                                @foreach ($RODCGA as $rCGA)

                                                    @if(in_array($rCGA->CODCGA,$usuario_codcga))
                                                        <li class="list-group-item"><label><input checked="checked" class="item1" name="codcga[]" type="checkbox" value="{{$rCGA->CODCGA}}"> {{$rCGA->CODCGA}} - {{$rCGA->DESCRI}}</label></li>
                                                    @else
                                                        <li class="list-group-item"><label><input class="item1" name="codcga[]" type="checkbox" value="{{$rCGA->CODCGA}}"> {{$rCGA->CODCGA}} - {{$rCGA->DESCRI}}</label></li>
                                                    @endif

                                                @endforeach
                                            </ul>
                                       </td>
                                        <td valign="top">
                                            <ul class="list-group" style="font-size:10px!important;padding-left:10px;">
                                            <li class="list-group-item active">UNIDADE</li>
                                            <li class="list-group-item" style="background-color:lightgrey"><label><input id="checkAll2" type="checkbox"> MARCAR/DESMARCAR TODOS </label></li>
                                                @foreach ($RODUNN as $rUNN)

                                                    @if(in_array($rUNN->CODUNN,$usuario_unidade))
                                                        <li class="list-group-item"><label><input checked="checked" class="item2" name="rodunn[]" type="checkbox" value="{{$rUNN->CODUNN}}"> {{$rUNN->CODUNN}} - {{$rUNN->DESCRI}}</label></li>
                                                    @else
                                                        <li class="list-group-item"><label><input class="item2" name="rodunn[]" type="checkbox" value="{{$rUNN->CODUNN}}"> {{$rUNN->CODUNN}} - {{$rUNN->DESCRI}}</label></li>
                                                    @endif

                                                @endforeach
                                            </ul>
                                        </td>
                                        <td valign="top">
                                            <ul class="list-group" style="font-size:10px!important;padding-left:10px;">
                                            <li class="list-group-item active">PACOTE</li>
                                            <li class="list-group-item" style="background-color:lightgrey"><label><input id="checkAll3" type="checkbox"> MARCAR/DESMARCAR TODOS </label></li>
                                                @foreach ($RODGRU as $pac)

                                                    @if(in_array($pac->CODIGO,$usuario_pacote))
                                                        <li class="list-group-item"><label><input checked="checked" class="item3" name="codigo[]" type="checkbox" value="{{$pac->CODIGO}}"> {{$pac->CODIGO}} - {{$pac->DESCRI}}</label></li>
                                                    @else
                                                        <li class="list-group-item"><label><input class="item3" name="codigo[]" type="checkbox" value="{{$pac->CODIGO}}"> {{$pac->CODIGO}} - {{$pac->DESCRI}}</label></li>
                                                    @endif

                                                @endforeach
                                        </ul>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        <div class="panel-footer">
                                {{ Form::submit('Atualizar', array('class' => 'btn btn-primary')) }}
                        </div>
                       </form>
                    </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
    $('#checkAll1').click(function () {
    $(':checkbox.item1').prop('checked', this.checked);
    });

    $('#checkAll2').click(function () {
    $(':checkbox.item2').prop('checked', this.checked);
    });

    $('#checkAll3').click(function () {
    $(':checkbox.item3').prop('checked', this.checked);
    });

        $(document).ready( function () {



          });
</script>
@endsection

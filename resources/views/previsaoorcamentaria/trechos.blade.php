@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp

@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff;
    background:  #FFBF00!important;
    font-weight: bold;
}
@media (max-width: 400px) {

}
.table {
        font-size:10px !important;
    }
#parent {
    min-height: height: calc(100vh - 130px);
    height: calc(100vh - 130px);
			}

#fixTable {
    width: 1800px !important;
}
.cabMes {
    width:100px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: center;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabClass {
    width:400px!important;
    text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabNum {
    width:50px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabAltura{
    height: 310px!important);
}
.cabValor {
    height:41px!important;
    vertical-align: middle!important;
}
.cabValorTit {
    height:41px!important;
    vertical-align: middle!important;
    font-weight: bold!important;
    text-transform: uppercase!important;
}
.cabSintetica{background-color:#B8CCE4;font-weight: bold!important}
.tituloTrecho{background-color:#F5F5F5!important;font-weight: bold!important}
.currency{width:100px;}
.currencyVal{width:100px;}

#parent::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #C1C1C1;
}

.panel-bordered>.panel-body {
    padding: 10px 10px 10px;
    overflow: hidden;
}
body {
    overflow: hidden;
}
.badge:hover {
  color: #ffffff;
  text-decoration: none;
  cursor: pointer;
}
.badge-error {
  background-color: #b94a48;
}
.badge-error:hover {
  background-color: #953b39;
}
.badge-warning {
  background-color: #f89406;
}
.badge-warning:hover {
  background-color: #c67605;
}
.badge-success {
  background-color: #468847;
}
.badge-success:hover {
  background-color: #356635;
}
.badge-info {
  background-color: #3a87ad;
}
.badge-info:hover {
  background-color: #2d6987;
}
.badge-inverse {
  background-color: #333333;
}
.badge-inverse:hover {
  background-color: #1a1a1a;
}
.btn {
    padding: 4px 6px;
    font-size: 12px;
    margin-top: 0px;
    margin-bottom: 0px;
    font-weight: bolder;
}
.formZero{
    margin:0px;
    padding:0px;
}
input {font-weight:bold;}

input[type="text"]:read-only:not([read-only="false"]) { color: blue; background-color: #eee; border-width: 1px!important;border-style: solid!important; border-color: #A9A9A9!important;}

</style>
<div id="#admin" class="page-content container-fluid">
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">

                        <form id="frmAdd" name="frmAdd" method="POST" action="{{route('previsaoorcamentaria/trechos')}}" class="formZero">
                            {{ csrf_field() }}
                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                            <input type="hidden" name="codgru" value="{{$codgru}}"/>
                            <input type="hidden" value="ok" name="addtrecho" />

                        </form>

                        <form id="frmRemover" name="frmAdd" method="POST" action="{{route('previsaoorcamentaria/trechos')}}" class="formZero">
                            {{ csrf_field() }}
                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                            <input type="hidden" name="codgru" value="{{$codgru}}"/>
                            <input type="hidden" value="ok" name="removertrecho" />
                            <input id="remover_trecho_id" type="hidden" value="" name="trecho" />
                        </form>

                <form id="my-form" name="frmSalvar" method="POST" action="{{route('previsaoorcamentaria/trechos')}}" class="formZero">
                    <input type="hidden" value="ok" name="salvar" />
                    {{ csrf_field() }}
                    <div  class="panel panel-bordered">
                    <div style="margin-left:14px;margin-top:8px;font-size:14px;display:table;width:100%">
                        <span class="badge badge-error">{{$CODCGA}}</span>
                        <span type="submit" class="badge badge-info">{{$centrodegasto}}</span> /
                        <span class="badge badge-error">{{$CODUNN}}</span><span class="badge badge-info">{{$unidade}}</span> /
                        <span class="badge badge-warning">{{$pacote}}</span> /
                        <span class="badge badge-warning">TRECHOS</span>


                        @if(isset($subitem))
                        / <span class="badge badge-warning">{{$subitem}}</span>
                        @endif
                        <div class="pull-right" style="margin-right:30px;width:300px;">

                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                            <input type="hidden" name="codgru" value="{{$codgru}}"/>
                            @if($permissaoeditar == 'S')
                            &nbsp&nbsp<button type="button" id="addtrecho" class="btn btn-success" > <i class="admin-check-circle"></i> + NOVO TRECHO </button>
                                @if(count($listaTrecho) > 0)
                                    <button type="submit" class="btn btn-success"> <i class="admin-check-circle"></i>  SALVAR DADOS </button>
                                @endif
                            @endif
                        </div>
                    </div>
                    @if(count($listaTrecho) > 0)
                        <div class="panel-body">
                            <div id="parent" >
                                <table id="fixTable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="cabNum">#</th>
                                            <th class="cabClass">Item</th>
                                            <th class="cabMes"></th>
                                            <th class="cabMes">Meta<br>Janeiro</th>
                                            <th class="cabMes">Meta<br>Fevereiro</th>
                                            <th class="cabMes">Meta<br>Março</th>
                                            <th class="cabMes">Meta<br>Abril</th>
                                            <th class="cabMes">Meta<br>Maio</th>
                                            <th class="cabMes">Meta<br>Junho</th>
                                            <th class="cabMes">Meta<br>Julho</th>
                                            <th class="cabMes">Meta<br>Agosto</th>
                                            <th class="cabMes">Meta<br>Setembro</th>
                                            <th class="cabMes">Meta<br>Outubro</th>
                                            <th class="cabMes">Meta<br>Novembro</th>
                                            <th class="cabMes">Meta<br>Dezembro</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @php
                                            $trecho = '';
                                            $num = 0;
                                            @endphp
                                            @foreach ($listaTrecho as $a)
                                            @if($a->TRECHO_ID !== $trecho)
                                                @php
                                                    if($a->TRECHO_ID !== $trecho ){
                                                        $num++;
                                                        $trecho  = $a->TRECHO_ID;
                                                    }
                                                @endphp
                                                <tr class="tituloTrecho">
                                                    <td style="" class="tituloTrecho" scope="row"></td>
                                                    <td class="tituloTrecho">
                                                            <button type="button" onclick="removeTrecho('{{$a->TRECHO_ID}}')" id="removerTrecho" class="btn btn-danger pull-left" > <i class="admin-trash"></i> REMOVER TRECHO </button>
                                                    </td>
                                                    @if($CODUNN == 23)
                                                    <td class="tituloTrecho" colspan="2">
                                                        MUNICÍPIO : <select name="MUNICIPIO_ID[{{$a->TRECHO_ID}}]" id="MUNICIPIO_ID_{{$a->TRECHO_ID}}">
                                                                    <option value="1" @if($a->MUNICIPIO_ID == 1) selected="selected" @endif>MANAUS</option>
                                                                    <option value="2" @if($a->MUNICIPIO_ID == 2) selected="selected" @endif>OUTROS</option>
                                                                    </select>
                                                    </td>
                                                    <td class="tituloTrecho" colspan="10">
                                                        DESCRIÇÃO DO TRECHO : <input style="width:450px;" type="text"  value="{{$a->DESCRITRECHO}}" class="" name="DESCRITRECHO[{{$a->TRECHO_ID}}]">
                                                    </td>
                                                    @else
                                                    <td class="tituloTrecho" colspan="12">
                                                        DESCRIÇÃO DO TRECHO : <input style="width:450px;" type="text"  value="{{$a->DESCRITRECHO}}" class="" name="DESCRITRECHO[{{$a->TRECHO_ID}}]">
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endif
                                                <tr>
                                                <td class="cabValor" scope="row">T-{{$num}}</td>
                                                <td class="cabValorTit">{{$a->DESCRI}}</td>
                                                <td class="cabValor">
                                                        <a onclick="replicar('_{{$a->ITEM}}_{{$a->TRECHO_ID}}','B')" href="#">REPLICAR<i class="admin-double-right"></i></a>
                                                </td>
                                                <td class="cabValor">
                                                <input @if($a->TIPO == 'F')readonly @endif type="text"  value="{{str_replace('.',',',$a->VAL1)}}" class="@if($a->TIPO <> 'F')currency @else currencyVal @endif sum_jan{{$a->TRECHO_ID}}" name="jan_01_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL2)}}" class="@if($a->TIPO <> 'F')currency @else currencyVal @endif um_fev{{$a->TRECHO_ID}}" name="fev_02_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL3)}}" class="@if($a->TIPO <> 'F')currency @else currencyVal @endif um_mar{{$a->TRECHO_ID}}" name="mar_03_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL4)}}" class="@if($a->TIPO <> 'F')currency @else currencyVal @endif um_abr{{$a->TRECHO_ID}}" name="abr_04_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL5)}}" class="@if($a->TIPO <> 'F')currency @else currencyVal @endif um_mai{{$a->TRECHO_ID}}" name="mai_05_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td>
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL6)}}" class="@if($a->TIPO <> 'F')currency @else currencyVal @endif um_jun{{$a->TRECHO_ID}}" name="jun_06_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL7)}}" class="@if($a->TIPO <> 'F')currency @else currencyVal @endif um_jul{{$a->TRECHO_ID}}" name="jul_07_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL8)}}" class="@if($a->TIPO <> 'F')currency @else currencyVal @endif um_ago{{$a->TRECHO_ID}}" name="ago_08_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td>
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL9)}}" class="@if($a->TIPO <> 'F')currency @else currencyVal @endif um_set{{$a->TRECHO_ID}}" name="set_09_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL10)}}" class="@if($a->TIPO <> 'F')currency @else currencyVal @endif sum_out{{$a->TRECHO_ID}}" name="out_10_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL11)}}" class=" @if($a->TIPO <> 'F')currency @else currencyVal @endif sum_nov{{$a->TRECHO_ID}}" name="nov_11_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input @if($a->TIPO == 'F')readonly @endif type="text" value="{{str_replace('.',',',$a->VAL12)}}" class=" @if($a->TIPO <> 'F')currency @else currencyVal @endif sum_dez{{$a->TRECHO_ID}}" name="dez_12_{{$a->ITEM}}_{{$a->TRECHO_ID}}">
                                                </td>
                                                </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                        </div>
                        @endif
                    </div>
                    </form>
                </div>

                <!--form name="addNovoTrecho">
                        <button type="submit" class="btn btn-warning" > <i class="admin-check-circle"></i> - REMOVER TRECHO </button>
                </form-->
            </div>
</div>
@endsection
@section('javascript')
<script>

			$(document).ready(function() {

                $('#my-form').submit(function (e) {
                   // $.LoadingOverlay("show");
                   $.LoadingOverlay("show", {
                        image       : "{{URL::asset('assets/images/admin_loader2.gif')}}",
                        imageAnimation          : ""
                    });

                })

				$("#fixTable").tableHeadFixer({"left" : 2});
                //var Scrollbar = window.Scrollbar;
            });
//Scrollbar.init(document.querySelector('#parent'));
            $('#addtrecho').click( function() {
                $('form#frmAdd').submit();
            // alert('oi')
            //document.getElementById("frmAdd").submit();
            });

          removeTrecho = function(techo){
                console.log(techo);
                $('#remover_trecho_id').val(techo);
                $('form#frmRemover').submit();
          }

          replicar = function(item,tipo){

            valor = $( "input[name*='jan_01"+item+"']" ).val();
            $( "input[name*='fev_02"+item+"']" ).val(valor);
            $( "input[name*='mar_03"+item+"']" ).val(valor);
            $( "input[name*='abr_04"+item+"']" ).val(valor);
            $( "input[name*='mai_05"+item+"']" ).val(valor);
            $( "input[name*='jun_06"+item+"']" ).val(valor);
            $( "input[name*='jul_07"+item+"']" ).val(valor);
            $( "input[name*='ago_08"+item+"']" ).val(valor);
            $( "input[name*='set_09"+item+"']" ).val(valor);
            $( "input[name*='out_10"+item+"']" ).val(valor);
            $( "input[name*='nov_11"+item+"']" ).val(valor);
            $( "input[name*='dez_12"+item+"']" ).val(valor);
            }





        $(function() {
          $(".currency").maskMoney({prefix:'', allowZero: true, allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
        });

        Number.prototype.format = function(n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~n));

            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };
</script>
@endsection

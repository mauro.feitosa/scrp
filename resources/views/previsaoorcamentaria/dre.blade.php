@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp

@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@stop

@section('content')
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap-select.css') }}">
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff;
    background:  #FFBF00!important;
    font-weight: bold;
}
@media (max-width: 400px) {

}
.table {
        font-size:10px !important;
    }
#parent {
    min-height: height: calc(100vh - 180px);
    height: calc(100vh - 180px);
			}

#fixTable {
    width: 2000px !important;
}
.cabMes {
    width:100px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: center;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabClass {
    width:200px!important;
    text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabNum {
    width:50px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabAltura{
    height: 280px!important);
}
.cabValor {
    height:41px!important;
    vertical-align: middle!important;
}
.cabSintetica{background-color:#B8CCE4;font-weight: bold!important}
.cabSinteticaSoma{background-color:#7DA2CE;color:honeydew;font-size:12px;font-weight:bold!important}
.currency{width:100px;}


#parent::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #C1C1C1;
}

.panel-bordered>.panel-body {
    padding: 10px 10px 10px;
    overflow: hidden;
}
body {
    overflow: hidden;
}
.badge:hover {
  color: #ffffff;
  text-decoration: none;
  cursor: pointer;
}
.badge-error {
  background-color: #b94a48;
}
.badge-error:hover {
  background-color: #953b39;
}
.badge-warning {
  background-color: #f89406;
}
.badge-warning:hover {
  background-color: #c67605;
}
.badge-success {
  background-color: #468847;
}
.badge-success:hover {
  background-color: #356635;
}
.badge-info {
  background-color: #3a87ad;
}
.badge-info:hover {
  background-color: #2d6987;
}
.badge-inverse {
  background-color: #333333;
}
.badge-inverse:hover {
  background-color: #1a1a1a;
}
.btn {
    padding: 4px 6px;
    font-size: 12px;
    margin-top: 0px;
    margin-bottom: 0px;
    font-weight: bolder;
}
.formZero{
    margin:0px;
    padding:0px;
}
.text-bold{
    font-weight: bold;
}

input {font-weight:bold;}

input[type="text"]:read-only:not([read-only="false"]) { color: blue; background-color: #eee; border-width: 1px!important;border-style: solid!important; border-color: #A9A9A9!important;}
/*body::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F5F5F5;
}
body::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F5F5F5;
}

body::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #555;
}*/
img.button_open{
  content:url('../images/button-open.png');
  cursor:pointer;
}

img.button_closed{
  content: url('../images/button-closed.png');
  cursor:pointer;
}
</style>
<div id="#admin" class="page-content container-fluid">
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                    {!!$boxFiltro!!}
                    <div  class="panel panel-bordered">
                        <div class="panel-body">
                            <div id="parent" >
                                {!!$data!!}
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-select.js') }}"></script>
<script>

			$(document).ready(function() {
				$("#fixTable").tableHeadFixer({"left" : 1});

                $(window).keydown(function(event){
                    if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                    }
                });

            });

</script>
<script type='text/javascript'>//<![CDATA[
    $(function(){
    function CreateGroup(group_name)
    {
        // Create Button(Image)
        $('td.' + group_name).prepend("<img class='" + group_name + " button_closed'> ");
        // Add Padding to Data
        $('tr.' + group_name).each(function () {
            var first_td = $(this).children('td').first();
            var padding_left = parseInt($(first_td).css('padding-left'));
            $(first_td).css('padding-left', String(padding_left + 25) + 'px');
        });
        RestoreGroup(group_name);

        // Tie toggle function to the button
        $('img.' + group_name).click(function(){
            ToggleGroup(group_name);
        });
    }

    function ToggleGroup(group_name)
    {
        ToggleButton($('img.' + group_name));
        RestoreGroup(group_name);
    }

    function RestoreGroup(group_name)
    {
        if ($('img.' + group_name).hasClass('button_open'))
        {
            // Open everything
            $('tr.' + group_name).show();

            // Close subgroups that been closed
            $('tr.' + group_name).find('img.button_closed').each(function () {
                sub_group_name = $(this).attr('class').split(/\s+/)[0];
                //console.log(sub_group_name);
                RestoreGroup(sub_group_name);
            });
        }

        if ($('img.' + group_name).hasClass('button_closed'))
        {
            // Close everything
            $('tr.' + group_name).hide();
        }
    }

    function ToggleButton(button)
    {
        $(button).toggleClass('button_open');
        $(button).toggleClass('button_closed');
    }

   // CreateGroup('group1');
   // CreateGroup('group2');


        CreateGroup('grupo2');
        CreateGroup('grupo3');
        CreateGroup('grupo4');
        CreateGroup('grupo5');
        CreateGroup('grupo6');
        CreateGroup('grupo7');
        CreateGroup('grupo8');
        CreateGroup('grupo9');
        CreateGroup('grupo10');
        CreateGroup('grupo11');
        CreateGroup('grupo12');
        CreateGroup('grupo13');
        CreateGroup('grupo14');
        CreateGroup('grupo15');
        CreateGroup('grupo16');
        CreateGroup('grupo17');
        CreateGroup('grupo18');
        CreateGroup('grupo19');
        CreateGroup('grupo20');
        CreateGroup('grupo1');
        CreateGroup('grupo21');
        CreateGroup('grupo22');
        CreateGroup('grupo23');
        CreateGroup('grupo24');
        CreateGroup('grupo25');
        CreateGroup('grupo26');
        CreateGroup('grupo27');
        CreateGroup('grupo28');
        CreateGroup('grupo29');
        CreateGroup('grupo30');
        CreateGroup('grupo31');
        CreateGroup('grupo32');
        CreateGroup('grupo33');
        CreateGroup('grupo34');
        CreateGroup('grupo35');
        CreateGroup('grupo36');
        CreateGroup('grupo37');
        CreateGroup('grupo38');
        CreateGroup('grupo39');
        CreateGroup('grupo40');
        CreateGroup('grupo41');
        CreateGroup('grupo42');
        CreateGroup('grupo43');
        CreateGroup('grupo44');
        CreateGroup('grupo1');
        CreateGroup('grupo47');
        CreateGroup('grupo48');
        CreateGroup('grupo49');
        CreateGroup('grupo50');
        CreateGroup('grupo1');
        CreateGroup('grupo51');
        CreateGroup('grupo52');
        CreateGroup('grupo53');
        CreateGroup('grupo54');
        CreateGroup('grupo45');
        CreateGroup('grupo46');
        CreateGroup('grupox');

    });//]]>

    </script>
@endsection

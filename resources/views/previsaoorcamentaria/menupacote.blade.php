@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');

@endphp
@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')
    <h1 class="page-title">
            <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }} \ Pacote
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
.topoMenu {
    background-color: #337ab7!important;
    color: #fffff!important;
    border-color: #337ab7!important;
}
.topoMenuTitulo {
    background-color: #337ab7!important;
    color: #fffff!important;
    font-size:12px;
    font-weight:bolder;
}
.negrito{
    font-weight: bold;
}
</style>
<div class="page-content container-fluid">
    <!--div class="clearfix container-fluid row"-->
        <div class="alerts"></div>
        <table><tr><td  style="vertical-align: top;">
            <div class="row">

                    <div class="col-md-12">
                            <div class="panel panel-bordered">
                                    <div class="panel-body">
                        <div class="list-group-item topoMenu">
                               <h4 class="topoMenuTitulo" style="color:#ffffff!important">{{$unidadeFilial['CODCGA']}} - {{$unidadeFilial['CENTRODEGASTO']}}</h4>
                               <h4 class="topoMenuTitulo" style="color:#ffffff!important"><span class="badge badge-warning">{{$unidadeFilial['CODUNN']}} - {{$unidadeFilial['UNIDADE']}}</span></h4>
                        </div>
                            @php
                                $x = 0;
                                $ver = true;
                            @endphp
                            @foreach ($listaPacote as $l)
                                @if($x==0)
                                   @php $x = 1; @endphp

                                   @if($ver)
                                    <!--form name="frmPacote5" action="{{route('previsaoorcamentaria/variaveis')}}" method="POST">
                                        {!! csrf_field() !!}
                                    <input type="hidden" name="codgru" value="{{$l->CODIGO}}">
                                    <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                    <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                                    <button type="submit" class="list-group-item" style="font-size:12px!important">
                                    VARIÁVEIS
                                    </button>
                                    </form-->
                                    @endif

                                    @if($l->CODIGO == 2 && ($CODUNN == 21 || $CODUNN == 23 ||  $CODUNN == 22 || $CODUNN == 26))

                                    <form name="frmPacote5" action="{{route('previsaoorcamentaria/trechos')}}" method="POST">
                                        {!! csrf_field() !!}
                                    <input type="hidden" name="codgru" value="{{$l->CODIGO}}">
                                    <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                    <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                                    <button type="submit" class="list-group-item" style="font-size:12px!important">
                                    SIMULADOR TRECHOS
                                    </button>
                                    </form>

                                    @endif


                                    <form name="frmPacote5" action="{{route('previsaoorcamentaria/receitarota')}}" method="POST">
                                        {!! csrf_field() !!}
                                    <input type="hidden" name="codgru" value="{{$l->CODIGO}}">
                                    <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                    <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                                    <button type="submit" class="list-group-item" style="font-size:12px!important">
                                    SIMULADOR RECEITA
                                    </button>
                                    </form>

                                    @if($ver)
                                    <form name="frmqlpop" action="{{route('previsaoorcamentaria/simuladorqlp')}}" method="POST">
                                        {!! csrf_field() !!}
                                    <input type="hidden" name="codgru" value="{{$l->CODIGO}}">
                                    <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                    <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                                    <button type="submit" class="list-group-item" style="font-size:12px!important">
                                    SIMULADOR QLP OP
                                    </button>
                                    </form>
                                    <form name="frmqlpadm" action="{{route('previsaoorcamentaria/simuladorqlpadm')}}" method="POST">
                                        {!! csrf_field() !!}
                                    <input type="hidden" name="codgru" value="{{$l->CODIGO}}">
                                    <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                    <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                                    <button type="submit" class="list-group-item" style="font-size:12px!important">
                                    SIMULADOR QLP ADM
                                    </button>
                                    </form>
                                    <form name="frmSmepi" action="{{route('previsaoorcamentaria/simuladorepi')}}" method="POST">
                                        {!! csrf_field() !!}
                                    <input type="hidden" name="codgru" value="4">
                                    <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                    <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                                    <button type="submit" class="list-group-item" style="font-size:12px!important">
                                    SIMULADOR EPI
                                    </button>
                                    </form>
                                    @endif
                                @endif
                                @if(in_array($l->CODIGO,$usuario_pacote))
                                    <form name="frmPacote{{$l->CODIGO}}" action="{{route('previsaoorcamentaria/pacote')}}" method="POST">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="codgru" value="{{$l->CODIGO}}">
                                        <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                        <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                                        <button type="submit" class="list-group-item" style="font-size:12px!important">
                                        {{$l->CODIGO}} - PACOTE	{{$l->DESCRI}}
                                    </button>
                                    </form>
                                @endif
                            @endforeach
                        </div>

                </div>



            </div>
        </td>
        <td style="padding-left:10px;vertical-align: top;">
            <div class="row">

                <div class="col-md-12">
                        <div class="panel panel-bordered">
                                <div class="panel-body">
                    <div class="list-group-item topoMenu">
                           <h4 class="topoMenuTitulo" style="color:#ffffff!important">EBITDA R$ {{number_format($ebitda['total'], 2, ',', '.')}}</h4>
                    </div>
                                <button  type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                JAN <span class="negrito">R$ </span> <span class="negrito @if($ebitda['01'] > 0) text-success @elseif($ebitda['01'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['01'], 2, ',', '.')}}</span>
                                </button>
                                <button  type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                FEV <span class="negrito">R$ </span> <span class="negrito @if($ebitda['02'] > 0) text-success @elseif($ebitda['02'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['02'], 2, ',', '.')}}</span>
                                </button>
                                <button type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                MAR <span class="negrito">R$ </span> <span class="negrito @if($ebitda['03'] > 0) text-success @elseif($ebitda['03'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['03'], 2, ',', '.')}}</span>
                                </button>
                                <button type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                ABR <span class="negrito">R$ </span> <span class="negrito @if($ebitda['04'] > 0) text-success @elseif($ebitda['04'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['04'], 2, ',', '.')}}</span>
                                </button>
                                <button type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                MAI <span class="negrito">R$ </span> <span class="negrito @if($ebitda['05'] > 0) text-success @elseif($ebitda['05'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['05'], 2, ',', '.')}}</span>
                                </button>
                                <button type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                JUN <span class="negrito">R$ </span> <span class="negrito @if($ebitda['06'] > 0) text-success @elseif($ebitda['06'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['06'], 2, ',', '.')}}</span>
                                </button>
                                <button type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                JUL <span class="negrito">R$ </span> <span class="negrito @if($ebitda['07'] > 0) text-success @elseif($ebitda['07'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['07'], 2, ',', '.')}}</span>
                                </button>
                                <button type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                AGO <span class="negrito">R$ </span> <span class="negrito @if($ebitda['08'] > 0) text-success @elseif($ebitda['08'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['08'], 2, ',', '.')}}</span>
                                </button>
                                <button type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                SET <span class="negrito">R$ </span> <span class="negrito @if($ebitda['09'] > 0) text-success @elseif($ebitda['09'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['09'], 2, ',', '.')}}</span>
                                </button>
                                <button type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                OUT <span class="negrito">R$ </span> <span class="negrito @if($ebitda['10'] > 0) text-success @elseif($ebitda['10'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['10'], 2, ',', '.')}}</span>
                                </button>
                                <button type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                NOV <span class="negrito">R$ </span> <span class="negrito @if($ebitda['11'] > 0) text-success @elseif($ebitda['11'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['11'], 2, ',', '.')}}</span>
                                </button>
                                <button type="button" class="list-group-item disabled" style="font-size:12px!important;cursor: default!important">
                                DEZ <span class="negrito">R$ </span> <span class="negrito @if($ebitda['12'] > 0) text-success @elseif($ebitda['12'] == 0) text-warning @else text-danger @endif">{{number_format($ebitda['12'], 2, ',', '.')}}</span>
                                </button>
                    </div>
            </div>
        </div>
        </td></tr></table>
    <!--/div-->
</div>
@endsection

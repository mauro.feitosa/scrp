@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');

@endphp
@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')
    <h1 class="page-title">
            <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }} \ Pacote
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
.topoMenu {
    background-color: #337ab7!important;
    color: #fffff!important;
    border-color: #337ab7!important;
}
.topoMenuTitulo {
    background-color: #337ab7!important;
    color: #fffff!important;
    font-size:12px;
    font-weight:bolder;
}
</style>
<div class="page-content container-fluid">
    <!--div class="clearfix container-fluid row"-->
        <div class="alerts"></div>
            <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-bordered">
                                    <div class="panel-body">
                        <div class="list-group-item topoMenu">
                               <h4 class="topoMenuTitulo" style="color:#ffffff!important">{{$unidadeFilial['CODCGA']}} - {{$unidadeFilial['CENTRODEGASTO']}}</h4>
                               <h4 class="topoMenuTitulo" style="color:#ffffff!important"><span class="badge badge-warning">{{$unidadeFilial['CODUNN']}} - {{$unidadeFilial['UNIDADE']}}</span></h4>
                        </div>

                        <form name="frmPacote5" action="{{route('previsaoorcamentaria/variaveis')}}" method="POST">
                            {!! csrf_field() !!}
                        <input type="hidden" name="codgru" value="{{$codgru}}">
                        <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                        <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                        <button type="submit" class="list-group-item" style="font-size:12px!important">
                        VARIÁVEIS
                        </button>
                        </form>

                            <form name="frmPacote5" action="{{route('previsaoorcamentaria/receitarota')}}" method="POST">
                                {!! csrf_field() !!}
                            <input type="hidden" name="codgru" value="{{$codgru}}">
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            RECEITA
                            </button>
                            </form>

                            <form name="frmPacote2" action="{{route('previsaoorcamentaria/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="{{$codgru}}">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            PACOTE
                            </button>
                            </form>

                        </div>
                </div>
            </div>
    <!--/div-->
</div>
@endsection

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp

@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff;
    background:  #FFBF00!important;
    font-weight: bold;
}
@media (max-width: 400px) {

}
.txtLoad{
    margin-top: 10px!important;
    padding-top: 10px!important;
}
.table {
        font-size:10px !important;
    }
#parent {
    min-height: height: calc(100vh - 132px);
    height: calc(100vh - 132px);
			}

#fixTable {
    width: 3200px !important;
}
.cabMes {
    width:100px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: center;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}

.cabMesReplica{
    width:50px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: center;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}

.cabClass {
    width:180px!important;
    text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabNum {
    width:20px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabAltura{
    height: 310px!important);
}
.cabValor {
    height:41px!important;
    vertical-align: middle!important;
}
.cabValorTot {
    height:41px!important;
    vertical-align: middle!important;
    background-color:#F5F5F5!important;
}
.cabSintetica{background-color:#B8CCE4;font-weight: bold!important}
.cabSinteticaSoma{background-color:#7DA2CE;color:honeydew;font-size:11px;font-weight:bold!important}
.currency{width:100px;}


#parent::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #C1C1C1;
}

.panel-bordered>.panel-body {
    padding: 10px 10px 10px;
    overflow: hidden;
}
body {
    overflow: hidden;
}
.badge:hover {
  color: #ffffff;
  text-decoration: none;
  cursor: pointer;
}
.badge-error {
  background-color: #b94a48;
}
.badge-error:hover {
  background-color: #953b39;
}
.badge-warning {
  background-color: #f89406;
}
.badge-warning:hover {
  background-color: #c67605;
}
.badge-success {
  background-color: #468847;
}
.badge-success:hover {
  background-color: #356635;
}
.badge-info {
  background-color: #3a87ad;
}
.badge-info:hover {
  background-color: #2d6987;
}
.badge-inverse {
  background-color: #333333;
}
.badge-inverse:hover {
  background-color: #1a1a1a;
}
.btn {
    padding: 4px 6px;
    font-size: 12px;
    margin-top: 0px;
    margin-bottom: 0px;
    font-weight: bolder;
}
.formZero{
    margin:0px;
    padding:0px;
}
.text-bold{
    font-weight: bold;
}

input {font-weight:bold;}

input[type="text"]:read-only:not([read-only="false"]) { color: blue; background-color: #eee; border-width: 1px!important;border-style: solid!important; border-color: #A9A9A9!important;}
/*body::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F5F5F5;
}
body::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F5F5F5;
}

body::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #555;
}*/
</style>
<div id="#admin" class="page-content container-fluid">
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                <form id="frmrateio" name="frmrateio" method="POST" action="{{route('previsaoorcamentaria/pacoterateio')}}" class="formZero">
                    <input type="hidden" value="ok" name="rateio" />
                    {{ csrf_field() }}
                    <input type="hidden" name="ratCODCLAP" value=""/>
                    <input type="hidden" name="ratPAICLAP" value=""/>
                    <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                    <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                    <input type="hidden" name="codgru" value="{{$codgru}}"/>
                </form>
                <form id="my-form" name="my-form" method="POST" action="{{route('previsaoorcamentaria/pacote')}}" class="formZero">
                    <input type="hidden" value="ok" name="salvar" />
                    {{ csrf_field() }}
                    <div  class="panel panel-bordered">
                    <div style="margin-left:14px;margin-top:8px;font-size:14px;display:table;width:100%">
                        <span class="badge badge-error">{{$CODCGA}}</span>
                        <span type="submit" class="badge badge-info">{{$centrodegasto}}</span> /
                        <span class="badge badge-error">{{$CODUNN}}</span><span class="badge badge-info">{{$unidade}}</span> /
                        <span class="badge badge-warning">{{$pacote}}</span> /
                        <span class="badge badge-warning">EBITDA TOTAL ANO : R$ {{ number_format($ebitda['total'], 2, ',', '.')}}</span>
                        @if(isset($subitem))
                        / <span class="badge badge-warning">{{$subitem}}</span>
                        @endif
                        <div class="pull-right" style="margin-right:30px;width:300px;">

                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                            <input type="hidden" name="codgru" value="{{$codgru}}"/>
                                @if($permissaoeditar == 'S')
                                <button type="submit" class="btn btn-success pull-right" > <i class="admin-check-circle"></i>  SALVAR DADOS </button>
                                @endif
                        </div>
                    </div>
                        <div class="panel-body">
                            <div id="parent" >
                                <table id="fixTable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="cabNum">#</th>
                                            <th class="cabClass">Classificação Sintetica / Analitica</th>
                                            <th class="cabMes">Total</th>
                                            <th class="cabMes">Centro de<br>Custo</th>
                                            <th class="cabMesReplica"></th>
                                            <th class="cabMes">Meta<br>Janeiro</th>
                                            <th class="cabMes">Meta<br>Fevereiro</th>
                                            <th class="cabMes">Meta<br>Março</th>
                                            <th class="cabMes">Meta<br>Abril</th>
                                            <th class="cabMes">Meta<br>Maio</th>
                                            <th class="cabMes">Meta<br>Junho</th>
                                            <th class="cabMes">Meta<br>Julho</th>
                                            <th class="cabMes">Meta<br>Agosto</th>
                                            <th class="cabMes">Meta<br>Setembro</th>
                                            <th class="cabMes">Meta<br>Outubro</th>
                                            <th class="cabMes">Meta<br>Novembro</th>
                                            <th class="cabMes">Meta<br>Dezembro</th>
                                            <th class="cabClass">Justificativa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="cabSinteticaSoma">
                                            <td scope="row"></td>
                                            <td>EBITDA TOTAL MES</td>
                                            <td>R$ <span class="total_dez">{{number_format($ebitda['total'], 2, ',', '.')}}</span></td>
                                            <td></td>
                                            <td></td>
                                            <td>R$ <span class="total_jan">{{number_format($ebitda['01'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_fev">{{number_format($ebitda['02'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_mar">{{number_format($ebitda['03'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_abr">{{number_format($ebitda['04'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_mai">{{number_format($ebitda['05'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_jun">{{number_format($ebitda['06'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_jul">{{number_format($ebitda['07'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_ago">{{number_format($ebitda['08'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_set">{{number_format($ebitda['09'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_out">{{number_format($ebitda['10'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_nov">{{number_format($ebitda['11'], 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_dez">{{number_format($ebitda['12'], 2, ',', '.')}}</span></td>
                                            <td></td>
                                        </tr>
                                        </tr>
                                        @foreach($pacoteSinteticasSoma as $ps)
                                        <tr class="cabSinteticaSoma">
                                                <td scope="row"></td>
                                                <td>TOTAL</td>
                                                <td>R$ <span class="total_dez{{$ps->CODGRU}}">{{number_format(($ps->VAL1+$ps->VAL2+$ps->VAL3+$ps->VAL4+$ps->VAL5+$ps->VAL6+$ps->VAL7+$ps->VAL8+$ps->VAL9+$ps->VAL10+$ps->VAL11+$ps->VAL12), 2, ',', '.')}}</span></td>
                                                <td></td>
                                                <td></td>
                                                <td>R$ <span class="total_jan{{$ps->CODGRU}}">{{number_format($ps->VAL1, 2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_fev{{$ps->CODGRU}}">{{number_format($ps->VAL2, 2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_mar{{$ps->CODGRU}}">{{number_format($ps->VAL3, 2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_abr{{$ps->CODGRU}}">{{number_format($ps->VAL4, 2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_mai{{$ps->CODGRU}}">{{number_format($ps->VAL5, 2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_jun{{$ps->CODGRU}}">{{number_format($ps->VAL6, 2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_jul{{$ps->CODGRU}}">{{number_format($ps->VAL7, 2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_ago{{$ps->CODGRU}}">{{number_format($ps->VAL8, 2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_set{{$ps->CODGRU}}">{{number_format($ps->VAL9, 2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_out{{$ps->CODGRU}}">{{number_format($ps->VAL10,2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_nov{{$ps->CODGRU}}">{{number_format($ps->VAL11,2, ',', '.')}}</span></td>
                                                <td>R$ <span class="total_dez{{$ps->CODGRU}}">{{number_format($ps->VAL12,2, ',', '.')}}</span></td>
                                                <td></td>
                                            </tr>
                                        @endforeach

                                        @foreach ($pacoteSinteticas as $s)
                                        <tr class="cabSintetica">
                                            <td scope="row"></td>
                                            <td>{{$s->CODCLAP}}-{{$s->DESCRI}}</td>
                                            <td>R$ <span class="total_dez{{$s->CODCLAP}}">{{number_format(($s->VAL1+$s->VAL2+$s->VAL3+$s->VAL4+$s->VAL5+$s->VAL6+$s->VAL7+$s->VAL8+$s->VAL9+$s->VAL10+$s->VAL11+$s->VAL12), 2, ',', '.')}}</span></td>
                                            <td></td>
                                            <td></td>
                                            <td>R$ <span class="total_jan{{$s->CODCLAP}}">{{number_format($s->VAL1 , 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_fev{{$s->CODCLAP}}">{{number_format($s->VAL2 , 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_mar{{$s->CODCLAP}}">{{number_format($s->VAL3 , 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_abr{{$s->CODCLAP}}">{{number_format($s->VAL4 , 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_mai{{$s->CODCLAP}}">{{number_format($s->VAL5 , 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_jun{{$s->CODCLAP}}">{{number_format($s->VAL6 , 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_jul{{$s->CODCLAP}}">{{number_format($s->VAL7 , 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_ago{{$s->CODCLAP}}">{{number_format($s->VAL8 , 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_set{{$s->CODCLAP}}">{{number_format($s->VAL9 , 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_out{{$s->CODCLAP}}">{{number_format($s->VAL10, 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_nov{{$s->CODCLAP}}">{{number_format($s->VAL11, 2, ',', '.')}}</span></td>
                                            <td>R$ <span class="total_dez{{$s->CODCLAP}}">{{number_format($s->VAL12, 2, ',', '.')}}</span></td>
                                            <td></td>
                                        </tr>
                                            @foreach ($pacoteAnaliticas as $a)
                                                @if($s->CODCLAP == $a->PAICLAP)
                                                <tr>

                                                <td class="cabValor" scope="row">{{$a->CODCLAP}}</td>
                                                <td class="cabValor">{{$a->DESCRI}}</td>

                                                <td class="cabValorTot">
                                                    <b>R$ {{number_format(($a->VAL1+$a->VAL2+$a->VAL3+$a->VAL4+$a->VAL5+$a->VAL6+$a->VAL7+$a->VAL8+$a->VAL9+$a->VAL10+$a->VAL11+$a->VAL12), 2, ',', '.')}}</b>
                                                 </td>

                                                <td>
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <a onclick="rateio('{{$a->CODCLAP}}','{{$a->PAICLAP}}','{{$CODCGA}}','{{$CODUNN}}','{{$pacote}}','{{$codgru}}')" href="#">{!! Html::image('images/btn-rateio.png') !!}</a>
                                                    @else
                                                    {{$a->CODCUS}}-{{$a->CUSTO}}
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO !== 'S')
                                                    @if(!in_array($a->CODCLAP,$analitBoqueia))
                                                    <a onclick="replicar('_{{$a->CODCLAP}}_{{$a->PAICLAP}}','B')" href="#">REPLICAR<i class="admin-double-right"></i></a>
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL1, 2, ',', '.')}}</span>
                                                    @else
                                                        @if(in_array($a->CODCLAP,$analitBoqueia))
                                                        <input type="text" readonly value="{{number_format($a->VAL1, 2, ',', '.')}}" class=" sum_jan{{$a->PAICLAP}}" name="jan_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                        @else
                                                        <input type="text" value="{{number_format($a->VAL1, 2, ',', '.')}}" class="currency sum_jan{{$a->PAICLAP}}" name="jan_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                        @endif
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL2, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{number_format($a->VAL2, 2, ',', '.')}}" class=" sum_fev{{$a->PAICLAP}}" name="fev_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL2, 2, ',', '.')}}" class="currency sum_fev{{$a->PAICLAP}}" name="fev_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL3, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{number_format($a->VAL3, 2, ',', '.')}}" class=" sum_mar{{$a->PAICLAP}}" name="mar_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL3, 2, ',', '.')}}" class="currency sum_mar{{$a->PAICLAP}}" name="mar_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL4, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{number_format($a->VAL4, 2, ',', '.')}}" class=" sum_abr{{$a->PAICLAP}}" name="abr_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL4, 2, ',', '.')}}" class="currency sum_abr{{$a->PAICLAP}}" name="abr_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL5, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{number_format($a->VAL5, 2, ',', '.')}}" class=" sum_mai{{$a->PAICLAP}}" name="mai_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL5, 2, ',', '.')}}" class="currency sum_mai{{$a->PAICLAP}}" name="mai_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL6, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{number_format($a->VAL6, 2, ',', '.')}}" class=" sum_jun{{$a->PAICLAP}}" name="jun_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL6, 2, ',', '.')}}" class="currency sum_jun{{$a->PAICLAP}}" name="jun_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL7, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{number_format($a->VAL7, 2, ',', '.')}}" class="sum_jul{{$a->PAICLAP}}" name="jul_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL7, 2, ',', '.')}}" class="currency sum_jul{{$a->PAICLAP}}" name="jul_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL8, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly  value="{{number_format($a->VAL8, 2, ',', '.')}}" class="sum_ago{{$a->PAICLAP}}" name="ago_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL8, 2, ',', '.')}}" class="currency sum_ago{{$a->PAICLAP}}" name="ago_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL9, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{number_format($a->VAL9, 2, ',', '.')}}" class="sum_set{{$a->PAICLAP}}" name="set_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL9, 2, ',', '.')}}" class="currency sum_set{{$a->PAICLAP}}" name="set_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL10, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{number_format($a->VAL10, 2, ',', '.')}}" class="sum_out{{$a->PAICLAP}}" name="out_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL10, 2, ',', '.')}}" class="currency sum_out{{$a->PAICLAP}}" name="out_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL11, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{number_format($a->VAL11, 2, ',', '.')}}" class="sum_nov{{$a->PAICLAP}}" name="novr_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL11, 2, ',', '.')}}" class="currency sum_nov{{$a->PAICLAP}}" name="nov_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->OBRIGARATEIO == 'S')
                                                    <span class="text-bold">{{number_format($a->VAL12, 2, ',', '.')}}</span>
                                                    @else
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{number_format($a->VAL12, 2, ',', '.')}}" class="sum_dez{{$a->PAICLAP}}" name="dez_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @else
                                                    <input type="text" value="{{number_format($a->VAL12, 2, ',', '.')}}" class="currency sum_dez{{$a->PAICLAP}}" name="dez_{{$a->CODCLAP}}_{{$a->PAICLAP}}_{{$a->CODCUS}}">
                                                    @endif
                                                    @endif
                                                </td>

                                                <td class="cabValor">
                                                @if($a->OBRIGARATEIO !== 'S')
                                                <input type="text" name="just_{{$a->CODCLAP}}_{{$a->PAICLAP}}" value="{{$a->TEXTO}}">
                                                @endif
                                                </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>



			$(document).ready(function() {


                $('#my-form').submit(function (e) {
                   // $.LoadingOverlay("show");
                   $.LoadingOverlay("show", {
                        image       : "{{URL::asset('assets/images/admin_loader2.gif')}}",
                        imageAnimation          : ""
                    });

                })

				$("#fixTable").tableHeadFixer({"left" : 3});

                $(window).keydown(function(event){
                    if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                    }
                });

            });

            rateio = function(CODCLAP,PAICLAP){
                $( "input[name*='ratCODCLAP']" ).val(CODCLAP);
                $( "input[name*='ratPAICLAP']" ).val(PAICLAP);
                $('form#frmrateio').submit();
            }

            replicar = function(item,tipo){
            valorJus = $( "input[name*='just"+item+"']" ).val();
            valor = $( "input[name*='jan"+item+"']" ).val();
            console.log(valorJus);

            $( "input[name*='"+item+"']" ).val(valor);
            $( "input[name*='just"+item+"']" ).val(valorJus);

            }


        $(function() {
          $(".currency").maskMoney({prefix:'R$ ', allowZero: true, allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

        });

        Number.prototype.format = function(n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~n));

            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };
        @php
           $mesJs = ["jan","fev","mar","abr","mai","jun","jul","ago","set","out","nov","dez"];
        @endphp


        @foreach ($mesJs as $m)

        @foreach ($pacoteSinteticas as $s)
        $(document).on("change", ".sum_{{$m}}{{$s->CODCLAP}}", function() {
        //    console.log('oi');
        var sum = 0;
        var tmp = 0.00;
            $(".sum_{{$m}}{{$s->CODCLAP}}").each(function(){
                //console.log($(this).val());
        var str = $(this).val();
        sum += parseFloat(str.replace('.','').replace(',','.').replace(' ',''));
        tmp = sum;
        console.log(sum);
        //tmp = ss.toString();
        console.log(tmp.format(2, 3, '.', ','));
        ///sum += +$(this).val();
            //$(".total_{{$s->CODCLAP}}").maskMoney('mask',tmp);
            $(".total_{{$m}}{{$s->CODCLAP}}").html(tmp.format(2, 3, '.', ','));
        });
        //$(".total_{{$s->CODCLAP}}").maskMoney('mask', sum);
        //$(".total_{{$s->CODCLAP}}").maskMoney('mask',tmp.replace(',','.').replace(' ',''));
        //$(".total_txt{{$s->CODCLAP}}").text($(".total_{{$s->CODCLAP}}").val());
        //$(".total_{{$s->CODCLAP}}").html(sum);
        //console.log(sum);
        });
        @endforeach

        @endforeach

</script>
@endsection

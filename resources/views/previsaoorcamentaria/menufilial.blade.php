@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }} \ Centro de Gasto
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
.topoMenu {
    background-color: #337ab7!important;
    color: #fffff!important;
    border-color: #337ab7!important;
}
.topoMenuTitulo {
    background-color: #337ab7!important;
    color: #fffff!important;
    font-size:12px;
    font-weight:bolder;
}
</style>
<div class="page-content container-fluid">
    <!--div class="clearfix container-fluid row"-->
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-bordered">
                            <div class="panel-body">
                    @foreach ($listaFilial as $f)
                    <div class="list-group">
                            <div class="list-group-item topoMenu">
                                    <h4 class="topoMenuTitulo" style="color:#ffffff!important">
                                        <span class="badge">{{$f->CODCGA}} - {{$f->CENTRODEGASTO}} </span>
                                        <span class="badge badge-info">EBITDA : </span>

                                        @php
                                            $valp = 0;
                                            if( array_sum($listaEbitda['r'][$f->CODCGA]) <= 0 ){
                                                $valp = 0;
                                            } else {
                                                $valp = (array_sum($listaEbitda[$f->CODCGA]) /  array_sum($listaEbitda['r'][$f->CODCGA])) * 100;

                                                //dd($listaEbitda['r'][$f->CODCGA]);

                                            }
                                        @endphp

                                        @if(array_sum($listaEbitda[$f->CODCGA]) > 0)
                                        <span class="badge badge-success">R$ {{ number_format(array_sum($listaEbitda[$f->CODCGA]), 2, ',', '.')}}</span>
                                        <span class="badge badge-success">% {{ number_format($valp, 2, ',', '.')}}</span>
                                        @elseif(array_sum($listaEbitda[$f->CODCGA]) == 0)
                                        <span class="badge badge-warning">R$ {{ number_format(array_sum($listaEbitda[$f->CODCGA]), 2, ',', '.')}}</span>
                                        <span class="badge badge-warning">% {{ number_format($valp, 2, ',', '.')}}</span>
                                        @else
                                        <span class="badge badge-error">R$ {{ number_format(array_sum($listaEbitda[$f->CODCGA]), 2, ',', '.')}}</span>
                                        <span class="badge badge-error">% {{ number_format($valp, 2, ',', '.')}}</span>
                                        @endif

                                    </h4>

                            </div>

                            @foreach ($listaUnidade as $u)
                                @if($f->CODCGA == $u->CODCGA)
                                    <form name="frmPacote5" action="{{route('previsaoorcamentaria/menupacote')}}" method="POST">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="CODCGA" value="{{$u->CODCGA}}">
                                        <input type="hidden" name="CODUNN" value="{{$u->CODUNN}}">
                                        <button type="submit" class="list-group-item" style="font-size:12px!important">
                                            {{$u->CODUNN}} - {{$u->UNIDADE}}

                                            @if($listaEbitda[$u->CODCGA][$u->CODUNN] > 0)
                                            <span class="badge badge-success">% {{ number_format($listaEbitda['p'][$u->CODCGA][$u->CODUNN], 2, ',', '.')}}</span>
                                            <span class="badge badge-success">R$ {{ number_format($listaEbitda[$u->CODCGA][$u->CODUNN], 2, ',', '.')}}</span>

                                            @elseif($listaEbitda[$u->CODCGA][$u->CODUNN] == 0)
                                            <span class="badge badge-warning">% {{ number_format($listaEbitda['p'][$u->CODCGA][$u->CODUNN], 2, ',', '.')}}</span>
                                            <span class="badge badge-warning">R$ {{ number_format($listaEbitda[$u->CODCGA][$u->CODUNN], 2, ',', '.')}}</span>
                                            @else
                                            <span class="badge badge-error">% {{ number_format($listaEbitda['p'][$u->CODCGA][$u->CODUNN], 2, ',', '.')}}</span>
                                            <span class="badge badge-error">R$ {{ number_format($listaEbitda[$u->CODCGA][$u->CODUNN], 2, ',', '.')}}</span>
                                            @endif
                                            <span class="badge badge-info">EBITDA : </span>

                                        </button>
                                    </form>
                                @endif
                            @endforeach

                    </div>
                    @endforeach
                            </div>
                    </div>
                </div>
            </div>
    <!--/div-->
</div>
@endsection
